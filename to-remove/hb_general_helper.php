<?php
require_once('general_helper.php'); //include the standard CMS general file - for both CMS and front end functions

/*
 *---------------------------------------------------------------
 * GENERAL HELPER FUNCTIONS - HUMMINGBIRD ONLY
 *---------------------------------------------------------------
 */
function url_generate($str)
{
	$str = strtolower($str);
    $str = htmlentities($str); // convert unusual characters to html entities, e.g. '&' --> '&amp;'
    $str = str_replace('&amp;', 'and', $str); // '&amp;' --> 'and'
    $str = str_replace('\'', '', $str); // remove appostrophes
    $str = str_replace('"', '', $str); // remove quotes
    $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str); // replace any '&nbsp;' with 'nbsp'
    //$str = preg_replace("/[^A-Z0-9]/i", ' ', $str); // replace upper case or number to space // why have this, strtolower below
    $str = preg_replace("/\s+/i", ' ', $str);
    
    $str = str_replace('_', '-', $str); // underscore to hyphen
    $str = rtrim($str);
    $str = ltrim($str);
    //$str = ereg_replace("[^a-z0-9 ]", "", $str); // remove any remaining strangeness
    $str = preg_replace("/([^a-z0-9 ])/i", "", $str); // remove any remaining strangeness
    $str = preg_replace("/\s+/i", ' ', $str); // double spaces to single
    $str = str_replace(' ', '_', $str); // space to hyphen
    $str = trim($str);
            
    return $str;
}



function hash_password($pass) {
    // always trim password of whitespace at beginning and end
    $pass = ltrim($pass);
    $pass = rtrim($pass);
    $pass = hash('sha256', $pass);
    return $pass;
}

function generate_salt(){
    $rand = time().rand(0,10000).'ticksalting'.$GLOBALS['site_url'];
    $salt = hash('sha256', $rand);
    $salt = substr($salt, 0, 32);
    return $salt;
}

function newsSearchForm($action, $searchterm = null, $page_id = null){
    newsRSSfeed($page_id);
    
    if ($searchterm == ''){
        if ($_POST){
            $searchterm = make_safe($_POST['s']);
        }else{
            $searchterm = make_safe($_GET['s']);
        }
    }

    $url = explode("?", $_SERVER["REQUEST_URI"]);

    // news search form ?>
    <form action="<?=$url[0]?>" method="get" id="news_search_form" name="news_search_form" class="stylish_form">
    <div class="news_search_form">
        <fieldset class="search">
            <legend>Search: </legend>
            <div class="search_box input_box">
                <input type="text" id="s" name="s" class="searchterm" value="<?=(($searchterm) AND ($searchterm != 'Search for news...'))?output_filter($searchterm):'Search for news...'?>"<?=(($searchterm) AND ($searchterm != 'Search for news...'))?'':' onfocus="this.value=\'\';this.onfocus=null;this.className=\'selected\';"';?>><input class="search_icon" type="image" src="/images/ux2/search-button.png" alt="Search">
            </div>
        </fieldset>
        <div class="clear_both"></div>
    </div>
    </form>
<? }


function newsSearch(){
    
}

function newsRSSfeed($page_id){
    $query = sprintf("SELECT feed FROM pageindex WHERE id = '%d' AND feed != ''",
    mysql_real_escape_string($page_id));
    $result = mysql_query($query) or die(error_handler('', 'mysql'));
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        
        $url = $row['feed']; ?>
        <div class="rss_feed_wrapper">
            <div class="rss_feed">
                <a href="<?=$url?>"><img src="/images/rss_icon_small.png" alt="RSS"> <span>subscribe by RSS</span></a>
            </div>
        </div>
    <? }
}

function pageBlogFeedCount($type, $id){
    if ($type == 'pages'){
        $query = sprintf("SELECT feed, feed_method, feed_limit FROM pageindex WHERE id = '%d' AND feed != ''",
        mysql_real_escape_string($id));
    }elseif ($type == 'microsite_pages'){
        $query = sprintf("SELECT feed, feed_method, feed_limit FROM microsite_pageindex WHERE id = '%d' AND feed != ''",
        mysql_real_escape_string($id));
    }else{
        return;
    }
    $result = mysql_query($query) or die(error_handler('', 'mysql'));
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        
        $url = $row['feed'];
        $feed_method = $row['feed_method'];
        $feed_limit = $row['feed_limit'];
        
        if (($feed_limit == 0) OR ($feed_limit > 100)) $feed_limit = 5;
        
        $url_count = strlen($url);
        $url = alias_url_strip($url); // RSS URL is entered by normal user, strip all possible domain, http, https etc. so we know what we have
        $url_pos = strpos($url, "http://"); //check if we have an external site.
        
        if ($url_pos === false) {
            $url = 'http://'.$GLOBALS['site_url'].$url; // go to correct place
        }
        
        $wordpress_articles_array = get_wordpress_rss_items_as_array($url); // get wordpress articles from RSS feed cache
        
        return count($wordpress_articles_array);
    }
}

function customPageBlogFeedDetails($array_object){
    echo '<p class="news_date_small">Posted on <strong>'.date("jS F Y",strtotime($array_object['pubDate'])).'</strong> by <strong>';
    if ($array_object['author_link'] != ''){
        echo '<a href="'.$array_object['author_link'].'" title="More from this author">'.$array_object['author'].'</a>';
    }else{
        echo $array_object['author'];
    }
    echo '</strong></p>';
}

function combine_password_salt($password, $salt){
    // always trim password of whitespace at beginning and end
    $password = ltrim($password);
    $password = rtrim($password);
    $hash = hash_password($password.'RanD0m'.$salt);
    return $hash;
}

function randomMD5(){
    return md5(rand(0,10000).'fhjgkl912'.time());
}

function check_galleries($text)
{
                $pattern    = '#\[gallery:([0-9]+)(.+)\]#';
    
                if(preg_match($pattern, $text, $matches))
                {       
                        $shortcode  = $matches[0];
                        $id         = $matches[1];
                        $gallery    = BuildGallery($id);
                        
                        return str_replace($shortcode, $gallery, $text);
                }
                else
                {
                        return $text;
                }
    
}

function check_contact_forms($text)
{
    require_once('_contact_forms.php');
    $builder = new contactFormBuilder();
    return $builder->scan_text($text);
}

function isPreview(){
    // check to see if we are previewing page, different checks are needed for preview or live view
    if (($_GET['preview'] == 'true') AND is_numeric($_GET['id']) AND ($_GET['id'] != '') AND (strlen($_GET['key']) == 32)){
        // seems we are previewing a page
        $query = sprintf("SELECT id FROM pagecontent_standard WHERE id = '%d' AND `key` = '%s'",
        mysql_real_escape_string($_GET['id']),
        mysql_real_escape_string($_GET['key']));
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            // we found this page content to preview
            return true;
        }else{
            // this page doesn't exist
            return false;
        }
    }else{
        return false;
    }
}

function getPageDetails() {
    global $sections, $sectionid, $pageid, $pagedetails, $includefilename, $path, $fullurl;
    
    // can only go 3 layers deep: section > parent_parent_page > parent_page > page
    
    // Work out where we are, and populate global variables
    $requri = explode('?',$_SERVER["REQUEST_URI"],2);
    $fullurl = substr(urldecode($requri[0]),1);
    //$url = $_GET['url'];
    $sectionid = 0;
    $pageid = 0;
    $pagedetails = array();
    
    getSections();
    
    if ($fullurl != ''){
        $path = explode('/',$fullurl,10);
        $path = array_filter($path);
        $num = count($path) - 1;
        $tryparseurl = true;
    }else{
        $includefilename = 'homepage.php';
        $tryparseurl = false;
    }
    $pagenotfound = false;
        
    if ($tryparseurl){
        // Read the URL
        if (isset($sections[$path[0]])){
            $sectionid = $sections[ $path[0] ]['id'];
            if (isset($path[1])){
                $pageurl = make_safe($path[1]);
            } else $pageurl = '';
        }else $pageurl = make_safe($path[0]);
        if ($pageurl == ''){
            // Get default page for this section
            $query = sprintf("SELECT pageindex.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = pageindex.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name FROM pageindex WHERE id = '%d'",
            mysql_real_escape_string($sections[$sectionid]['defaultpageid']));
            if (!isPreview()){
                // not previewing, page must be live
                $query .= sprintf(" AND live = 1");
            }
        }else{
            // Get specific page
            if (isset($path[4]) && $path[4] != ''){
                // this is a sub-sub-sub-page, rather confusingly 'a' alias is the 4th tier page row (this page), 'b' alias is it's parent page, 'c' alias is the parent's parent, 'd' alias is the parent's parent's parent
                $query = sprintf("SELECT a.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = a.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name,
                (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = b.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = c.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_name,
                (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = d.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_parent_name,
                b.url AS parent_url,
                c.url AS parent_parent_url,
                d.url AS parent_parent_parent_url FROM pageindex a JOIN pageindex b ON b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 JOIN pageindex c ON c.id = b.parent_id AND c.sectionid = a.sectionid AND c.deleted = 0 JOIN pageindex d ON d.id = c.parent_id AND d.sectionid = a.sectionid AND d.deleted = 0  WHERE a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND c.url = '%s' AND d.url = '%s' AND a.deleted = 0",
                mysql_real_escape_string($sectionid),
                mysql_real_escape_string($path[4]), // a url
                mysql_real_escape_string($path[3]), // b url
                mysql_real_escape_string($path[2]), // c url
                mysql_real_escape_string($path[1])); // d url
                if (!isPreview()){
                    // not previewing, page must be live
                    $query .= sprintf(" AND a.live = 1");
                }
            }elseif (isset($path[3]) && $path[3] != ''){
                // this is a sub-sub-page, rather confusingly 'a' alias is the 3rd tier page row (this page), 'b' alias is it's parent page, 'c' alias is the parent's parent
                $query = sprintf("SELECT a.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = a.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name,
                (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = b.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = c.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_name,
                b.url AS parent_url,
                c.url AS parent_parent_url FROM pageindex a JOIN pageindex b ON b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 JOIN pageindex c ON c.id = b.parent_id AND c.sectionid = a.sectionid AND c.deleted = 0 WHERE a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND c.url = '%s' AND a.deleted = 0",
                mysql_real_escape_string($sectionid),
                mysql_real_escape_string($path[3]), // a url
                mysql_real_escape_string($path[2]), // b url
                mysql_real_escape_string($path[1])); // c url
                if (!isPreview()){
                    // not previewing, page must be live
                    $query .= sprintf(" AND a.live = 1");
                }
                
            }elseif (isset($path[2]) && $path[2] != ''){
                // This is a subpage, rather confusingly 'a' alias is the 2nd tier page row (this page), 'b' alias is parent page
                $query = sprintf("SELECT a.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = a.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name,
                (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = b.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                b.url AS parent_url FROM pageindex a JOIN pageindex b ON b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 WHERE a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND a.deleted = 0",
                mysql_real_escape_string($sectionid),
                mysql_real_escape_string($path[2]), // a url
                mysql_real_escape_string($path[1])); // b url
                if (!isPreview()){
                    // not previewing, page must be live
                    $query .= sprintf(" AND a.live = 1");
                }
            }else{
                $query = sprintf("SELECT pageindex.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = pageindex.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name FROM pageindex WHERE url = '%s' AND sectionid = '%d' AND deleted = 0",
                mysql_real_escape_string($pageurl),
                mysql_real_escape_string($sectionid));
                if (!isPreview()){
                    // not previewing, page must be live
                    $query .= sprintf(" AND live = 1");
                }
            }
        }
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            $pagedetails = mysql_fetch_array($result);
            if ($path[1] == ''){
                // no page url, only section url ($path[0])
                // we need to populate default page path for subnav to work
                $path[1] = $pagedetails['url'];
            }
            mysql_free_result($result);
        }else{
            // Didn't find page... if it is a subpage, we check the parent page to see if it handles its own subpages (i.e. they have customchildren = 1 in the pageindex table)
            $pagenotfound = true; // start from page not found and work up until we find a custom handler
            
            // work up the URL from the right to see if page above at some point handles it's own children, if so then we continue and let that page's functionality deal with URL
            
            $i = 10;
            while (($i > 1) AND ($pagenotfound)){
                if (isset( $path[$i] ) && $path[$i] != ''){
                    if ($i > 4){
                        // more than 4 levels deep, check up path
                        // this is a sub-sub-sub-page, rather confusingly 'a' alias is the 4th tier page row (this page), 'b' alias is it's parent page, 'c' alias is the parent's parent, 'd' alias is the parent's parent's parent
                        $query = sprintf("SELECT a.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = a.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name,
                        (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = b.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                        (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = c.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_name,
                        (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = d.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_parent_name,
                        b.url AS parent_url,
                        c.url AS parent_parent_url,
                        d.url AS parent_parent_parent_url FROM pageindex a JOIN pageindex b ON b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 JOIN pageindex c ON c.id = b.parent_id AND c.sectionid = a.sectionid AND c.deleted = 0 JOIN pageindex d ON d.id = c.parent_id AND d.sectionid = a.sectionid AND d.deleted = 0  WHERE a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND c.url = '%s' AND d.url = '%s' AND a.deleted = 0",
                        mysql_real_escape_string($sectionid),
                        mysql_real_escape_string($path[$i-1]), // a url
                        mysql_real_escape_string($path[$i-2]), // b url
                        mysql_real_escape_string($path[$i-3]), // c url
                        mysql_real_escape_string($path[$i-4])); // d url
                        if (!isPreview()){
                            // not previewing, page must be live
                            $query .= sprintf(" AND a.live = 1");
                        }
                    }elseif ($i > 3){
                        // more than 3 levels deep, check up path
                        // this is a sub-sub-page, rather confusingly 'a' alias is the 3rd tier page row (this page), 'b' alias is it's parent page, 'c' alias is the parent's parent
                        $query = sprintf("SELECT a.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = a.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name,
                        (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = b.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                        (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = c.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_name,
                        b.url AS parent_url,
                        c.url AS parent_parent_url FROM pageindex a JOIN pageindex b ON b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 JOIN pageindex c ON c.id = b.parent_id AND c.sectionid = a.sectionid AND c.deleted = 0 WHERE a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND c.url = '%s' AND a.deleted = 0",
                        mysql_real_escape_string($sectionid),
                        mysql_real_escape_string($path[$i-1]), // a url
                        mysql_real_escape_string($path[$i-2]), // b url
                        mysql_real_escape_string($path[$i-3])); // c url
                        if (!isPreview()){
                            // not previewing, page must be live
                            $query .= sprintf(" AND a.live = 1");
                        }
                        
                    }elseif ($i > 2){
                        // more than 2 levels deep, check up path
                        // This is a subpage, rather confusingly 'a' alias is the 2nd tier page row (this page), 'b' alias is parent page
                        $query = sprintf("SELECT a.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = a.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name,
                        (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = b.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                        b.url AS parent_url FROM pageindex a JOIN pageindex b ON b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 WHERE a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND a.deleted = 0",
                        mysql_real_escape_string($sectionid),
                        mysql_real_escape_string($path[$i-1]), // a url
                        mysql_real_escape_string($path[$i-2])); // b url
                        if (!isPreview()){
                            // not previewing, page must be live
                            $query .= sprintf(" AND a.live = 1");
                        }
                    }else{
                        // more than 1 level deep, check up path
                        $query = sprintf("SELECT pageindex.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = pageindex.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name FROM pageindex WHERE url = '%s' AND sectionid = '%d' AND deleted = 0",
                        mysql_real_escape_string($path[$i-1]),
                        mysql_real_escape_string($sectionid));
                        if (!isPreview()){
                            // not previewing, page must be live
                            $query .= sprintf(" AND live = 1");
                        }
                    }
                    $result = mysql_query($query) or die(mysql_error());
                    if (mysql_num_rows($result) > 0){
                        if ($pagedetails = mysql_fetch_array($result)){
                            if ($pagedetails['customchildren'] == 1){
                                // This page handles its own children, so
                                // redirect to parent page
                                $pagenotfound = false;
                            } else $pagedetails = array();
                        }
                    }else $pagenotfound = true;
                }
                $i--;
            }
        }
    }
    
    if ($pagenotfound){
        // 404
        $url = 'http'.((!empty($_SERVER['HTTPS']))?'s':'').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        
        $query = sprintf("INSERT INTO 404_log (url, referer, ip, datetime) VALUES ('%s', '%s', '%s', '%s')",

        mysql_real_escape_string($url),
        mysql_real_escape_string($_SERVER['HTTP_REFERER']),
        mysql_real_escape_string(returnIPaddress()),
        mysql_real_escape_string(date("Y-m-d H:i:s")));
        mysql_query($query) or die(mysql_error());
        
        $query = sprintf("SELECT pageindex.*, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = pageindex.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name FROM pageindex WHERE pageindex.url = 'error-404'");      
        $result = mysql_query($query) or die(mysql_error());
        $pagedetails = mysql_fetch_array($result);
        header("HTTP/1.0 404 Not Found");
        //echo 'Not found';
    }
    if ($pagedetails['meta_id'] < 1) $pagedetails['meta_id'] = 1;
    $query = sprintf("SELECT * FROM metadata WHERE id = '%d'",
    mysql_real_escape_string($pagedetails['meta_id']));
    if ($result = mysql_query($query)){
        $pagedetails['metadata'] = mysql_fetch_array( $result );
        mysql_free_result($result);
    }
}

function getMicrositePageDetails($microsite_id) {
    global $sections, $sectionid, $pageid, $pagedetails, $includefilename, $path, $fullurl;
    
    // can only go 3 layers deep: section > parent_parent_page > parent_page > page
    
    // Work out where we are, and populate global variables
    $requri = explode('?',$_SERVER["REQUEST_URI"],2);
    $fullurl = substr(urldecode($requri[0]),1);
    //$url = $_GET['url'];
    $sectionid = 0;
    $pageid = 0;
    $pagedetails = array();
    
    getMicrositeSections($microsite_id);
    
    if ($fullurl != ''){
        $path = explode('/',$fullurl,10);
        $path = array_filter($path);
        $num = count($path) - 1;
        $tryparseurl = true;
    }else{
        $includefilename = 'microsite_homepage.php';
        $tryparseurl = false;
    }
    $pagenotfound = false;
        
    if ($tryparseurl){
        // Read the URL
        if (isset($sections[$path[0]])){
            $sectionid = $sections[ $path[0] ]['id'];
            if (isset($path[1])){
                $pageurl = make_safe($path[1]);
            } else $pageurl = '';
        }else $pageurl = make_safe($path[0]);
        if ($pageurl == ''){
            // Get default page for this section
            $query = sprintf("SELECT microsite_pageindex.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = microsite_pageindex.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name FROM microsite_pageindex WHERE microsite_id = '%d' AND id = '%d'",
            mysql_real_escape_string($microsite_id),
            mysql_real_escape_string($sections[$sectionid]['defaultpageid']));
            if (!isPreview()){
                // not previewing, page must be live
                $query .= sprintf(" AND live = 1");
            }
        }else{
            // Get specific page
            if (isset($path[4]) && $path[4] != ''){
                // this is a sub-sub-sub-page, rather confusingly 'a' alias is the 4th tier page row (this page), 'b' alias is it's parent page, 'c' alias is the parent's parent, 'd' alias is the parent's parent's parent
                $query = sprintf("SELECT a.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = a.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name,
                (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = b.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = c.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_name,
                (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = d.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_parent_name,
                b.url AS parent_url,
                c.url AS parent_parent_url,
                d.url AS parent_parent_parent_url FROM microsite_pageindex a JOIN microsite_pageindex b ON b.microsite_id = a.microsite_id AND b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 JOIN microsite_pageindex c ON c.microsite_id = b.microsite_id AND c.id = b.parent_id AND c.sectionid = a.sectionid AND c.deleted = 0 JOIN microsite_pageindex d ON d.microsite_id = c.microsite_id AND d.id = c.parent_id AND d.sectionid = a.sectionid AND d.deleted = 0 WHERE a.microsite_id = '%d' AND a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND c.url = '%s' AND d.url = '%s' AND a.deleted = 0",
                mysql_real_escape_string($microsite_id),
                mysql_real_escape_string($sectionid),
                mysql_real_escape_string($path[4]), // a url
                mysql_real_escape_string($path[3]), // b url
                mysql_real_escape_string($path[2]), // c url
                mysql_real_escape_string($path[1])); // d url
                if (!isPreview()){
                    // not previewing, page must be live
                    $query .= sprintf(" AND a.live = 1");
                }
            }elseif (isset($path[3]) && $path[3] != ''){
                // this is a sub-sub-page, rather confusingly 'a' alias is the 3rd tier page row (this page), 'b' alias is it's parent page, 'c' alias is the parent's parent
                $query = sprintf("SELECT a.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = a.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name,
                (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = b.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = c.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_name,
                b.url AS parent_url,
                c.url AS parent_parent_url FROM microsite_pageindex a JOIN microsite_pageindex b ON b.microsite_id = a.microsite_id AND b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 JOIN microsite_pageindex c ON c.microsite_id = b.microsite_id AND c.id = b.parent_id AND c.sectionid = a.sectionid AND c.deleted = 0 WHERE a.microsite_id = '%d' AND a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND c.url = '%s' AND a.deleted = 0",
                mysql_real_escape_string($microsite_id),
                mysql_real_escape_string($sectionid),
                mysql_real_escape_string($path[3]), // a url
                mysql_real_escape_string($path[2]), // b url
                mysql_real_escape_string($path[1])); // c url
                if (!isPreview()){
                    // not previewing, page must be live
                    $query .= sprintf(" AND a.live = 1");
                }
                
            }elseif (isset($path[2]) && $path[2] != ''){
                
                // This is a subpage, rather confusingly 'a' alias is the 2nd tier page row (this page), 'b' alias is parent page
                $query = sprintf("SELECT a.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = a.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name,
                (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = b.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                b.url AS parent_url FROM microsite_pageindex a JOIN microsite_pageindex b ON b.microsite_id = a.microsite_id AND b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 WHERE a.microsite_id = '%d' AND a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND a.deleted = 0",
                mysql_real_escape_string($microsite_id),
                mysql_real_escape_string($sectionid),
                mysql_real_escape_string($path[2]), // a url
                mysql_real_escape_string($path[1])); // b url
                if (!isPreview()){
                    // not previewing, page must be live
                    $query .= sprintf(" AND a.live = 1");
                }
            }else{
                $query = sprintf("SELECT a.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = a.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name
                FROM microsite_pageindex a 
                WHERE a.microsite_id = '%d' AND a.sectionid = '%d' AND a.url = '%s' AND a.deleted = 0",
                mysql_real_escape_string($microsite_id),
                mysql_real_escape_string($sectionid),
                mysql_real_escape_string($pageurl)); // a url
                if (!isPreview()){
                    // not previewing, page must be live
                    $query .= sprintf(" AND live = 1");
                }
            }
        }
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            $pagedetails = mysql_fetch_array($result);
            if ($path[1] == ''){
                // no page url, only section url ($path[0])
                // we need to populate default page path for subnav to work
                $path[1] = $pagedetails['url'];
            }
            mysql_free_result($result);
        }else{
            // Didn't find page... if it is a subpage, we check the parent page to see if it handles its own subpages (i.e. they have customchildren = 1 in the microsite_pageindex table)
            $pagenotfound = true; // start from page not found and work up until we find a custom handler
            
            // work up the URL from the right to see if page above at some point handles it's own children, if so then we continue and let that page's functionality deal with URL
            
            $i = 10;
            while (($i > 1) AND ($pagenotfound)){
                if (isset( $path[$i] ) && $path[$i] != ''){
                    if ($i > 4){
                        // more than 4 levels deep, check up path
                        // this is a sub-sub-sub-page, rather confusingly 'a' alias is the 4th tier page row (this page), 'b' alias is it's parent page, 'c' alias is the parent's parent, 'd' alias is the parent's parent's parent
                        $query = sprintf("SELECT a.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = a.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name,
                        (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = b.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                        (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = c.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_name,
                        (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = d.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_parent_name,
                        b.url AS parent_url,
                        c.url AS parent_parent_url,
                        d.url AS parent_parent_parent_url FROM microsite_pageindex a JOIN microsite_pageindex b ON b.microsite_id = a.microsite_id AND b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 JOIN microsite_pageindex c ON c.microsite_id = b.microsite_id AND c.id = b.parent_id AND c.sectionid = a.sectionid AND c.deleted = 0 JOIN microsite_pageindex d ON d.microsite_id = c.microsite_id AND d.id = c.parent_id AND d.sectionid = a.sectionid AND d.deleted = 0 WHERE a.microsite_id = '%d' AND a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND c.url = '%s' AND d.url = '%s' AND a.deleted = 0",
                        mysql_real_escape_string($microsite_id),
                        mysql_real_escape_string($sectionid),
                        mysql_real_escape_string($path[$i-1]), // a url
                        mysql_real_escape_string($path[$i-2]), // b url
                        mysql_real_escape_string($path[$i-3]), // c url
                        mysql_real_escape_string($path[$i-4])); // d url
                        if (!isPreview()){
                            // not previewing, page must be live
                            $query .= sprintf(" AND a.live = 1");
                        }
                    }elseif ($i > 3){
                        // more than 3 levels deep, check up path
                        // this is a sub-sub-page, rather confusingly 'a' alias is the 3rd tier page row (this page), 'b' alias is it's parent page, 'c' alias is the parent's parent
                        $query = sprintf("SELECT a.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = a.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name,
                        (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = b.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                        (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = c.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_parent_name,
                        b.url AS parent_url,
                        c.url AS parent_parent_url FROM microsite_pageindex a JOIN microsite_pageindex b ON b.microsite_id = a.microsite_id AND b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 JOIN microsite_pageindex c ON c.microsite_id = b.microsite_id AND c.id = b.parent_id AND c.sectionid = a.sectionid AND c.deleted = 0 WHERE a.microsite_id = '%d' AND a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND c.url = '%s' AND a.deleted = 0",
                        mysql_real_escape_string($microsite_id),
                        mysql_real_escape_string($sectionid),
                        mysql_real_escape_string($path[$i-1]), // a url
                        mysql_real_escape_string($path[$i-2]), // b url
                        mysql_real_escape_string($path[$i-3])); // c url
                        if (!isPreview()){
                            // not previewing, page must be live
                            $query .= sprintf(" AND a.live = 1");
                        }
                        
                    }elseif ($i > 2){
                        // more than 2 levels deep, check up path
                        // This is a subpage, rather confusingly 'a' alias is the 2nd tier page row (this page), 'b' alias is parent page
                        $query = sprintf("SELECT a.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = a.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name,
                        (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = b.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS parent_name,
                        b.url AS parent_url FROM microsite_pageindex a JOIN microsite_pageindex b ON b.microsite_id = a.microsite_id AND b.id = a.parent_id AND b.sectionid = a.sectionid AND b.deleted = 0 WHERE a.microsite_id = '%d' AND a.sectionid = '%d' AND a.url = '%s' AND b.url = '%s' AND a.deleted = 0",
                        mysql_real_escape_string($microsite_id),
                        mysql_real_escape_string($sectionid),
                        mysql_real_escape_string($path[$i-1]), // a url
                        mysql_real_escape_string($path[$i-2])); // b url
                        if (!isPreview()){
                            // not previewing, page must be live
                            $query .= sprintf(" AND a.live = 1");
                        }
                    }else{
                        // more than 1 level deep, check up path
                        $query = sprintf("SELECT a.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = a.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name
                        FROM microsite_pageindex a 
                        WHERE a.microsite_id = '%d' AND a.sectionid = '%d' AND a.url = '%s' AND a.deleted = 0",
                        mysql_real_escape_string($microsite_id),
                        mysql_real_escape_string($sectionid),
                        mysql_real_escape_string($path[$i-1])); // a url
                        if (!isPreview()){
                            // not previewing, page must be live
                            $query .= sprintf(" AND live = 1");
                        }
                    }
                    $result = mysql_query($query) or die(mysql_error());
                    if (mysql_num_rows($result) > 0){
                        if ($pagedetails = mysql_fetch_array($result)){
                            if ($pagedetails['customchildren'] == 1){
                                // This page handles its own children, so
                                // redirect to parent page
                                $pagenotfound = false;
                            } else{
                                $pagedetails = array();
                            }
                        }
                    }else $pagenotfound = true;
                }
                $i--;
            }
        }
    }
    if ($pagenotfound){
        // 404
        $query = sprintf("SELECT microsite_pageindex.*, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = microsite_pageindex.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name FROM microsite_pageindex WHERE microsite_pageindex.url = 'error-404'");        
        $result = mysql_query($query) or die(mysql_error());
        $pagedetails = mysql_fetch_array($result);
        header("HTTP/1.0 404 Not Found");
    }
    $query = sprintf("SELECT * FROM microsites WHERE id = '%d' AND live = 1 AND deleted = 0;",
    mysql_real_escape_string($microsite_id));
    $result = mysql_query($query) or die(mysql_error());
    if ($result){
        $row = mysql_fetch_array($result);
        $pagedetails['metadata']['title'] = $row['title'];
        $pagedetails['metadata']['description'] = $row['description'];
        $pagedetails['metadata']['keywords'] = $row['keywords'];
    }
}

function displayNavMenu($pageid){
    // left-hand, sub-navigation menu show/hide
    $query = sprintf("SELECT nav_menu FROM pageindex WHERE id = '%d'",
    mysql_real_escape_string($pageid));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        if ($row['nav_menu'] == 1){
            return true;
        }else{
            return false;
        }
    }else{
        // default to display side menu
        return true;
    }
}

function displaySideMenu($pageid){
    // right-hand, module column show/hide
    $query = sprintf("SELECT side_menu FROM pageindex WHERE id = '%d'",
    mysql_real_escape_string($pageid));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        if ($row['side_menu'] == 1){
            return true;
        }else{
            return false;
        }
    }else{
        // default to display side menu
        return true;
    }
}

function redirectCMS(){    
    $requri = explode('?',$_SERVER["REQUEST_URI"],2);
    $fullurl = '/'.substr(urldecode($requri[0]),1);
    if ((substr($fullurl, -1) != '/') AND (substr_count($fullurl,'?') == 0) AND (substr_count($fullurl,'#') == 0)){
        $fullurl .= '/';
    }
    
    $query = sprintf("SELECT * FROM redirects WHERE type != 'visible' AND `from` = '%s' AND `deleted` = 0 ORDER BY id DESC LIMIT 1;",
    mysql_real_escape_string($fullurl));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        
        if ($row['type'] == 'permanent'){
            header('HTTP/1.1 301 Moved Permanently');
        }
        header("Location: ".$row['to']);
        exit;
    }
}

function visible_redirect(){
    $requri = explode('?',$_SERVER["REQUEST_URI"],2);
    $fullurl = '/'.substr(urldecode($requri[0]),1);
    if ((substr($fullurl, -1) != '/') AND (substr_count($fullurl,'?') == 0) AND (substr_count($fullurl,'#') == 0)){
        $fullurl .= '/';
    }
    
    $query = sprintf("SELECT * FROM redirects WHERE type = 'visible' AND `from` = '%s' AND `deleted` = 0 ORDER BY id DESC LIMIT 1;",
    mysql_real_escape_string($fullurl));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result); ?>
        <h1>This page has been moved, you will be re-directed in a moment</h1>
        <p>If you are not re-directed please <a href="<?=$row['to']?>">click here</a>.</p>
        <script type="text/javascript">
        <!--
        function delayedRedirect(){
            window.location = "<?=$row['to']?>";
        }
        $JQuery(document).ready(function(){
            setTimeout('delayedRedirect()', 3000);
        });
        //-->
        </script>
        <? return true;
    }else{
        return false;
    }
}

function is_microsite($domain){
    $query = sprintf("SELECT * FROM microsites WHERE domain = '%s' AND live = 1 AND deleted = 0",
    mysql_real_escape_string($domain));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        return true;
    }else{
        return false;
    }
}

function microsite_id($domain){
    $query = sprintf("SELECT * FROM microsites WHERE domain = '%s' AND deleted = 0",
    mysql_real_escape_string($domain));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        
        $microsite_id = $row['id'];
        
        return $microsite_id;
    }else{
        return;
    }
}

function microsite_template($microsite_id){
    $query = sprintf("SELECT header, content, footer FROM microsites WHERE id = '%d' AND deleted = 0",
    mysql_real_escape_string($microsite_id));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        $microsite_template = $row;
        
        return $microsite_template;
    }else{
        return;
    }
}

function microsite_styling($microsite_id){
    $query = sprintf("SELECT * FROM microsite_styles WHERE microsite_id = '%d'",
    mysql_real_escape_string($microsite_id));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        $microsite_styling = $row;
        
        return $microsite_styling;
    }else{
        return;
    }
}

function getSections(){
    global $sections;
    if (isset($sections)) return $sections;
    $sections = array();
    if ($result = mysql_query("SELECT * FROM sections WHERE live = 1 AND deleted = 0;")){
        while ($row = mysql_fetch_array($result)){
            $sections[$row['id']] = $row;
            $sections[$row['url']] = $row;
        }
    }
    return $sections;
}

function getMicrositeSections($microsite_id){
    global $sections;
    if (isset($sections)) return $sections;
    $sections = array();
    $query = sprintf("SELECT * FROM microsite_sections WHERE microsite_id = '%d' AND live = 1 AND deleted = 0;",
    mysql_real_escape_string($microsite_id));
    $result = mysql_query($query) or die(mysql_error());
    if ($result){
        while ($row = mysql_fetch_array($result)){
            $sections[$row['id']] = $row;
            $sections[$row['url']] = $row;
        }
    }
    return $sections;
}

function buildBreadcrumb($pageid, $sectionid, $force = false){

    $section_breadcrumb['defaultpage'] = array();
    $section_breadcrumb['name'] = array();
    $section_breadcrumb['url'] = array();
    $section_breadcrumb['id'] = array();
    
    $page_breadcrumb['defaultpage'] = array();
    $page_breadcrumb['name'] = array();
    $page_breadcrumb['url'] = array();
    $page_breadcrumb['id'] = array();
    
    if ($sectionid > 0){
        $sections = getSections(); // sets sections if not globally set
        
        // breadcrumb starts with section, if this page has a section (not all do) start here
        $section_breadcrumb['name'][] = $sections[$sectionid]['name'];
        $section_breadcrumb['url'][] = $sections[$sectionid]['url'];
        $section_breadcrumb['id'][] = $sectionid;
        $defaultpageid = $sections[$sectionid]['defaultpageid'];
    }
    
    // next we get pages, use while loop as we don't know how nested pages are
    while(!$end){
        $query = sprintf("SELECT pageindex.url, pageindex.parent_id, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = pageindex.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name FROM pageindex WHERE id = '%d' AND sectionid = '%d' AND deleted = 0",
        mysql_real_escape_string($pageid),
        mysql_real_escape_string($sectionid));
        
        if (!$force){
            // not forcing display, page must be live
            $query .= sprintf(" AND live = 1");
        }
        
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            if ($defaultpageid == $pageid){
                // this is the default page for this section
                $page_breadcrumb['defaultpage'][] = 1;
            }else{
                $page_breadcrumb['defaultpage'][] = 0;
            }
            $row = mysql_fetch_array($result);

            $page_breadcrumb['name'][] = $row['name'];
            $page_breadcrumb['url'][] = $row['url'];
            $page_breadcrumb['id'][] = $pageid;
            
            if ($row['parent_id'] > 0){
                $pageid = $row['parent_id']; // set pageid for next level up tree
            }else{
                // no parent to this page, exit while
                $end = true;
            }
            mysql_free_result($result);
        }else{
            // somehow we have gone into another query loop but have no result
            // happens when there is no default page set, for now no recovery, just exit
            $end = true;
        }
    }
    // reverse page array, as built backwards up tree
    $page_breadcrumb['defaultpage'] = array_reverse($page_breadcrumb['defaultpage']);
    $page_breadcrumb['name'] = array_reverse($page_breadcrumb['name']);
    $page_breadcrumb['url'] = array_reverse($page_breadcrumb['url']);
    $page_breadcrumb['id'] = array_reverse($page_breadcrumb['id']);
    
    // create padding for section as default page not valid
    $section_breadcrumb['defaultpage'][] = 0;
    
    // combine the 2 arrays
    $breadcrumb['defaultpage'] = array_merge($section_breadcrumb['defaultpage'], $page_breadcrumb['defaultpage']);
    $breadcrumb['name'] = array_merge($section_breadcrumb['name'], $page_breadcrumb['name']);
    $breadcrumb['url'] = array_merge($section_breadcrumb['url'], $page_breadcrumb['url']);
    $breadcrumb['id'] = array_merge($section_breadcrumb['id'], $page_breadcrumb['id']);
    
    return $breadcrumb;
}

function buildMicrositeBreadcrumb($microsite_id, $pageid, $sectionid, $force = false){
    $section_breadcrumb['defaultpage'] = array();
    $section_breadcrumb['name'] = array();
    $section_breadcrumb['url'] = array();
    $section_breadcrumb['id'] = array();
    
    $page_breadcrumb['defaultpage'] = array();
    $page_breadcrumb['name'] = array();
    $page_breadcrumb['url'] = array();
    $page_breadcrumb['id'] = array();
    
    if ($sectionid > 0){
        $sections = getMicrositeSections($microsite_id); // sets sections if not globally set
        
        // breadcrumb starts with section, if this page has a section (not all do) start here
        $section_breadcrumb['name'][] = $sections[$sectionid]['name'];
        $section_breadcrumb['url'][] = $sections[$sectionid]['url'];
        $section_breadcrumb['id'][] = $sectionid;
        $defaultpageid = $sections[$sectionid]['defaultpageid'];
    }
    
    // next we get pages, use while loop as we don't know how nested pages are
    while(!$end){
        $query = sprintf("SELECT microsite_pageindex.url, microsite_pageindex.parent_id, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = microsite_pageindex.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name FROM microsite_pageindex WHERE microsite_id = '%d' AND id = '%d' AND sectionid = '%d' AND deleted = 0",
        mysql_real_escape_string($microsite_id),
        mysql_real_escape_string($pageid),
        mysql_real_escape_string($sectionid));
        
        if (!$force){
            // not forcing display, page must be live
            $query .= sprintf(" AND live = 1");
        }
        
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            if ($defaultpageid == $pageid){
                // this is the default page for this section
                $page_breadcrumb['defaultpage'][] = 1;
            }else{
                $page_breadcrumb['defaultpage'][] = 0;
            }
            $row = mysql_fetch_array($result);

            $page_breadcrumb['name'][] = $row['name'];
            $page_breadcrumb['url'][] = $row['url'];
            $page_breadcrumb['id'][] = $pageid;
            
            if ($row['parent_id'] > 0){
                $pageid = $row['parent_id']; // set pageid for next level up tree
            }else{
                // no parent to this page, exit while
                $end = true;
            }
            mysql_free_result($result);
        }else{
            // somehow we have gone into another query loop but have no result
            // happens when there is no default page set, for now no recovery, just exit
            $end = true;
        }
    }
    // reverse page array, as built backwards up tree
    $page_breadcrumb['defaultpage'] = array_reverse($page_breadcrumb['defaultpage']);
    $page_breadcrumb['name'] = array_reverse($page_breadcrumb['name']);
    $page_breadcrumb['url'] = array_reverse($page_breadcrumb['url']);
    $page_breadcrumb['id'] = array_reverse($page_breadcrumb['id']);
    
    // create padding for section as default page not valid
    $section_breadcrumb['defaultpage'][] = 0;
    
    // combine the 2 arrays
    $breadcrumb['defaultpage'] = array_merge($section_breadcrumb['defaultpage'], $page_breadcrumb['defaultpage']);
    $breadcrumb['name'] = array_merge($section_breadcrumb['name'], $page_breadcrumb['name']);
    $breadcrumb['url'] = array_merge($section_breadcrumb['url'], $page_breadcrumb['url']);
    $breadcrumb['id'] = array_merge($section_breadcrumb['id'], $page_breadcrumb['id']);
    
    return $breadcrumb;
}

function buildSubnav($sectionid, $force = false){
    global $pagedetails;
/*
if ($sectionid > 0){
        $sections = getSections(); // sets sections if not globally set
        
        // breadcrumb starts with section, if this page has a section (not all do) start here
        $section_breadcrumb['name'][] = $sections[$sectionid]['name'];
        $section_breadcrumb['url'][] = $sections[$sectionid]['url'];
    }
*/
    // first get all pages in this section
    $query = sprintf("SELECT id, url, position, parent_id, (SELECT pagecontent_standard.name FROM pagecontent_standard WHERE pagecontent_standard.pageid = pageindex.id AND pagecontent_standard.published = 1 ORDER BY pagecontent_standard.datetime DESC LIMIT 1) AS name FROM pageindex WHERE sectionid = '%d' AND deleted = 0",
    mysql_real_escape_string($sectionid));
    
    if (!$force){
        // not forcing display, page must be live, and show in nav, or currently active
        $query .= sprintf(" AND (show_in_nav = 1 OR id = '%d' OR id = '%d') AND live = 1",
        mysql_real_escape_string($pagedetails['id']),
        mysql_real_escape_string($pagedetails['parent_id']));
    }
    
    $query .= sprintf(" ORDER BY position");
    $all_pages = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($all_pages) > 0){
        while ($page = mysql_fetch_array($all_pages)){
            $page_breadcrumb[$page['id']][0] = $page;
        }
        mysql_free_result($all_pages);
        return $page_breadcrumb;
    }else{
        // no pages found
        return;
    }
}

function buildMicrositeSubnav($microsite_id, $sectionid, $force = false){
    // first get all pages in this section
    $query = sprintf("SELECT id, url, position, parent_id, (SELECT microsite_pagecontent_standard.name FROM microsite_pagecontent_standard WHERE microsite_pagecontent_standard.pageid = microsite_pageindex.id AND microsite_pagecontent_standard.published = 1 ORDER BY microsite_pagecontent_standard.datetime DESC LIMIT 1) AS name FROM microsite_pageindex WHERE microsite_id = '%d' AND sectionid = '%d' AND show_in_nav = 1 AND deleted = 0",
    mysql_real_escape_string($microsite_id),
    mysql_real_escape_string($sectionid));
    
    if (!$force){
        // not forcing display, page must be live
        $query .= sprintf(" AND live = 1");
    }
    
    $query .= sprintf(" ORDER BY position");
    $all_pages = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($all_pages) > 0){
        while ($page = mysql_fetch_array($all_pages)){
            $page_breadcrumb[$page['id']][0] = $page;
        }
        mysql_free_result($all_pages);
        return $page_breadcrumb;
    }else{
        // no pages found
        return;
    }
}

function image_prep($body){
    // apply appropriate floating, padding to images in text
    $result = preg_match_all('#<img[^>]*>#i', $body, $matches);
    if ($result > 0){
        // post has an image, find image src
        for($i=0;$i<$result;$i++){
            $image = $matches[0][$i];
            
            if (substr_count($image, 'float: left;') == 1){
                $class = 'alignleft';
                
                $image = str_replace('float: left;', '', $image); // floating handled in class
            }elseif (substr_count($image, 'float: right;') == 1){
                $class = 'alignright';
                
                $image = str_replace('float: right;', '', $image); // floating handled in class
            }else{
                $class = 'alignnull';
            }
            
            if ((substr_count($image, 'title="') == 1) AND (substr_count($image, 'title=""') == 0)){
                // has title
                $title_result = preg_match('/title="[^"]*"/i', $image, $title_matches);
                if ($title_result > 0){
                    $title = $title_matches[0];
                    $title = str_replace('title="', '', $title);
                    $title = str_replace('"', '', $title);
                    
                    if (substr_count($image, '"width: ') == 1){
                        $image_width_result = preg_match('/"width: [0-9]*px;/i', $image, $image_width_matches);
                        if ($image_width_result > 0){
                            $width = $image_width_matches[0];
                            $width = str_replace('"width: ', '', $width);
                            $width = str_replace('px;', '', $width);
                        }
                    }elseif (substr_count($image, ' width: ') == 1){
                        $image_width_result = preg_match('/ width: [0-9]*px;/i', $image, $image_width_matches);
                        if ($image_width_result > 0){
                            $width = $image_width_matches[0];
                            $width = str_replace(' width: ', '', $width);
                            $width = str_replace('px;', '', $width);
                        }
                    }else{
                        $image_width_result = preg_match('/ src="[^"]*"/i', $image, $image_width_matches);
                        if ($image_width_result > 0){
                            $source = $image_width_matches[0];
                        }
                        
                        $file = $GLOBALS['docRoot'].$source;
                        
                        list($width, $height) = getimagesize($file);
                    }
                    
                    // remove margin
                    if (substr_count($image, 'margin: ') == 1){
                        $image_margin_result = preg_match('/margin: [0-9\s]*px;/i', $image, $image_margin_matches);
                        if ($image_margin_result > 0){
                            $image = str_replace($image_margin_matches[0], '', $image);
                        }
                    }
                    
                    // remove padding
                    if (substr_count($image, 'padding: ') == 1){
                        $image_padding_result = preg_match('/padding: [0-9\s]*px;/i', $image, $image_padding_matches);
                        if ($image_padding_result > 0){
                            $image = str_replace($image_padding_matches[0], '', $image);
                        }
                    }
                    
                    if ($width > $GLOBALS['standard_content_width']){
                        // image is too large for content
                        $width = $GLOBALS['standard_content_width'];
                        
                        $image_width_result = preg_match('/width: [0-9\s]*px;/i', $image, $image_width_matches);
                        if ($image_width_result > 0){
                            $image = str_replace($image_width_matches[0], 'width: '.$width.'px;', $image);
                        }
                    }
                    
                    if ($width > $GLOBALS['standard_content_width']){
                        $width = $GLOBALS['standard_content_width'];
                    }
                    
                    $image_html = '<span class="caption '.$class.'"';
                    if ($width != ''){
                    
                        $image_html .= ' style="width:'.$width.'px;"';
                    }
                    
                    $image_html .= '>'.$image.'<span class="caption-text"';
                    
                    if ($width != ''){
                        $image_html .= ' style="width:'.$width.'px;"';
                    }
                    $image_html .= '>'.$title.'</span></span>';
                }else{
                    $image_html = $image;
                }
            }else{
                if (substr_count($image, 'class="') == 1){
                    // image already has classes, need to append these
                    $image_html = str_replace('class="', 'class="'.$class.' ', $image);
                }else{
                    $image_html = str_replace('src="', 'class="'.$class.'" src="', $image);
                }
            }
            $body = str_replace($matches[0][$i], $image_html, $body); // replace old image HTML with new image HTML
        }
        
        // had images, now
        
    }
    return $body;
}

    
function PageGalleries($type, $id, $content){
    if ($type == 'pages'){
        $query = sprintf("SELECT gallery, gallery_method FROM pageindex WHERE id = '%d'",
        mysql_real_escape_string($id));
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            $row = mysql_fetch_array($result);
            
            $gallery_content = BuildGallery($row['gallery']);
            
            if ($row['gallery_method'] == 'replace'){
                // replacing div in body of content of form <div id="gallery">(stuff)</div> with {gallery}
                // with reg_replace, for simpler replace with str_replace
                if (substr_count($content, '<div id="gallery"') == 0){
                    // <div id="gallery"> not found, try looking for class instead
                    if (substr_count($content, '<div class="gallery"') == 0){
                        $pattern = '/<div class="gallery">+(.*?)[^(<\/div>)]*<\/div>/s';
                        $replace = '{gallery}';
                        $content = preg_replace($pattern, $replacement, $content);
                    }
                }else{
                    $pattern = '/<div id="gallery">+(.*?)[^(<\/div>)]*<\/div>/s';
                    $replace = '{gallery}';
                    $content = preg_replace($pattern, $replace, $content);              
                }
                $content = str_replace('{gallery}', '<div class="gallery replace">'.$gallery_content.'</div>', $content); // now do easy replace
            }elseif ($row['gallery_method'] == 'before'){
                // gallery content to appear before page content
                $content = '<div class="gallery before">'.$gallery_content.'</div>'.$content;
            }elseif ($row['gallery_method'] == 'after'){
                // gallery content to appear after page content
                $content = $content.'<div class="gallery after">'.$gallery_content.'</div>';
            }
            return $content;
        }else{
            // no gallery
            return $content;
        }
    }elseif ($type == 'microsite_pages'){
        $query = sprintf("SELECT gallery, gallery_method FROM microsite_pageindex WHERE id = '%d'",
        mysql_real_escape_string($id));
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            $row = mysql_fetch_array($result);
            
            $gallery_content = BuildGallery($row['gallery']);
            
            if ($row['gallery_method'] == 'replace'){
                // replacing div in body of content of form <div id="gallery">(stuff)</div> with {gallery}
                // with reg_replace, for simpler replace with str_replace
                if (substr_count($content, '<div id="gallery"') == 0){
                    // <div id="gallery"> not found, try looking for class instead
                    if (substr_count($content, '<div class="gallery"') == 0){
                        $pattern = '/<div class="gallery">+(.*?)[^(<\/div>)]*<\/div>/s';
                        $replace = '{gallery}';
                        $content = preg_replace($pattern, $replacement, $content);
                    }
                }else{
                    $pattern = '/<div id="gallery">+(.*?)[^(<\/div>)]*<\/div>/s';
                    $replace = '{gallery}';
                    $content = preg_replace($pattern, $replace, $content);              
                }
                $content = str_replace('{gallery}', '<div class="gallery replace">'.$gallery_content.'</div>', $content); // now do easy replace
            }elseif ($row['gallery_method'] == 'before'){
                // gallery content to appear before page content
                $content = '<div class="gallery before">'.$gallery_content.'</div>'.$content;
            }elseif ($row['gallery_method'] == 'after'){
                // gallery content to appear after page content
                $content = $content.'<div class="gallery after">'.$gallery_content.'</div>';
            }
            return $content;
        }else{
            // no gallery
            return $content;
        }
    }
}

function BuildGallery($id){
    $uploadsfolder = '/uploads/images/';
    
    $query = sprintf("SELECT * FROM galleries WHERE id = '%d' AND live = 1 AND deleted = 0",
    mysql_real_escape_string($id));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        
        if ($row['type'] == 'slideshow'){
            $query = sprintf("SELECT * FROM gallerycategories WHERE gallery_id = '%d' AND live = 1 AND deleted = 0",
            mysql_real_escape_string($id));
            $gallery_category_result = mysql_query($query) or die(mysql_error());
            if (mysql_num_rows($gallery_category_result) > 0){
                if (mysql_num_rows($gallery_category_result) > 1){
                    $gallery_content .= '<form action="" name="form1" method="get">';
                    $gallery_content .= '<select class="gallery_category" name="gallery_category" onChange="document.form1.submit()">';
                    $gallery_content .= '<option value="">All Categories</option>';
                    while ($gallery_category_row = mysql_fetch_array($gallery_category_result)){
                        $gallery_content .= '<option value="'.$gallery_category_row['id'].'"';
                        if ($gallery_category_row['id'] == $_GET['gallery_category']){
                            $gallery_content .= ' selected="selected"';
                        }
                        $gallery_content .= '>'.$gallery_category_row['name'].'</option>';
                    }
                    $gallery_content .= '</select>';
                    $gallery_content .= '</form>';
                }
                
                if ($_GET['gallery_category']){
                    // viewing individual category
                    $query = sprintf("SELECT gallerycontent.* FROM gallerycontent JOIN gallerycategories ON gallerycategories.id = gallerycontent.category_id WHERE gallerycontent.gallery_id = '%d' AND gallerycontent.category_id = '%d' AND gallerycontent.deleted = 0 AND gallerycontent.live = 1 AND gallerycategories.deleted = 0 AND gallerycategories.live = 1 ORDER BY gallerycontent.position",
                    mysql_real_escape_string($id),
                    mysql_real_escape_string($_GET['gallery_category']));
                }else{
                    $query = sprintf("SELECT gallerycontent.* FROM gallerycontent JOIN gallerycategories ON gallerycategories.id = gallerycontent.category_id WHERE gallerycontent.gallery_id = '%d' AND gallerycontent.deleted = 0 AND gallerycontent.live = 1 AND gallerycategories.deleted = 0 AND gallerycategories.live = 1 ORDER BY gallerycontent.position",
                    mysql_real_escape_string($id));
                }
                $image_result = mysql_query($query) or die(mysql_error());
                if (mysql_num_rows($image_result) > 0){
                    $gallery_content .= '<div class="slideshow_gallery">';
                    
                    $gallery_content .= '<div id="galleriffic" class="galleriffic">';
                    $gallery_content .= '<div id="controls" class="controls"></div>';
                    $gallery_content .= '<div class="slideshow-container"><div id="loading" class="loader"></div><div id="slideshow" class="slideshow"></div></div>';
                    $gallery_content .= '<div id="caption" class="caption-container"></div>';
                    $gallery_content .= '</div>';
                    $gallery_content .= '<div id="thumbs" class="navigation"><ul class="thumbs noscript">'; 
                
                    while ($image_row = mysql_fetch_array($image_result)){
                        $gallery_content .= '<li><a class="thumb" name="optionalCustomIdentifier" href="'.$uploadsfolder.'large/'.$image_row['filename'].'"  title="'.htmlentities($image_row['title']).'"><img src="'.$uploadsfolder.'thumb/'.$image_row['filename'].'" alt="'.htmlentities($image_row['title']).'" /></a>';
                        $gallery_content .= '<div class="caption">';
                        $gallery_content .= '<div class="image-title">'.htmlentities($image_row['title']).'</div>';
                        $gallery_content .= '<div class="image-desc">'.nl2br(htmlentities($image_row['description'])).'</div>';
                        $gallery_content .= '</div>';
                        $gallery_content .= '</li>';
                    }
                    
                    $gallery_content .= '</ul>';
                    $gallery_content .= '<div class="clear_both"></div>';
                    $gallery_content .= '</div>';
                    $gallery_content .= '<div class="clear_both"></div>';
                    $gallery_content .= '</div>
                    <script type="text/javascript" src="/min/?f=js/galleriffic_initialise.js"></script>';
                }
            }
        }elseif ($row['type'] == 'lightbox'){
            $query = sprintf("SELECT * FROM gallerycategories WHERE gallery_id = '%d' AND live = 1 AND deleted = 0",
            mysql_real_escape_string($id));
            $gallery_category_result = mysql_query($query) or die(mysql_error());
            if (mysql_num_rows($gallery_category_result) > 0){
                if (mysql_num_rows($gallery_category_result) > 1){
                    $gallery_content .= '<form action="" name="form1" method="get">';
                    $gallery_content .= '<select class="gallery_category" name="gallery_category" onChange="document.form1.submit()">';
                    $gallery_content .= '<option value="">All Categories</option>';
                    while ($gallery_category_row = mysql_fetch_array($gallery_category_result)){
                        $gallery_content .= '<option value="'.$gallery_category_row['id'].'"';
                        if ($gallery_category_row['id'] == $_GET['gallery_category']){
                            $gallery_content .= ' selected="selected"';
                        }
                        $gallery_content .= '>'.$gallery_category_row['name'].'</option>';
                    }
                    $gallery_content .= '</select>';
                    $gallery_content .= '</form>';
                }
                if ($gallery_category_result['id']){
                    // viewing individual category
                    $query = sprintf("SELECT gallerycontent.*, gallerycategories.name AS category FROM gallerycontent JOIN gallerycategories ON gallerycategories.id = gallerycontent.category_id WHERE gallerycontent.gallery_id = '%d' AND gallerycontent.category_id = '%d' AND gallerycontent.deleted = 0 AND gallerycontent.live = 1 AND gallerycategories.deleted = 0 AND gallerycategories.live = 1 ORDER BY gallerycontent.position",
                    mysql_real_escape_string($id),
                    mysql_real_escape_string($catid));
                }else{
                    $query = sprintf("SELECT gallerycontent.*, gallerycategories.name AS category FROM gallerycontent JOIN gallerycategories ON gallerycategories.id = gallerycontent.category_id WHERE gallerycontent.gallery_id = '%d' AND gallerycontent.deleted = 0 AND gallerycontent.live = 1 AND gallerycategories.deleted = 0 AND gallerycategories.live = 1 ORDER BY gallerycontent.position",
                    mysql_real_escape_string($id));
                }
                $image_result = mysql_query($query) or die(mysql_error());
                if (mysql_num_rows($image_result) > 0){
                    $gallery_content .= '<div class="lightbox_gallery">';
                    
                    while ($image_row = mysql_fetch_array($image_result)){
                        $gallery_content .= '<a href="'.$uploadsfolder.'large/'.$image_row['filename'].'" rel="prettyPhoto[group]" title="'.htmlentities($image_row['category']).': '.htmlentities($image_row['title']).'"><img src="'.$uploadsfolder.'thumb/'.$image_row['filename'].'" alt="'.htmlentities($image_row['title']).'" /></a>';
                    }
                    $gallery_content .= '</div>';
                }
            }
        }
        
        if ($row['public'] == 1){
            $gallery_content .= '<h2>Submit your own photos</h2>';
            $gallery_content .= '<form class="contact_form" action="/gallery-submission/gallery-submission/" method="post" enctype="multipart/form-data">';
            
            $gallery_content .= '<input type="hidden" name="gallery" value="'.$id.'" />';
            
            $gallery_content .= '<div><label for="title">Title:<em>*</em></label><input id="title" name="title" type="text" value="" /></div>';
            
            $query = sprintf("SELECT * FROM gallerycategories WHERE gallery_id = '%d' AND live = 1 AND deleted = 0",
            mysql_real_escape_string($id));
            $gallery_category_result = mysql_query($query) or die(mysql_error());
            if (mysql_num_rows($gallery_category_result) > 0){
                if (mysql_num_rows($gallery_category_result) > 1){
                    $gallery_content .= '<div><label for="category">Category:<em>*</em></label>';
                    $gallery_content .= '<select id="category" name="category">';
                    $gallery_content .= '<option value="">--Choose category--</option>';
                    while ($gallery_category_row = mysql_fetch_array($gallery_category_result)){
                        $gallery_content .= '<option value="'.$gallery_category_row['id'].'"';
                        if ($gallery_category_row['id'] == $_GET['gallery_category']){
                            $gallery_content .= ' selected="selected"';
                        }
                        $gallery_content .= '>'.$gallery_category_row['name'].'</option>';
                    }
                    $gallery_content .= '</select></div>';
                }else{
                    $gallery_category_row = mysql_fetch_array($gallery_category_result);
                    
                    $gallery_content .= '<input type="hidden" name="category" value="'.$gallery_category_row['id'].'" />';
                }
            }
            $gallery_content .= '<div><label for="description">Description:</label><br /><textarea name="description" rows="5" cols="40"></textarea></div>';
            $gallery_content .= '<div><label for="image">Image upload:<em>*</em></label><input type="file" id="image" name="image" /></div>';
            $gallery_content .= '<div><label id="form_submit_label" for="form_submit"><em>* Required Field</em></label><input id="form_submit" name="form_submit" type="submit" value="Submit" /></div>';
            $gallery_content .= '</form>';
        }
    }
    return $gallery_content;
}

function PageBlogFeed($type, $id){
    global $path;
    
    if ($type == 'pages'){
        $query = sprintf("SELECT url, customchildren FROM pageindex WHERE id = '%d' AND feed != ''",
        mysql_real_escape_string($id));
    }elseif ($type == 'microsite_pages'){
        $query = sprintf("SELECT url, customchildren FROM microsite_pageindex WHERE id = '%d' AND feed != ''",
        mysql_real_escape_string($id));
    }else{
        return;
    }
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        
        $page_url = $row['url'];
        $customchildren = $row['customchildren'];
    }
    
    if ($customchildren == 1){
        // can handle its articles inside this page
        $root_url = '/';
        $found_this_page = false;
        foreach($path AS $part){
            if ($found_this_page){
                $url_array[] = $part; // [0] => step, [1] => 1,2,3
            }
            if (($part != $page_url) AND (!$found_this_page)){
                $root_url .= $part.'/';
            }else{
                $found_this_page = true;
            }
        }
        
        $root_url .= $page_url.'/';
        
        for($i = 0; $i < count($url_array); $i++){
            if (($url_array[$i] == 'page') AND (is_numeric($url_array[$i+1])) AND ($url_array[$i+1] != '')){
                $page = $url_array[$i+1];
            }
        }
        if ($page == ''){
            $page = 1;
        }
        
        customPageBlogFeed($type, $id, $root_url, $page);
    
    }else{  
        if ($type == 'pages'){
            $query = sprintf("SELECT feed, feed_method, feed_limit FROM pageindex WHERE id = '%d' AND feed != ''",
            mysql_real_escape_string($id));
        }elseif ($type == 'microsite_pages'){
            $query = sprintf("SELECT feed, feed_method, feed_limit FROM microsite_pageindex WHERE id = '%d' AND feed != ''",
            mysql_real_escape_string($id));
        }else{
            return;
        }
        
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            $row = mysql_fetch_array($result);
            
            $url = $row['feed'];
            $feed_method = $row['feed_method'];
            $feed_limit = $row['feed_limit'];
            
            if (($feed_limit == 0) OR ($feed_limit > 100)) $feed_limit = 5;
            
            $url_count = strlen($url);
            $url = alias_url_strip($url); // RSS URL is entered by normal user, strip all possible domain, http, https etc. so we know what we have
            $url_pos = strpos($url, "http://"); //check if we have an external site.
            
            if ($url_pos === false) {
                // not external URL
                $url = 'http://'.$GLOBALS['site_url'].$url; // go to correct place
            }
            
            $wordpress_articles_array = get_wordpress_rss_items_as_array($url); // get wordpress articles from RSS feed cache
            
            //var_dump($wordpress_articles_array);
            
            for($i = 0; $i < $feed_limit+1; $i++){
                if ($i < $feed_limit){
                    $array_object = $wordpress_articles_array[$i]; // get first article, we only want one
                    if ($array_object['title'] != ''){
                        echo '<div class="article_feed">';
                        echo '<h2><a href="'.$array_object['link'].'">'.$array_object['title'].'</a></h2>';  
        
                        echo '<p class="news_date_small">Posted on '.date("jS F Y",strtotime($array_object['pubDate'])).' by ';
                        if ($url_pos === false) {
                            // not external URL
                            if ($array_object['author_link'] != ''){
                                echo '<a href="'.$array_object['author_link'].'" title="More from this author">'.$array_object['author'];
                                if ($array_object['author_photo'] != ''){
                                    echo '<span class="author_photo_thumb">'.$array_object['author_photo'].'</span>';
                                }
                                echo '</a>';
                            }else{
                                echo $array_object['author'];
                                if ($array_object['author_photo'] != ''){
                                    echo '<span class="author_photo_thumb">'.$array_object['author_photo'].'</span>';
                                }
                            }
                        }else{
                            echo $array_object['author'];
                        }
    
                        //$avatar = get_author_avatar($array_object['author']);
                        if ($avatar != '') echo '<span class="avatar>'.$avatar.'</span>';
                        echo '</p>';
                        
                        if ($feed_method == 'full'){
                            echo $array_object['content'];
                        }else{
                            echo '<p>';
                            if ($array_object['image'] != ''){
                                echo '<a href="'.$array_object['link'].'"><img src="'.$array_object['image'].'" alt="" style="float:left;padding:0 10px 10px 0;max-height:100px; max-width:100px !important;" /></a>';
                            }
                            //echo str_replace("Continue reading ", "Continue reading ".$array_object['title']." ", $array_object['description']);
                            echo $array_object['description'];
                            echo '</p>';
                        }
                        echo '</div>';
                    }
                }else{
                    // outside limit, but have more articles
                    if (substr_count($url, '/feed/') == 1){
                        echo '<div class="article_feed">';
                        echo '<h2><a href="'.str_replace("/feed/", "/", $url).'">Read more articles &gt;</a></h2>';
                        echo '</div>';
                    }
                }
            }
        }
    }
}

function customPageBlogFeed($type, $id, $link, $page){
    if ($type == 'pages'){
        $query = sprintf("SELECT feed, feed_method, feed_limit FROM pageindex WHERE id = '%d' AND feed != ''",
        mysql_real_escape_string($id));
    }elseif ($type == 'microsite_pages'){
        $query = sprintf("SELECT feed, feed_method, feed_limit FROM microsite_pageindex WHERE id = '%d' AND feed != ''",
        mysql_real_escape_string($id));
    }else{
        return;
    }
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        
        $url = $row['feed'];
        $feed_method = $row['feed_method'];
        $feed_limit = $row['feed_limit'];
        
        if (($feed_limit == 0) OR ($feed_limit > 100)) $feed_limit = 5;
        
        $url_count = strlen($url);
        $url = alias_url_strip($url); // RSS URL is entered by normal user, strip all possible domain, http, https etc. so we know what we have
        $url_pos = strpos($url, "http://"); //check if we have an external site.
        
        if ($url_pos === false) {
            $url = 'http://'.$GLOBALS['site_url'].$url; // go to correct place
        }
        
        $wordpress_articles_array = get_wordpress_rss_items_as_array($url); // get wordpress articles from RSS feed cache
        
        //var_dump($wordpress_articles_array);
        
        $pages = ceil(count($wordpress_articles_array) / $feed_limit);

        if ($pages > 1){
            echo '<div class="pagination">';
            if ($page < $pages) echo '<a href="'.$link.'page/'.($page + 1).'/" class="older">&larr; Older news</a>';
            if ($page > 1) echo '<a href="'.$link.'page/'.($page - 1).'/" class="newer">Newer news &rarr;</a>';
            echo '</div>';
        }
        
        if ($page > 1){
            $start = ($page-1)*$feed_limit;
        }else{
            $start = 0;
        }
        
        $end = $start + $feed_limit;
        
        for($i = $start; $i < $end; $i++){
            $array_object = $wordpress_articles_array[$i]; // get first article, we only want one
            if ($array_object['title'] != ''){
                $array_object['link'] = str_replace("/news/all-news/", $link, $array_object['link']);
                
                echo '<div class="article_feed">';
                echo '<h2><a href="'.$array_object['link'].'">'.$array_object['title'].'</a></h2>';  

                echo '<p class="news_date_small">Posted on '.date("jS F Y",strtotime($array_object['pubDate'])).' by ';
                
                if ($array_object['author_link'] != ''){
                    echo '<a href="'.$array_object['author_link'].'" title="More from this author">'.$array_object['author'];
                    if ($array_object['author_photo'] != ''){
                        echo '<span class="author_photo_thumb">'.$array_object['author_photo'].'</span>';
                    }
                    echo '</a>';
                }else{
                    echo $array_object['author'];
                    if ($array_object['author_photo'] != ''){
                        echo '<span class="author_photo_thumb">'.$array_object['author_photo'].'</span>';
                    }
                }
                echo '</p>';
                
                if ($feed_method == 'full'){
                    echo $array_object['content'];
                }else{
                    echo '<p>';
                    if ($array_object['image'] != ''){
                        echo '<a href="'.$array_object['link'].'"><img src="'.$array_object['image'].'" alt="" style="float:left;padding:0 10px 10px 0;max-height:100px; max-width:100px !important;" /></a>';
                    }
                    //echo str_replace("Continue reading ", "Continue reading ".$array_object['title']." ", $array_object['description']);
                    echo str_replace("/news/all-news/", $link, $array_object['description']);
                    echo '</p>';
                }
                echo '</div>';
            }
        }
        if ($pages > 1){
            echo '<div class="pagination">';
            if ($page < $pages) echo '<a href="'.$link.'page/'.($page + 1).'/" class="older">&larr; Older news</a>';
            if ($page > 1) echo '<a href="'.$link.'page/'.($page - 1).'/" class="newer">Newer news &rarr;</a>';
            echo '</div>';
        }
    }
}

function PageDownloads($type, $id){
    if ($type == 'pages'){
        $table = 'downloads_xref';
    }elseif ($type == 'microsite_pages'){
        $table = 'microsite_downloads_xref';
        $type = 'pages';
    }elseif ($type == 'events'){
        $table = 'downloads_xref';
        $type = 'events';
    }elseif ($type == 'jobs'){
        $table = 'downloads_xref';
        $type = 'jobs';
    }else{
        return;
    }

    $query = sprintf("SELECT dl.*, dx.order_id, dx.id FROM downloads AS dl, %s AS dx WHERE dx.deleted = 0 AND dl.deleted = 0 AND dl.live = 1 AND dl.doctype = 'video' AND dx.type = '%s' AND dx.page_id = '%d' AND dx.dl_id = dl.id ORDER BY dx.order_id",
    mysql_real_escape_string($table),
    mysql_real_escape_string($type),
    mysql_real_escape_string($id));
    $getdl = mysql_query($query);
    $num_dl = mysql_num_rows( $getdl );
    if ($num_dl > 0) {
        // have videos, include scripts
        /*$downloads .='<script type="text/javascript" src="/js/mootools.js"></script>';
        $downloads .='<script type="text/javascript" src="/js/overlay.js"></script>';
        $downloads .='<script type="text/javascript" src="/js/multibox.js"></script>';*/
    
        $downloads .= '<div class="dl_list video">';
        $downloads .= '<h3>Videos</h3>';
        $downloads .= '<ul>';
        while ($dl = mysql_fetch_array($getdl)){
            if ($dl['embed'] != ''){
                $downloads .= '<li>'.content_embed_scaling($dl['embed']).'</li>';
            }else{
                
                $downloads .= '<li><a href="/uploads/videos/'.$dl['filename'].'" class="flowplayer_video" id="player'.$dl['id'].'"></a>';
                        
                $downloads .= '<script type="text/javascript">
                <!--
                flowplayer("player'.$dl['id'].'", "/js/flowplayer/flowplayer-3.2.7.swf",{
                    clip: {
                        autoPlay: false,
                        scaling: \'fit\'
                    }
                });
                //-->
                </script></li>';
            }
        }
        $downloads .= '</ul>';
        $downloads .= '</div>';
    }
    
    $query = sprintf("SELECT dl.*, dx.order_id, dx.id FROM downloads AS dl, %s AS dx WHERE dx.deleted = 0 AND dl.deleted = 0 AND dl.live = 1 AND dl.doctype = 'audio' AND dx.type = '%s' AND dx.page_id = '%s' AND dx.dl_id = dl.id ORDER BY dx.order_id",
    mysql_real_escape_string($table),
    mysql_real_escape_string($type),
    mysql_real_escape_string($id));
    $getdl = mysql_query($query);
    $num_dl = mysql_num_rows( $getdl );
    if ($num_dl > 0) {
        // have audio
        $downloads .= '<div class="dl_list video">';
        $downloads .= '<h3>Audio</h3>';
        $downloads .= '<ul>';
        while ($dl = mysql_fetch_array($getdl)){
            if ($dl['embed'] != ''){
                $downloads .= '<li>'.$dl['name'].'<br />'.content_embed_scaling($dl['embed']).'</li>';
            }else{
                $downloads .= '<li>'.$dl['name'].'<br />
                <div id="player'.$dl['id'].'" class="flowplayer_audio" href="/uploads/audio/'.$dl['filename'].'"></div>';
                $downloads .= '<script type="text/javascript">
                <!--
                flowplayer("player'.$dl['id'].'", "/js/flowplayer/flowplayer-3.2.7.swf",{
                    // fullscreen button not needed here
                    plugins: {
                        controls: {
                            fullscreen: false,
                            height: 30,
                            autoHide: false
                        }
                    },
                    clip: {
                        type: "audio",
                        autoPlay: false,
                        // optional: when playback starts close the first audio playback
                        onBeforeBegin: function() {
                            $f("player'.$dl['id'].'").close();
                        }
                    }
                });
                //-->
                </script></li>';
            }
        }
        $downloads .= '</ul>';
        $downloads .= '</div>';
    }

    $query = sprintf("SELECT dl.*, dx.order_id, dx.id FROM downloads AS dl, %s AS dx WHERE dx.deleted = 0 AND dl.deleted = 0 AND dl.live = 1 AND dl.doctype != 'video' AND dl.doctype != 'audio' AND dx.type = '%s' AND dx.page_id = '%s' AND dx.dl_id = dl.id ORDER BY dx.order_id",
    mysql_real_escape_string($table),
    mysql_real_escape_string($type),
    mysql_real_escape_string($id));
    //echo '<!--'.$query.'-->';
    $getdl = mysql_query($query);
    $num_dl = mysql_num_rows( $getdl );
    if ($num_dl > 0) {
        // we have downloads
        $google_analytics_code = download_google_analytics();
        
        $downloads .= '<div class="dl_list downloads">';
        $downloads .= '<h3>Downloads</h3>';
        $downloads .= '<ul>';
        while ($dl = mysql_fetch_array($getdl)) {
            $link = '/uploads/documents/'.$dl['filename'];
            
            $this_code = str_replace('{link}', $link, $google_analytics_code);
            
            $downloads .= '<li><a href="/uploads/documents/'.$dl['filename'].'" '.$this_code.' target="_blank">'.stripslashes($dl['name']).'</a></li>';
        }
        $downloads .= '</ul>';
        $downloads .= '</div>';
        
    }
    return $downloads;
}

function toggle_box_generation($html) {
    $pattern = '/<div>\s+(?!=<){TOGGLE_(.*?)}<\/div>/';
    $result = preg_match_all($pattern, $html, $matches);
    
    if($result > 0) {
        foreach($matches[0] as $match) {
            $toggle_html_start = '<div class="accordion">{TITLE}<div id="{ID}" class="accordion_module">';
            $toggle_html_end = '</div></div>';
            
            $toggle_id = trim(strip_tags($match));// lets get the toggle box ID
            $toggle_id = str_replace("{TOGGLE_", "", $toggle_id); 
            $toggle_id = str_replace("}", "", $toggle_id);
            $toggle_ids[] = $toggle_id;
            
            $query = sprintf("SELECT id FROM modules WHERE modules.live = 1 AND modules.deleted = 0 AND modules.id = '%d'",
            mysql_real_escape_string($toggle_id));
            $result = mysql_query($query) or die(error_handler('', 'mysql'));
            if(mysql_num_rows($result) > 0) {
                for($i = 0; $i <= 9; $i++){
                    if($i == 0) {
                        $new_query = sprintf("SELECT modulecontent.field, modulecontent.value FROM modules LEFT JOIN modulecontent ON modules.id = modulecontent.moduleid WHERE modules.live = 1 AND modules.deleted = 0 AND modules.id = '%d' AND field = 'tab_main_title' AND modulecontent.deleted = 0 ORDER BY `modulecontent`.`id` ASC",
                        mysql_real_escape_string($toggle_id));
                    }
                    else {
                        $new_query = sprintf("SELECT modulecontent.field, modulecontent.value FROM modules LEFT JOIN modulecontent ON modules.id = modulecontent.moduleid WHERE modules.live = 1 AND modules.deleted = 0 AND modules.id = '%d' AND (field = '%s' OR field = '%s') AND modulecontent.deleted = 0 ORDER BY `modulecontent`.`id` ASC",
                        mysql_real_escape_string($toggle_id),
                        mysql_real_escape_string('tab_'.$i.'_title'),
                        mysql_real_escape_string('*tab_'.$i.'_content*'));
                    }
                    $new_result = mysql_query($new_query) or die(error_handler('', 'mysql'));
                    if(mysql_num_rows($new_result) > 0) {
                        while($row = mysql_fetch_assoc($new_result)) {
                            if($row['field'] == 'tab_main_title') {
                                $toggle_html_start = str_replace("{TITLE}", "<h3>".$row['value']."</h3>", $toggle_html_start);  
                            }
                            elseif($row['field'] == 'tab_'.$i.'_title' AND $row['value'] != '') {
                                $tab_title = $row['value'];
                            }
                            elseif($row['field'] == '*tab_'.$i.'_content*' AND $row['value'] != '') {
                                $toggle_html .= '
                                    <div class="ui-accordion-contents">
                                        <h4 class="arial">{TOG_TITLE'.$i.'}</h4>
                                        <div class="expandable_box">
                                            <div class="ui-accordion-content">
                                                '.$row['value'].'
                                            </div>
                                        </div>
                                    </div>';
                                $toggle_html = str_replace('{TOG_TITLE'.$i.'}', $tab_title, $toggle_html);
                                $tab_title = '';
                            }
                        }
                    }
                    
                    $toggle_html_start = str_replace('{ID}', "toggle_".$toggle_id, $toggle_html_start);
                }
                
                // Now update the main HTML 
                $html = str_replace($match, $toggle_html_start.$toggle_html.$toggle_html_end, $html);   
            }
            else {
                $html = str_replace($match, "", $html); 
            }
        }
    }
    
    if($toggle_id != '') {
        $html .= '
        <script type="text/javascript">
            $JQuery(document).ready(function() {
                $JQuery(".expandable_box").hide("fast");
    
                $JQuery(".accordion .ui-accordion-contents h4").click(function() {
                    $JQuery(this).next(".expandable_box").slideToggle("fast");
                    
                    if($JQuery(this).parent().hasClass("active")) {
                        $JQuery(this).parent().removeClass("active");
                    }
                    else {
                        $JQuery(this).parent().addClass("active");
                    }
                });';
                    
        foreach($toggle_ids as $id) {
            $html .= '$JQuery("#toggle_'.$id.' .ui-accordion-contents").last().addClass("last");';
        }
                            
        $html .= 'return false;
            });
        </script>'; 
    }
    
    return $html;
}

function youtube_link_handling($html){
    $pattern = '/<a[^>]+href=\"http:\/\/www.youtube.com\/[^\"]+\"[^>]*>.*<\/a>/Ui';
    $result = preg_match_all($pattern, $html, $matches);
    if ($result > 0){
        // found link to youtube
        foreach($matches[0] AS $match){
            $video_link = $match;
            
            if (substr_count($video_link, '/user/') == 0){
                // not a link to youtube user
                $new_video_link = str_replace('href="', 'rel="prettyPhoto" href="', $video_link);
                
                $new_html = $new_video_link;
            
                $html = str_replace($video_link, $new_html, $html);
            }
        }
    }
    return $html;
}

function image_lightbox_handling($html){
    // each image with class="lightbox" needs appropriate lightbox handling
    
    $pattern = '/<a[^>]+>.*<img[^>]+class=\"[^\"]*lightbox[^\"]*\"[^>]+>.*<\/a>/Ui';
    //$pattern = '/<img[^>]+class=\"[^\"]*lightbox[^\"]*\"[^>]+>/Ui';
    $result = preg_match_all($pattern, $html, $matches);
    if ($result > 0){
        foreach($matches[0] AS $match){
            $image_tag = $match;
            $new_image_tag = str_replace('<a', '<a rel="prettyPhoto[pageimagegrouping]"', $image_tag);
            
            $title_pattern = '/title=\"[^\"]*\"/Ui';
            $title_result = preg_match_all($title_pattern, $html, $title_matches);
            if ($title_result > 0){
                $title = str_replace('title="', '', $title_matches[0][0]);
                $title = str_replace('"', '', $title);
                $new_image_tag = str_replace('<a', '<a title="'.$title.'"', $new_image_tag);
            }
            $html = str_replace($image_tag, $new_image_tag, $html);
        }
    }
    return $html;
}

function youtube_embed_scaling($html, $desired_width = null, $desired_height = null){
    $pattern = '/<iframe[^>]+src=\"http:\/\/www.youtube.com\/+[^\"]+\"[^>]*>/Ui';
    $result = preg_match_all($pattern, $html, $matches);
    if ($result > 0){
        foreach($matches[0] AS $match){
            $new_html = content_embed_scaling($match, $desired_width, $desired_height);
            
            $html = str_replace($match, $new_html, $html);
        }
    }
    return $html;
}

function youtube_embed_processing($html){
    // each youtube embed requires some custom handling
    $pattern = '/<iframe[^>]+src=\"http:\/\/www.youtube.com\/+[^\"]+\"+[^>]*>/Ui';
    $result = preg_match_all($pattern, $html, $matches);
    if ($result > 0){
        foreach($matches[0] AS $match){
            $video_iframe = $match;
            
            // add wmode=transparent to video url to prevent issue with video over the top of lightbox when active
            $video_url_pattern = '/\"http:\/\/www.youtube.com\/+[^\"]+\"/Ui';
            $video_url_result = preg_match_all($video_url_pattern, $video_iframe, $video_url_matches);
            if ($video_url_result > 0){
                $video_iframe = $video_url_matches[0][0];
                
                $video_iframe_replace = str_replace('"', '', $video_iframe); // strip quotes from start, end, add later
                if (substr_count($video_iframe_replace, '?') > 0){
                    $video_iframe_replace = $video_iframe_replace.'&amp;wmode=transparent';
                }else{
                    $video_iframe_replace = $video_iframe_replace.'?wmode=transparent';
                }
                $video_iframe_replace = '"'.$video_iframe_replace.'"';
                
                $html = str_replace($video_iframe, $video_iframe_replace, $html);
            }
        }
    }
    return $html;
}

function content_embed_scaling($html, $desired_width = null, $desired_height = null){
    // process for width
    if ($desired_width == ''){
        $desired_width = $GLOBALS['standard_content_width'];
    }
    
    $replacement = '{width}';
    
    $pattern = '/width=\"[^\"]+\"/Ui';
    $result = preg_match_all($pattern, $html, $matches);
    if ($result > 0){
        // html has width set as width="{n}", get numeric value and replace with placeholder
        $string = $matches[0][0];
        $string = str_replace('width="', '', $string);
        $string = str_replace('"', '', $string);
        $width = trim($string);
        
        $html = preg_replace($pattern, 'width="'.$replacement.'"', $html);
    }
    
    $pattern = '/width:[^px]+px;/Ui';
    $result = preg_match_all($pattern, $html, $matches);
    if ($result > 0){
        // html has width set as width:{n}px;, get numeric value and replace with placeholder
        $string = $matches[0][0];
        $string = str_replace('width:', '', $string);
        $string = str_replace('px;', '', $string);
        $width = trim($string);
        
        $html = preg_replace($pattern, 'width:'.$replacement.'px;', $html);
    }
    
    if ($width > $desired_width){
        $new_width = $desired_width;
    }else{
        $new_width = $width;
    }
    
    $html = str_replace($replacement, $new_width, $html);
    
    // process for height
    if ($desired_height == ''){
        $desired_height = $GLOBALS['standard_content_height'];
    }
    
    $replacement = '{height}';
    
    $pattern = '/height=\"[^\"]+\"/Ui';
    $result = preg_match_all($pattern, $html, $matches);
    if ($result > 0){
        // html has width set as height="{n}", get numeric value and replace with placeholder
        $string = $matches[0][0];
        $string = str_replace('height="', '', $string);
        $string = str_replace('"', '', $string);
        $height = trim($string);
        
        $html = preg_replace($pattern, 'height="'.$replacement.'"', $html);
    }
    
    $pattern = '/height:[^px]+px;/Ui';
    $result = preg_match_all($pattern, $html, $matches);
    if ($result > 0){
        // html has width set as height:{n}px;, get numeric value and replace with placeholder
        $string = $matches[0][0];
        $string = str_replace('height:', '', $string);
        $string = str_replace('px;', '', $string);
        $height = trim($string);
        
        $html = preg_replace($pattern, 'height:'.$replacement.'px;', $html);
    }
    
    if ($height > $desired_height){
        $new_height = $desired_height;
    }else{
        $new_height = $height;
    }
    
    $html = str_replace($replacement, $new_height, $html);
    
    return $html;       
}

function embed_video_link($html, $id_only = false){
    if (strpos($html, 'youtube.com/embed/') !== false){
        // look for src="http://www.youtube.com/embed/Mf6JCpJjdiY"
        $pattern = '/http:\/\/www.youtube.com\/embed\/[^&"\/]+?/Ui';
        $result = preg_match_all($pattern, $html, $matches);
        if ($result > 0){
            // html has width set as height:{n}px;, get numeric value and replace with placeholder
            $string = $matches[0][0];
            $string = str_replace('http://www.youtube.com/embed/', '', $string);
            $string = str_replace('http://youtube.com/embed/', '', $string);
            $string = str_replace('youtube.com/embed/', '', $string);
            $string = str_replace('"', '', $string);
            
            $link = 'http://www.youtube.com/watch?v='.$string;
        }
    }elseif (strpos($html, 'youtube.com/watch') !== false){
        // look for src="http://www.youtube.com/watch?v=Mf6JCpJjdiY&xxx"
        $pattern = '/youtube.com\/watch\?v=[^&"]+?/Ui';
        $result = preg_match_all($pattern, $html, $matches);
        if ($result > 0){
            // html has width set as height:{n}px;, get numeric value and replace with placeholder
            $string = $matches[0][0];
            $string = str_replace('http://www.youtube.com/watch?v=', '', $string);
            $string = str_replace('http://youtube.com/watch?v=', '', $string);
            $string = str_replace('youtube.com/watch?v=', '', $string);
            $string = str_replace('"', '', $string);
            
            $link = 'http://www.youtube.com/watch?v='.$string;
        }
    }elseif(strpos($html, 'youtu.be') !== false){
        if(strpos($html, 'youtu.be/embed') !== false){
            // look for "http://youtu.be/embed/2h-dUWWtdAE"
            $pattern = '/youtu.be\/embed\/[^&"\/]+?/Ui';
            $result = preg_match_all($pattern, $html, $matches);
            if ($result > 0){
                // html has width set as height:{n}px;, get numeric value and replace with placeholder
                $string = $matches[0][0];
                $string = str_replace('http://www.youtu.be/embed/', '', $string);
                $string = str_replace('http://youtu.be/embed/', '', $string);
                $string = str_replace('youtu.be/embed/', '', $string);
                $string = str_replace('"', '', $string);
                
                $link = 'http://www.youtube.com/watch?v='.$string;
            }
        }else{
            // look for "http://youtu.be/2h-dUWWtdAE"
            $pattern = '/youtu.be\/[^&"\/]+?/Ui';
            $result = preg_match_all($pattern, $html, $matches);
            if ($result > 0){
                // html has width set as height:{n}px;, get numeric value and replace with placeholder
                $string = $matches[0][0];
                $string = str_replace('http://www.youtu.be/', '', $string);
                $string = str_replace('http://youtu.be/', '', $string);
                $string = str_replace('youtu.be/', '', $string);
                $string = str_replace('"', '', $string);
                
                $link = 'http://www.youtube.com/watch?v='.$string;
            }
        }
    }
    if ($id_only){
        return $string;
    }

    return $link;
}

function PageEmbedSets($type, $id, $position, $title = null, $link = null, $site = null){
    if ($type == 'pages'){
        $query = sprintf("SELECT embed_sets.* FROM embed_sets_xref JOIN embed_sets ON embed_sets.id = embed_sets_xref.embed_id WHERE embed_sets_xref.object_id = '%s' AND embed_sets_xref.type = '%s' AND embed_sets.position1 = '%s' AND embed_sets.live = 1 AND embed_sets.deleted = 0 AND embed_sets_xref.deleted = 0",
        mysql_real_escape_string($id),
        mysql_real_escape_string($type),
        mysql_real_escape_string($position));
    }elseif ($type == 'microsite_pages'){
        $type = 'pages';
        $query = sprintf("SELECT embed_sets.* FROM microsite_embed_sets_xref JOIN embed_sets ON embed_sets.id = microsite_embed_sets_xref.embed_id WHERE microsite_embed_sets_xref.object_id = '%s' AND microsite_embed_sets_xref.type = '%s' AND embed_sets.position1 = '%s' AND embed_sets.live = 1 AND embed_sets.deleted = 0 AND microsite_embed_sets_xref.deleted = 0",
        mysql_real_escape_string($id),
        mysql_real_escape_string($type),
        mysql_real_escape_string($position));
    }else{
        return;
    }
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        $row = mysql_fetch_array($result);
        $html = '<div class="embed_'.$row['position1'].' embed_'.$row['position3'].'_'.$row['position2'].'">'.$row['code'].'</div>';
        
        if ($title != ''){
            $html = str_replace('{title}', $title, $html);
        }else{
            $html = str_replace('{title}', $pagedetails['name'], $html);
        }
        if ($link != ''){
            $html = str_replace('{link}', $link, $html);
        }else{
            $html = str_replace('{link}', 'http://'.$GLOBALS['site_url'].$_SERVER['REQUEST_URI'], $html);
        }
        if ($site != ''){
            $html = str_replace('{site}', $site, $html);
        }else{
            $html = str_replace('{site}', $pagedetails['metadata']['title'], $html);
        }
        return $html;
    }
}

function PageComments($type, $id){
    global $microsite_id;
    
    if ($GLOBALS['page_comments'] == 'yes'){
        switch ($type) {
            case 'pages':
                $table = 'pageindex';
            break;
            case 'microsite_pages':
                $table = 'microsite_pageindex';
            break;

            default:
                return;
        }
        $query = sprintf("SELECT comments FROM `%s` WHERE id = '%s' AND comments = 1",
        mysql_real_escape_string($table),
        mysql_real_escape_string($id));
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            // this page has comments
            include($GLOBALS['docRoot'].'/lib/_comments.php');
            return EchoComments($type, $id);
        }
    }
}

    function convert_date($day, $month, $year) {
        if($day < 10) { $day = "0" . $day; }
        if($month < 10) { $month = "0" . $month; }
        return $year . "-" . $month . "-" . $day;
    }
    
    function convert_date_back($date) {
        list($Year,$Month,$Day) = explode('-',$date);
        $date = array(
            'day' => $Day,
            'month' => $Month,
            'year' => $Year
        );
        return $date;
    }

    function output_date($date, $seperator) {
        list($Year,$Month,$Day) = explode('-',$date);
        return $Day . $seperator . $Month . $seperator . $Year;
    }

    function getLastDayOfMonth($month, $year) {
        return idate('d', mktime(0, 0, 0, ($month + 1), 0, $year));
    }

    function paginateFE($limit, $sql, $current_page, &$totalresults, &$navhtml) {
        // Paginate function for the front-end
        if ( $result = mysql_query( $sql ) ) {
            $num = mysql_num_rows( $result );
            $totalresults = $num;
            mysql_free_result( $result );
        } else {
            $num = 0;
        }
        // Retrieve current page from GET
        if ( isset( $_GET['pg'] ) ) {
            $pg = $_GET['pg'];
        } else $pg = 1;

        $html = '';
        $current = $pg;
        $numpg = ceil($num / $limit);
        $prev = ($pg != 1) ? $current - 1 : "none";
        $next = ($pg != $numpg) ? $current + 1 : "none";
        $limitvalue = ($pg * $limit) - $limit;
        
        if ( $numpg > 1 ) {
            if ( $pg == 1 ) { 
                $html .= "<p align=\"center\">&laquo; Prev | "; 
            } else { 
                $html .= "<p align=\"center\"><a href=\"" . $current_page . "pg=$prev\">&laquo; Prev</a> | "; 
            }
            if ($pg == "" || $pg == 1 || $pg == 2 || $pg == 3 || $pg == 4 || $pg == 5) {
                for($p = 1; $p <= 10 && $p <= $numpg; $p++) {
                    if($p == $pg) { $html .= "<strong>$p</strong> | "; } 
                        else $html .= "<a href=\"" . $current_page . "pg=$p\">$p</a> | ";
                }
            } else {                    
                for($p = ($pg - 5); $p <= $numpg && $p < ($pg + 5); $p++) {
                    if ( $p == $pg ) { $html .= "<strong>$p</strong> | "; } 
                        else $html .= "<a href=\"" . $current_page . "pg=$p\">$p</a> | ";
                }
            }
            if ( $pg == $numpg ) { $html .= " Next &raquo;</p>"; } 
                else $html .= "<a href=\"" . $current_page . "pg=$next\"> Next &raquo;</a></p>"; 
        }
        $getpage = $sql . " LIMIT $limitvalue, $limit";
        $navhtml = $html;
        echo $html;
        return mysql_query( $getpage );
    }
    
function link_lightbox_handling($html){
    // each link with class="lightbox" needs appropriate lightbox handling
    
    $pattern = '/<a[^>]+class=\"[^\"]*lightbox[^\"]*\"[^>]+>.*<\/a>/Ui';
    $result = preg_match_all($pattern, $html, $matches);
    if ($result > 0){
        foreach($matches[0] AS $match){
            $link_tag = $match;
            $new_link_tag = str_replace('<a', '<a rel="prettyPhoto[]"', $link_tag);
            
            $html = str_replace($link_tag, $new_link_tag, $html);
        }
    }
    return $html;
}

function strip_slashes($text){
    if (get_magic_quotes_gpc()) {
        $text = stripslashes($text);
    }
    return $text;
}

function convert_smart_quotes($text){
    // function cleans input for unusual characters, usually from copy+paste from Word editor
    $text = str_replace(chr(145), "'", $text); // naughty curly apostrophe to normal
    $text = str_replace(chr(146), "'", $text); // naughty curly apostrophe to normal
    $text = str_replace(chr(147), '"', $text); // naughty curly quote to normal
    $text = str_replace(chr(148), '"', $text); // naughty curly quote to normal
    $text = str_replace(chr(151), "-", $text); // naughty emdash to hyphen
    $text = str_replace("–", "-", $text); // naughty emdash to hyphen
    
    return $text;
}

function html_clean($text){
    /*
    // convert ampersand
    $text = str_replace('&', '&amp;',$text);
    // convert pounds
    $text = str_replace('£', '&pound;',$text);
    $text = str_replace('$', '&#36;',$text);
    // convert quotes
    $text = str_replace('"', '&quot;',$text);
    // convert pointy brackets
    $text = str_replace('<', '&lt;',$text);
    $text = str_replace('>', '&gt;',$text);
    $text = str_replace('–', '-',$text);
    $text = str_replace('Å', 'A',$text);
    
    $text = convert_smart_quotes($text);
    */
    $text = htmlspecialchars($text);
    return $text;
}

function html_link_video_embed_clean($text){
    $text = str_replace('&', '&amp;',$text);
    
    return $text;
}

function no_strip_html_tag_clean($text){
    $text = str_replace('&', '&amp;',$text);
    $text = str_replace('–', '&#8211;',$text);
    $text = str_replace(chr(151), "-", $text); // naughty emdash to hyphen
    $text = str_replace(chr(2212), "-", $text);
    
    return $text;
}

function firstItemNoMargin($body){
    $pattern = '/^[\s]*<[^>]*>/'; // find first tag in body content, so we can add no margin top class to it
    if (preg_match($pattern, $body, $matches)){
        $match = $matches[0];
        
        if (substr_count($match, 'class="') > 0){
            $replacement = str_replace('class="', 'class="no_top_margin ', $match);
        }else{
            $replacement = str_replace('>', ' class="no_top_margin">', $match);
        }
        $body = preg_replace($pattern, $replacement, $body);
    }
    return $body;
}


function url_to_link($text){
    // This could do with improving, use regex; faster, neater
    
    // goes through block of text, builds arrays by breaking at spaces " " and line breaks "<br>" or "<br />"
    // turns http://www.something.com, https://www.something.com and www.something.com into clickable links
    // rebuilds text block
    if ((substr_count($text, 'http://') > 0) OR (substr_count($text, 'https://') > 0) OR (substr_count($text, 'www.') > 0)){
        $text_array = explode(" ", $text);
        while ($word = current($text_array)){
            if ((substr($word, 0, 7) == 'http://') OR (substr($word, 0, 8) == 'https://')){
                $word = '<a href="'.$word.'">'.$word.'</a>';
            }elseif (substr($word, 0, 4) == 'www.'){
                $word = '<a href="http://'.$word.'">'.$word.'</a>';
            }
            $text_string .= $word.' ';
            next($text_array);
        }
        $text = $text_string;
        unset($text_string);
        $text_array = explode($GLOBALS['break_type'], $text); // break at line breaks, from global break type: <br> or <br />
        while ($word = current($text_array)){
            if ((substr($word, 0, 7) == 'http://') OR (substr($word, 0, 8) == 'https://')){
                $word = '<a href="'.$word.'">'.$word.'</a>';
            }elseif (substr($word, 0, 4) == 'www.'){
                $word = '<a href="http://'.$word.'">'.$word.'</a>';
            }
            $text_string .= $word.$GLOBALS['break_type'];
            next($text_array);
        }
        $text = $text_string;
    }
    return $text;
}

function form_input_filter($text, $string_length = null, $allowed_tags = null, $strip_html = true){
    $text = strip_slashes($text);
    if ($strip_html){
        if ($allowed_tags === null){
            $text = strip_tags($text); // remove html tags
        }else{
            $text = strip_tags($text, $allowed_tags); // remove html tags
        }
    }
    $text = trim($text); // remove white space
    if (($string_length !== null) AND (is_numeric($string_length)) AND ($string_length > 0)){
        $text = substr($text,0,$string_length); // shorten string to string_length characters
    }
    $text = convert_smart_quotes($text);
    return $text;
}

function output_filter($text, $newline = true, $url_to_link = false, $strip_html = true, $string_length = null){
    if ($strip_html){
        // remove html, then clean
        $text = strip_tags($text);
        $text = html_clean($text);
    }else{
        $text = no_strip_html_tag_clean($text);
    }
    
    if ($string_length !== null){
        $new_text = substr($text,0,$string_length); // shorten string to string_length characters
        if (strlen($text) > $string_length){
            $new_text .= '...';
        }
        $text = $new_text;
    }
    
    if ($newline){
        $text = str_replace("\n", $GLOBALS['break_type'], $text);
        if ($GLOBALS['break_type'] == '<br>'){
            $text = str_replace("<br />", "<br>", $text);
        }else{
            $text = str_replace("<br>", "<br />", $text);
        }
    }
    
    if ($url_to_link){
        $text = url_to_link($text);
    }
    return $text;
}

function datetime_to_human($datetime){
    return date("j M \a\\t H:i",strtotime($datetime));
}
function email_obfuscation($mail) {
    if (substr_count($mail, 'mailto:') == 0){
        // object is directly entered email address
        $link = 'document.write(\'<a href="mailto:' . $mail . '">' . $mail . '</a>\');';
    }else{
        // object is already a mail link
        $link = 'document.write(\'' . str_replace("'", "\'", $mail) . '\');';
    }
    $js_encode = '';
    for ($x = 0; $x < strlen($link); $x++) {
        $js_encode .= '%' . bin2hex($link{$x});
    }

    $link = '<span><script type="text/javascript">eval(unescape(\''.$js_encode.'\'));</script>';
    $link .= '<object><noscript>You need JavaScript enabled to see this email address.</noscript></object></span>';
    return $link;
}

function find_clean_emails($text) {
    if (strpos($text, '@') === FALSE)
        return $text;
    // Split at <a> and </a> so that we can avoid encoding addresses in link text.
    $t = preg_replace(":(</?a):i", "\001\\1", strtr($text, "\r\n", "\002\003"));
    $a = explode("\001", $t);
    $n = count($a);

    for ($i = 0; $i < $n; ++$i) {
        if (preg_match('/^(<a[^>]*)mailto:([^@]+@[-.a-z0-9]+)(.*)/i', $a[$i], $m)) {
            // object is already a mail link
            
            //$a[$i] = email_obfuscation($m[2]);
            
            $a[$i] = email_obfuscation($m[0].'</a>');
            
            $a[1+$i] = substr($a[1+$i],4);
        }else if (!preg_match('/^(.*)(<input[^>]+)[-.a-z0-9]+@[-.a-z0-9]+.*\/>/i',$a[$i])) {
            // object is directly entered email address
            while (preg_match('/(.*)[^-.a-z0-9]([-.a-z0-9]+)(@[-.a-z0-9]+)(.*)$/i', $a[$i], $m)) {
                $a[$i] = $m[1] . email_obfuscation($m[2].$m[3]) .  $m[4];
            }
        }
    }
    return strtr(implode('', $a), "\002\003", "\r\n");
}

function page_content_prep($body, $microsite_id = null){
    
    //$body = find_clean_emails($body); // convert email links to Javascript obsfucated links
                    
    $body = image_prep($body); // apply appropriate floating, padding to images in text, handle captions
    
    $body = document_google_analytics($body);
    
    $body = youtube_embed_processing($body); // each youtube embed requires some custom handling
    
    $body = youtube_link_handling($body); // each youtube link requires some custom handling
    
    $body = image_lightbox_handling($body); // each image with class="lightbox" needs appropriate lightbox handling
    
    return $body;
}

function document_google_analytics($body){
    $google_analytics_code = download_google_analytics();
    
    if (count($GLOBALS['documenttypes']) > 0){
        // we have document types
        foreach ($GLOBALS['documenttypes'] AS $document_file_type){
            $document_pattern .= $document_file_type.'|';
        }
        $document_pattern = substr($document_pattern, 0, -1);
        
        $pattern = '/href="+(.*?)[^"]*\.('.$document_pattern.')"/s';
        
        $result = preg_match_all($pattern, $body, $matches);
        if ($result){
            foreach($matches[0] AS $match){
                $link = $match;
                
                $link = str_replace('href="', '', $link);
                
                $link = str_replace('"', '', $link);
                
                $this_code = str_replace('{link}', $link, $google_analytics_code);
    
                $body = str_replace('href="'.$link.'"', 'href="'.$link.'" '.$this_code, $body);
            }
        }
    }
    return $body;
}

function trackEvent_google_analytics($action, $label, $value){
    // onClick="_gaq.push(['_trackEvent', 'Membership', 'Payment', 'Credit/debit card payment']);"
    if ($GLOBALS['google-analytics-codes']){
        $google_analytics_code = 'onClick="javascript: _gaq.push(';
        $alpha = range('a', 'z');
        $i = 0;
        foreach($GLOBALS['google-analytics-codes'] AS $code){
            $google_analytics_code .= ' [\''.$alpha[$i].'._trackEvent\', \''.$action.'\', \''.$label.'\', \''.$value.'\']';
            if (count($GLOBALS['google-analytics-codes']) > $i+1){
                $google_analytics_code .= ',';
            }
            $i++;
        }
        $google_analytics_code .= ');"';
    }
    return $google_analytics_code;
}


function download_google_analytics(){
    if ($GLOBALS['google-analytics-codes']){
        $google_analytics_code = 'onClick="javascript: _gaq.push(';
        $alpha = range('a', 'z');
        $i = 0;
        foreach($GLOBALS['google-analytics-codes'] AS $code){
            $google_analytics_code .= ' [\''.$alpha[$i].'._trackPageview\', \'{link}\']';
            if (count($GLOBALS['google-analytics-codes']) > $i+1){
                $google_analytics_code .= ',';
            }
            $i++;
        }
        $google_analytics_code .= ');"';
    }
    return $google_analytics_code;
}

function form_google_analytics_linkByPost(){
    if ($GLOBALS['google-analytics-codes']){
        $google_analytics_code = 'onsubmit="_gaq.push(';
        
        $alpha = range('a', 'z');
        $i = 0;
        foreach($GLOBALS['google-analytics-codes'] AS $code){
            $google_analytics_code .= ' [\''.$alpha[$i].'._linkByPost\', this]';
            if (count($GLOBALS['google-analytics-codes']) > $i+1){
                $google_analytics_code .= ',';
            }
            $i++;
        }
        $google_analytics_code .= ');"';
    }
    return $google_analytics_code;
}

function createDateDropdowns( $class = '', $name = 'date', $currentvalue = '', $yearupper = 120, $yearlower = 120, $optional = true, $monthnames = true, $widths = array( 50, 80, 60 ) ) {
    // Create date dropdowns for easy date selection
    
    $months = array(1=>'January', 'February', 'March', 'April', 'May', 'June', 'July',
                    'August', 'September', 'October', 'November', 'December');
    if ( $currentvalue == '' && !$optional ) $currentvalue = date('Y-m-d');
    $currentyear = date('Y');
    if ( $currentvalue != '' ) {
        list($year, $month, $day) = explode( '-', $currentvalue );
        if ( $currentyear + $yearupper < $year ) $year = $currentyear + $yearupper;
    }
    $html='';
    
    // Create the day dropdown
    $html .= "<select class=\"$class\"";
    $html .= ' style="width:' . $widths[0] . 'px;"';
    $html .= ' id="' . $name . 'day"';
    $html .= ' name="' . $name . 'day"';
    $html .= ' >';
    if ( $optional ) {
        $html .= '<option value="0"';
        if ( $currentvalue == '' ) $html .= ' selected="selected"';
        $html .='>-</option>';
    }
    for ( $i=1; $i<32; $i++ ) {
        $html .= '<option value="' . $i . '"';
        if ( $i == $day ) $html .= ' selected="selected"';
        $html .= '>' . $i . '</option>';
    }
    $html .= '</select>';
    
    // Create the month dropdown
    $html .= "<select class=\"$class\"";
    $html .= ' style="width:' . $widths[1] . 'px;"';
    $html .= ' id="' . $name . 'month"';
    $html .= ' name="' . $name . 'month"';
    $html .= ' >';
    if ( $optional ) {
        $html .= '<option value="0"';
        if ( $currentvalue == '' ) $html .= ' selected="selected"';
        $html .='>-</option>';
    }
    if ( $monthnames ) {
        // Show month names
        for ( $i=1; $i<13; $i++ ) {
            $html .= '<option value="' . $i . '"';
            if ( $i == $month ) $html .= ' selected="selected"';
            $html .= '>' . $months[$i] . '</option>';
        }
        
    } else {
        // Show month numbers
        for ( $i=1; $i<13; $i++ ) {
            $html .= '<option value="' . $i . '"';
            if ( $i == $month ) $html .= ' selected="selected"';
            $html .= '>' . $i . '</option>';
        }
    }
    $html .= '</select>';
    
    // Create the year dropdown
    $html .= "<select class=\"$class\"";
    $html .= ' style="width:' . $widths[2] . 'px;"';
    $html .= ' id="' . $name . 'year"';
    $html .= ' name="' . $name . 'year"';
    $html .= ' >';
    if ( $optional ) {
        $html .= '<option value="0"';
        if ( $currentvalue == '' ) $html .= ' selected="selected"';
        $html .='>-</option>';
    }
    for ( $i = ( $currentyear - $yearlower ); $i <= ( $currentyear + $yearupper ); $i++ ) {
        $html .= '<option value="' . $i . '"';
        if ( $i == $year ) $html .= ' selected="selected"';
        $html .= '>' . $i . '</option>';
    }
    $html .= '</select>';
    return $html;
}

function readDateDropdowns( $name = 'date' ) {
    // Reads and returns the date from the date dropdowns
    $day = $_POST[$name .'day'];
    if ( $day < 1 ) return '';
    $month = $_POST[$name .'month'];
    if ( $month < 1 ) return '';
    $year = $_POST[$name .'year'];
    if ( $year < 1 ) return '';
    return "$year-$month-$day";
}

function mySQLDateToUK( $date, $monthname = true ) {
    $months = array(1=>'January', 'February', 'March', 'April', 'May', 'June', 'July',
                    'August', 'September', 'October', 'November', 'December');
    list( $year, $month, $day ) = explode( '-', $date );
    if ( $monthname ) return $day . ' ' . $months[(int)$month] . ' ' . $year;
    return $day . ' ' . $month . ' ' . $year;
}

function strtoproper($text){
    $text_array = explode(" ", $text);
    while ($word = current($text_array)){
        $string .= strtoupper(substr($word, 0, 1)).strtolower(substr($word, 1));
        next($text_array);
    }
    return $string;
}


function return_decimal($num, $points, $db = false){
    $num = str_replace(',','', $num);
    
    if($db === true) {
        return number_format($num, $points, '.', '');
    }
    else {
        return number_format($num, $points, '.', ',');
    }
}

function https_redirect(){
    global $secure_site_url;
    if ((strtolower($_SERVER['https']) === 'on') || ($_SERVER['SERVER_PORT'] === '443')){
        // running https
    }else{
        if ($_SERVER['HTTP_HOST'] == 'localhost'){
            // operating on localhost, testing environment
        }elseif($GLOBALS['https_enabled']){
            // running http and https is available, forward to https page
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $secure_site_url.":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $secure_site_url.$_SERVER["REQUEST_URI"];
            }
            
            header("Location: $pageURL");
            exit;
        }
    }
}

function http_redirect(){
    global $site_url;
    if ((strtolower($_SERVER['https']) === 'on') || ($_SERVER['SERVER_PORT'] === '443')){
        header("Location: http://$site_url" . $_SERVER["REQUEST_URI"]);
        exit;
    }
}

function https_link_switch($url, $force = false){
    // first of all move aliases of this site, as not needed
    $url = alias_url_strip($url);
    
    if (strpos($url, 'http://') !== false) {
        // we have http:// in url
        if ((strtolower($_SERVER['https']) === 'on') || ($_SERVER['SERVER_PORT'] === '443') || $force){
            // running https, need https:// resource source
            $url = str_replace('http://', 'https://', $url);    
        }
    }
    
    return $url;
}

function ampLinkClean($link){
    $link = str_replace('&', '&amp;', $link);
    $link = str_replace('&amp;amp;', '&amp;', $link);
    return $link;
}

function returnIPaddress(){
    return $_SERVER['REMOTE_ADDR'];
}

function lat_long_distance($lat1, $lon1, $lat2, $lon2, $unit) { 
    $theta = $lon1 - $lon2; 
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)); 
    $dist = acos($dist); 
    $dist = rad2deg($dist); 
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return ($miles * 1.609344); 
    }elseif ($unit == "N") {
        return ($miles * 0.8684);
    }else {
        return $miles;
    }
}

function get_rss_items_as_array($url){
    // retrieve xml)
    if($xml = simplexml_load_file($url, 'SimpleXMLElement', LIBXML_NOCDATA)) {
    //var_dump($xml);
    echo '<br /><br /><br /><br /><br />';
        return object2array($xml);
    } else {
        return false;
    }
}

function object2array($object){
    $return = NULL;
    
    if(is_array($object)){
        foreach($object as $key => $value)
            $return[$key] = object2array($value);
    }else{
        $var = get_object_vars($object);
           
        if($var){
            foreach($var as $key => $value)
                $return[$key] = ($key && !$value) ? NULL : object2array($value);
        }
        else return $object;
    }
    return $return;
}

function get_tickbox_announcements_rss_items_as_array($url, $age = 900){
    if (trim($url) != ''){
        $data = data_from_DB_cache($url, $age);
        
        // retrieve xml
        if($xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA)) {
            $result["heading"]   = $xml->xpath("/rss/heading");
            $result["title"]    = $xml->xpath("/rss/channel/article/title");
            $result["type"] = $xml->xpath("/rss/channel/article/type");
            $result["link"] = $xml->xpath("/rss/channel/article/link");
            $result["content"] = $xml->xpath("/rss/channel/article/content");
            $result["date"] = $xml->xpath("/rss/channel/article/date");
            
            foreach($result as $key => $attribute) {
                $i = 0;
                foreach($attribute as $element){
                    $ret[$i][$key] = (string)$element;
                    $i++;
                }
            }
            return $ret;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

function get_wordpress_rss_items_as_array($url, $age = 900){
    if (trim($url) != ''){
        $data = data_from_DB_cache($url, $age);
        
        // retrieve xml
        if($xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA)) {
            $result["title"]   = $xml->xpath("/rss/channel/item/title");
            $result["link"] = $xml->xpath("/rss/channel/item/link");
            $result["category"] = $xml->xpath("/rss/channel/item/category");
            $result["description"] = $xml->xpath("/rss/channel/item/description");
            $result["content"] = $xml->xpath("/rss/channel/item/content:encoded/text()");
            $result["author"] = $xml->xpath("/rss/channel/item/dc:creator");
            $result["author_link"] = $xml->xpath("/rss/channel/item/dc:creatorLink");
            $result["author_photo"] = $xml->xpath("/rss/channel/item/dc:creatorPhoto");
            $result["pubDate"] = $xml->xpath("/rss/channel/item/pubDate");
            
            foreach($result as $key => $attribute) {
                $i = 0;
                foreach($attribute as $element){
                    $element = str_replace(chr(226).chr(128).chr(152),"'",$element); // â€˜ --> curly single quote left
                    $element = str_replace(chr(226).chr(128).chr(153),"'",$element); // â€™ --> curly single quote right
                    $element = str_replace(chr(226).chr(128).chr(147), "-", $element); // â€“ --> -
                    $element = str_replace(chr(226).chr(128).chr(156),'"',$element); // â€œ --> curly double quote left
                    $element = str_replace(chr(226).chr(128).chr(157),'"',$element); // â€ --> curly double quote right
                    $element = str_replace(chr(226).chr(128).chr(162),"&#8226;",$element); // â€¢ --> (bullet)
                    $element = str_replace(chr(226).chr(128).chr(166), "&#8230;", $element); // â€¦ --> -
                    $element = str_replace(chr(194), "", $element); // Â --> (nothing)
                    $element = str_replace(chr(195).chr(169), "&eacute;", $element); // Ã© --> é
                    
                    if(substr_count($element,chr(226)) > 0){
                        //echo 'here:'.byteValueToASCII($element);
                    }
                    
                    $element = str_replace('IFRAME Embed for Youtube', '', $element);
                    
                    if (($key == 'content') OR ($key == 'description')){
                        // pattern:<a rel="nofollow" href="http://feeds.wordpress.com/1.0/gocomments/sbsproject.wordpress.com/1143/"><img alt="" border="0" src="http://feeds.wordpress.com/1.0/comments/sbsproject.wordpress.com/1143/" /></a>
                        $patterns[0] = '#<a rel="nofollow" href="[^"]*"><img alt="" border="0" src="http://feeds.wordpress.com/[^"]*" /></a>#i';
                        $patterns[1] = '#<img alt="" border="0" src="http://stats.wordpress.com/[^"]*" width="1" height="1" />#i';
                        $patterns[2] = '#<p class="wp-caption-text">[^</p>]*</p>#i'; // remove captions
                        
                        $replacements[0] = '';
                        $replacements[1] = '';
                        $replacements[2] = '';
                        $element = preg_replace($patterns, $replacements, $element);
                        
                        $element = str_replace('<p>&nbsp;</p>', '', $element); // remove empty line paddings
                        $element = str_replace('&#160;', '', $element); // remove nbsp
                        
                        $element = trim($element);
                    }
                    
                    if ($key == 'content'){
                        // see if we can find image in post
                        $image_result = preg_match_all('#<img[^>]*>#i', (string)$element, $matches);
            
                        if ($image_result > 0){
                            // post has an image, find image src
                            $string = $matches[0][0];
                            
                            $image_source_result = preg_match_all('# src="[^"]*"#i', $string, $source_matches);
                            if ($image_source_result > 0){
                                $image_source = $source_matches[0][0];
                                $image_source = str_replace(' src="', '', $image_source);
                                $image_source = str_replace('"', '', $image_source);
                                $ret[$i]['image'] = $image_source;
                            }
                        }
                        //byteValueToASCII($element);
                    }
                    $ret[$i][$key] = (string)$element;
                    $i++;
                }
            }
            
            foreach ($ret AS $article){
                // strip out empty articles
                if ($article['title'] != ''){
                    $article_array[] = $article;
                }
            }
            return $article_array;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

function get_youtube_rss_items_as_array($url, $age = 900){
    if (trim($url) != ''){
        $data = data_from_DB_cache($url, $age);
        
        // retrieve xml
        if($xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA)) {
            $result["title"]   = $xml->xpath("/rss/channel/item/title");
            $result["link"] = $xml->xpath("/rss/channel/item/link");
            $result["pubDate"] = $xml->xpath("/rss/channel/item/pubDate");
            
            foreach($result as $key => $attribute) {
                $i = 0;
                foreach($attribute as $element){
                    if ($key == 'link'){
                        // work out thumbnail for video
                        $image_result = preg_match_all('/v=[^&]*/i', (string)$element, $matches);
                        if ($image_result > 0){
                            // post has an image, find image src
                            $image_source = $matches[0][0];
                            $image_source = str_replace('v=', '', $image_source);
                            $image_source = str_replace('&', '', $image_source);
                            
                            //$ret[$i]['image'] = 'http://i.ytimg.com/vi/'.$image_source.'/default.jpg'; // small thumb
                            
                            $ret[$i]['image'] = 'http://i3.ytimg.com/vi/'.$image_source.'/0.jpg';
                        }
                        //byteValueToASCII($element);
                    }
                    $ret[$i][$key] = (string)$element;
                    $i++;
                }
            }
            return $ret;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

/*
// defunct, caching version above used instead
function get_wordpress_rss_items_as_array($url) {
    if (trim($url) != ''){
        // retrieve xml
        if($xml = simplexml_load_file($url, 'SimpleXMLElement', LIBXML_NOCDATA)) {
            $result["title"]   = $xml->xpath("/rss/channel/item/title");
            $result["link"] = $xml->xpath("/rss/channel/item/link");
            $result["description"] = $xml->xpath("/rss/channel/item/description");
            $result["content"] = $xml->xpath("/rss/channel/item/content:encoded/text()");
            $result["author"] = $xml->xpath("/rss/channel/item/dc:creator");
            $result["pubDate"] = $xml->xpath("/rss/channel/item/pubDate");
    
            foreach($result as $key => $attribute) {
                $i = 0;
                foreach($attribute as $element){
                    $element = str_replace(chr(226).chr(128).chr(152),"&#8216;",$element); // â€˜ --> curly single quote left
                    $element = str_replace(chr(226).chr(128).chr(153),"&#8217;",$element); // â€™ --> curly single quote right
                    $element = str_replace(chr(226).chr(128).chr(147), "-", $element); // â€“ --> -
                    $element = str_replace(chr(226).chr(128).chr(156),"&#8220;",$element); // â€œ --> curly double quote left
                    $element = str_replace(chr(226).chr(128).chr(157),"&#8221;",$element); // â€ --> curly double quote right
                    $element = str_replace(chr(226).chr(128).chr(162),"&#8226;",$element); // â€¢ --> (bullet)
                    $element = str_replace(chr(226).chr(128).chr(166), "&#8230;", $element); // â€¦ --> -
                    if(substr_count($element,chr(226)) > 0){
                        //echo 'here:'.byteValueToASCII($element);
                    }
                    
                    if ($key == 'content'){
                        // see if we can find image in post
                        $image_result = preg_match_all('#<img[^>]*>#i', (string)$element, $matches);
        
                        if ($result > 0){
                            // post has an image, find image src
                            $string = $matches[0][0];
                            
                            $image_source_result = preg_match_all('# src="[^"]*"#i', $string, $source_matches);
                            if ($image_source_result > 0){
                                $image_source = $source_matches[0][0];
                                $image_source = str_replace(' src="', '', $image_source);
                                $image_source = str_replace('"', '', $image_source);
                                $ret[$i]['image'] = $image_source;
                            }
                        }
                        //byteValueToASCII($element);
                    }
                    $ret[$i][$key] = (string)$element;
                    $i++;
                }
            }
            return $ret;
        } else{
            return false;
        }
    }else{
        return false;
    }
}
*/

function data_from_DB_cache($url, $age = 900){
    if (trim($url) != ''){
        $query = sprintf("SELECT data FROM rss_cache WHERE url = '%s' AND datetime > '%s'",
        mysql_real_escape_string($url),
        mysql_real_escape_string(date("Y-m-d H:i:s", time()-$age))); // updated within last n seconds
        //mysql_real_escape_string(date("Y-m-d H:i:s", time()))); // now
        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0){
            $row = mysql_fetch_array($result);
            $data = $row['data'];
        }else{
            // not new, or doesnt exist
            if ($data = file_get_contents($url)){
                $query = sprintf("SELECT data FROM rss_cache WHERE url = '%s'",
                mysql_real_escape_string($url));
                $result = mysql_query($query) or die(mysql_error());
                if (mysql_num_rows($result) > 0){
                    // updating old data
                    $query = sprintf("UPDATE rss_cache SET data = '%s', datetime = '%s' WHERE url = '%s'",
                    mysql_real_escape_string($data),
                    mysql_real_escape_string(date("Y-m-d H:i:s")),
                    mysql_real_escape_string($url));
                    mysql_query($query) or die(mysql_error());
                }else{
                    // inserting new data
                    $query = sprintf("INSERT INTO rss_cache (url, data, datetime) VALUES ('%s', '%s', '%s')",
                    mysql_real_escape_string($url),
                    mysql_real_escape_string($data),
                    mysql_real_escape_string(date("Y-m-d H:i:s")));
                    mysql_query($query) or die(mysql_error());
                }
            }else{
                // couldn't get data from source, get from database
                $query = sprintf("SELECT data FROM rss_cache WHERE url = '%s'",
                mysql_real_escape_string($url));
                $result = mysql_query($query) or die(mysql_error());
                if (mysql_num_rows($result) > 0){
                    $row = mysql_fetch_array($result);
                    $data = $row['data'];
                }
            }
        }
        return $data;
    }
}

/**
* Formats a timestamp nicely with an adaptive "x units of time ago" message.
* Based on the original Twitter JavaScript badge. Only handles past dates.
* @return string Nicely-formatted message for the timestamp.
* @param $time Output of strtotime() on your choice of timestamp.
*/

function niceTime($time) {
    $delta = time() - $time;
    if ($delta < 60) {
        return 'less than a minute ago';
    } else if ($delta < 120) {
        return 'about a minute ago';
    } else if ($delta < (45 * 60)) {
        return floor($delta / 60) . ' minutes ago';
    } else if ($delta < (90 * 60)) {
        return 'about an hour ago';
    } else if ($delta < (24 * 60 * 60)) {
        return 'about ' . floor($delta / 3600) . ' hours ago';
    } else if ($delta < (48 * 60 * 60)) {
        return '1 day ago';
    } else {
        return floor($delta / 86400) . ' days ago';
    }
}

function alias_url_strip($url){
    foreach ($GLOBALS['site_aliases'] AS $site_alias){
        if (strpos($url, $site_alias) !== false){
            foreach($GLOBALS['protected_aliases'] AS $key => $protected_aliases){
                $url = str_replace($protected_aliases, '{protect'.$key.'}', $url); // protect protected urls
            }
            // remove domain part from 'from' link
            $url = str_replace('@'.$site_alias, '@{domain}', $url); // protect email addresses
            $url = str_replace('http://'.$site_alias, '', $url);
            $url = str_replace('https://'.$site_alias, '', $url);
            $url = str_replace($site_alias, '', $url);
            $url = str_replace('@{domain}', '@'.$site_alias, $url); // re-instate email addresses
            foreach($GLOBALS['protected_aliases'] AS $key => $protected_aliases){
                $url = str_replace('{protect'.$key.'}', $protected_aliases, $url); // protect protected urls
            }
        }
    }
    return $url;
}

function byteValueToASCII($string){
    // returns ASCII value of each character in string, on new line
    for($i = 0; $i < strlen($string); $i++){
        $character = substr($string, $i, 1);
        echo $character.': '.ord($character)."<br />";
    }
}

function encrypt($text){
    return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $GLOBALS['encrypt_decrypt_key'], $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
}

function decrypt($text){
    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $GLOBALS['encrypt_decrypt_key'], base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
}

function microsite_domain($microsite_id){
    $query = sprintf("SELECT domain FROM microsites WHERE id = '%d' AND live = 1 AND deleted = 0;",
    mysql_real_escape_string($microsite_id));
    $result = mysql_query($query) or die(mysql_error());
    if ($result){
        $row = mysql_fetch_array($result);
        return $row['domain'];
    }
}

function if_british_summertime($time = null){
    if ($time == ''){
        $time = time();
    }
    // declare some start variables 
    $ThisYear = date("Y", $time);
    $MarStartDate = ($ThisYear."-03-25");
    $OctStartDate = ($ThisYear."-10-25");
    $MarEndDate = ($ThisYear."-03-31");
    $OctEndDate = ($ThisYear."-10-31");
    
    // work out the Unix timestamp for 1:00am GMT on the last Sunday of March, when BST starts
    while ($MarStartDate <= $MarEndDate){
        $day = date("l", strtotime($MarStartDate));
        if ($day == "Sunday"){
            $BSTStartDate = ($MarStartDate);
        }
        $MarStartDate++;
    }
    
    $BSTStartDate = (date("U", strtotime($BSTStartDate))+(60*60));
    /*echo "BST this year starts at 1:00am GMT on ";
    echo date("l, dS M", $BSTStartDate);
    echo "<br>";*/

    // work out the Unix timestamp for 1:00am GMT on the last Sunday of October, when BST ends
    while ($OctStartDate <= $OctEndDate){
        $day = date("l", strtotime($OctStartDate)); 
        if ($day == "Sunday"){
            $BSTEndDate = ($OctStartDate); 
        }
        $OctStartDate++;
    }
    
    $BSTEndDate = (date("U", strtotime($BSTEndDate))+(60*60));
    
    /*echo "BST this year ends at 1:00am GMT on ";
    echo date("l, dS M", $BSTEndDate);
    echo "<br>";*/

    // Check to see if we are now in BST 
    if (($time >= $BSTStartDate) && ($time <= $BSTEndDate)){ 
        //echo "We are now in BST";
        return true;
    }else{
        //echo "We are now in GMT";
        return false;
    }
}

//Check whether we need to clear the users shopping basket.
function checkBasket() {
    $query = sprintf("SELECT * FROM remove_basket WHERE cust_id = '%s'",
    mysql_real_escape_string($_SESSION['customer']));
    $result = mysql_query($query);
    
    if(mysql_num_rows($result) > 0) {
        unset($_SESSION['contents']);
        unset($_SESSION['total']);
        unset($_SESSION['subtotal']);
        unset($_SESSION['vat']);
        unset($_SESSION['discount']);
        $query = sprintf("DELETE FROM remove_basket WHERE cust_id = '%s'",
        mysql_real_escape_string($_SESSION['customer']));
        mysql_query($query);    
    }
}

function word_friendly_substr($text,$start = 0, $end = 255){
    $regex = '/^.{'.($start+1).','.($end+1).'}\b/s';
    if (preg_match($regex, $text, $match)){
        $text = $match[0];
    }
    return $text;
}

function getTermsAndConditions() {
    $url = ''; //build url string
    
    //get terms and conditions page based on url - NEEDS TO CHANGED IF 
    $query = mysql_query("SELECT id, sectionid FROM pageindex WHERE url = 'terms-and-conditions' AND live = 1");
    if(mysql_num_rows($query) > 0) {
        while($row = mysql_fetch_assoc($query)) {
            $breadcrumbs = buildBreadcrumb($row['id'], $row['sectionid'], true);
            $max = count($breadcrumbs['url']);
            for ($i = 0; $i <= $max; $i++){
                $url .= '/'.$breadcrumbs['url'][$i];
            }
        }
    }?>
    <p><input type="checkbox" name="accept_terms" /> I accept the <a href="<?=$url?>" onclick="target='_blank';">terms and conditions</a>.</p>
<? }

function centreOpen($centre_id, $date){
    $query = sprintf("SELECT * FROM centre_closed_days WHERE centre_id = '%d' AND date = '%s' AND deleted = 0",
    mysql_real_escape_string($centre_id),
    mysql_real_escape_string($date));
    $result = mysql_query($query) or die(mysql_error());
    if (mysql_num_rows($result) > 0){
        return false;
    }else{
        return true;
    }
}

function currentURL($urlString = false){
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    
    if ($urlString){
        $pageURL .= '?'.$_SERVER["QUERY_STRING"];
    }
    
    return $pageURL;
}

function getWordpressPostTitle() {
    global $post;
    
    return get_the_title();
}

function social_buttons($title, $url){
    return; ?>
    <div class="social_media">
        <div class="button">
            <a href="http://twitter.com/share" class="twitter-share-button" data-url="<?=$url?>" data-text="<?=$title?>" data-count="none"<? if ($GLOBALS['twitter_user'] != '') echo ' data-via="'.$GLOBALS['twitter_user'].'"';?>>Tweet</a>
            <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
        </div>
        <div class="button google_plus">
            <!-- Place this tag where you want the +1 button to render -->
            <g:plusone size="medium" annotation="none" href="<?=$url?>"></g:plusone>
            <!-- Place this render call where appropriate -->
            <script type="text/javascript">
            <!--
            window.___gcfg = {lang: 'en-GB'};
            (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/plusone.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
            //-->
            </script>
        </div>
        <div class="button digg">
            <script type="text/javascript">
            <!--
            (function() {
            var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
            s.type = 'text/javascript';
            s.async = true;
            s.src = 'http://widgets.digg.com/buttons.js';
            s1.parentNode.insertBefore(s, s1);
            })();
            //-->
            </script>
            <a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=<?=urlencode($url)?>&amp;title=<?=urlencode($title)?>"></a>
        </div>
        <div class="button facebook_like last">
            <div id="fb-root"></div>

            <script type="text/javascript">
            <!--
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            //-->
            </script>
            <div class="fb-like" data-href="http://<?=$GLOBALS['site_url']?>" data-send="true" data-layout="button_count" data-width="200" data-show-faces="false"></div>
        </div>
    </div>
    <div style="clear:both;">&nbsp;</div>
<? }

function PageSocialButtons($type, $id, $url = null){
    switch ($type){
    
        case 'standard':
        case 'landing_page':
            $query = sprintf("SELECT name, body FROM pagecontent_standard WHERE pageid = '%d' AND published = 1 ORDER BY datetime DESC LIMIT 0,1",
            mysql_real_escape_string($id));
            $result = mysql_query($query) or die(mysql_error());
            if (mysql_num_rows($result)){
                $row = mysql_fetch_array($result);
                
                $title = $row['name'];
                $body = $row['body'];
            }
            $title_addition = ' page on '.$GLOBALS['site_name'];
            
        break;
        
        case 'shop':
            $id_array = explode(',', $id);
            
            if ($id_array[0] == 'product'){
                $query = sprintf("SELECT name FROM shop_prods WHERE url = '%s' AND live = 1 AND deleted = 0",
                mysql_real_escape_string($id_array[1]));
                $result = mysql_query($query) or die(mysql_error());
                if (mysql_num_rows($result)){
                    $row = mysql_fetch_array($result);
                    
                    $title = $row['name'];
                }
                $title_addition = ' from the '.$GLOBALS['site_name'].' Shop';
            }elseif ($id_array[0] == 'category'){
                $query = sprintf("SELECT name FROM shop_cats WHERE id = '%d' AND live = 1 AND deleted = 0",
                mysql_real_escape_string($id_array[1]));
                $result = mysql_query($query) or die(mysql_error());
                if (mysql_num_rows($result)){
                    $row = mysql_fetch_array($result);
                    
                    $title = $row['name'];
                }
                $title_addition = ' products from the '.$GLOBALS['site_name'].' Shop';
            }else{
                $title_addition = $GLOBALS['site_name'].' Shop';
            }
        break;
        
        case 'events':
            $query = sprintf("SELECT title FROM events WHERE id = '%d' AND deleted = 0 LIMIT 1",
            mysql_real_escape_string($id));
            $result = mysql_query($query) or die(mysql_error());
            if (mysql_num_rows($result)){
                $row = mysql_fetch_array($result);
                
                $title = $row['title'];
                
                $query = sprintf("SELECT * FROM images WHERE images.page_id = '%d' AND images.type = 'events' AND images.deleted = 0 ORDER BY order_id",
                mysql_real_escape_string($id));
                $image_result = mysql_query($query) or die(mysql_error());
                if (mysql_num_rows($image_result) > 0) {
                    $image_row = mysql_fetch_array($image_result);
                    
                    $body = '<img src="/'.$GLOBALS['upload_dir'].'/images/small/'.$image_row['filename'].'" alt="" />';
                }
            }
            $title_addition = ' event at '.$GLOBALS['site_name'];
        break;
        
        case 'centre_news':
            $title_addition = 'Centre News from '.$GLOBALS['site_name'];
        break;
        
        default:
            // no handler
            return;
        break;
    }
    
    if ($image = getFirstImage($body, true)){
        // have found image
    }else{
        $image = 'http://'.$GLOBALS['site_url'].'/images/luton-culture-logo.png';
    }
    
    if ($url == ''){
        $url = currentURL();
    } ?>
    <div class="page_head_share">
        <span class="share_text">Share this</span>
        <span class="button">
            <a title="Share on Facebook" onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?=urlencode($title.$title_addition)?>&amp;p[summary]=<?=urlencode($summary)?>&amp;p[url]=<?=urlencode($url)?>&amp;&amp;p[images][0]=<?=urlencode($image)?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><img src="/images/addthis_facebook.png" width="20" height="20" alt="Share on Facebook" /></a>
        </span>
        <span class="button">
            <a title="Share on Twitter" onClick="window.open('http://twitter.com/home?status=<?=urlencode($title.$title_addition)?>%20<?=urlencode($url)?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><img src="/images/addthis_twitter.png" width="20" height="20" alt="Share on Twitter" /></a>
        </span>
        <span class="button">
            <a title="Share on Google Plus" onClick="window.open('https://plusone.google.com/_/+1/confirm?hl=en&amp;url=<?=urlencode($url)?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)"><img src="/images/addthis_googleplus.png" width="20" height="20" alt="Share to Google Plus" /></a>
        </span>
        <span class="button email_this">
            <div class="addthis_toolbox addthis_default_style"
            addthis:url="<?=$url?>"
            addthis:title="<?=$title.$title_addition?>">
                <a title="Share by email" class="addthis_button_email"><img src="/images/addthis_email.png" width="20" height="20" alt="Share by email" /></a>
            </div>
            <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ee72311153ee037"></script>
        </span>
        <div style="clear:both;">&nbsp;</div>
    </div>
<? }

function getFirstImage($content, $include_site = false){
    $image_result = preg_match_all('#<img[^>]*>#i', (string)$content, $matches);
    if ($image_result > 0){
        // post has an image, find image src
        $string = $matches[0][0];
        
        $image_source_result = preg_match_all('# src="[^"]*"#i', $string, $source_matches);
        if ($image_source_result > 0){
            $image_source = $source_matches[0][0];
            $image_source = str_replace(' src="', '', $image_source);
            $image_source = str_replace('"', '', $image_source);
            $image = $image_source;
        }else{
            return;
        }
    }else{
        return;
    }
    
    if ($include_site){
        if (!substr_count($image, 'http') > 0){
            if (substr($image, 0, 1) != '/'){
                $image = 'http://'.$GLOBALS['site_url'].'/'.$image;
            }else{
                $image = 'http://'.$GLOBALS['site_url'].$image;
            }
        }
    }
    return $image;
}

function eventNumberPad($number){
    if (strlen($number) < 2){
        $number = '0'.$number;
    }
    return $number;
}

function tag_processing($tags){
    $tags = strtolower($tags);
    $tags = str_replace(',', ' ', $tags);
    $tags = str_replace('  ', ' ', $tags);
    $tags = str_replace(' ', ',', $tags);
    $tag_array = explode(',', $tags);
    
    $new_tag_array = array();
    
    foreach($tag_array AS $tag){
        if (!in_array($tag, $new_tag_array)){
            $new_tag_array[] = $tag;
        }
    }
    $new_tags = implode(',', $new_tag_array);
    return $new_tags;
}

function moduleLinkHandler($handling){
    switch($handling){
        case 'new':
            return ' target="_blank"';
            break;
        case 'pretty':
            return ' rel="prettyPhoto"';
            break;
        default:
            return;
            break;
    }
}

function pagePartsGetter($pagedetails){
    global $path;
    
    $root_url = '/';
    $part_array = array();
    $found_this_page = false;
    
    $first = true;
    foreach($path AS $part){
        if ($first){
            // skip first part of path, this is the section
            $root_url .= $part.'/';
        }else{
            if ($found_this_page){
                $part_array[] = $part; // [0] => start year, [1] => start month, [2] => start day, [3] => event url
            }
            if (($part != $pagedetails['url']) AND (!$found_this_page)){
                $root_url .= $part.'/';
            }elseif(!$found_this_page){
                $found_this_page = true;
                $root_url .= $part.'/';
            }
        }
        $first = false;
    }
    $array = array( 'root_url' => $root_url,
                    'part_array' => $part_array);
    return $array;
}

function displayError($error){
    if (!$error) return;
    if ($error == '') return;
    echo '<a id="submit"></a><span class="validationerror">'.$error.'</span>';
}

function eventStartTimeDropdown($default_val = '') {
    $html = '<option value="">--- Please select ---</option>'; //base value
    
    for($i = 0; $i < 24; $i++) {
        //am pm
        for($interval = 0; $interval < 4; $interval++) {
            //00, 15, 30, 45 past the hour
            $this_int = $interval * 15;
            
            //Build strings
            $time = str_pad($i, 2, "0", STR_PAD_LEFT);
            $this_int = str_pad($this_int, 2, "0", STR_PAD_LEFT);
            
            //Make value
            $value = $time.":".$this_int;
            
            if($i % 12 == 0 AND $interval % 4 == 0) {
                if($i == 0) {
                    $value .= ' (am)';
                }
                else if($i == 12) {
                    $value .= ' (pm)';
                }
            }
            
            //is this option selected?
            $selected = ($default_val != '' AND $default_val == $value) ? 'selected="selected"':'';
            
            $html .= '<option value="'.$value.'" '.$selected.'>'.$value.'</option>';
        }
    }
    
    echo $html;
}


function checkMailChimpSubscription($list_id, $api, $retrieved_list_groups, $data) {
    $subscription_array = array(); //subscription array for user preferences
    $data_results = array(); // array for returning mailchimp results.
    
    if ($data['forename'] != ''){
        $subscription_array['FNAME'] = $data['forename'];
    }
    
    if ($data['surname'] != ''){
        $subscription_array['LNAME'] = $data['surname'];
    }
    
    #Check the email does not exist.
    $retval = $api->listMemberInfo($list_id, array($data['email']));
    
    if($retval['success'] == 0) {
        ## not subscribed - add them
        $retval = $api->listSubscribe($list_id, $data['email'], $subscription_array, $data['format'], true, true, false);
        
        if ($api->errorCode){
            $data_results['state'] = false;
            $data_results['return'] = 'error';
            $data_results['error_msg'] = '<p>Error: '.$api->errorCode.': '.$api->errorMessage.'</p>';
        } else {
            $data_results['state'] = true;
            $data_results['return'] = 'added';
            //echo "Subscribed - look for the confirmation email!\n"; 
        }
    }
    else {
        if ($api->errorCode){
            $data_results['state'] = false;
            $data_results['return'] = 'error';
            $data_results['error_msg'] = '<p>Error: '.$api->errorCode.': '.$api->errorMessage.'</p>';
        } 
        else {
            foreach($retval['data'] as $k => $v){
                $subscriber_id = $v['id'];
                $status = $v['status'];
                break;
            }
            
            if($status == 'unsubscribed') {
                $retval = $api->listSubscribe($list_id, $data['email'], $subscription_array, $data['format'], true, true, false);
                
                if ($api->errorCode){
                    $data_results['state'] = false;
                    $data_results['return'] = 'error';
                    $data_results['error_msg'] = '<p>Error: '.$api->errorCode.': '.$api->errorMessage.'</p>';
                } else {
                    $data_results['state'] = true;
                    $data_results['return'] = 'added';
                    //echo "Subscribed - look for the confirmation email!\n"; 
                }
            }
            else {
                if($subscriber_id != '') {
                    $data_results['state'] = false;
                    $data_results['return'] = 'already_added';
                    $data_results['prefs_link'] = $GLOBALS['base_mailchimp_url']."&id=".$list_id."&e=".$subscriber_id;
                }
            }
        }
    }
    
    return $data_results;
}

function sendMailChimpError($message) {
    $headers = "From: MailChimp Checker <".$GLOBALS['mail_sender'].">\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    
    mail("darylp@tickboxmarketing.co.uk", "MAILCHIMP ERROR!", $message, $headers);
}

/*
Copyright 2007-2008 Brenton Fletcher. http://bloople.net/num2text
You can use this freely and modify it however you want.
*/
function convertNumber($num)
{
   list($num, $dec) = explode(".", $num);

   $output = "";

   if($num{0} == "-")
   {
      $output = "negative ";
      $num = ltrim($num, "-");
   }
   else if($num{0} == "+")
   {
      $output = "positive ";
      $num = ltrim($num, "+");
   }
   
   if($num{0} == "0")
   {
      $output .= "zero";
   }
   else
   {
      $num = str_pad($num, 36, "0", STR_PAD_LEFT);
      $group = rtrim(chunk_split($num, 3, " "), " ");
      $groups = explode(" ", $group);

      $groups2 = array();
      foreach($groups as $g) $groups2[] = convertThreeDigit($g{0}, $g{1}, $g{2});

      for($z = 0; $z < count($groups2); $z++)
      {
         if($groups2[$z] != "")
         {
            $output .= $groups2[$z].convertGroup(11 - $z).($z < 11 && !array_search('', array_slice($groups2, $z + 1, -1))
             && $groups2[11] != '' && $groups[11]{0} == '0' ? " and " : ", ");
         }
      }

      $output = rtrim($output, ", ");
   }

   if($dec > 0)
   {
      $output .= " point";
      for($i = 0; $i < strlen($dec); $i++) $output .= " ".convertDigit($dec{$i});
   }

   return $output;
}

function convertGroup($index)
{
   switch($index)
   {
      case 11: return " decillion";
      case 10: return " nonillion";
      case 9: return " octillion";
      case 8: return " septillion";
      case 7: return " sextillion";
      case 6: return " quintrillion";
      case 5: return " quadrillion";
      case 4: return " trillion";
      case 3: return " billion";
      case 2: return " million";
      case 1: return " thousand";
      case 0: return "";
   }
}

function convertThreeDigit($dig1, $dig2, $dig3)
{
   $output = "";

   if($dig1 == "0" && $dig2 == "0" && $dig3 == "0") return "";

   if($dig1 != "0")
   {
      $output .= convertDigit($dig1)." hundred";
      if($dig2 != "0" || $dig3 != "0") $output .= " and ";
   }

   if($dig2 != "0") $output .= convertTwoDigit($dig2, $dig3);
   else if($dig3 != "0") $output .= convertDigit($dig3);

   return $output;
}

function convertTwoDigit($dig1, $dig2)
{
   if($dig2 == "0")
   {
      switch($dig1)
      {
         case "1": return "ten";
         case "2": return "twenty";
         case "3": return "thirty";
         case "4": return "forty";
         case "5": return "fifty";
         case "6": return "sixty";
         case "7": return "seventy";
         case "8": return "eighty";
         case "9": return "ninety";
      }
   }
   else if($dig1 == "1")
   {
      switch($dig2)
      {
         case "1": return "eleven";
         case "2": return "twelve";
         case "3": return "thirteen";
         case "4": return "fourteen";
         case "5": return "fifteen";
         case "6": return "sixteen";
         case "7": return "seventeen";
         case "8": return "eighteen";
         case "9": return "nineteen";
      }
   }
   else
   {
      $temp = convertDigit($dig2);
      switch($dig1)
      {
         case "2": return "twenty-$temp";
         case "3": return "thirty-$temp";
         case "4": return "forty-$temp";
         case "5": return "fifty-$temp";
         case "6": return "sixty-$temp";
         case "7": return "seventy-$temp";
         case "8": return "eighty-$temp";
         case "9": return "ninety-$temp";
      }
   }
}
      
function convertDigit($digit)
{
   switch($digit)
   {
      case "0": return "zero";
      case "1": return "one";
      case "2": return "two";
      case "3": return "three";
      case "4": return "four";
      case "5": return "five";
      case "6": return "six";
      case "7": return "seven";
      case "8": return "eight";
      case "9": return "nine";
   }
}

function checkMailChimpRegistration($email) {
    require_once($GLOBALS['docRoot'].'/lib/classes/mailchimp/MCAPI.class.php');
                        
    $api = new MCAPI($GLOBALS['mailchimp_api_key']);
    $api->useSecure(true);
    
    $list_id = $GLOBALS['base_mailchimp_list_id'];
    
    if ($api->errorCode){
        echo "Unable to load lists()!";
        echo "\n\tCode=".$api->errorCode;
        echo "\n\tMsg=".$api->errorMessage."\n";
    } 
    else {
        $retval = $api->listMemberInfo($list_id, array($email));
        if($retval['success'] == 0) {
            foreach($retval['data'] as $k => $v){
                $subscriber_id = $v['id'];
                $status = $v['status'];
                break;
            }

            if($status == 'unsubscribed') {?>
                <p>Your are not currently subscribed to <?=$GLOBALS['site_name']?> news. To register, <a href="http://stpetershospice.us7.list-manage.com/subscribe/post?u=df4c474ec7d02f1656441b42b&amp;id=<?=$list_id?>">click here</a>.</p>
            <?php }
            else {?>
                <p>You are signed up to receive news from <?=$GLOBALS['site_name']?>. To edit your preferences please <a href="http://stpetershospice.us7.list-manage.com/subscribe/post?u=df4c474ec7d02f1656441b42b&amp;id=<?=$list_id?>&e=<?=$subscriber_id?>">click here</a>.</p> 
            <?php }
        }
        else {?>
            <p>Your are not currently subscribed to <?=$GLOBALS['site_name']?> news. To register, <a href="http://stpetershospice.us7.list-manage.com/subscribe/post?u=df4c474ec7d02f1656441b42b&amp;id=<?=$list_id?>">click here</a>.</p>
        <?php }
    }
}

function uniqueUpdateTime($case){
    switch($case){
        case 'modules':
            $query = sprintf("SELECT MAX(datetime) AS update_datetime FROM moduletemplates");
            $result = mysql_query($query) or die(mysql_error());
            if (mysql_num_rows($result) > 0){
                $row = mysql_fetch_array($result);
                
                return md5($row['update_datetime']);
            }
        break;
        case 'embed_sets':
            $query = sprintf("SELECT MAX(datetime) AS update_datetime FROM embed_sets");
            $result = mysql_query($query) or die(mysql_error());
            if (mysql_num_rows($result) > 0){
                $row = mysql_fetch_array($result);
                
                return md5($row['update_datetime']);
            }
        break;
        default:
            if (file_exists($GLOBALS['docRoot'].$case)){
                return md5(filemtime($GLOBALS['docRoot'].$case));
            }
        break;
    }
}

//Generate item description to pass to PayPal.
function generateItemDescription($order_id) {
    $query = sprintf("SELECT * FROM shop_transactions WHERE trans_key = '%s' AND status = ''",
    mysql_real_escape_string($_SESSION['trans_key']));
    $result = mysql_query($query);
    
    //check if we have a transaction
    if (mysql_num_rows($result) == 1){
        $row = mysql_fetch_assoc($result);
        
        $item_query = sprintf("SELECT * FROM shop_order_details WHERE order_id = '%s'",
        mysql_real_escape_string($row['id']));
        $item_result = mysql_query($item_query);
        
        $item_desc = $row['order_id'].": ";
        $rows = mysql_num_rows($item_result);
        
        $a = 1;
        while($item_row = mysql_fetch_assoc($item_result)) {
            $item_desc .= $item_row['quantity']."x ".$item_row['description']." - &pound;".$item_row['unit_price'];
            
            if($item_row['quantity'] > 1) {
                $item_desc .= ' each';  
            }
            
            if($a != $rows) {
                $item_desc .= ', ';
            }
            $a = $a + 1;
        }
    }
    
    return $item_desc.".";
}

function cleanHomepageSwitcher($exploded_array) {
    $array = array();
    
    foreach($exploded_array as $item) {
        $item = trim($item);
        if($item != '') $array[] = $item;
    }
        
    return $array;
}

function hex2rgb($hex) {
    $hex = str_replace("#", "", $hex);
    
    if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
    } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
    }
    
    $rgb = array($r, $g, $b);
    return $rgb; // returns an array with the rgb values
}

function postcodeToLatLng($postcode, $country = 'UK') {
    $postcode = str_replace(" ", "", $postcode);
    $url = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$postcode.'+'.$country.'&sensor=false';
    
    $get_json_object = file_get_contents($url);
    //echo $get_json_object;
    if ($get_json_object){
        $parsed = json_decode($get_json_object);
        
        if ($parsed->status == 'OK'){
            $result = $parsed->results[0];
            $data['latitude'] = $result->geometry->location->lat;
            $data['longitude'] = $result->geometry->location->lng;
        }
    }
    
    return $data;
}

function uri_segments($num) {   
    $base = explode('?',$_SERVER['REQUEST_URI']);
    $segments = explode('/', rtrim($base[0], '/'));
    if($segments[$num] === NULL)
    {
        return false;
    }
    else
    {
        return $segments[$num];
    }
}
    
function pp($arr) {
    echo '<pre>'.htmlentities(print_r($arr, true)).'</pre>';
}

function error_handler($message, $case){
    global $query;
    
    switch($case){
        case 'mysql':
            $query .= '<br />info: '.mysql_info();
            $error = mysql_error().': '.$query;
        break;
        default:
            $error = $message;
        break;
    }
    
    $error .= '<br />On page '.currentURL();
    $error .= '<br />In '.basename(__FILE__);
    
    // now handle the error, what do we do?
    switch ($GLOBALS['production_mode']){
        case 'development':
        case 'testing':
            // echo on the screen
            echo $error;
        break;
        default:
            // email error to webmaster
            email_sender($GLOBALS['webmaster_email'], 'Error on '.$GLOBALS['site_url'], $error);
        break;
    }
}

function email_sender($to, $subject, $message, $headers = null, $from = null){
    if ($headers == ''){
        $headers = "From: ";
        if ($from != ''){
            $headers .= $from;
        }else{
            $headers .= $GLOBALS['site_name'].' <'.$GLOBALS['mail_sender'].'>';
        }
        $headers .= "\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
    }
    return mail($to, $subject, wordwrap($message,70), $headers);
}





/*
 *---------------------------------------------------------------
 * FUNCTIONS

hb_define_routes(array('/event/(:any)/' 	=> 'event/something', 
					'/event/something/'	=> 'event/something/else/'), 'event');

hb_define_permissions(array('Events' 		      => 'title',
						 'Add Event'	      => 'add_event',
						 'Delete Event'  	  => 'delete_event'
						 'Subscribe to Event' => 'subscribe_event'));

hb_define_widgets();

 *---------------------------------------------------------------
 */


function hb_define_routes($route_array)
{

}

function hb_clean_routes($route_array)
{

}

function hb_define_permissions($permission_array) 
{

}

function hb_clean_permissions($permission_array)
{

}

function hb_define_widgets($widget_array) 
{

}

function hb_clean_widgets($widget_array)
{

}