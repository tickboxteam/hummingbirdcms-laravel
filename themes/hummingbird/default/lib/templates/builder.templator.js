$(document).ready(function()
{
    $("#flexicontainer").parent().prepend('<button id="save_button" class="hide"></button>');

    if($('#flexicontainer #hb-template-wrapper').children().length > 0)
    {
        var html = $('#flexicontainer').find('#hb-template-wrapper').html();
        $('#flexicontainer').html(html);

            if($('#flexicontainer').children().length > 0)
            {   
                $("#flexicontainer .hb-item").each(function()
                {   
                    if($(this).hasClass('hb_container'))
                    {
                        $(this).data('type', 'container');
                        $(this).prepend(getTools('container'));
                    }
                    else if($(this).hasClass("hb_row"))
                    {
                        $(this).data('type', 'hb_row');
                        $(this).prepend(getTools('row'));
                    }
                    else if($(this).hasClass("column"))
                    {
                        $(this).data('type', 'column');
                        $(this).prepend(getTools('column'));
                        $(this).append('<div class="add-btn text-center"><i class="fa fa-plus fa-2x"></i></div>');
                    }
                    
                    $(this).addClass('f_pad');
                });

                $("#flexicontainer .item").each(function()
                {
                    var tools = '<div class="tools hide"><div class="tool handle hide"><i class="fa fa-arrows"></i></div><div class="tool remove"><a class="remove_item" href="#"><i class="fa fa-trash"></i></a></div></div>';

                    if($(this).hasClass('image-wrapper'))
                    {
                        $(this).prepend(tools);
                    }
                    else if($(this).hasClass("redactor-wrapper"))
                    {
                        var id = randString();
                        var html = $(this).html();
                        $(this).html('');
                        $(this).append(tools+'<textarea id="'+id+'" class="redactor-content" name="redactor[]">'+html+'</textarea>');
                    }
                });

                init_redactor();

                init();
            }
    }
    else
    {
        if($('#flexicontainer').children().length > 0 && $('#flexicontainer .empty-text').length == 0)
        {
                str = '<div class="container hb-item column f_pad hb_container">';
                str += '<div class="tools">';
                str += '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
                str += '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
                str +=            '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>';
                str +=            '<div class="tool"><a class="replicate-container" href="#"><i class="fa fa-copy"></i></a></div>'; 
                str += '</div>';

                str += '<div class="row hb-item f_pad clearfix row-wrapper hb_row">';
                str +=     '<div class="tools">';
                str +=             '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
                str +=            '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
                str +=            '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>'; 
                str +=        '</div>';

                str += '<div class="col-md-12 f_pad column hb_column hb-item">';
                str +=     '<div class="tools">';
                // str += '<div class="tool"><a href="#" class="add_tool"><i class="fa fa-edit"></i></a></div>';
                str += '<div class="tool"><a class="remove_tool" href="#"><i class="fa fa-times"></i></a></div>';
                str += '<div class="tool draghandle"><i class="fa fa-arrows"></i></div>';
                str += '<div class="tool"><a class="settings_tool" href="#"><i class="fa fa-gears"></i></a></div>';
                str += '<div class="tool"><a class="replicate-column" href="#"><i class="fa fa-copy"></i></a></div>'; 
                str += '</div>';//eof tools

                var tools = '<div class="tools hide"><div class="tool handle hide"><i class="fa fa-arrows"></i></div><div class="tool remove"><a class="remove_item" href="#"><i class="fa fa-trash"></i></a></div></div>';

                var id = randString();

                str += '<div class="item redactor-wrapper relative">'+tools+'<textarea id="'+id+'" class="redactor-content" name="redactor[]">'+$("#generated_html").html()+'</textarea></div>';

                str += '<div class="add-btn text-center"><i class="fa fa-plus fa-2x"></i></div>';
                str += '</div>'; //eof column
                str += '</div>'; //eof row
                str += '</div>';//eof container

                $("#flexicontainer").html(str);

                init_redactor();

                init();
        }
    }

    $('form a.update').on('click', function(e)
    {
        e.preventDefault();
        $("#flexicontainer .empty-text").remove();   
        $("#action").val($(this).data('action'));
        generateHTML($(this), true);
    });
});