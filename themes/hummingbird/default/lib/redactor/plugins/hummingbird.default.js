if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.undo_redo = function()
{
    return {
        init: function()
        {
            /* Undo and Re-do typing */
            var undo = this.button.addFirst('undo', 'Undo');
            var redo = this.button.addAfter('undo', 'redo', 'Redo');
 
            this.button.addCallback(undo, this.buffer.undo);
            this.button.addCallback(redo, this.buffer.redo);
        }
    };
};

RedactorPlugins.underline = function()
{
    return {
        init: function()
        {
            /* underline feature */
            var underline = this.button.addAfter('italic', 'underline', 'Underline');
            this.button.addCallback(underline, this.underline.format);
        },
        format: function()
        {
            this.inline.format('u');
        }
    };
};

RedactorPlugins.super_subscript = function()
{
    return {
        init: function()
        {
            var sup = this.button.add('superscript', 'Superscript');
            var sub = this.button.add('subscript', 'Subscript');
 
            // make your added buttons as Font Awesome's icon
            this.button.setAwesome('superscript', 'fa-superscript');
            this.button.setAwesome('subscript', 'fa-subscript');
 
            this.button.addCallback(sup, this.super_subscript.formatSup);
            this.button.addCallback(sub, this.super_subscript.formatSub);
        },
        formatSup: function()
        {
            this.inline.format('sup');
        },
        formatSub: function()
        {
            this.inline.format('sub');
        }
    };
}; 