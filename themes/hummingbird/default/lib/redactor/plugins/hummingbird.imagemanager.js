if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
	RedactorPlugins.hb_image_manager = function()
	{
		return {
	        getTemplate: function()
	        {
	            return String()
	            + '<section id="redactor-modal-advanced">'
	            + '<label>Enter a text</label>'
	            + '<textarea id="mymodal-textarea" rows="6"></textarea>'
	            + '</section>';
	        },
			init: function()
			{
				var button = this.button.add('hb_image_manager', 'Image Manager');
				this.button.addCallback(button, this.hb_image_manager.load);

	            // make your added button as Font Awesome's icon
	            this.button.setAwesome('hb_image_manager', 'fa-image');
			},
			load: function()
			{
				var _this = this;

				/* Get modal */
		        var data = {};
		        data.collection = $(this).data('collection-id');
		        data.title = $(this).data('page-title');
		        data.type = 'images';

		        $.ajax({
		            dataType: "json",
		            cache: false,
		            url: '/hummingbird/media/globalMediaLibrary',
		            data: data,
		            success: $.proxy(function(data)
		            {  
		                if($("#media-lib.redactor").length <= 0)
		                {
		                    $("body").append(data.html);
		                }

		                $(".modal").modal('hide');
		                $("#media-lib").addClass('redactor').modal('show');
		            }, this)
		        });

		        /* Media library - images only */
		        $(document).on('click', '#media-lib.redactor #library .view-collection', function(e)
		        {
		            e.preventDefault();

		            $("#library").html('<h3>Loading...</h3>');

		            var data = {};
		            data.action = 'library';
		            data.collection = $(this).data('id');
		            data.type = 'images';

		            $.ajax({
		                dataType: "json",
		                data: data,
		                cache: false,
		                url: '/hummingbird/media/globalMediaLibrary',
		                success: $.proxy(function(data)
		                {
		                    data = $(data.html);
		                    html = data.find('#library').html();

		                    $("#media-lib.redactor #library").html(html);
		                }, this)
		            });
		        });

		        $(document).on('click', '#media-lib.redactor .gallery .thumb', function(e)
		        {
		            e.preventDefault();

		            $("#library").html('<h3>Loading...</h3>');

		            var data = {};
		            data.action = 'item';
		            data.item = $(this).data('item-id');
		            data.featured = true;

		            $.ajax({
		                dataType: "json",
		                data: data,
		                cache: false,
		                url: '/hummingbird/media/globalMediaLibrary',
		                success: $.proxy(function(data)
		                {
		                    data = $(data.html);
		                    html = data.html();

		                    $("#media-lib.redactor #library").html(html);

		                    if($("#media-lib.redactor .modal-footer").length <= 0)
		                    {
		                    	$("#media-lib.redactor .modal-content").append('<div class="modal-footer"><footer><button type="submit" class="create-image btn btn-primary">Insert Image</button></footer></div>');
		                    }
		                }, this)
		            });
		        });

		        $(document).on('click', '#media-lib.redactor .create-image', function(e)
		        {
		        	e.stopImmediatePropagation();
		            e.preventDefault();

		            _this.hb_image_manager.insert();

		            $(".modal").modal('hide');
		        });

		        $(document).on('change', '#media-lib.redactor #url', function(e)
		        {
		            e.preventDefault();

		            $(".url-show").toggleClass('hide');
		        });

		        $("#media-lib.redactor .create-image").unbind('click');
			},
			insert: function(e)
			{
	            // holder.find('.remove-media-library').parent().removeClass('hide');
	            // holder.find('.media-image').val($("#media-lib.redactor #library #src").val()); //reset value
	            // holder.find('.image-holder').removeClass('hide').find('img').attr('src', $("#media-lib.redactor #library #src").val());
	            // holder.find('.redactor-media-library').data('collection-id', $("#media-lib.redactor #library #collection").val());

	            var html = '';
	            var image_el = this.image;

	            var src = $("#media-lib.redactor #library #src").val();
	            var title = $("#media-lib.redactor #library #title").val();
	            var alt = $("#media-lib.redactor #library #alt").val();
	            var url = $("#media-lib.redactor #library #url").val();
	            var target_window = $("#media-lib.redactor #library #target_window").val();
	            var link = $("#media-lib.redactor #library #link").val();

	            /* Do we have a link */
	            if(url == 'url')
	            {
	            	html += '<a href="'+link+'" title="'+ title + '" target="'+target_window+'">';
	            }

	            /* Standard image */
	            html += '<img src="'+ src +'" title="'+ title + '" alt="'+ alt + '" />';

	            /* Do we have a link */
	            if(url == 'url')
	            {
	            	html += '</a>';
	            }

	            image_el.insert(html);
			}
		};
	};
})(jQuery);