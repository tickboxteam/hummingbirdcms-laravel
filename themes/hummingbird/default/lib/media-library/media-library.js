var holder; //library holder for search and featured images

/* Media library - images only */
$(document).on('click', '#media-lib.browse #library .view-collection', function(e)
{
    e.preventDefault();

    $("#library").html('<h3>Loading...</h3>');

    var data = {};
    data.action = 'library';
    data.collection = $(this).data('id');

    $.ajax({
        dataType: "json",
        data: data,
        cache: false,
        url: '/hummingbird/media/globalMediaLibrary',
        success: $.proxy(function(data)
        {
            data = $(data.html);
            html = data.find('#library').html();

            $("#media-lib.browse #library").html(html);
        }, this)
    });
});

$(document).on('click', '#media-lib.browse .gallery .thumb', function(e)
{
    e.preventDefault();

    $("#library").html('<h3>Loading...</h3>');
    $("#media-lib.browse .modal-footer").remove();

    var data = {};
    data.action = 'item';
    data.item = $(this).data('item-id');
    data.featured = true;

    $.ajax({
        dataType: "json",
        data: data,
        cache: false,
        url: '/hummingbird/media/globalMediaLibrary',
        success: $.proxy(function(data)
        {
            data = $(data.html);
            html = data.html();

            $("#media-lib.browse #library").html(html);

            $("#media-lib.browse #library").closest('.modal-content').append('<div class="modal-footer"><footer><button type="submit" class="create-image btn btn-primary">Insert Image</button></footer></div>');
        }, this)
    });
});

$(document).on('click', '#media-lib.browse .create-image', function(e)
{
    e.preventDefault();

    holder.find('.remove-media-library').parent().removeClass('hide');
    holder.find('.media-image').val($("#media-lib.browse #library #src").val()); //reset value
    holder.find('.image-holder').removeClass('hide').find('img').attr('src', $("#media-lib.browse #library #src").val());
    holder.find('.browse-media-library').data('collection-id', $("#media-lib.browse #library #collection").val());

    $("#media-lib.browse").modal('hide');
});

$(document).on('change', '#media-lib.browse #url', function(e)
{
    e.preventDefault();

    $(".url-show").toggleClass('hide');
});

// if($(".browse-media-library").length > 0)
// {
    $(document).on('click', '.browse-media-library', function(e)
    {
        e.preventDefault();

        holder = $(this).closest('.media-library-holder'); //library holder

        var data = {};
        data.collection = $(this).data('collection-id');
        data.title = $(this).data('page-title');
        data.type = 'all';

        $.ajax({
            dataType: "json",
            cache: false,
            url: '/hummingbird/media/globalMediaLibrary',
            data: data,
            success: $.proxy(function(data)
            {  
                if($("#media-lib.browse").length <= 0)
                {
                    $("body").append(data.html);
                }

                $("#media-lib").modal('hide');
                $("#media-lib").addClass('browse').modal('show');
            }, this)
        });
    });
// }

// if($(".remove-media-library").length > 0)
// {
    $(document).on('click', '.remove-media-library', function(e)
    {
        e.preventDefault();

        $(this).parent().addClass('hide'); //hide remove button

        holder = $(this).closest('.media-library-holder'); //library holder
        holder.find('.media-image').val(''); //reset value
        holder.find('.browse-media-library').data('collection-id', '');
        holder.find('.image-holder').addClass('hide');
    });
// }