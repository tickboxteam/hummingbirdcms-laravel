/*!
 *
 * Hummingbird CMS (http://hummingbirdcms.com)
 * Hummingbird JavaScript v1.0.0
 * Copyright 2014-2015 Tickbox Marketing Ltd.
 *
 */

var width = $(window).width();
var height = $(window).height();

var redactor = false;
var plugins_arr = [];

function init_redactor()
{
    // if(redactor) destroy_redactor();

    if(RedactorPlugins && plugins_arr.length == 0)
    {
        $.each(RedactorPlugins, function(key, value)
        {
            plugins_arr.push(key);
        });
    }

    if(plugins_arr.length > 0)
    {
        redactor = true;
        $('.redactor-content').redactor(
        {
            plugins: plugins_arr,
            replaceDivs: false,
            toolbarFixed: false,
            paragraphize: false
        });
    }

    if(!redactor)
    {
        $('.redactor-content').redactor({
            replaceDivs: false,
            toolbarFixed: false,
            paragraphize: false
        });
    }


    if($('.redactor-toolbar .re-fullscreen').length > 0)
    {
        $('.redactor-toolbar .re-fullscreen').click(function()
        {
            $('.wrapper').toggleClass('addOpacity');
        })
    }
}

function destroy_redactor()
{
    $('.redactor-content').each(function()
    {
        $("#"+$(this).attr('id')).redactor('core.destroy');
    });
}

$(document).ready(function()
{   
	/* Initialise */

    $(".switch-screen-size").click(function(e)
    {
        e.preventDefault();

        $('.wrapper').css('width', $(this).data('width'));
        $('.wrapper').css('height', $(this).data('height'));
    });

    $("#nav-toggle").click(function(e)
    {
        e.preventDefault();

        updateViewportDims();

        if(width > 768)
        {
        	// we are looking on desktops
	        if($("#sidebar").hasClass("collapsed"))
	        {
	        	//the name is removed until the transition has finished
	        	$("#sidebar .brand span").addClass('hide');
	        }

	        $(".menu ul:not(.submenu)").slideDown();
	        $("li.option, .hasSubMenu").removeClass('active');

	        $("#sidebar, .main-content").toggleClass('collapsed');

	        if(!$("#sidebar").hasClass("collapsed"))
	        {
	        	//the name is shown once the transition has ended
	        	setTimeout(function()
	        	{
	        		$("#sidebar .brand span").removeClass('hide');
	        	}, 500); //delay of CSS transition
	        }
	    }
        else
        {
        	// iPad/iPhone?
        	$('body').toggleClass('open-sidebar');
        }
    });

    // $(".content-section, .search-box input, .profile").click(function(e)
    // {
    // 	e.preventDefault();
    // 	updateViewportDims();
    	
    // 	if(width <= 768 && $('body').hasClass('open-sidebar'))
    // 	{
    //     	// iPad/iPhone?
    //     	$('body').removeClass('open-sidebar');
    // 	}
    // });

    if($(".hasSubMenu").length > 0)
    {
        $(".hasSubMenu").click(function(e)
        {
            e.preventDefault();

            if(!$("#sidebar").hasClass("collapsed"))
            {
	            var el = $(this);
	            var el_li = el.closest('li.option');
	            var el_sub = el_li.find('ul');
	            var el_first_child = el_li.children(':first');

	            el_li.toggleClass('active');
	            el_first_child.toggleClass('active');

	            if(el_sub.is(':visible'))
	            {
	                el_sub.slideUp();
	                el_first_child.find('i.fa-chevron-right').removeClass('rotate');
	            }
	            else
	            {
	                el_sub.slideDown();
	                el_first_child.find('i.fa-chevron-right').addClass('rotate');
	            }
	        }
        });
    }

    if($("#sidebar .nav ul li").length > 0)
    {
		$( "#sidebar .nav ul li" ).hover(function() 
		{
			if(!$(this).parent().hasClass('submenu'))
			{
				$(this).addClass("nav-hover");

				var height = $("#sidebar").outerHeight();
				var el_height = $(this).parent().height();
				var el_sub_height = $(this).parent().find('.submenu').height();
				var el_position = $(this).parent().offset();
				var top = height - el_position.top - (el_height + el_sub_height) - $("#sidebar .brand").outerHeight();

				if(top < 0)
				{
					$(this).find('span.title').css('top', top + 'px');
					$(this).parent().find('.submenu').css('top', top + 'px');
				}
			}
		}, function () {
			if(!$(this).parent().hasClass('submenu'))
			{
            	$(this).closest('li').removeClass("nav-hover");
        	}
        });
    }

    /* All redactor based content areas */
    if($(".redactor-content").length > 0)
    {
        init_redactor();
    }

    if($("#clock").length > 0)
    {
    	updateClock();
		setInterval('updateClock()', 1000);
    }

    if($("#return-to-top").length > 0)
    {
        var content = $('html,body').outerHeight();

        if (content < window) 
        {
            $('#return-to-top').fadeIn();
        }
        else 
        {
            $('#return-to-top').fadeOut();
        }

        $('#return-to-top').click(function(e)
        {
            e.preventDefault();
            $('body,html').animate({
                scrollTop : 0
            },1000);

            return false;
        });

        $(window).scroll(function()
        {
            if ($(this).scrollTop() > (content/4)) 
            {
                $('#return-to-top').fadeIn('slow');
            } 
            else 
            {
                $('#return-to-top').fadeOut('slow');
            }
        });
    }


    function addTaxonomy(tax_group, _select, html)
    {
        if(tax_group.find('.taxonomy-holder .no-taxonomy').length > 0)
        {
            tax_group.find('.taxonomy-holder .no-taxonomy').addClass('hide');
        }

        tax_group.find('.taxonomy-holder').append(html);
        _select.selectpicker('refresh');
    }

    if($(".add-taxonomy").length > 0)
    {
        $(".add-taxonomy").click(function(e)
        {
            e.preventDefault();

            var html = '<div class="taxonomy relative"><input type="hidden" name="taxonomy[]" value="{item-id}" /><span class="badge badge--hummingbird badge-sm">{item-name}</span><a class="taxonomy-remove" title="Remove Option" href="javascript:void(0);" style="color:red;">x</a></div>';

            /* Elements */
            var tax_group = $(this).closest('.taxonomy-group');
            var _select = tax_group.find('select');

            /* Variables */
            var _select_val = _select.val(); //remove white space
            var type = _select.data('type'); //type of taxonomy

            if($.isArray(_select_val))
            {
                var _html = '';

                $.each(_select_val, function( index, value )
                {
                    var _select_text = _select.find('option[value="'+value+'"]').text();
                    _select_text     = _select_text.replace("&nbsp;", "");
                    _select_text     = _select_text.replace(" ", "");

                    var _this_html = html;
                    _this_html = _this_html.replace("{item-id}", value);
                    _this_html = _this_html.replace("{item-name}", $.trim(_select_text));

                    _html += _this_html;

                    // _select.find('option[value="'+value+'"]').remove(); //remove array of items
                    _select.find('option[value="'+value+'"]').attr("disabled", true); //remove array of items
                });

                html = _html;

                addTaxonomy(tax_group, _select, $.trim(html));
            }
            else if(_select_val !== null)
            {
                var _select_text = _select.find('option:selected').text();
                _select_text     = _select_text.replace("&nbsp;", "");
                _select_text     = _select_text.replace(" ", "");

                html = html.replace("{item-id}", _select_val);
                html = html.replace("{item-name}", $.trim(_select_text));

                // _select.find('option:selected').remove(); //remove single item
                _select.find('option:selected').attr('disabled', true); //remove single item

                addTaxonomy(tax_group, _select, html);
            }
            else
            {
                var search_val = $.trim(tax_group.find('.bs-searchbox input').val());
                    
                var data = {};
                data.ajax = true;
                data.name = search_val;

                $.ajax({
                    type: "POST",
                    url: '/hummingbird/' + type,
                    data: data,
                    success: function(response)
                    {
                        //success
                        html = html.replace("{item-id}", response.data.id);
                        html = html.replace("{item-name}", response.data.name);
                    },
                    error: function(response)
                    {
                        //error
                        html = '';
                        console.log(response);
                    },
                    complete: function(response)
                    {
                        //complete now insert taxonomy
                        addTaxonomy(tax_group, _select, html);
                    }
                });
            }
        });
    }

    $(document).on('click', '.taxonomy-remove', function(e)
    {
        e.preventDefault();

        var parent = $(this).closest('.taxonomy-group'); //parent element
        var select = parent.find('select'); // get the select element
        var _this = $(this).closest('.taxonomy'); //this element

        /* Variables */
        var id = _this.find('input').val();
        var text = _this.find('.badge').text();

        /* Add back to select */
        // select.append($("<option></option>")
        //     .attr("value",id)
        //     .text(text));        
        parent.find('select option[value="' + id + '"]').removeAttr('disabled');

        /* Remove 'tick' */
        select.find('option[value="' + id + '"]').prop("selected", false);

        /* remove from view */
        _this.remove();

        /* We have nothing left - add the "nothing to view" back in */
        if(parent.find('.taxonomy-holder .taxonomy').length == 0)
        {
            parent.find('.no-taxonomy').removeClass('hide');
        }

        /* re-initialise select */
        select.selectpicker('refresh');
    });

    $(document).on('hidden.bs.modal', '#media-lib.modal', function (e)
    {
        $(this).remove();
    });

    setContentHeight();
});

function updateViewportDims()
{
	width = $(window).width();
	height = $(window).height();
}

function setContentHeight()
{
    var header_section = $(".header-section").outerHeight();
    var content_section = $(".content-section").outerHeight();
    var footer_section = $("#footer").outerHeight();

    // console.log(header_section);
    // console.log(content_section);
    // console.log(footer_section);

    var content_height = header_section + content_section;

    /* Side bar min-height */
    // var new_height = (height < content_height) ? content_height:height;
    // if(height < new_height) 
    // {
        // $("#sidebar").css('min-height', new_height + 'px');
    // }

    /* Content area min-height */
    var sidebar_height = $("#sidebar").outerHeight();
    var new_content_height = sidebar_height - header_section - footer_section;

    if($(".header-flash").length > 0)
    {
        new_content_height -= $(".header-flash").outerHeight();
    }

    $(".content-section").css('min-height', new_content_height + 'px');
}




function updateClock ( )
    {
    var currentTime = new Date ( );
    var currentHours = currentTime.getHours ( );
    var currentMinutes = currentTime.getMinutes ( );
    var currentSeconds = currentTime.getSeconds ( );
 
    // Pad the minutes and seconds with leading zeros, if required
    currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
    currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
 
    // Choose either "AM" or "PM" as appropriate
    var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";
 
    // Convert the hours component to 12-hour format if needed
    // currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;
 
    // Convert an hours component of "0" to "12"
    // currentHours = ( currentHours == 0 ) ? 12 : currentHours;
 
    // Compose the string for display
    var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
     
     
    $("#clock").html(currentTimeString);
         
 }
