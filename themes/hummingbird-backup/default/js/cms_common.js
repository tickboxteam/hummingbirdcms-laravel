function remove_item(e)
{
    e.preventDefault();
    $(this).parent().remove();
}

$(document).ready(function(){
    
    $(document).on('click', '.remove-item', remove_item);
    
    if($('#add-menu-item').length > 0) {
        
        $('#add-menu-item').click(function(){
            
            if($('#items_label').val() == '') {
                alert('Please supply a label');
            }
            
            if($('#item_select').val() == '' && $('#items_freestyle').val() == '') {
                return false;
            }
            
            var val = $( "#item_select option:selected" ).text();
            var page_id = $('#item_select').val();
            var label = $('#items_label').val();
            var level = $('#items_level').val();
            
            if (page_id == '') {
                val = $('#items_freestyle').val();
                page_id = 0;
            }
            
            // put value into a list area
            $('#menu-items-area').append('<li class="ui-state-default ui-sortable-handle">\n\
                '+label+'<a href="#" class="remove-item right">x</a>\n\
                <input type="hidden" value="'+val+'" name="menu_items[url][]" />\n\
                <input type="hidden" value="'+page_id+'" name="menu_items[page_id][]" />\n\
                <input type="hidden" value="'+label+'" name="menu_items[label][]" />\n\
            </li> ');
            
            // reset item-selector for new item
            $('#item_select').val("");
            $('#items_freestyle').val("");
            $('#items_label').val("");
        });
    }
    
    if($('#add-module').length > 0) {
        
        $('#add-module').click(function(){
            
            if($('#item_select').val() == '') {
                return false;
            }
            
            var val = $( "#item_select option:selected" ).text();
            var module_id = $('#item_select').val();
            
            if (module_id == '') {
                module_id = 0;
            }
            
            // put value into a list area
            $('#modules-area').append('<li class="ui-state-default ui-sortable-handle">\n\
                '+val+'<a href="#" class="remove-item right">x</a>\n\
                <input type="hidden" value="'+module_id+'" name="modules[]" />\n\
            </li> ');
            
            // reset item-selector for new item
            $('#item_select').val("");
        });
    }
    
    if($('#item_select').length > 0) {
        
        $('#item_select').change(function(){
            if ($(this).val() != '') {
                $('#items_freestyle').val("");
                $('#items_label').val($( "#item_select option:selected" ).text());
            }
        });
    }
    
    if($('.sortable').length >0 ) {
        $('.sortable').sortable();
    }

    if($("#category").length >0 ) {
        $("#category").change(function()
        {
            if($(this).val() == 'NEW')
            {
                $(".custom_field").removeClass("hidden");
            }
            else
            {
                $(".custom_field").addClass("hidden");
            }
        });
    }
    
});

function toggleView(element)
{
    $('.section_nav a.subnav_' + element + ', .subnavlist_' + element).toggle();
    
}