
window.setInterval(function() {
    
    var pathArray = window.location.pathname.split( '/' );
    
    var data = $('form').serialize() + "&page_id=" + pathArray[pathArray.length - 1];

    $.ajax({
        url: "/cms/pages/autosave",
        data: data,
        type: "POST"
    })
    .done(function(msg) {
        console.log('Auto save: '+ msg);
    });


}, 15000);