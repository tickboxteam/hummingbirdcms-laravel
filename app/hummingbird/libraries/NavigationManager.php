<?php 

/**
 * Navigation builder and manager
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class NavigationManager
{
    /**
     * @var array List of registered items.
     */
    protected $menu_items;

    protected $backend_url;

    protected $frontend_url;

    protected $icons;

    protected $cms_types;

    /**
     * Constructor for the main navigation builder
     * Stores front/backend URLs (prepares CMS with icons and types) 
     *
     */ 
    public function __construct()
    {
        if(strpos($_SERVER["REQUEST_URI"], General::backend_url()))
        {
            $this->backend_url = General::backend_url();    
            
            $this->assign_icons();

            $this->assign_cms_types();
        }
        else
        {
            $this->frontend_url = General::frontend_url(); 
        }
    }

    /**
     * Temporary icon system for the CMS. Manual entry for 
     * determining the Model's icon for the main navigation
     *
     */ 
    public function assign_icons()
    {
        $this->icons = array(
            '/hummingbird/' => 'fa fa-home',
            '/hummingbird/pages/' => 'fa fa-edit',
            '/hummingbird/templates-new/' => 'fa fa-code',
            '/hummingbird/categories/' => 'fa fa-sitemap',
            '/hummingbird/tags/' => 'fa fa-tags',
            '/hummingbird/media/' => 'fa fa-image',
            '/hummingbird/users/' => 'fa fa-users',
            '/hummingbird/roles/' => 'fa fa-eye-slash',
            '/hummingbird/settings/' => 'fa fa-gears',
            '/hummingbird/errors/' => 'fa fa-warning',
            '/hummingbird/redirections/' => 'fa fa-repeat',
            '/hummingbird/activity-logs/' => 'fa fa-history',
            '/hummingbird/menus/' => 'fa fa-sitemap',
            '/hummingbird/plugins/' => 'fa fa-plug',
            '/hummingbird/events/' => 'fa fa-calendar',
            '/hummingbird/blog/' => 'fa fa-edit',
            '/hummingbird/modules/' => 'fa fa-cubes',
            '/hummingbird/module-templates/' => 'fa fa-cubes',
            '/hummingbird/widgets/' => 'fa fa-th-large',
            '/hummingbird/themes/' => 'fa fa-newspaper-o',
            '/hummingbird/updates/' => 'fa fa-cloud-download'
        );
    }

    /**
     * Temporary model link for the CMS. Used for permissions
     *
     */ 
    public function assign_cms_types()
    {
        $this->cms_types = array(
            '404 Log' => 'Error',
            'Activity Log' => 'Activitylog',
            'Blog' => 'Blog',
            'Categories' => 'Categories',
            'Dashboard' => TRUE, //available to all
            'Events' => 'Events',
            'Media Centre' => 'Media',
            'Menus' => 'Menu',
            'Module Templates' => 'Moduletemplate',
            'Modules' => 'Module',
            'Pages' => 'Page',
            'Plugins' => 'Plugin',
            'Redirects' => 'Redirections',
            'Roles' => 'Role',
            'Tags' => 'Tags',
            'Templates' => 'Template',
            'Themes' => 'Theme',
            'Updates' => NULL, //no model, super user only
            'Users' => 'User',
            'Widgets' => 'Widget',
            'News' => 'Blog',
        );
    }
    
    /**
     * Generate the CMS navigation based on elements installed
     *  
     * @return Array $menu_items Containing results for navigation in groups
     *
     */
    public function get_cms_nav()
    {
        $this->menu_items = array();

        $nav_items = CmsNav::where('live', '=', 1)->whereNull('parent')->whereNull('deleted_at')->orderBy('name', 'ASC')->get();

        if(count($nav_items) > 0)
        {
            foreach($nav_items as $item)
            {
                $link = str_replace("//", "", '/' . $this->backend_url . '/' . $item->link . '/');

                if(Auth::user()->isSuperUser()  OR TRUE === $this->cms_types[$item->name] OR Auth::user()->hasAccess('read', $this->cms_types[$item->name]))
                {
                    $icon = (!isset($this->icons[$link])) ? '':$this->icons[$link];

                    $this->menu_items[$item->section][$item->name] = array(
                        'label' => $item->name,
                        'link' => $link,
                        'icon' => $icon,
                        'children' => array()
                    );

                    $finished_children = false;

                    while(!$finished_children)
                    {
                        $children = CmsNav::where('live', '=', 1)->whereNull('deleted_at')->where('parent', '=', $item->id)->get();

                        if(count($children) > 0)
                        {
                            foreach($children as $child)
                            {
                                $icon = (!isset($this->icons[$link])) ? '':$this->icons[$link];
                                $link = str_replace("//", "", '/' . $this->backend_url . '/' . $child->link . '/');

                                $this->menu_items[$item->section][$item->name]['children'][] = array(
                                    'label' => $child->name,
                                    'link' => $link,
                                    'icon' => $icon
                                );
                            }
                        }

                        $finished_children = true;
                        continue;
                    }
                }
            }
        }

        ksort($this->menu_items);

        return $this->menu_items;
    }
    
    /**
     * Build and return the navigation
     *
     * @param $menu Menu to display
     * @param Array $args Arguments for displaying the navigation
     * @return Outputs, using the PHP buffer, HTML navigation
     *
     */
    public function buildNavigation($menu, Array $args)
    {
        if($menu)
        {
            $menu = Menu::where('id', '=', $menu)->whereNull('deleted_at')->first();

            if(null !== $menu)
            {
                $menu_items = $menu->menuitems;

                if(count($menu_items) > 0)
                {
                    ob_start();

                    /* Wrapper classes */
                    $has_wrapper = (!$args['wrapper']) ? false:true;
                    $wrapper_id = (isset($args['wrapper']['id'])) ? $args['wrapper']['id']:false;
                    $wrapper_classes = (isset($args['wrapper']['classes'])) ? $args['wrapper']['classes']:false;

                    /* list classes */
                    $list_classes = (isset($args['items']['classes'])) ? $args['items']['classes']:false;

                    /* Item classes */
                    $item_classes = (isset($args['items']['classes'])) ? $args['items']['classes']:false;?>

                    <?php if($has_wrapper) {?> <ul <?php if($wrapper_id !== false) echo 'id="'.$wrapper_id.'"';?> class="<?=$wrapper_classes?>"> <?php }?>
                        <?php foreach($menu_items as $item)
                        {
                            $this_list_class = ($this->checkCurrentPage($args, $item->url)) ? ' active':'';?>

                            <li class="<?=$list_classes?> <?=$this_list_class?>"><a href="<?=$item->url?>" class="<?=$item_classes?>"><?=$item->menu_item_name?></a></li>
                        <?php }?>
                    <?php if($has_wrapper) {?> </ul> <?php }?>

                    <?php $content = ob_get_clean();

                    echo $content;
                }
            }
        }
    }

    /**
     * 
     *
     * @param 
     * @param 
     * @return 
     *
     */
    public function checkCurrentPage($args, $url_to_check)
    {
        if($url_to_check == '/' AND !isset($args['current_page']['path'])) return true;

        if($url_to_check == $args['current_page']['path']) return true;

        if(in_array(str_replace("/", "", $url_to_check), $args['current_page_details'])) return true;

        return;
    }

}