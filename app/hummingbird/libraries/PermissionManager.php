<?php namespace Hummingbird\Libraries;

/**
 * Permission builder
 *
 * @package hummingbird
 * @author Daryl Phillips, Tickbox Marketing 2014-2015
 *
 */
class PermissionManager
{
    protected $permission;

    protected $perm_actions;

    /**
     * Standard permissions for CMS include CRUD - WITHOUT READ NO PERMISSION IS AVAILABLE
     * C = CREATE
     * R = READ
     * U = UPDATE
     * D = DELETE
     */
    protected $standard_actions = array('CREATE', 'READ', 'UPDATE', 'DELETE');

    /**
     * 
     */
    public function __construct()
    {
        $this->permission = new \Permission();

        $this->perm_actions = $this->permission->getActions();
    }

    /**
     * Standard permission actions, CRUD, are installed
     * @param 
     */
    public function install()
    {
        foreach($this->standard_actions as $action)
        {
            (new \PermissionAction)->insert($action);
        }
    }

    /**
     * Bespoke permissions required for CMS
     * @param 
     */
    public function install_actions($actions)
    {
        if(count($actions) > 0)
        {
            foreach($actions as $action)
            {
                (new \PermissionAction)->insert($action);
            }
        }
    }

    /**
     * 
     * @param 
     * @return 
     */
    public function install_permissions(Array $data)
    {
        $perms = $data['perms'];

        if(count($perms) > 0)
        {
            /* Create type - if not installed */
            $perm_type = (new \PermissionType)->insert($data, true);

            /* Insert all actions (if not created) */
            $insert_data = array();

            /* loop through all permissions needed and create insert array */
            foreach($perms as $perm)
            {   
                /* is this action installed? */
                $perm_action = (new \PermissionAction)->action($perm)->take(1)->first();
                
                /* NO action - create one */
                if(!$perm_action)
                {
                    $perm_action = (new \PermissionAction)->insert($action, true);
                }

                /* Add to insert array */
                $insert_data[] = array(
                    'perm_type' => $perm_type->id,
                    'perm_action' => $perm_action->id
                );
            }

            /* Insert permissions into xref */
            \DB::table('perm_types_actions_xref')->insert($insert_data);
        }
    }

    /**
     * 
     * @param 
     * @return 
     */
    public function checkPermissionExists($action)
    {
        if(null !== $this->perm_actions[$action]) return true;
    }


    public function get_standard_actions()
    {
        return $this->standard_actions;
    }
}

?>