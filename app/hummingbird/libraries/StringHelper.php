<?php
	/**
	 * A library to allow String manipulation
	 *
	 * @author Daryl Phillips
	 * @copyright Copyright &copy; 2015, Tickbox Marketing
	 */
	class StringHelper
	{
		/**
		 * Allow a string to be wrapped with a delimiter
		 *
		 * @param String $str 	String to edit
		 * @param String $delimiter 	Standard (front and end) delimiter
		 * @param String/NULL $start_delimiter 	Starting delimiter, if different
		 * @param String/NULL $end_delimiter Ending delimiter, if different
		 * @return String 
		*/
		public static function wrap_text($str, $delimiter, $start_delimiter = NULL, $end_delimiter = NULL)
		{
			if($delimiter == '' AND is_null($start_delimiter) AND is_null($end_delimiter)) return $str;
			
			if(!is_null($start_delimiter) AND is_null($end_delimiter)) return $start_delimiter . $str . $delimiter;

			if(is_null($start_delimiter) AND !is_null($end_delimiter)) return $delimiter . $str . $end_delimiter;

			if(!is_null($start_delimiter) AND !is_null($end_delimiter)) return $start_delimiter . $str . $end_delimiter;

			return $delimiter . $str . $delimiter;
		}
	}
?>