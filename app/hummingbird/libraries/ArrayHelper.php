<?php 

/**
 * Array Helper
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class ArrayHelper
{
    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function cleanExplodedArray(Array $exploded_items, $remove_items = false)
    {
    	$new_data = array();

    	if(count($exploded_items) > 0)
    	{
    		foreach($exploded_items as $item)
    		{
    			if($remove_items !== false)
    			{
    				$item = str_replace($remove_items, "", $item);
    			}

    			if($item != '') $new_data[] = trim($item);
    		}
    	}

    	return $new_data;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function object_to_array($object) 
    {
        return (array) $object;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function array_to_object($array)
    {
        return (object) $array;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function sort($sort_func, &$array)
    {
        // dd($sort_func);
        $new_array = $array;
        $sort_func($new_array);

        // General::pp($array, true);

        return $new_array;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function csv_to_array($filename = '', $delimiter = ',')
    {
        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;
     
        $header = NULL;
        $data = array();
        
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function get_array_key_by_value($items, $value, $field = '')
    {
        if(!is_array($items)) return false;

        foreach($items as $key => $item)
        {
            if($field == '')
            {
                if(in_array($value, $items[$key]))
                {
                    return $key;
                }
            }
            else if ( isset($item[$field]) AND $item[$field] === $value )
            {
                return $key;
            }
        }

        return false;
    }
}
?>