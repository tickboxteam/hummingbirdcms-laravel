<?php

/*
 * Class dedicated to installing a new site
 * 
 */
class Installer
{
    private $db_host;
    private $db_name;
    private $db_user;
    private $db_password;
    private $db_prefix;
    
    public $errors = array();
    
    public function setupCredentials($data)
    {        
        $rules = array(
            'db_host' => 'required',
            'db_name' => 'required',
            'db_user' => 'required',
            'db_password' => 'required',
            'db_prefix' => 'alpha_dash'
        );
        
        if($this->valid($data, $rules)) {
            if($this->connectToDb()) {
                return $this->setupEnvironmentFile($data);
            }
        }
        
        return false;
    }
    
    public function has_errors()
    {
        return count($this->errors) > 0;
    }
    
    public static function isInstalled()
    {
        return Config::get('HummingbirdBase::hummingbird.install');
    }
    
    public static function uri()
    {
        return Config::get('HummingbirdBase::hummingbird.installURI');
    }
    
    private function valid($data, $rules, $messages = array())
    {        
        $validator = Validator::make($data, $rules, $messages);        
        $this->errors = $validator->messages();
        
        return $validator->passes();
    }
    
    private function connectToDb()
    {
        // todo: quick test to see if that works 
        // http://fideloper.com/laravel-multiple-database-connections
        // http://stackoverflow.com/questions/14524181/laravel-4-multiple-tenant-application-each-tenant-its-own-database-and-one-gl
        return true;
    }
    
    public static function get_types()
    {
        return array(
            'standard' => 'Standard (Install with commonly used features pre-configured)',
            'advanced' => 'Advanced (Select your plugins/modules)'
        );
    }
    
    private function setupEnvironmentFile($data)
    {        
        $contents = "<?php

            return array(

                'DB_HOST' => '".$data['db_host']."',
                'DB_NAME' => '".$data['db_name']."',
                'DB_USER' => '".$data['db_user']."',
                'DB_PASSWORD' => '".$data['db_password']."',
                'DB_PREFIX' => '".$data['db_prefix']."',
            );";

        $filename = (App::environment() == 'production' OR App::environment() == '') ? '.env.php' : '.env' . App::environment(). '.php';
        
        return File::put(public_path($filename), $contents);
    }
    
    public function runMigrations()
    {        
        try {
            Artisan::call('dump-autoload');

            // create core cms tables
            CmsAreas::install();

            Artisan::call('db:seed');

            App::make("UpdatesController")->versionsBehind(TRUE);

            $this->data['settings'] = Setting::where('key', '=', 'system_update')->first();

            /* Seed new data */
            if(class_exists('SystemUpdateSeeder')) (new SystemUpdateSeeder('1.0.0', $this->data['settings']->value['latest_version'], $this->data['settings']->value['versions'], $this->data['settings']->value['behind']))->run();
        } catch (Exception $e) {
            // rollback
            die($e);
            Artisan::call('migrate:reset');
            return false;
        }
        
        return true;
    }
    
    public function runPluginMigrations($inputs)
    {
        try {
            
            Plugins::install($inputs['plugins']);
            
        } catch (Exception $e) {
            // rollback
            
            return false;
        }
        
        return true;
    }
    
    public function complete()
    {
        $current_value = Config::get('HummingbirdBase::hummingbird.install');
        FileHelper::replaceContents("'install' => false", "'install' => true", app_path() . '/hummingbird/config/hummingbird.php');
    }
    
    public function updateSiteSettings($data)
    {
        $rules = array(
            'site_name' => 'required',
            'site_url' => 'required|regex:/^[a-zA-Z0-9_\\-\\.]*$/',
            'site_cms_url' => 'required|alpha',
            'site_key' => 'required',
            'site_password_strength' => 'required|integer',
            'site_installtype' => 'required',
            'site_timezone' => 'required'
        );
        
        if($this->valid($data, $rules)) {
            
            $app_config = app_path().'/config/app.php';
            $hummingbird_config = app_path().'/hummingbird/config/hummingbird.php';
            
            FileHelper::replaceContents("'site_name' => '".Config::get('HummingbirdBase::hummingbird.site_name')."'", "'site_name' => '".$data['site_name']."'", $hummingbird_config);
            FileHelper::replaceContents("'url' => '".Config::get('app.url')."'", "'url' => 'http://".$data['site_url']."'", $app_config);
            if($data['site_cms_url'] != '') {
                FileHelper::replaceContents("'backendURI' => '".Config::get('HummingbirdBase::hummingbird.backendURI')."'", "'backendURI' => '".$data['site_cms_url']."'", $hummingbird_config);
            }
            FileHelper::replaceContents("'key' => '".Config::get('app.key')."'", "'key' => '".$data['site_key']."'", $app_config);
            FileHelper::replaceContents("'passwordstrength' => '".Config::get('HummingbirdBase::hummingbird.passwordstrength')."'", "'passwordstrength' => '".$data['site_password_strength']."'", $hummingbird_config);
            if($data['site_timezone'] != '') {
                FileHelper::replaceContents("'timezone' => '".Config::get('app.timezone')."'", "'timezone' => '".$data['site_timezone']."'", $app_config);
            }
            
            // the below is refusing to persist (it has worked in the past)
            Session::put('installtype', $data['site_installtype']);
            
            return true;
        }
        
        return false;
    }
    
    public function addAdministrator($data)
    {        
        $superadmin_role = Role::where('name', '=', 'superadmin')->first();
        
        $user = (new User)->fill($data);
        $user->password_confirmation = $data['password_confirmation'];
        $user->status = 'active';
        $user->role_id = $superadmin_role->id;
        $user->hidden = 1;
        
        if(!$user->save(User::getRules('create'))) {
                      
            $this->errors = $user->errors();
            return false;
        }
        
        return true;
    }
    
    public function checkFilePermissions()
    {
        // directories to check: storage and uploads
        $writable_directories = array(base_path().'/uploads', app_path().'/storage');
        
        foreach($writable_directories as $ind => $dir) {
            if (FileHelper::is_really_writable($dir)) {
                unset($writable_directories[$ind]);
            }
        }
        
        if(!empty($writable_directories)) {
            $this->errors['directories'] = $writable_directories;
            dd($writable_directories);
            return false;
        }
        
        return true;
    }
}

?>