<?php


class CmsAreas
{
    public static function all()
    {
        return FileHelper::directories(app_path() . '/hummingbird');
    }
    
    public static function install()
    {
        // $plugins = self::all();
        
        // foreach($plugins as $plugin) {
        //     Artisan::call('migrate', [
        //         '--path'=>"/app/hummingbird/$plugin/migrations"
        //     ]);
        // }

        Artisan::call('migrate', [
            '--path'=>"/app/hummingbird/database/migrations"
        ]);
    }
}

?>
