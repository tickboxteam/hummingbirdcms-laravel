<?php
	/**
	 * Produces random strings of characters.
	 *
	 * Gibberish is created, dependent on different conditions.  Basic use involves the method
	 * {@link gibber} to form a string, of a fixed length, comprised of characters from
	 * {@link $input_array}.  Other methods are provided to customise the characters in $input_array
	 * and to choose an encoding type for the output string.
	 *
	 * @author Chris Throup
	 * @copyright Copyright &copy; 2004, Chris Throup
	 */
	class Lipsum {
		/**
		 * Contains valid characters to be selected from for the outputted gibberish.
		 *
		 * Structured as an array with each character represented by its uncode value.
		 *
		 * @var array
		 */
		var $input_array;
		/**
		 * Contains characters selected as output gibberish.
		 *
		 * Structured as an array with each character represented by its uncode value.
		 *
		 * @var array
		 */
		var $output_array;
		/**
		 * Output gibberish.
		 *
		 * Produced by the (@link encode_none) and other encode_ methods from {@link $output_array}.
		 *
		 * @var string
		 */
		var $output_string;
		/**
		 * Determines the form of encoding {@link $output_string} will be subject to.
		 *
		 * <p>Used when embedding the output in different contexts, for example HTML, URLs,
		 * text files or dynamic images.</p>
		 * <p>Choice of values are:</p>
		 * <ul>
		 *   <li><kbd>none</kbd> - no encoding.</li>
		 *   <li><kbd>special</kbd> - the bare minimum of characters will be encoded with HTML entities.</li>
		 *   <li><kbd>all</kbd> - all characters with a know HTML entity will be encoded.</li>
		 *   <li><kbd>hex</kbd> - all characters will be encoded as HTML numeric entities with hexadecimal values.</li>
		 *   <li><kbd>dec</kbd> - all characters will be encoded as HTML numeric entities with decimal values.</li>
		 *   <li><kbd>rawurl</kbd> - encodes characters for use in a URL, following the {@link http://www.ietf.org/rfc/rfc1738.txt RFC 1738} standard.</li>
		 *   <li><kbd>url</kbd> - encodes characters for use in a URL, similar to <kbd>rawurl</kbd>, but space characters are replaced with + instead of %20.</li>
		 * </ul>
		 *
		 * @var string
		 */
		var $output_encoding;
		/**
		 * The desired length of the output gibberish.
		 *
		 * @var int
		 */
		var $output_length;
		
		/**
		 * Constructor method to assign default states to object properties.
		 *
		 * Expect all properties to be overridden by user, but need to be predefined.
		 */
		function Gibberish() {
			$this->input_array = array();
			$this->output_array = array();
			$this->output_string = '';
			$this->output_encoding = 'none';
			$this->output_length = 1;
		}
		
		/**
		 * Sets object property {@link $input_array} based on $input_variable.
		 *
		 * Method can cope with $input_variable containing an array, a string or a number.
		 * The variable contents are converted to a numerical array (if necessary) amd
		 * stored in {@link $input_array}.
		 *
		 * @param mixed $input_variable
		 * @return array contents of {@link $input_array} set by this function call
		 */
		function set_input_array($input_variable) {
			array_splice($this->input_array, 0);
			if (is_array($input_variable)) {
				for ($count = 0; $count < sizeof($input_variable); $count++) {
					if (is_string($input_variable[$count])) {
						$this->input_array[$count] = ord($input_variable[$count][0]);
					} else {
						$this->input_array[$count] = $input_variable[$count];
					}
				}
			} elseif (is_string($input_variable)) {
				for ($count = 0; $count < strlen($input_variable); $count++) {
					$this->input_array[$count] = ord($input_variable[$count]);
				}
			} else {
				$this->input_array[0] = (int) $input_variable;
			}
			return $this->input_array;
		}
		
		/**
		 * Sets the encoding method to be used for the output gibberish.
		 *
		 * If $input_encoding contains a valid encoding method, {@link $output_encoding}
		 * is set appropriately.  Otherwise, a value of <kbd>none</kbd> is used.
		 *
		 * @see $output_encoding
		 * @param string $input_encoding
		 * @return string contents of {@link $output_encoding} set by this function call
		 */
		function set_output_encoding($input_encoding) {
			switch (strtolower($input_encoding)) {
				case 'none':
				case 'special':
				case 'all':
				case 'hex':
				case 'dec':
				case 'url':
				case 'rawurl':
					$this->output_encoding = strtolower($input_encoding);
					break;
				default:
					$this->output_encoding = 'none';
					break;
			}
			return $this->output_encoding;
		}
		
		/**
		 * Sets the length of gibberish to be output.
		 *
		 * If $input_length contains a string, {@link $output_length} is set to its length.
		 * Otherwise $input_encoding is cast to an integer (if necessary) and {@link $output_length}
		 * is set to ths value.
		 *
		 * @param string $input_length
		 * @return int contents of {@link $output_length} set by this function call
		 */
		function set_output_length($input_length) {
			if (is_string($input_length)) {
				$this->output_length = strlen($input_length);
			} else {
				$this->output_length = (int) $input_length;
			}
			return $this->output_length;
		}
		
		/**
		 * Returns the contents of {@link $input_array}.
		 *
		 * @return array contents of {@link $input_array}
		 */
		function get_input_array() {
			return $this->input_array;
		}
		
		/**
		 * Returns the contents of {@link $output_array}.
		 *
		 * @return array contents of {@link $output_array}
		 */
		function get_output_array() {
			return $this->output_array;
		}
		
		/**
		 * Returns a string equivalent to the contents of {@link $input_array}.
		 *
		 * @return string contents of {@link $input_array} converted to a string
		 */
		function get_input_string() {
			$input_string = '';
			foreach ($this->input_array as $char) {
				$input_string .= sprintf('%c', $char);
			}
			return $input_string;
		}
		
		/**
		 * Returns the last output_string set by an encode_ method.
		 *
		 * @return string contents of {@link $output_string}
		 */
		function get_output_string() {
			return $this->output_string;
		}
		
		/**
		 * Returns the currently set encoding method for output gibberish.
		 *
		 * @return string contents of {@link $output_encoding}
		 */
		function get_output_encoding() {
			return $this->output_encoding;
		}
		
		/**
		 * Returns the currently set length of output gibberish.
		 *
		 * @return int contents of {@link $output_length}
		 */
		function get_output_length() {
			return $this->output_length;
		}
		
		// Main gibber method
		function gibber() {
			for ($count = 0; $count < $this->output_length; $count++) {
				$this->output_array[$count] = $this->input_array[mt_rand(0, sizeof($this->input_array) - 1)];
			}
			switch ($this->output_encoding) {
				case 'none':
				case 'special':
				case 'all':
				case 'hex':
				case 'dec':
				case 'url':
				case 'rawurl':
					$function = "encode_{$this->output_encoding}";
					$this->$function();
					break;
				default:
					$this->encode_none();
			}
			return $this->output_string;
		}
		
		// Methods to set input_array
		function gibber_alpha() {
			$output_array = array();
			for ($count = 0x0041; $count <= 0x007a; $count++) {
				if ((($count >= 0x0041) && ($count <= 0x005a)) || (($count >= 0x0061) && ($count <= 0x007a))) {
					$output_array[] = $count;
				}
			}
			$this->set_input_array($output_array);
			return $this->gibber();
		}

		function gibber_caps() {
			$output_array = array();
			for ($count = 0x0041; $count <= 0x005a; $count++) {
				$output_array[] = $count;
			}
			$this->set_input_array($output_array);
			return $this->gibber();
		}

		function gibber_nocaps() {
			$output_array = array();
			for ($count = 0x0061; $count <= 0x007a; $count++) {
				$output_array[] = $count;
			}
			$this->set_input_array($output_array);
			return $this->gibber();
		}

		function gibber_num() {
			$output_array = array();
			for ($count = 0x0030; $count <= 0x0039; $count++) {
				$output_array[] = $count;
			}
			$this->set_input_array($output_array);
			return $this->gibber();
		}

		function gibber_alphanum() {
			$output_array = array();
			for ($count = 0x0030; $count <= 0x007a; $count++) {
				if ((($count >= 0x0030) && ($count <= 0x0039)) || (($count >= 0x0041) && ($count <= 0x005a)) || (($count >= 0x0061) && ($count <= 0x007a))) {
					$output_array[] = $count;
				}
			}
			$this->set_input_array($output_array);
			return $this->gibber();
		}

		function gibber_capsnum() {
			$output_array = array();
			for ($count = 0x0030; $count <= 0x005a; $count++) {
				if ((($count >= 0x0030) && ($count <= 0x0039)) || (($count >= 0x0041) && ($count <= 0x005a))) {
					$output_array[] = $count;
				}
			}
			$this->set_input_array($output_array);
			return $this->gibber();
		}

		function gibber_nocapsnum() {
			$output_array = array();
			for ($count = 0x0030; $count <= 0x007a; $count++) {
				if ((($count >= 0x0030) && ($count <= 0x0039)) || (($count >= 0x0061) && ($count <= 0x007a))) {
					$output_array[] = $count;
				}
			}
			$this->set_input_array($output_array);
			return $this->gibber();
		}
		
		function gibber_mixed() {
			$output_array = array();
			for ($count = 0x0021; $count <= 0x007e; $count++) {
				$output_array[] = $count;
			}
			$this->set_input_array($output_array);
			return $this->gibber();
		}
		
		function gibber_symbols() {
			$output_array = array();
			for ($count = 0x0021; $count <= 0x007e; $count++) {
				if ((($count >= 0x0021) && ($count <= 0x0029)) || ($count == 0x0040) || (($count >= 0x005b) && ($count <= 0x0060)) || (($count >= 0x007b) && ($count <= 0x007e))) {
					$output_array[] = $count;
				}
			}
			$this->set_input_array($output_array);
			return $this->gibber();
		}
		
		// Encoding methods
		function encode_special() {
			$output_string = '';
			for ($count = 0; $count < $this->output_length; $count++) {
				$output_string .= sprintf('%c', $this->output_array[$count]);
			}
			$this->output_string = htmlspecialchars($output_string, ENT_QUOTES);
			return $this->output_string;
		}
		
		function encode_all() {
			$output_string = '';
			for ($count = 0; $count < $this->output_length; $count++) {
				$output_string .= sprintf('%c', $this->output_array[$count]);
			}
			$this->output_string = htmlentities($output_string, ENT_QUOTES);
			return $this->output_string;
		}
		
		function encode_hex() {
			$output_string = '';
			for ($count = 0; $count < $this->output_length; $count++) {
				$output_string .= sprintf('&#x%04x;', $this->output_array[$count]);
			}
			$this->output_string = $output_string;
			return $this->output_string;
		}
		
		function encode_dec() {
			$output_string = '';
			for ($count = 0; $count < $this->output_length; $count++) {
				$output_string .= sprintf('&#%03d;', $this->output_array[$count]);
			}
			$this->output_string = $output_string;
			return $this->output_string;
		}
		
		function encode_url() {
			$output_string = '';
			for ($count = 0; $count < $this->output_length; $count++) {
				$output_string .= sprintf('%c', $this->output_array[$count]);
			}
			$this->output_string = urlencode($output_string);
			return $this->output_string;
		}
		
		function encode_rawurl() {
			$output_string = '';
			for ($count = 0; $count < $this->output_length; $count++) {
				$output_string .= sprintf('%c', $this->output_array[$count]);
			}
			$this->output_string = rawurlencode($output_string);
			return $this->output_string;
		}

		function encode_none() {
			$output_string = '';
			for ($count = 0; $count < $this->output_length; $count++) {
				$output_string .= sprintf('%c', $this->output_array[$count]);
			}
			$this->output_string = $output_string;
			return $this->output_string;
		}
	}
?>