<?php 

/**
 * Database helper functions for Hummingbird3
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class DatabaseHelper
{
    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public static function getDatabaseQueries($limit = false, $output = false, $order = 'desc')
    {
        $last_queries = DB::getQueryLog();

        // we have a limit of queries to show
        if((is_numeric($limit) AND $limit > 0) OR $limit == 'all')
        {
            $limit = ($limit == 'all') ? count($last_queries):$limit; //do we have a new limit

            /* Order of database items */
            switch($order)
            {
                case 'desc':
                    $last_queries = array_reverse($last_queries);
                    break;
            }          

            for($i = 0; $i < $limit; $i++)
            {
                General::pp($last_queries[$i]);
            }  
        }
        else
        {
            if(!$output)
            {
                /* Show last query only */
                General::pp(end($last_queries));
            }
            else
            {
                // $last_query = end($last_queries);
                // $query      = $last_query['query'];
                // $bindings   = $last_query['bindings'];

                // foreach($bindings as $binding)
                // {
                //     preg_replace('/(?)/', $binding, $query, 1);
                // }

                // echo $query;die();
            }
        }   
    }
}