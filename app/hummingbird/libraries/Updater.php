<?php 

class Updater
{
	protected $api				= 'aHR0cHM6Ly9iaXRidWNrZXQub3JnL2FwaS8xLjAvcmVwb3NpdG9yaWVzL3RpY2tib3h0ZWFtL2h1bW1pbmdiaXJkY21zLWxhcmF2ZWwvdGFncy8=';
	protected $master 			= 'aHR0cHM6Ly9iaXRidWNrZXQub3JnL3RpY2tib3h0ZWFtL2h1bW1pbmdiaXJkY21zLWxhcmF2ZWwvZ2V0L21hc3Rlci56aXA/eD0xNDU1Nzk2ODU2';
	protected $backup_path;
	protected $update_path;
	protected $database_migration_path;
	protected $current_version;
	protected $core_protected_folders 	= ['/app/storage', '/app/config', '/themes/public', 'app/local'];
	protected $core_protected_files	 	= ['.env', '.git', '.htaccess', 'robots.txt', 'composer.lock', '/app/hummingbird/config/hummingbird.php', '/app/hummingbird/controllers/UpdatesController.php', '/app/hummingbird/libraries/Updater.php', 'updates.blade.php', 'SystemUpdateSeeder.php', 'v101.php'];
	protected $core_move_directories 	= ['/themes/hummingbird'];
	protected $rollback_directories 	= ['/backups', '/uploads'];
	protected $clean_up_files 			= ['app/hummingbird/controllers/UpdatesController.php', 'app/hummingbird/libraries/Updater.php'];

	public $compress 			= TRUE;
	public $debug				= FALSE;
	public $messages			= [];

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function __construct()
	{
		$this->api 				= base64_decode($this->api);
		$this->master 			= base64_decode($this->master);
		$this->current_version 	= Config::get('HummingbirdBase::hummingbird.version');
		$this->latest_version   = NULL;
		$this->backup_path 		= base_path() . '/backups';
		$this->update_path 		= base_path() . '/updates';
		$this->database_migration_path = substr(HUMMINGBIRD_PATH.'/', strpos(HUMMINGBIRD_PATH.'/', '/app/')) . 'database/migrations';
		$this->allow_backup 	= TRUE;

		/* Store DB credentials */
		$this->setDatabaseCredentials();

		/* Check backup folder exists */
		if (!File::exists($this->backup_path))
		{
			File::makeDirectory($this->backup_path, 0775, true);
		}

		/* Check update folder exists */
		if (!File::exists($this->update_path))
		{
			File::makeDirectory($this->update_path, 0775, true);
		}
	}

    /**
     * Get the API for checking the latest tags available
     *
     * @return String $api
     *
     */ 
	public function getAPI()
	{
		return $this->api;
	}

    /**
     * Get the GIT Master location for latest updates
     *
     * @return String $master
     *
     */ 
	public function getMaster()
	{
		return $this->master;
	}

    /**
     * Get the GIT Master location for latest updates
     *
     * @return String $master
     *
     */ 
	public function getUpdatePath()
	{
		return $this->update_path;
	}

    /**
     * Get messages from the Updater
     *
     * @param String $key
     * @return Array $messages
     *
     */ 
	public function getMessages($key = '')
	{
		return ($key != '' AND isset($this->messages[$key])) ? $this->messages[$key]:NULL;
	}

    /**
     * Clear messages
     *
     */ 
	public function clearMessages()
	{
		$this->messages = [];
	}

    /**
     * Retreive the latest core versions from repository. Sends back a list of tags of the latest releases.
     * QUERY_STRING x to make the repository is up-to-date
     *
     * @return Array Tags of updates from the repositories
     *
     */ 
	public function getCoreVersions($current_version, $all_versions = FALSE)
	{
		$listed_versions = array();
		$version_keys = array();
		$versions = array();

		$versions_uo = (array) json_decode(file_get_contents($this->getAPI() . '?x=' . time()));

		foreach($versions_uo as $version_key => $version)
		{
			$versions[] = $version_key;
		}

		natsort($versions);
		$versions = array_reverse($versions);
		$this->latest_version = key($versions);

		foreach($versions as $version_id)
		{
			$listed_versions[$version_id] = $versions_uo[$version_id];

			if(strpos($version_id, $current_version) !== FALSE AND !$all_versions) break;
		}

		return $listed_versions;
	}

    /**
     * Set the database crendentials. Used for creating a database backup later
     *
     */ 
	public function setDatabaseCredentials()
	{
		$this->database = array(
	        'host' => 'localhost',
	       	'database-name' => getenv('DB_NAME'),
	        'database-username' => getenv('DB_USER'),
	        'database-password' => getenv('DB_PASSWORD')
		);
	}

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function setCurrentBackupDirectory($location)
	{
		if($location != '') $this->currentBackupDirectory = $location;
	}

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function removeCurrentBackupDirectory()
	{
		if(!isset($this->currentBackupDirectory)) return; 

		if($this->currentBackupDirectory != '')
		{
			if(File::exists($this->currentBackupDirectory)) File::deleteDirectory($this->currentBackupDirectory);
		}
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function getThemeVersions()
	{
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function getPluginVersions()
	{
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function getHookVersions()
	{
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function run()
	{
		$this->backup_folder = str_replace("$", "", substr(Hash::make(time()), 0, 10));

		File::makeDirectory($this->backup_path . "/" . $this->backup_folder, 0775, true);
		File::makeDirectory($this->backup_path . "/" . $this->backup_folder . "/database", 0775, true);
		
		$this->backup_database();
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function run_command($command)
	{
		exec($command);
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function backup_site($backup_name)
	{
		$backup_name = base_path() . "/$backup_name";
		$output = [];

		// exec("tar -zcvf $backup_name --exclude='backups' --exclude='uploads' --exclude='*.svn*' --exclude='*.git*' --exclude='*Updater.php*' --exclude='*UpdatesController.php*' " .base_path(), $output);
		exec("tar -zcvf " . escapeshellarg($backup_name) . " --exclude='backups' --exclude='uploads' --exclude='*Updater.php*' --exclude='*UpdatesController.php*' --exclude='*.svn' --exclude='*.git*' --exclude='*.files.tar.gz*'  --exclude='app/storage' -C " . escapeshellarg(base_path()) . " .", $output);

		return $output;
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function backup_database($location = '')
	{
		/* Set database path */
        $backupPath = ($location == '') ? $this->backup_path . "/" . $this->backup_folder . "/database/export.sql":$location;
        	
        /* Backup commands */
        $command =  "/Applications/MAMP/Library/bin/mysqldump -u" . $this->database['database-username'] . " -p" . $this->database['database-password'] . " " . $this->database['database-name'] . " > " . $backupPath;
        $this->run_command($command);

        /* check file has been created */
        if(!File::exists($backupPath)) return FALSE; //NO EXPORT FAIL, FAIL, FAIL

        /* Compress file */
        $this->compress_file($backupPath);

        return TRUE;
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function reinstate_database($backupPath = '')
	{
		if($backupPath != '')
		{
			if($this->compress AND File::exists($backupPath . '/export.sql.gz'))
			{
				$command = "gunzip -c " . escapeshellarg($backupPath . '/export.sql.gz') . " > " . $backupPath . "/export.sql";
				$this->run_command($command);
			}

			if(File::exists($backupPath . '/export.sql'))
			{
				/* Reinstate commands */
		        $command =  "/Applications/MAMP/Library/bin/mysql -u" . escapeshellarg($this->database['database-username']) . " -p" . escapeshellarg($this->database['database-password']) . " " . $this->database['database-name'] . " < " . escapeshellarg($backupPath . '/export.sql');
		        $this->run_command($command);

		        return TRUE;
	        }

	        return FALSE;
        }

        return FALSE;
	}

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function backup_themes()
	{
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function compress_file($location)
	{
		try
		{
			if(File::exists($location))
			{
		        /* Compress */
		        if($this->compress)
		        {
		        	$command = 'gzip -9 ' . $location;
		        	$this->run_command($command);
				}
			}
		}
		catch (Illuminate\Filesystem\FileNotFoundException $exception)
		{
		    die("The file doesn't exist");
		}
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function migrate_files($type = 'core')
	{
		/* remove any temporary files */
		$this->removeUpdateFiles($type);

		/* try and download Master */
		try
		{
		    $contents = file_get_contents($this->getMaster());

			$file = $this->update_path . '/core.zip';

			if(null !== $contents)
			{
			    File::put($file, $contents);

			    if(File::exists($file))
			    {
					$zip = new ZipArchive;
					if ($zip->open($file) === TRUE)
					{
						$directories_created = [];
						$update_location = trim($zip->getNameIndex(0), '/');
						$zip->extractTo(base_path() . '/updates/');

						// Renames the directory
						rename(base_path() . '/updates/' . $update_location, base_path() . '/updates/' . 'core');

						$zip->close();

						if($this->debug) $files = [];

						$filesInFolder = File::allFiles(base_path() . '/updates/' . 'core');

						foreach($filesInFolder as $file)
						{
						    $_file = pathinfo($file);
						    $_file['dirname'] = str_replace(base_path() . '/updates/' . 'core', '', $_file['dirname']);

						    if($this->canMoveFile('core', $_file))
						    {
					    		$updated_file = trim($_file['dirname']);
					    		$move_file = str_replace('/updates/core', '', $updated_file);

					    		$move_to_arr = explode("/", $move_file);

					    		if($move_to_arr[0] == '') unset($move_to_arr[0]);

					    		/* have we recently created this folder */
					    		if(!in_array(base_path() . '/' . implode("/", $move_to_arr), $directories_created))
					    		{
							        if(!File::exists(base_path() . '/' . implode("/", $move_to_arr)))
						        	{
								    	try
								    	{
							        		File::makeDirectory(base_path() . '/' . implode("/", $move_to_arr), 0755, true);
							        		$directories_created[] = base_path() . '/' . implode("/", $move_to_arr); //store this for later
									    }
										catch (Exception $e) 
										{
											if(!isset($this->messages['moved_files'])) $this->messages['moved_files'] = TRUE; //we've created a directory
											$this->messages['files'] = "Problem creating the necessary directories. Please fix your permissions and try again.";
											Log::error($e);
											return FALSE;
										}
									}
								}

								/* Try moving the upgraded file(s) */
						    	try
						    	{
							        File::move($this->update_path . '/core' . $_file['dirname'] . '/' . $_file['basename'], base_path() . $_file['dirname'] . '/' . $_file['basename'], true);

							        if(!isset($this->messages['moved_files'])) $this->messages['moved_files'] = TRUE; //we've moved some files

								    if($this->debug) $files[] = $_file['dirname'] . '/' . $_file['basename'];

								    // Log::info('File &quot;' . $_file['dirname'] . '/' . $_file['basename'] . '&quot; has been migrated');
							    }
								catch (Exception $e) 
								{
									$this->messages['files'] = "There is a problem moving one or more files. Please fix your permissions and try again.";
									Log::error($e);
									return FALSE;
								}
						    }
						}

						Artisan::call('hb3:update');
					}
					else 
					{
						$this->messages['files'] = "Your system currently doesn't support 'ZIP Archives', please contact your system administrator.";
						Log::error($this->messages['files']);
						return FALSE;
					}
			    }
			}
			else
			{
				$this->messages['files'] = "Hummingbird3 updates could not be setup, please contact your system administrator.";
				Log::error($this->messages['files']);
				return FALSE;
			}
		}
		catch (Illuminate\Filesystem\FileNotFoundException $exception)
		{
			$this->messages['files'] = "Hummingbird3 updates could not be downloaded at this time, please try again later.";
			Log::error($this->messages['files']);
			return FALSE;
		}
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function clean_up_new_files($type = 'core')
	{
		$location = $this->update_path . '/core/';
		$new_location = base_path() . '/';

		if(count($this->clean_up_files) > 0)
		{
			foreach($this->clean_up_files as $file)
			{
				if(File::exists($location . $file))
				{
					try
					{
						File::move($location . $file, $new_location . $file, true);
					}
					catch (Exception $e)
					{
						Log::error($e);
					}
				}
			}
		}
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function migrate_database($type = 'core', $current_version, $latest_version, $versions, $behind)
	{
		/* Complete database migrations */
		Log::info("Migrating new database changes for Hummingbird v" . $latest_version);
			Artisan::call('migrate', array("--path" => $this->database_migration_path));
		Log::info("Database changes migrated");

		/* Seed new data */
		if(class_exists('SystemUpdateSeeder')) (new SystemUpdateSeeder($current_version, $latest_version, $versions, $behind))->run();
	}


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function completeUpdate($type = 'core', $data = array())
	{
		switch($type)
		{
			case 'core':
			default:
				/* Update Config file - Physical */
				Config::write("HummingbirdBase::hummingbird.version", $data['version'], 'app/hummingbird/config');
				Config::write("HummingbirdBase::other.build", $data['build'], 'app/hummingbird/config');

				/* Update Config file - Live */
				Config::set("HummingbirdBase::hummingbird.version", $data['version']);
				Config::set("HummingbirdBase::other.build", $data['build']);

				/* Dump composer */
				Artisan::call('hb3:dumpautoload');
				break;
		}
	}

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
	public function removeUpdateFiles($type = 'core')
	{
		switch($type)
		{
			case 'core':
			default:
				File::delete($this->update_path . '/core.zip');
				File::deleteDirectory($this->update_path . '/core');

				if(File::exists($this->update_path . '/.complete'))
				{
					File::delete($this->update_path . '/.complete');
				}
				break;
		}
	}

    /**
     *
     *
     *
     * @param String $type - type of file to move 
     * @param Array $_file - File information
     * @return boolean true|false - If file can be moved
     *
     */ 
    public function canMoveFile($type = 'core', $_file)
    {
    	if(!is_array($_file) OR empty($_file)) return FALSE;

    	switch($type)
    	{
    		case 'core':
    		default:
    			$protected_items = array_merge($this->core_protected_folders, $this->core_protected_files);

    			if(count($protected_items) > 0)
    			{
    				foreach($protected_items as $item)
    				{
						// The !== operator can also be used.  Using != would not work as expected because 
						// the position of 'a' is 0. The statement (0 != false) evaluates to false.
    					if(strpos(trim($_file['dirname'] . '/' . $_file['basename']), trim($item)) !== FALSE) return FALSE;
    				}
    			}

    			return TRUE;
    			break;
    	}

    	return FALSE;
    }

    /**
	*
	*
	*
	* @param String $type - type of file to move 
	* @param Array $_file - File information
	* @return boolean true|false - If file can be moved
	*
	*/ 
    public function reinstateRecentBackup($type = 'core', $data = array(), $status_code = 10)
    {
    	/* NO backup to re-instate - Throw notice */
    	if(!isset($this->currentBackupDirectory) OR !File::exists($this->currentBackupDirectory . '/files.tar.gz') OR (!File::exists($this->currentBackupDirectory . '/database/export.sql.gz') AND !File::exists($this->currentBackupDirectory . '/database/export.sql')))
    	{
    		$this->messages['backup-reinstate-error'] = 'There is no backup to re-instate. Website will remain in maintenance mode until backup has been replaced. Please contact your website administrator.';
    		
    		Log::error($this->messages['backup-reinstate-error']);

    		return FALSE;
    	}

    	/* Proceed with backup re-instating */
    	while($status_code > 0)
    	{
		    /**
			*
			* 30 = Configuration file edits - roll back through (database migrations and file changes)
			* 20 = Database migrations - roll back through (database migrations and file changes)
			* 10 = Failed at directories and files - roll back to backup state (no database changes)
			*/
    		switch($status_code)
    		{
    			case 20:
					Log::info("Rolling database back");

						/* If there is no database just roll back nicely */
						if(!$this->reinstate_database($this->currentBackupDirectory ."/database"))
						{
							Artisan::call('migrate:rollback');
					
							/* Downgrade seed data */
							if(class_exists('SystemUpdateSeeder')) (new SystemUpdateSeeder($data['current_version'], $data['latest_version'], $data['versions'], $data['behind']))->run('downgrade');
						}
					
					Log::info("Database rolled back to previous state");
					$status_code = 10;
					break;
    			case 10:
    				/* Replace all the files inside route */
    				exec("tar -C " . escapeshellarg(base_path()) . " -xvf " . escapeshellarg($this->currentBackupDirectory ."/files.tar.gz"));
    				$status_code = -1;
    				break;
    		}
    	}

    	return TRUE;
    }

    public function cleanUpgrade()
    {
        if(File::exists($this->getUpdatePath() . '/.complete')) File::delete($this->getUpdatePath() . '/.complete');

        /* Move last remaining files as part of a clean up */
        $this->clean_up_new_files();
        Log::info("Last remaining files have been migrated.");

        // remove any temporary files 
        $this->removeUpdateFiles();
        Log::info("HB3 core upgrade files have been removed.");

        /* Session forget */
        Session::forget('clean_upgrade');

        /* Redirect back */
        return Redirect::to(Request::url());
    }
}?>