<?php 

/**
 * Hummingbird3 ModuleParser and Handler
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class ModuleParser
{
    protected $module;

    protected $module_template;

    protected $module_bag;

    protected $loop_tags_pattern = '#\{{(.*?)\}}#';

    protected $loop_settings_pattern = '#\[(.*?)\]#';

    protected $loop_loops_pattern = '#\{(loop:[a-zA-Z0-9\_\:\|\=\,\.\ ]+)\}(.+)\{\/loop\}#';

    protected $loop_loops_tags_pattern = '#\{(.*?)\}#';

    protected $fields = array();

    protected $field_counts;

    protected $characters_to_remove = array("{", "}", "[", "]", ":");

    protected $tabs = false;

    public $tabs_data = array('min' => 1, 'fields_used' => array(), 'fields' => array());

    public function __construct(Moduletemplate $module_template, Module $module, Page $page = null)
    {
        $this->module = $module;

        $this->module_template = $module_template;

        if(isset($page))
        {
            $this->page = $page;
        }

        $this->parseTemplate();
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function parseTemplate()
    {
        $this->cleanTemplate();
        $this->loop_tags();
        $this->loop_loops();
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function cleanTemplate()
    {
        $this->module_template->html = str_replace(array("\r\n", "\r", "\n"), "", $this->module_template->html);
    }


    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function cleanTag($tag, $remove = array())
    {
        $remove = array_merge($remove, $this->characters_to_remove);

        return str_replace($remove, "", $tag);
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function get_var_by_key($key = false)
    {
        if(isset($key))
        {
            return $this->$key;
        }

        return false;
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function loop_tags()
    {
        $loop_tags = array();

        /* get all types */
        preg_match_all($this->loop_tags_pattern, $this->module_template->html, $loop_tags);

        if(count($loop_tags) > 0)
        {   
            $this->field_counts = count($loop_tags[1]);

            foreach($loop_tags[1] as $key => $tag)
            {
                $this->loop_settings($key, $tag);
            }
        }
    }

    public function loop_loops()
    {
        $loops = array();

        /* get all types */
        preg_match_all($this->loop_loops_pattern, $this->module_template->html, $loops);
        // $this->module_template->html = preg_replace($this->loop_loops_pattern, "", $this->module_template->html);

        // return;

        // General::pp($loops, true);

        if(count($loops) > 0)
        {
            foreach($loops[1] as $key => $tag)
            {
                // General::pp($loops[1], true);

                if(strpos($tag, 'loop:tabs') !== false)
                {
                    $this->tabs = true;
                    $loop_tags = array();

                    // General::pp($loops[1], true);

                    $loops_settings = explode("|", $tag);

                    if(count($loops_settings) > 0)
                    {
                        // General::pp($loops_settings, true);

                        foreach($loops_settings as $loop_setting)
                        {
                            if($loop_setting != '')
                            {
                                if(strpos($loop_setting, ":") !== false)
                                {
                                    $loop_info = explode(':', $loop_setting);
                                }
                                else if(strpos($loop_setting, "=") !== false)
                                {
                                    $loop_info = explode('=', $loop_setting);
                                }
                                
                                $this->tabs_data[$loop_info[0]] = $loop_info[1];
                            }
                        }
                    }


                    /* get all types */
                    preg_match_all($this->loop_loops_tags_pattern, $loops[2][$key], $loop_tags);

                    if(count($loop_tags) > 0)
                    {   
                        foreach($loop_tags[1] as $key => $tag)
                        {
                            $original_tag = $tag;
                            $settings_arr = $this->loop_settings($key, $tag, false);
                            $settings = $settings_arr['settings'];

                            if($tag == 'content[title=Summary:default=Summary text here]')
                            {
                                // General::pp($settings, true);
                            }

                            $tag = $settings_arr['tag'];

                            if(!isset($settings['duplicate']) OR $settings['duplicate'] != 'true')
                            {
                                if(isset($this->tabs_data['fields_used']) AND in_array($tag, $this->tabs_data['fields_used']))
                                {
                                    $counter = 1;

                                    while(in_array($tag . '_' . $counter, $this->tabs_data['fields_used']))
                                    {
                                        $counter++;
                                    }

                                    $tag .= '_' . $counter;
                                }

                                $settings['name'] = $settings['id'] = $tag;
                                $settings['default'] = (isset($settings_arr['settings']['default'])) ? $settings_arr['settings']['default']:'';
                                $settings['original_name'] = $original_tag;

                                $this->tabs_data['fields_used'][] = $tag;

                                $this->tabs_data['fields'][][$tag] = array(
                                    'settings' => $settings
                                ); 
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *
     *
     *
     * @param    
     * @return   
     *
     */ 
    public function loop_settings($key, $original_tag, $callback = true)
    {
        $settings_matches = array();
        $settings_arr = array();
        $tag = $original_tag;

        /* check for any settings */
        preg_match_all($this->loop_settings_pattern, $tag, $settings_matches);
        
        if(count($settings_matches[0]) > 0)
        {
            $settings = str_replace(array("[", "]"), "", $settings_matches[0][0]);
            $settings = explode(":", $settings);

            foreach($settings as $setting)
            {
                $field = explode("=", $setting);
                $settings_arr[$field[0]] = $field[1];
                $tag = str_replace($setting, "", $tag);
            } 
        }

        $settings_arr['original_name'] = $original_tag;

        $tag = $this->cleanTag($tag);

        if($callback)
        {
            return $this->addToBag($tag, $settings_arr);
        }
        
        return array('tag' => $tag, 'settings' => $settings_arr);
    }

    public function addToBag($tag, $settings)
    {
        if(!isset($settings['duplicate']) OR (isset($settings['duplicate']) AND $settings['duplicate'] != 'true'))
        {
            if(isset($this->fields) AND in_array($tag, $this->fields))
            {
                $counter = 1;

                while(in_array($tag . '_' . $counter, $this->fields))
                {
                    $counter++;
                }

                $tag .= '_' . $counter;
            }

            $settings['name'] = $settings['id'] = $tag;
            $settings['default'] = (isset($settings['default'])) ? $settings['default']:'';
            $settings['no_output'] = (isset($settings['no_output']) AND $settings['no_output'] == 'true') ? true:false;

            $this->fields[] = $tag;

            $this->module_bag['fields'][][$tag] = array(
                'settings' => $settings
            );
        }
    }

    public function hasTabs()
    {
        return ($this->tabs) ? true:false;
    }


    public function get_field_title($tag, $settings)
    {
        if(isset($settings['title']) AND $settings['title'] != '')
        {
            return $settings['title'];
        }

        return $tag;
    }

    public function get_field_default($tag, $settings, $current_val = '')
    {
        if($current_val == '' AND isset($settings['default']) AND $settings['default'] != '')
        {
            return $current_val;
        }
    }


    /**
     * 
     * 
     *
     * @param    
     * @return   
     *
     */ 
    public function render()
    {
        if(null !== $this->module)
        {
            $module_template = $this->module_template->html;
            $module_data = $this->module->module_data;

            /* Render Static Fields */
            if(count($this->module_bag['fields']) > 0)
            {
                foreach($this->module_bag['fields'] as $items)
                {
                    foreach($items as $tag => $item)
                    {
                        $new_html = '';
                        $replace_tag = $item['settings']['original_name'];

                        if(isset($module_data['structure'][$item['settings']['name']]) AND $module_data['structure'][$item['settings']['name']] != '' AND $item['settings']['no_output'] !== true)
                        {
                            switch($tag)
                            {
                                case 'image':
                                    if(isset($item['settings']['is_bg']) AND $item['settings']['is_bg'] == 'true')
                                    {
                                        $new_html = $module_data['structure'][$item['settings']['name']];
                                    }
                                    else
                                    {
                                        $new_html = '<img src="'.$module_data['structure'][$item['settings']['name']].'" />';
                                    }
                                    break;
                                default:
                                    $new_html = $module_data['structure'][$item['settings']['name']];
                                    break;
                            }
                        }
                        
                        $module_template = str_replace(array("{{" . $replace_tag . "}}", "{" . $replace_tag . "-replica}"), $new_html, $module_template);
                    }
                }
            }

            /* Render loops - standard */
            if($this->hasTabs())
            {
                $tabs_data_live = explode(",", $module_data['structure']['tab_settings']); //explode
                $tabs_order = array();
                $tabs_html = '';

                foreach($tabs_data_live as $item)
                {
                    if(trim($item) != '')
                    {
                        $tabs_order[] = trim($item);
                    }
                }

                $loops = array();

                /* get all types */
                preg_match_all($this->loop_loops_pattern, $module_template, $loops);

                $module_template = str_replace($loops[0][0], "{MODULEDATA}", $module_template);

                $loop_template = $loops[2][0];

                // for($i = 1; $i <= count($this->tabs_data['min']); $i++)
                foreach($tabs_order as $i)
                {
                    $copied_template = $loop_template;

                    foreach($this->tabs_data['fields'] as $items)
                    {                        
                        foreach($items as $tag => $item)
                        {
                            $new_html = '';

                            switch($tag)
                            {
                                case 'image':
                                    if(isset($item['settings']['is_bg']) AND $item['settings']['is_bg'] == 'true')
                                    {
                                        $new_html = $module_data['structure']['tab_' . $i . '_' . $item['settings']['name']];
                                    }
                                    else
                                    {
                                        $new_html = '<img src="'.$module_data['structure']['tab_' . $i . '_' . $item['settings']['name']].'" />';
                                    }
                                    break;
                                default:
                                    $new_html = $module_data['structure']['tab_' . $i . '_' . $item['settings']['name']];
                                    break;
                            }

                            $copied_template = str_replace("{".$item['settings']['original_name']."}", $new_html, $copied_template);
                        }
                    }

                    $tabs_html .= $copied_template;
                }

                $module_template = str_replace("{MODULEDATA}", $tabs_html, $module_template);
            }

            if(strpos($_SERVER["HTTP_HOST"], 'bristolhealthpartners') !== false)
            {
                if($this->module->id == 7)
                {
                    $theme = Themes::activeTheme();

                    $events = Events::live()->future()->orderBy('importance', 'DESC')->orderBy('start_date', 'ASC')->take(12)->get();
                    $event_html = '';

                    try
                    {
                        $contents = File::get(base_path() . "/themes/public/$theme/tmp/modules-homepage-events.htm");

                        if(count($events) > 0)
                        {
                            $event_html = '';

                            foreach($events as $event)
                            {
                                $start_date = (time() > $event->start_date) ? time():$event->start_date;
                                $summary = ($event->summary != '') ? $event->summary:substr(strip_tags($event->description), 0, 255);
                                $url = "/events/view/" . DateTimeHelper::date_format($event->start_date, 'Y/m/d') . "/". $event->url . "/". $event->id . "/";
                                $image = General::findImage($event, array('featured_image'), '/themes/public/bhp/img/holding.png');

                                $event_tmp = $contents;
                                $event_tmp = str_replace("[event-url]", $url, $event_tmp);
                                $event_tmp = str_replace("[event-title]", $event->title, $event_tmp);
                                $event_tmp = str_replace("[event-image]", $image, $event_tmp);
                                $event_tmp = str_replace("[event-date]", date("j F Y", $start_date), $event_tmp);
                                $event_tmp = str_replace("[event-summary]", $summary, $event_tmp);
                            
                                $event_html .= $event_tmp;
                            }
                        }
                    }
                    catch (Illuminate\Filesystem\FileNotFoundException $exception)
                    {
                        // $event_html = '';
                        // die($exception);
                    }

                    $module_template = str_replace('{loop-events}', $event_html, $module_template);
                }
                elseif($this->module->id == 8)
                {
                    $theme = Themes::activeTheme();

                    $news_articles = Post::where('post_date', '<=', date("Y-m-d H:i:s"))->where('status', 'public')->live()->whereNotExists(function($query)
                    {
                       $query->select('*')->from('taxonomy_relationships')->whereRaw('taxonomy_relationships.item_id = news.id AND taxonomy_relationships.term_id IN (229)');
                    })
                    ->orderBy('post_date', 'DESC')
                    ->take(9)
                    ->get();

                    $news_html = '';

                    try
                    {
                        $contents = File::get(base_path() . "/themes/public/$theme/tmp/modules-homepage-news.htm");

                        if(count($news_articles) > 0)
                        {
                            $news_html = '';

                            foreach($news_articles as $post)
                            {
                                $url = '/latest-news/' . date("Y/m/d", $post->post_date) . '/' . $post->permalink . '/' . $post->id;

                                $summary = ($post->summary != '') ? trim($post->summary):trim(substr(strip_tags($post->content), 0, 255));
                                $summary = (ends_with($summary, '.')) ? $summary:$summary.'...';
                                $image = General::findImage($post, array('featured_image'), '/themes/public/bhp/img/holding.png');

                                $news_tmp = $contents;
                                $news_tmp = str_replace("[news-url]", $url, $news_tmp);
                                $news_tmp = str_replace("[news-title]", $post->title, $news_tmp);
                                $news_tmp = str_replace("[news-image]", $image, $news_tmp);
                                $news_tmp = str_replace("[news-date]", date("j F Y", $post->post_date), $news_tmp);
                                $news_tmp = str_replace("[news-summary]", $summary, $news_tmp);

                                $news_html .= $news_tmp;
                            }
                        }
                    }
                    catch (Illuminate\Filesystem\FileNotFoundException $exception)
                    {
                        // $news_html = '';
                        // die($exception);
                    }

                    $module_template = str_replace('{loop-news}', $news_html, $module_template);
                }
                else if($this->module->id == 12)
                {
                    $theme = Themes::activeTheme();

                    $news_articles = Post::whereNull('deleted_at')
                                        ->where('post_date', '<=', date("Y-m-d H:i:s"))
                                        ->live()
                                        ->orderBy('post_date', 'DESC')
                                        ->whereHas('taxonomy_relationships', function($query)
                                        {
                                            $query->where('taxonomy_relationships.term_id', '=' , 229);
                                        })  
                                        ->get();

                    $news_html = '';

                    try
                    {
                        $contents = File::get(base_path() . "/themes/public/$theme/tmp/modules-news-roller.htm");

                        if(count($news_articles) > 0)
                        {
                            $news_html = '';

                            foreach($news_articles as $post)
                            {
                                $url = '/latest-news/' . date("Y/m/d", $post->post_date) . '/' . $post->permalink . '/' . $post->id;

                                $summary = ($post->summary != '') ? trim($post->summary):trim(substr(strip_tags($post->content), 0, 255));
                                $summary = (ends_with($summary, '.')) ? $summary:$summary.'...';
                                $image = General::findImage($post, array('featured_image'), '/themes/public/bhp/img/holding.white.png');
                                $authors = (isset($post->author) AND $post->author != '') ? $post->author:false;

                                $news_tmp = $contents;
                                $news_tmp = str_replace("[news-url]", $url, $news_tmp);
                                $news_tmp = str_replace("[news-title]", $post->title, $news_tmp);
                                $news_tmp = str_replace("[news-image]", $image, $news_tmp);
                                $news_tmp = str_replace("[news-summary]", $summary, $news_tmp);

                                if($authors !== false)
                                {
                                    $news_tmp = str_replace("[news-authors]", $authors, $news_tmp);                                    
                                    $news_tmp = str_replace("[no-authors]", "", $news_tmp);
                                }
                                else
                                {
                                    $news_tmp = str_replace("[news-authors]", "", $news_tmp);                                    
                                    $news_tmp = str_replace("[no-authors]", "hide", $news_tmp);
                                }

                                $news_html .= $news_tmp;
                            }
                        }
                    }
                    catch (Illuminate\Filesystem\FileNotFoundException $exception)
                    {
                        // $news_html = '';
                        // die($exception);
                    }

                    $module_template = str_replace('{loop-research-news}', $news_html, $module_template);  
                }
                else if($this->module->id == 13)
                {
                    $theme = Themes::activeTheme();

                    $news_articles = Post::whereNull('deleted_at')
                                        ->where('post_date', '<=', date("Y-m-d H:i:s"))
                                        ->live()
                                        ->orderBy('post_date', 'DESC')
                                        ->whereHas('taxonomy_relationships', function($query)
                                        {
                                            $query->where('taxonomy_relationships.term_id', '=' , 230);
                                        })  
                                        ->get();
                    $news_html = '';

                    try
                    {
                        $contents = File::get(base_path() . "/themes/public/$theme/tmp/modules-research-news-roller.htm");

                        if(count($news_articles) > 0)
                        {
                            $news_html = '';

                            foreach($news_articles as $post)
                            {
                                $url = '/latest-news/' . date("Y/m/d", $post->post_date) . '/' . $post->permalink . '/' . $post->id;

                                $summary = ($post->summary != '') ? trim($post->summary):trim(substr(strip_tags($post->content), 0, 255));
                                $summary = (ends_with($summary, '.')) ? $summary:$summary.'...';
                                $image = General::findImage($post, array('featured_image'), '/themes/public/bhp/img/holding.png');

                                $news_tmp = $contents;
                                $news_tmp = str_replace("[news-url]", $url, $news_tmp);
                                $news_tmp = str_replace("[news-title]", $post->title, $news_tmp);
                                $news_tmp = str_replace("[news-image]", $image, $news_tmp);
                                $news_tmp = str_replace("[news-date]", date("j F Y", $post->post_date), $news_tmp);
                                $news_tmp = str_replace("[news-summary]", $summary, $news_tmp);

                                $news_html .= $news_tmp;
                            }
                        }
                    }
                    catch (Illuminate\Filensystem\FileNotFoundException $exception)
                    {
                        // $news_html = '';
                        // die($exception);
                    }

                    $module_template = str_replace('{loop-research-news-articles}', $news_html, $module_template);  
                }
                else if($this->module->id == 34)
                {
                    $theme = Themes::activeTheme();

                    $twitter = Setting::where('key', '=', 'twitter-feed-cache')->first();
                    $latest_tweets = (null !== $twitter) ? $twitter->value:null;

                    if(null === $latest_tweets OR (isset($latest_tweets['expiry']) AND $latest_tweets['expiry'] < time()))
                    {
                        require base_path() . "/themes/public/$theme/lib/twitter/autoload.php";

                        $twitter_connect = new Abraham\TwitterOAuth\TwitterOAuth('5LgPTBmTF4et8tWyXUap7DFZ1', '14S8WVhDY32U2GBrjraQB3gcD6BhNywDuV16Tu5aAzvWwk3R88', '1857070388-77yGJj8QbQJJ6cnA4NFFoxMGrGfVh684LOOpDRH', 'wfIXEhVwdWHolbsj021gTBQOUr2qonYFPQSzSjx1FusqV');
                        $content = $twitter_connect->get("account/verify_credentials");
                        $tweets = $twitter_connect->get("statuses/user_timeline", array("count" => 5, "exclude_replies" => true));

                        $latest_tweets_arr = array();
                        $latest_tweets_arr['tweets'] = $tweets;
                        $latest_tweets_arr['expiry'] = time()+(60*15); //15 minutes

                        $latest_tweets_arr = (null === $latest_tweets) ? (new Setting)->newSetting('twitter-feed-cache', $latest_tweets_arr):(new Setting)->updateSettings('twitter-feed-cache', $latest_tweets_arr);
                    }
                    else
                    {
                        $tweets = $latest_tweets['tweets'];
                    }

                    try
                    {
                        $contents = File::get(base_path() . "/themes/public/$theme/tmp/twitter-template.htm");

                        if(count($tweets) > 0)
                        {
                            $tweet_html = '';

                            foreach($tweets as $tweet)
                            {
                                $timestamp = strtotime($tweet->created_at);

                                $tweet_html_tmp = $contents;
                                $tweet_html_tmp = str_replace("[date]", DateTimeHelper::date_format($timestamp, 'j F'), $tweet_html_tmp);

                                /* 
                                * REF: http://www.simonwhatley.co.uk/parsing-twitter-usernames-hashtags-and-urls-with-javascript 
                                *
                                    function twitterize($raw_text) {
                                        $output = $raw_text;
                                        
                                        // parse urls
                                        $pattern = '/([A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/.=]+)/i';
                                        $replacement = '$1';
                                        $output = preg_replace($pattern, $replacement, $output);
                                        
                                        // parse usernames
                                        $pattern = '/[@]+([A-Za-z0-9-_]+)/';
                                        $replacement = '@$1';
                                        $output = preg_replace($pattern, $replacement, $output);
                                        
                                        // parse hashtags
                                        $pattern = '/[#]+([A-Za-z0-9-_]+)/';
                                        $replacement = '#$1';
                                        $output = preg_replace($pattern, $replacement, $output);
                                        
                                        return $output;
                                    }
                                */


                                $twitter_post = $tweet->text;

                                // parse urls
                                $pattern = '/([A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/.=]+)/i';
                                $replacement = "<a href=\"$1\" target=\"_blank\">$1</a>";
                                $twitter_post = preg_replace($pattern, $replacement, $twitter_post);
                                
                                // parse usernames
                                $pattern = '/[@]+([A-Za-z0-9-_]+)/';
                                $replacement = "<a href=\"http://twitter.com/$1\" target=\"_blank\">@$1</a>";;
                                $twitter_post = preg_replace($pattern, $replacement, $twitter_post);
                                
                                // parse hashtags
                                $pattern = '/[#]+([A-Za-z0-9-_]+)/';
                                $replacement = "<a href=\"https://twitter.com/hashtag/$1?src=hash\" target=\"_blank\">#$1</a>";
                                $twitter_post = preg_replace($pattern, $replacement, $twitter_post);

                                $tweet_html_tmp = str_replace("[tweet]", $twitter_post, $tweet_html_tmp);

                                $tweet_html .= $tweet_html_tmp;
                            }
                        }
                    }
                    catch (Illuminate\Filensystem\FileNotFoundException $exception)
                    {
                        // $news_html = '';
                        // die($exception);
                    }

                    $module_template = str_replace('{tweets-list}', $tweet_html, $module_template);
                }
                else if($this->module->moduletemplate_id == 9)
                {
                    $return_html = '';
                    $found = false;

                    $related = DB::table('taxonomy_relationships')->whereIn('tax_type', array('Events', 'Post'))->where('term_id', '=', $this->module->module_data['structure']['settings-category-id'])->get();

                    /* Do we have related articles */
                    if(null !== $related)
                    {
                        $data = array();

                        foreach($related as $article)
                        {
                            switch($article->tax_type)
                            {
                                case 'Events':
                                    $event = (new $article->tax_type)->where('id', '=', $article->item_id)->where(function($query)
                                    {
                                        return $query->future();
                                    })->first();

                                    if(null !== $event)
                                    {
                                        $data[$event->start_date][] = $event;    
                                    }

                                    break;
                                case 'Post':
                                    $post   = Post::where('id', '=', $article->item_id)->whereNull('deleted_at')->future()->where('status', '=', 'public')->first();

                                    if(null !== $post)
                                    {
                                        $data[$post->post_date][] = $post;   
                                    }
                                    break;
                            }
                        }

                        if(count($data) > 0) $found = true;
                    }

                    if($found)
                    {
                        krsort($data); //sort by date DESC

                        $theme = Themes::activeTheme();
                        
                        try
                        {
                            $contents = File::get(base_path() . "/themes/public/$theme/tmp/hits-news-events.htm");
                            $limit = (count($data) > $this->module->module_data['structure']['settings-limit']) ? $this->module->module_data['structure']['settings-limit']:count($data);

                            while($limit > 0)
                            {
                                foreach($data as $key => $items)
                                {
                                    foreach($items as $item)
                                    {
                                        $return_html_tmp = $contents;

                                        switch(get_class($item))
                                        {
                                            case 'Events':
                                                $start_date = (time() > $item->start_date) ? time():$item->start_date;
                                                $summary = ($item->summary != '') ? $item->summary:substr(strip_tags($item->description), 0, 255);
                                                $url = "/events/view/" . DateTimeHelper::date_format($item->start_date, 'Y/m/d') . "/". $item->url . "/". $item->id . "/";
                                                $image = General::findImage($item, array('featured_image'), '/themes/public/bhp/img/holding.white.png');

                                                $return_html_tmp = str_replace("[url]", $url, $return_html_tmp);
                                                $return_html_tmp = str_replace("[title]", $item->title, $return_html_tmp);
                                                $return_html_tmp = str_replace("[image]", $image, $return_html_tmp);
                                                $return_html_tmp = str_replace("[date]", date("j F Y", $start_date), $return_html_tmp);
                                                $return_html_tmp = str_replace("[summary]", $summary, $return_html_tmp);

                                                break;
                                            case 'Post':
                                                $url = '/latest-news/' . date("Y/m/d", $item->post_date) . '/' . $item->permalink . '/' . $item->id;

                                                $summary = ($item->summary != '') ? trim($item->summary):trim(substr(strip_tags($item->content), 0, 255));
                                                $summary = (ends_with($summary, '.')) ? $summary:$summary.'...';
                                                $image = General::findImage($item, array('featured_image'), '/themes/public/bhp/img/holding.white.png');

                                                $return_html_tmp = str_replace("[url]", $url, $return_html_tmp);
                                                $return_html_tmp = str_replace("[title]", $item->title, $return_html_tmp);
                                                $return_html_tmp = str_replace("[image]", $image, $return_html_tmp);
                                                $return_html_tmp = str_replace("[date]", date("j F Y", $item->post_date), $return_html_tmp);
                                                $return_html_tmp = str_replace("[summary]", $summary, $return_html_tmp);

                                                break;
                                        } 

                                        $return_html .= $return_html_tmp;
                                        
                                        $limit--; //decrease counter
                                    }
                                }
                            }
                        }
                        catch (Illuminate\Filensystem\FileNotFoundException $exception)
                        {
                            // $news_html = '';
                            // die($exception);
                        }                        
                    }
                    else
                    {
                        $return_html = '<p class="white">No articles could be found.</p>';
                    }
                    
                    $module_template = str_replace('{loop-hit-news-events}', $return_html, $module_template);
                }
                else if($this->module->moduletemplate_id == 10)
                {
                    $return_html    = $module_template;
                    $hits_html      = '';

                    $found = false;

                    $page_mods_cats = array(
                        4 => array('page' => 18, 'module' => 1),
                        3 => array('page' => 16, 'module' => 17),
                        8 => array('page' => 8, 'module' => 3),
                        6 => array('page' => 26, 'module' => 4),
                        7 => array('page' => 28, 'module' => 26),
                        10 => array('page' => 34, 'module' => 5),
                        9 => array('page' => 31, 'module' => 19),
                        11 => array('page' => 36, 'module' => 27),
                        13 => array('page' => 48, 'module' => 21),
                        17 => array('page' => 56, 'module' => 30),
                        14 => array('page' => 50, 'module' => 28),
                        5 => array('page' => 20, 'module' => 18),
                        16 => array('page' => 53, 'module' => 29),
                        19 => array('page' => 60, 'module' => 32),
                        18 => array('page' => 57, 'module' => 31),
                        21 => array('page' => 63, 'module' => 2),
                        20 => array('page' => 62, 'module' => 33),
                        12 => array('page' => 46, 'module' => 20)
                    );

                    $child_cats = Categories::type()->whereNull('deleted_at')->where('parent', '=', 2)->get();

                    if(count($child_cats) > 0)
                    {   
                        $ids = array();

                        foreach($child_cats as $cat)
                        {
                            $ids[] = $cat->id;
                        }

                        $related_pages = DB::table('taxonomy_relationships')->whereIn('term_id', $ids)->where('tax_type', '=', 'Page')->where('item_id', '=', $this->page->id)->get();

                        if(count($related_pages) > 0)
                        {
                            $cats_ids = array();
                            $page_ids = array();

                            foreach($related_pages as $page)
                            {
                                $page_id    = $page_mods_cats[$page->term_id]['page']; 
                                $modu_id    = $page_mods_cats[$page->term_id]['module'];

                                if(!in_array($page_id, $page_ids)) $page_ids[] = $page_id;
                                if(!in_array($page_id, $cats_ids)) $cats_ids[$page_id] = $modu_id;
                            }

                            /* get live pages */ 
                            $pages = Page::where('status', '=', 'public')->whereIn('id', $page_ids)->where('permalink', '!=', $_SERVER['REQUEST_URI'])->get();

                            if(count($pages) > 0)
                            {  
                                /* Get template */
                                $__template = Moduletemplate::whereNull('deleted_at')->where('id', '=', 1)->get()->first();

                                /* We have a template */
                                if(null !== $__template)
                                {
                                    foreach($pages as $page)
                                    {
                                        if(isset($cats_ids[$page->id]))
                                        {
                                            $ModuleBuilder = new ModuleParser($__template, Module::find($cats_ids[$page->id])); //build the ModuleParser
                                            $return_html_tmp = $ModuleBuilder->render();

                                            $hits_html .= $return_html_tmp;
                                        }
                                    }
                                }

                                $return_html = str_replace("{related-hits}", $hits_html, $return_html);
                            }
                            else
                            {
                                $return_html = '';
                            }
                        }
                        else
                        {
                            $return_html = '';
                        }
                    }
                    else
                    {
                        $return_html = '';
                    }

                    $module_template = $return_html;
                }
            }

            return trim($module_template) . trim($this->parseCSS()) . trim($this->parseJS());
        }

        return '';
    }

    public function parseCSS()
    {
        if($this->module_template->css != '')
        {
            return '<style type="text/css">' . $this->module_template->css . '</style>';
        }
    }

    public function parseJS()
    {return;
        if($this->module_template->js != '')
        {
            return '<script>' . $this->module_template->js . '</script>';
        }
    }
}
