<?php

class Themes {

    public static function all() 
    {
        return FileHelper::directories(base_path() . '/themes/public');
    }

    public static function activate($theme) {
        
        // check theme exists
        if (is_dir(base_path() . "/themes/public/$theme")) {
            // set config setting if so. best to set a db setting
            $site_settings = Setting::where('key', '=', 'activeTheme')->first();

            if(!$site_settings)
            {
                $setting = new Setting;
                $setting->key = 'activeTheme';
                $setting->value = serialize(array('theme' => $theme));
                $setting->save();
            }
            else
            {
                $setting = Setting::where('key', '=', 'activeTheme')->first();
                $setting->value = serialize(array('theme' => $theme));
                $setting->save();
            }
        }
    }
    
    public static function activeTheme()
    {
        $setting = Setting::getWhere('key', '=', 'activeTheme');        
        return $setting ? $setting['theme'] : 'default';
    }
    
    public static function themeDescription($theme)
    {
        $file = base_path() . '/themes/public/'.$theme.'/info.json';
    
        if (is_file($file)) {
            $theme_desc = File::get($file); 
            return json_decode($theme_desc);
        }
        
        return false;
    }
}

?>
