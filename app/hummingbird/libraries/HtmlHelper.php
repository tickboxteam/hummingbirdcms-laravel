<?php 

/**
 * HTML Helper functions for Hummingbird3
 *
 * @package hummingbird
 * @author Daryl Phillips
 *
 */
class HtmlHelper
{
    /**
     *
     * 
     *
     *
     */  
    public static function message( $type = NULL , $data = array(), $html = '')
    {
    	if( null !== $type )
    	{
    		ob_start();

    		switch($type)
    		{
    			case 'temp-redirect':?>

		            <h1>This page has been moved, you will be re-directed in a moment</h1>
		            <p>If you are not re-directed please <a href="[url]" target="_blank">click here</a>.</p>

    				<?php break;
    			case '404':?>

    				<h1>404: Page not found</h1>';
    				<p>Sorry, but the page you are looking for has not been found.</p>
       				<p>Try checking the URL for errors, then hit the refresh button on your browser.</p>

    				<?php break;
    		}

    		$html = ob_get_clean();
    	}

    	if(empty($data)) return $html;

    	return HtmlHelper::field_merge($html, $data);
    }

    /**
     *
     * 
     *
     *
     */  
    public static function field_merge( $string, $data )
    {
    	foreach($data as $key => $value)
    	{
    		if(is_array($value)) $string = $this->field_merge($string, $value);

    		$string = str_replace("[$key]", "$value", $string);
    	}

    	return $string;
    }


	public function protectedForm(Array $args)
	{
		if(null === Session::get('login-'.$args['login-to']))
		{
			if(null !== Input::get('login'))
			{
				if($args['username'] != '' AND (Input::get('username') == $args['username']))
				{
					if(Hash::check(Input::get('password'), $args['password']))
					{
						Session::put('login-'.$args['login-to'], time(), 60);
						return true;
					}
					else
					{
						$error = 'Login failed, please try again.';
					}
				}
				else
				{
					$error = 'Login failed, please try again.';
				}
			}

			ob_start();?>

	        <form class="page-protected" action="<?php Request::url();?>" method="post">
	        	<input type="hidden" name="login" value="1" />

	        	<h4>Protected</h4>
	        	<p>Please enter the login details below.</p>

	        	<?php if(isset($error) AND $error != '')
	        	{
	        		echo '<p style="color:red;">' . $error . '</p>';
	        	}?>

	        	<?php if(isset($args['username']) AND $args['username'] == true)
	        	{?>
	        		<label for="username">Username:</label>
	        		<input type="text" id="username" name="username" placeholder="Username" autofocus=""/>
	        	<?php }?>

	        	<label for="password">Password</label>
				<input type="password" id="password" name="password">
				<button type="submit">Sign in</button>
	        </form>

	        <?php return $content = ob_get_clean();
        }
        else
    	{
    		return true;
    	}
	}


    public static function maintenanceMode()
    {
        ob_start();?>

        <h3>Website under maintenance</h3>
        <p>I'm sorry, but the website is temporairly down. Please try again soon.</p>

        <?php return $content = ob_get_clean();
    }
}
?>