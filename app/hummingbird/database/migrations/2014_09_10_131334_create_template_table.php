<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */  
    public function up() {
        Schema::create('templates', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('html');
            $table->text('description');
            $table->tinyInteger('locked')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('templates');
    }

}
