<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('media-items', function($table) 
        {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('filename', 255);
            $table->text('location');
            $table->string('caption', 255);
            $table->text('alt');
            $table->text('description');
            $table->tinyInteger('parent')->nullable();
            $table->tinyInteger('collection')->nullable();
            $table->tinyInteger('mc_only')->nullable();
            $table->tinyInteger('locked')->nullable();
            $table->tinyInteger('is_image')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('media-items');
	}

}
