<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedirectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('redirections', function($table)
        {
            $table->increments('id');
            $table->string('from')->unique();
            $table->string('to');
            $table->string('type');
            $table->string('comments', 255);
            $table->integer('no_used')->default(0);
            $table->datetime('last_used')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('redirections');
	}

}
