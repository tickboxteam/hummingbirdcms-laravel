<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void  
     * `name` varchar(200) NOT NULL,
      `moduletemplate_id` int(11) NOT NULL,
      `module_data` text NOT NULL,
      `live` tinyint(4) NOT NULL DEFAULT '0',
     */
    public function up() {
        Schema::create('modules', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('notes');
            $table->integer('moduletemplate_id')->nullable();
            $table->text('module_data');
            $table->tinyInteger('live');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('modules');
    }

}
