<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageversionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() 
    {
        Schema::create('pages_versions', function($table)
        {
            $table->increments('id');
            $table->text('hash');
            $table->integer('page_id');
            $table->string('title');
            $table->text('url');
            $table->text('permalink')->nullable();
            $table->integer('parentpage_id')->nullable();
            $table->longText('content');
            $table->boolean('enable_comments');
            $table->string('status')->default('draft');
            $table->tinyInteger('protected')->default(0);
            $table->string('username');
            $table->string('password');
            $table->tinyInteger('show_in_nav')->default(1);
            $table->tinyInteger('searchable')->default(1);
            $table->text('featured_image');
            $table->text('search_image');
            $table->string('custom_menu_name', 255);
            $table->tinyInteger('locked')->default();
            $table->integer('user_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('pageversions');
    }

}
