<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuletemplatesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void  
     */
    public function up() {
        Schema::create('moduletemplates', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('html');
            $table->string('css');
            $table->string('js');
            $table->string('areas');
            $table->string('notes')->nullable();
            $table->string('plugin');
            $table->string('plugin_data');
            $table->string('type');
            $table->tinyInteger('editable')->default(1);
            $table->tinyInteger('live')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('moduletemplates');
    }

}
