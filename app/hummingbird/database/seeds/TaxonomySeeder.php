<?php namespace Hummingbird\Taxonomy\Seeds;

use Illuminate\Database\Seeder as Seeder;

class TaxonomySeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();
        
        // ordering is important here
        $this->call('CategoriesSeeder');
        $this->call('TagsSeeder');
    }

}