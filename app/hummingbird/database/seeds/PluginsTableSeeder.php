<?php

class PluginsTableSeeder extends HbSeeder 
{

    /*
     * requires templates to have been created first
     */
    public function add_seed_data()
    {
        
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {
        $cmsnav = new CmsNav;
        $cmsnav->name = 'Plugins';
        $cmsnav->link = 'plugins';
        $cmsnav->section = 'Plugins';
        $cmsnav->live = 1;
        $cmsnav->save();
        
    }

}
