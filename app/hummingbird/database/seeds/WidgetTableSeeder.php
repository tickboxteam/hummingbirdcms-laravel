<?php

class WidgetTableSeeder extends HbSeeder
{

    public function add_seed_data()
    {
        
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {
        $cmsnav = new CmsNav;
        $cmsnav->name = 'Widgets';
        $cmsnav->link = 'widgets';
        $cmsnav->section = 'Content';
        $cmsnav->live = 1;
        $cmsnav->save();
        
    }

}
