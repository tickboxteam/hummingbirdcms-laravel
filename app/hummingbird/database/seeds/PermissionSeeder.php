<?php

class PermissionSeeder extends HbSeeder
{
    // these vars are to stop us using the wrong id, and make it more readable below
    // ACTIONS
    // private $CREATE_ID = 1;
    // private $UPDATE_ID = 2;
    // private $READ_ID = 3;
    // private $DELETE_ID = 4;
    // private $LOCK_ID = 5;
    // private $PUBLISH_ID = 6;
    // private $PREVIEW_ID = 7;
    // TYPES
    // private $PAGES_ID = 1;
    // private $USERS_ID = 2;
    // private $ERRORS_ID = 3;
    // private $ROLES_ID = 4;

    public function add_seed_data()
    {
        // /* Insert Permission Types */
        // $PermTypesArr = array(
        //     array(
        //         'type' => 'pages',
        //         'display_name' => 'Content Management',
        //         'live' => 1
        //     ),
        //     array(
        //         'type' => 'users',
        //         'display_name' => 'User Management',
        //         'live' => 1
        //     ),
        //     array(
        //         'type' => 'errors',
        //         'display_name' => 'Error Logs',
        //         'live' => 1
        //     ),
        //     array(
        //         'type' => 'roles',
        //         'display_name' => 'Role Management',
        //         'live' => 1
        //     )
        // );

        // DB::table('permission_types')->insert($PermTypesArr);

        // /* Insert Permission Actions */
        // $PermActions = array(
        //     array(
        //         'action' => 'Create' // id 1
        //     ),
        //     array(
        //         'action' => 'Update' // id 2
        //     ),
        //     array(
        //         'action' => 'Read' // id 3
        //     ),
        //     array(
        //         'action' => 'Delete' // id 4
        //     ),
        //     array(
        //         'action' => 'Lock' // id 5
        //     ),
        //     array(
        //         'action' => 'Publish' // id 6
        //     ),
        //     array(
        //         'action' => 'Preview' // id 7
        //     )
        // );

        // DB::table('permission_actions')->insert($PermActions);


        // /* Insert Permissions, Types and Actions (XREF) */
        // $PermTypesActionsArr = array(
        //     array(
        //         'perm_type' => $this->PAGES_ID,
        //         'perm_action' => $this->CREATE_ID
        //     ),
        //     array(
        //         'perm_type' => $this->PAGES_ID,
        //         'perm_action' => $this->UPDATE_ID
        //     ),
        //     array(
        //         'perm_type' => $this->PAGES_ID,
        //         'perm_action' => $this->READ_ID
        //     ),
        //     array(
        //         'perm_type' => $this->PAGES_ID,
        //         'perm_action' => $this->DELETE_ID
        //     ),
        //     array(
        //         'perm_type' => $this->PAGES_ID,
        //         'perm_action' => $this->LOCK_ID
        //     ),
        //     array(
        //         'perm_type' => $this->PAGES_ID,
        //         'perm_action' => $this->PUBLISH_ID
        //     ),
        //     array(
        //         'perm_type' => $this->USERS_ID,
        //         'perm_action' => $this->CREATE_ID
        //     ),
        //     array(
        //         'perm_type' => $this->USERS_ID,
        //         'perm_action' => $this->UPDATE_ID
        //     ),
        //     array(
        //         'perm_type' => $this->USERS_ID,
        //         'perm_action' => $this->LOCK_ID
        //     ),
        //     array(
        //         'perm_type' => $this->USERS_ID,
        //         'perm_action' => $this->READ_ID
        //     ),
        //     array(
        //         'perm_type' => $this->USERS_ID,
        //         'perm_action' => $this->DELETE_ID
        //     ),
        //     array(
        //         'perm_type' => $this->ERRORS_ID,
        //         'perm_action' => $this->READ_ID
        //     ),
        //     array(
        //         'perm_type' => $this->ERRORS_ID,
        //         'perm_action' => $this->DELETE_ID
        //     ),
        //     array(
        //         'perm_type' => $this->ROLES_ID,
        //         'perm_action' => $this->READ_ID
        //     ),
        //     array(
        //         'perm_type' => $this->ROLES_ID,
        //         'perm_action' => $this->CREATE_ID
        //     ),
        //     array(
        //         'perm_type' => $this->ROLES_ID,
        //         'perm_action' => $this->READ_ID
        //     ),
        //     array(
        //         'perm_type' => $this->ROLES_ID,
        //         'perm_action' => $this->PREVIEW_ID
        //     ),
        //     array(
        //         'perm_type' => $this->ROLES_ID,
        //         'perm_action' => $this->DELETE_ID
        //     )
        // );

        // DB::table('perm_types_actions_xref')->insert($PermTypesActionsArr);
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        $permissions = new Hummingbird\Libraries\PermissionManager;
        $permissions->install();
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {
        
    }
}
