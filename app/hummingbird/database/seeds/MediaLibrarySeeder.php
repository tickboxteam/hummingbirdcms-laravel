<?php

class MediaLibrarySeeder extends HbSeeder
{

    public function add_seed_data()
    {
        // superadmin user will be added on install

        $this->image_sizes = Setting::where('key', '=', 'media')->first();

        $this->image_sizes = (!isset($this->image_sizes->id) OR null === $this->image_sizes->id) ? false:$this->image_sizes->value;

        if(!isset($this->image_sizes['sizes']) OR count($this->image_sizes['sizes']) == 0)
        {
            $this->image_sizes['sizes'] = array(
                0 => array (
                    'name' => 'File Manager Thumbnail',
                    'type' => 'Default',
                    'description' => 'File Manager Thumbnails',
                    'width' => 300
                )
            );

            (new Setting)->newSetting('media', $this->image_sizes);
        }

    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        $permissions = new Hummingbird\Libraries\PermissionManager;

        /* array of permissions required for user to access/use. */
        $actions_required = array('upload');
        $actions_required = array_merge($permissions->get_standard_actions(), $actions_required);

        $permissions->install_actions($actions_required);

        /* Now install permissions for CMS area */
        $data = array(
            'type' => get_class(new Media),
            'label' => 'Media Centre',
            'group' => 'Content',
            'description' => 'Manage files in the HB3 Media Centre',
            'live' => 1,
            'perms' => $actions_required
        );

        $permissions->install_permissions($data);
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {
        $cmsnav = new CmsNav;
        $cmsnav->name = 'Media Centre';
        $cmsnav->link = 'media';
        $cmsnav->section = 'Content';
        $cmsnav->live = 1;
        $cmsnav->save();
        
    }

}
