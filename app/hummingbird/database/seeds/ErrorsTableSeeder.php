<?php

class ErrorsTableSeeder extends HbSeeder
{

    public function add_seed_data()
    {
     
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        $permissions = new Hummingbird\Libraries\PermissionManager;

        /* array of permissions required for user to access/use. */
        $actions_required = array('read', 'delete', 'export');
        $permissions->install_actions($actions_required);

        /* Now install permissions for CMS area */
        $data = array(
            'type' => get_class(new Error),
            'label' => 'Error Management',
            'group' => 'System Management',
            'description' => 'Manage system errors',
            'live' => 1,
            'perms' => $actions_required
        );

        $permissions->install_permissions($data);
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {
        $cmsnav = new CmsNav;
        $cmsnav->name = '404 Log';
        $cmsnav->link = 'errors';
        $cmsnav->section = 'System';
        $cmsnav->live = 1;
        $cmsnav->save();   
        
    }

}
