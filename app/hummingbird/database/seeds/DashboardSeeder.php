<?php namespace Hummingbird\Dashboard\Seeds;

class DashboardSeeder extends \Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();
        
        // ordering can be importnat here
        $this->call('DashboardTableSeeder');
        $this->call('ErrorsTableSeeder');
        $this->call('SettingTableSeeder');
        $this->call('RedirectionTableSeeder');
        $this->call('WidgetTableSeeder');
    }

}
