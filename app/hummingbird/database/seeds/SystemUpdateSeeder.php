<?php 

use Illuminate\Database\Seeder as Seeder;

class SystemUpdateSeeder extends Seeder
{
    public function __construct($current_version, $latest_version, $versions, $behind)
    {
        $this->current_version  = $current_version;
        $this->latest_version   = $latest_version;
        $this->versions_behind  = $behind;
        $this->all_versions     = $versions;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($callback = 'run')
    {
        unset($this->all_versions[$this->current_version]); //we dont need this

        foreach($this->all_versions as $version_key => $version)
        {
            $version_key = str_replace(".", "", "v".$version_key);

            if(class_exists($version_key))
            {
                $obj = new $version_key;

                if(method_exists($obj, $callback)) $obj->$callback();
            }          
        }
    }
}
