<?php

/*
 * Parent class to organise all plugin seed installs
 */

abstract class HbPluginSeeder extends HbSeeder
{

    public abstract function register_plugin(); 
    public abstract function unseed();   

    public function run()
    {
        parent::run();
        $this->register_plugin();
    }

}

?>
