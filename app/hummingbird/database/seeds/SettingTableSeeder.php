<?php

class SettingTableSeeder extends HbSeeder
{

    public function add_seed_data()
    {
        $setting = new Setting;
        $setting->newSetting('responsive', array('framework'=>'bootstrap'));
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        $permissions = new Hummingbird\Libraries\PermissionManager;

        /* array of permissions required for user to access/use. */
        $actions_required = array('read', 'update');
        $permissions->install_actions($actions_required);

        /* Now install permissions for CMS area */
        $data = array(
            'type' => get_class(new Setting),
            'label' => 'Settings',
            'group' => 'System Management',
            'description' => 'Manage site settings',
            'live' => 1,
            'perms' => $actions_required
        );

        $permissions->install_permissions($data);   
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    { 
        // new settings page?
    }

}
