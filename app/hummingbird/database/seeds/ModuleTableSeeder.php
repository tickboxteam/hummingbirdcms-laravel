<?php

class ModuleTableSeeder extends HbSeeder
{


    public function add_seed_data()
    {
        // $template = Moduletemplate::where('name', '=', 'Hello World Module Template')->get()->first();

        // $module = new Module;
        // $module->name = 'Demo module';
        // $module->moduletemplate_id = $template->id;
        // $module->live = 1;
        // $module->save();
        
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        
    }

    public function register_widgets()
    {
        
    }

    public function add_cmsnav_items()
    {        
        $cmsnav = new CmsNav;
        $cmsnav->name = 'Modules';
        $cmsnav->link = 'modules';
        $cmsnav->section = 'Content';
        $cmsnav->live = 1;
        $cmsnav->save();
        
    }

}
