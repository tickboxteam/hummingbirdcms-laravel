<?php

class ThemeInfoSeeder extends HbSeeder
{
    public function add_cmsnav_items()
    {
        $cmsnav = new CmsNav;
        $cmsnav->name = 'Themes';
        $cmsnav->link = 'themes';
        $cmsnav->section = 'System';
        $cmsnav->live = 1;
        $cmsnav->save();         
    }

    public function add_seed_data()
    {
        $setting = new Setting;
        $setting->key = 'activeTheme';
        $setting->value = serialize(array('theme' => 'bootstrap'));
        $setting->save();        
    }

    public function register_blocks()
    {
        
    }

    public function register_modules()
    {
        
    }

    public function register_permissions()
    {
        
    }

    public function register_widgets()
    {
        
    }
}

?>
