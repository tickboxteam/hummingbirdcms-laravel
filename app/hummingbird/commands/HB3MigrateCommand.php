<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class HB3MigrateCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'hb3:migrate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for migrating new database changes';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->database_migrations_path = HUMMINGBIRD_PATH . "/database/migrations";
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		Log::info("Migrating new database changes for Hummingbird v" . $this->option('version'));

		Artisan::call("php artisan migrate --path=" . $this->database_migrations_path );

		Log::info("Database changes migrated");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}
}
