<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class InstallCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Install Hummingbird.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{        
            
        Artisan::call('down');
        
        Artisan::call('dump-autoload');
        $this->info('Autoloads refreshed');
        
        // reset to nothing
        Artisan::call('migrate:reset');
        $this->info('Migrations reset');
        
        // create core tables
        Artisan::call('migrate');
        $this->info('Core migrations done');
        
        // create core cms plugin tables
        CmsAreas::install();
        $this->info('CMS migrations done');
                
        // create other plugin tables
        Plugins::install();
        $this->info('External plugins installed');
        
        Artisan::call('db:seed');        
        $this->info('Seeds run');      
        $this->info('');
            
        Artisan::call('up');
        
        $this->info('Migrations all installed!');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
