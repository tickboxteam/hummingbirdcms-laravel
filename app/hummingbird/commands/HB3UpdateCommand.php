<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class HB3UpdateCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'hb3:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for updating composer';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->path = base_path() . '/';
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		set_time_limit(-1);
		putenv('COMPOSER_HOME=' . $this->path);

		if(file_exists('/usr/local/bin/composer.phar')) $command = 'php /usr/local/bin/composer.phar update -d ' . $this->path;
		if(file_exists('/usr/local/bin/composer')) $command = 'php /usr/local/bin/composer update -d ' . $this->path;

		if(!isset($command))
		{
			$command = 'php ~/composer.phar update -d ' . $this->path;
		}

		if(isset($command))
		{
			Log::info("Composer now updating");
			exec($command);
			Log::info("Composer updated");
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}
}
