<?php 

use Illuminate\Support\Facades\Facade;

class NavigationMenu extends Facade
{
    /**
     * Get the registered name of the component.
     * 
     * Resolves to:
     * 
     * @return string
     */
    protected static function getFacadeAccessor() { return 'cms.navigation'; }
}
