<?php 

// INSTALL
Route::any(Installer::uri().'/done', function() {
    return View::make('HummingbirdBase::install.done');
});

Route::group(array('before' => 'ready.before'), function(){
    Route::controller(Installer::uri(),'InstallController');
});

// if (Installer::isInstalled()) {
//     return Redirect::to('/');
// }

// SYSTEM READY
// everything should go inside this filter, which will redirect to install if we're not setup
Route::group(array('before' => 'install.before'), function(){

    // LOGIN PAGES
    Route::any(General::backend_url().'/login','AuthController@login');
    Route::get(General::backend_url().'/logout','AuthController@logout');

    // BASE HUMMINGBID PAGES
    Route::get(General::backend_url(), array('before' => 'auth.check', function()
    {
        /* we are at the dashboard just return the view */
        $dashboard = new DashboardController;
        return $dashboard->index();
    }));

    // HUMMINGBIRD MODULES
    Route::group(array('prefix' => General::backend_url(), 'before' => 'auth.check'),  function() {   
        // Route::any('{slug}', 'BackendController@run')->where('slug', '(.*)?');

        Route::any(General::backend_url().'/forbidden', 'CmsbaseController@forbidden');

        // AJAX ROUTING
        Route::group(array('prefix' => 'ajax'), function(){
            //todo: some checking via a filter?addModuleToArea

            /* 
                Route::get('/ajax*', 'AjaxController@run');
            */

            Route::get('modules/modulesForArea/{area}', 'ModuleController@getModulesForArea');
            Route::get('pagemodules/addPagemodule/{area}/{page_id}/{module_id}/{position?}', 'PagemoduleController@addPagemodule');
            Route::get('pagemodules/pagemodulesForArea/{area}/{page_id}', 'PagemoduleController@getPagemodulesForArea');
        });

        // CRON JOB ROUTING
        Route::group(array('prefix' => 'cron'), function(){
            /* 
                Route::get('/cron*', 'CronController@run');
            */
        });
        
        // INDIVIDUAL ROUTES


        Route::get('pages/export', 'PageController@getExport');
        Route::post('pages/export', 'PageController@postExport');
        
        Route::post('pages/search', 'PageController@search');
        Route::get('pages/versions/{page_id?}', 'PageVersionController@getVersions');
        Route::get('pageversions/restoreVersion/{version_id?}', 'PageVersionController@restoreVersion');
        
        Route::get('themes/activate/{theme}', 'ThemeController@activate');
        Route::get('plugins/install/{plugin}', 'PluginController@install');
        Route::get('plugins/uninstall/{plugin}', 'PluginController@uninstall');
        Route::post('activity-logs/search', 'ActivitylogController@search');



        // GENERAL CMS ROUTES        
        Route::controller('typography', 'TypoController');
        Route::controller('ui', 'UiController');
        Route::controller('ui-el', 'UiElementsController');
        Route::controller('ui-grids', 'UiGridsController');

            // Route::get('removeUserPreview', 'UserController@removePreviewPerms');
            // Route::get('removeRolePreview', 'RoleController@removePreviewPerms');
            Route::get('users/export', 'UserController@getExport'); 
            Route::post('users/export', 'UserController@postExport');
            Route::post('users/import', 'UserController@import');
            Route::any('profile', 'UserController@profile');
            Route::get('users/preview/{user_id?}', 'UserController@preview');
        Route::controller('users', 'UserController');

            Route::any('pages/re-order/', 'PageController@reorder');
            Route::any('pages/publish/{page_id}/{value}', 'PageController@publish');
            Route::any('pages/import/{action?}', 'PageController@import');
        Route::controller('pages', 'PageController');
        
            Route::any('menus/deleteItem/{id}', 'MenuController@deleteItem');
        Route::controller('menus', 'MenuController');
        Route::controller('menu-items', 'MenuItemController');
        
            Route::any('errors/import', 'ErrorController@import');
        Route::resource('errors', 'ErrorController');
            
            Route::any('settings/import', 'SettingsController@import');
        Route::resource('settings', 'SettingsController');

        Route::controller('page-modules', 'PageModuleController');

            Route::any('modules/replicate/{id}', 'ModuleController@replicate');
        Route::controller('modules', 'ModuleController');
        Route::controller('module-templates', 'ModuleTemplateController');
        
            Route::any('redirections/import', 'RedirectionsController@import');
        Route::resource('redirections', 'RedirectionsController');
        
        Route::resource('plugins', 'PluginController');
        Route::resource('themes', 'ThemeController');

            Route::get('roles/preview/{role_id?}', 'RoleController@preview');
        Route::controller('roles', 'RoleController');

            Route::get('templates-new/get-html/{id}', 'TemplateNewController@returnTemplateHTML');
        Route::controller('templates-new', 'TemplateNewController');
        Route::controller('templates', 'TemplateController');

        Route::controller('widgets', 'WidgetController');
        Route::controller('cms-navigation', 'CmsNavController');
        Route::controller('activity-logs', 'ActivitylogController');

            Route::any('updates/run', 'UpdatesController@run_these_updates');
            Route::any('updates/progress', 'UpdatesController@return_progress');
        Route::resource('updates', 'UpdatesController', array('only' => array('index')));

            Route::any('tags/deleted/reinstate/{id}', 'TagsController@reinstate');
            Route::any('tags/deleted/purge/{id}', 'TagsController@purge');
            Route::any('tags/deleted/', 'TagsController@showDeleted');

            // Route::any('tags/deleted/reinstate/{id}', array('uses' => 'TagsController@reinstate',
            //                             'as' => 'tags.reinstate'));
            // Route::any('tags/deleted/purge/{id}', array('uses' => 'TagsController@purge',
            //                             'as' => 'tags.purge'));
            // Route::any('tags/deleted/', array('uses' => 'TagsController@showDeleted',
            //                             'as' => 'tags.deleted'));

        Route::resource('tags', 'TagsController');

            Route::any('categories/children/', 'CategoriesController@getChildren');
            Route::any('categories/deleted/reinstate/{id}', 'CategoriesController@reinstate');
            Route::any('categories/deleted/purge/{id}', 'CategoriesController@purge');
            Route::any('categories/deleted/', 'CategoriesController@showDeleted');
        Route::resource('categories', 'CategoriesController');
        
        Route::any('media/settings/update/{id}', 'MediaController@updateSetting');
        Route::any('media/settings/delete', 'MediaController@deleteSetting');
        Route::any('media/settings/store', 'MediaController@createSetting');
        Route::any('media/settings/{id}', 'MediaController@showSettings');
        Route::any('media/settings/', 'MediaController@settings');
        Route::any('media/postEditImage/{media}', 'MediaController@postEditImage');
        Route::any('media/editMediaItem/{media}', 'MediaController@postEditMediaItem'); //modal request for page
        Route::any('media/edit-image/{image}', 'MediaController@editImage'); //modal request for page    
        Route::any('media/globalMediaLibrary', 'MediaController@globalMediaLibrary'); //
        Route::any('media/mediaLibraryRequest', 'MediaController@mediaLibraryRequest'); //modal request for page
        Route::any('media/getMediaModalTemplate', 'MediaController@getMediaModalTemplate'); //template for Redactor
        Route::any('media/updateUploadedFiles', 'MediaController@updateUploadedFiles'); // updating meta content for file uploads (ajax)
        Route::any('media/uploadFile/', 'MediaController@uploadFile'); // uploading single or individual
        Route::any('media/importFiles', 'MediaController@importFiles'); //importing selected files
        Route::any('media/delete-item/{media}', 'MediaController@destroyMedia');
        Route::any('media/delete/{collection}', 'MediaController@destroy');
        Route::any('media/view-media/{item}', 'MediaController@viewItem'); //modal request for page
        Route::any('media/update/{collection}', 'MediaController@update');
        Route::any('media/edit/{collection}', 'MediaController@edit');
        Route::any('media/view/{collection}', 'MediaController@index');
        Route::resource('media', 'MediaController');
    });

    // FRONTEND PAGES
    Route::any('{slug}', array('uses' => 'FrontendController@show'))->where('slug', '(.*)?');

    App::missing(function($exception)
    {
        // 404
        return App::make('CmsbaseController')->error($exception);
        // Route::any(General::backend_url().'/forbidden', 'CmsbaseController@forbidden');
        // return View::make('HummingbirdBase::cms.errors.404', array('exception' => $exception)); 
    });
});

// todo: put this into macros and load it at the correct time
Form::macro('actionlink', function($action, $type, $url)
{
    $allowed = Auth::user()->hasAccess($action, $type);
    
    $html = "<a href='/$url'";
    $html .= $allowed ? '' : ' disabled';
    $html .= '>'.ucfirst($action).'</a>';
    
    return $html;
});
