@extends('HummingbirdBase::install.layout')

@section('content')
<div class="welcome">
    <h1>We are about to install Hummingbird 3.0</h1>
    <a href="/{{Installer::uri()}}/database">Continue</a>
</div>
@stop
