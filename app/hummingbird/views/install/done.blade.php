@extends('HummingbirdBase::install.layout')

@section('content')
<div class="welcome">
    <h1>Welcome to your new site!</h1>
    <a href="/">Homepage</a>&nbsp;
    <a href="/{{App::make('backend_url')}}">CMS</a>
</div>
@stop
