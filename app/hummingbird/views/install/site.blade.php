@extends('HummingbirdBase::install.layout')

@section('content')
<div class='step'>
    <h2>Step {{$step}}</h2>
    <form method='post' action=''>
        <fieldset>
            <label>Site Name:</label>
            <input type="text" placeholder="Your website name here" name="site_name" value="{{Input::old('site_name')}}">
            {{$errors->first('site_name')}}
        </fieldset>
        <fieldset>
            <label>Site URL:</label>
            <input type="text" placeholder="www.yourdomain.com" name="site_url" value="{{Input::old('site_url')}}">
            {{$errors->first('site_url')}}
        </fieldset>
        <fieldset>
            <label>CMS URL:</label>
            <input type="text" placeholder="this-is-your-cms-url" name="site_cms_url" value="{{Input::old('site_cms_url')}}">
            {{$errors->first('site_cms_url')}}
        </fieldset>
        <fieldset>
            <label>Encryption Key:</label>
            <input type="text" placeholder="{{Str::limit(Crypt::encrypt(time()), 32, '')}}" name="site_key" value="{{Str::limit(Crypt::encrypt(time()), 32, '')}}">
            {{$errors->first('site_key')}}
        </fieldset>
        <fieldset>
            <label>Password Strength (%):</label>
            <input type="text" placeholder="80" name="site_password_strength" value="{{Input::old('site_password_strength')}}">
            {{$errors->first('site_password_strength')}}
        </fieldset>
        <fieldset>
            {{ Form::label('site_installtype', 'Installation Type ') }}
            {{ Form::select('site_installtype', Installer::get_types(), Input::old('site_installtype')) }}
            {{$errors->first('site_installtype')}}
        </fieldset>
        <fieldset>
            {{ Form::label('site_timezone', 'Timezone ') }}
            {{ Form::select('site_timezone', DateTimeHelper::timezone_select_list(), Config::get('app.timezone')) }}
            {{$errors->first('site_timezone')}}
        </fieldset>
        
        <input type='submit' value='Continue' />
    </form>
</div>
@stop