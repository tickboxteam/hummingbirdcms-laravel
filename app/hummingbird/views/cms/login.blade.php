<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <!-- Title and other stuffs -->
        <title>Login - MoodStrap</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">

        <!-- Stylesheets -->
        <link href="/themes/hummingbird-backup/default/css/bootstrap.css" rel="stylesheet"/>
        <link rel="stylesheet" href="/themes/hummingbird-backup/default/css/font-awesome.css"/>
        <link href="/themes/hummingbird-backup/default/css/moodstrap-style.css" rel="stylesheet"/>

        <!-- HTML5 Support for IE -->
        <!--[if lt IE 9]>
        <script src="js/html5shim.js"></script>
        <![endif]-->

        <!-- Favicon -->
        <link rel="shortcut icon" href="/themes/hummingbird/default/favicon.ico">
    </head>

    <body>

        <!-- Form area -->
        <div class="admin-form">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <!-- Widget starts -->
                        <div class="widget">
                            <!-- Widget head -->
                            <div class="widget-head">
                                <i class="icon-lock"></i> Login 
                            </div>

                            <div class="widget-content">
                                <div class="padd">
                                    <!-- Login form -->
                                    {{Form::open(array('url' => App::make('backend_url').'/login', 'method' => 'post', 'class' => 'form-horizontal'))}}
                                        <!-- Email -->
                                        <div class="form-group">
                                            <label class="control-label col-lg-3" for="username">Email or Username:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="username" name="username" placeholder="Email / Username">
                                            </div>
                                        </div>
                                        <!-- Password -->
                                        <div class="form-group">
                                            <label class="control-label col-lg-3" for="password">Password</label>
                                            <div class="col-lg-9">
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                            </div>
                                        </div>
                                        <!-- Remember me checkbox and sign in button -->
                                        <div class="form-group">
                                            <div class="col-lg-9 col-lg-offset-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="remember_me" name="remember_me" value="1"> Remember me
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-lg-offset-3">
                                            <button type="submit" class="btn btn-danger">Sign in</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                        <br />
                                    {{Form::close()}}

                                </div>
                            </div>

                            <div class="widget-foot">
                                 <!--<a href="#">Forgotten password?</a>-->
                            </div>
                        </div>  
                    </div>
                </div>
            </div> 
        </div>



        <!-- JS -->
        <script src="/themes/hummingbird-backup/default/js/jquery.js"></script>
        <script src="/themes/hummingbird-backup/default/js/bootstrap.js"></script>
    </body>
</html>