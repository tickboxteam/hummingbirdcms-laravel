@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/timeline/css/style.css"> <!-- Resource style -->
@stop


@section('content')

<h1>Activity Logs</h1>

@if(count($activitylogs) > 0)
    <section id="cd-timeline" class="cd-container">
        @foreach($activitylogs as $activitylog)   
            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-picture">
                    <img src="/themes/hummingbird/default/lib/timeline/img/cd-icon-picture.svg" alt="Picture">
                </div> <!-- cd-timeline-img -->

                <div class="cd-timeline-content">
                    <h2>{{$activitylog->description}}</h2>
                    <p>{{$activitylog->notes}}</p>
                    <span class="cd-date">{{ date("M d, Y", strtotime($activitylog->created_at))}}</span>
                </div> <!-- cd-timeline-content -->
            </div> <!-- cd-timeline-block -->
        @endforeach
    </section> <!-- cd-timeline -->
@endif

@section('scripts')
    <script src="/themes/hummingbird/default/lib/timeline/js/main.js"></script> <!-- Resource jQuery -->
@stop

@stop
