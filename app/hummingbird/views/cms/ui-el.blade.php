@extends('HummingbirdBase::cms.layout')

@section('content')

<h1 class="normal">UI Elements</h1>

<section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <!--breadcrumbs start -->
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Current page</li>
                    </ul>
                    <!--breadcrumbs end -->
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a class="active-trail active" href="#">Pages</a>
                        </li>
                        <li>
                            <a class="current" href="#">Elements</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-md-12">
                        <!--progress bar start-->
                        <section class="panel">
                            <header class="panel-heading">
                                Basic Progress Bars
                        <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-cog"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                            </header>
                            <div class="panel-body">
                                <div class="progress progress-xs">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                        <span class="sr-only">60% Complete</span>
                                    </div>
                                </div>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete</span>
                                    </div>
                                </div>
                                <p class="text-muted">
                                    Stacked Progress Bars
                                </p>
                                <div class="progress progress-sm">
                                    <div class="progress-bar progress-bar-success" style="width: 35%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 20%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 10%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!--progress bar end-->

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!--progress bar start-->
                        <section class="panel">
                            <header class="panel-heading">
                                Striped Progress Bars
                        <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-cog"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                            </header>
                            <div class="panel-body">
                                <div class="progress progress-striped progress-sm">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                                <div class="progress progress-striped progress-sm">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                                <div class="progress progress-striped progress-sm">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                                <div class="progress progress-striped progress-sm">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                                <p class="text-muted">
                                    Animated Progress Bars
                                </p>
                                <div class="progress progress-striped active progress-sm">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                        <span class="sr-only">45% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!--progress bar end-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!--collapse start-->
                        <div class="panel-group m-bot20" id="accordion">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Collapsible Group Item #1
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            Collapsible Group Item #2
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            Collapsible Group Item #3
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--collapse end-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!--tooltips start-->
                        <section class="panel">
                            <div class="panel-body btn-gap">
                                <button title="" data-placement="top" data-toggle="tooltip" class="btn btn-default tooltips" type="button" data-original-title="Tooltip on top">Tooltip on top</button>
                                <button title="" data-placement="left" data-toggle="tooltip" class="btn btn-default tooltips" type="button" data-original-title="Tooltip on left"> left</button>
                                <button title="" data-placement="bottom" data-toggle="tooltip " class="btn btn-default tooltips" type="button" data-original-title="Tooltip on bottom"> bottom</button>
                                <button title="" data-placement="right" data-toggle="tooltip" class="btn btn-default tooltips" type="button" data-original-title="Tooltip on right"> right</button>
                            </div>
                        </section>
                        <!--tooltips end-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!--popover start-->
                        <section class="panel">
                            <div class="panel-body btn-gap">
                                <button data-original-title="Popovers in top" data-content="And here's some amazing content. It's very engaging. right?" data-placement="top" data-trigger="hover" class="btn btn-info popovers">Popover on Top</button>
                                <button data-original-title="Popovers in bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-placement="bottom" data-trigger="hover" class="btn btn-info popovers">Bottom</button>
                                <button data-original-title="Popovers in right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-placement="right" data-trigger="hover" class="btn btn-info popovers">Right</button>
                                <button data-original-title="Popovers in left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-placement="left" data-trigger="hover" class="btn btn-info popovers">Left</button>
                            </div>
                        </section>
                        <!--popover end-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!--pagination start-->
                        <section class="panel">
                            <header class="panel-heading">
                                Pagination
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-cog"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                            </header>
                            <div class="panel-body">
                                <div>
                                    <ul class="pagination pagination-lg">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li class="active"><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                                <div class="text-center">
                                    <ul class="pagination">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li class="active"><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                                <div>
                                    <ul class="pagination pagination-sm pull-right">
                                        <li><a href="#">«</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li class="active"><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#">»</a></li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <!--pagination end-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!--pagination start-->
                        <section class="panel">
                            <header class="panel-heading">
                                Initail Collaps bar
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-up"></a>
                                <a href="javascript:;" class="fa fa-cog"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                            </header>
                            <div class="panel-body" style="display: none;">
                                contents goes here
                            </div>
                        </section>
                        <!--pagination end-->
                    </div>
                </div>

            </div>

            <div class="col-lg-6">
            <!--tab nav start-->
            <section class="panel">
                <header class="panel-heading tab-bg-dark-navy-blue ">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#home">Home</a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#about">About</a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#profile">Profile</a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#contact">Contact</a>
                        </li>
                    </ul>
                </header>
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="home" class="tab-pane active">
                            Home
                        </div>
                        <div id="about" class="tab-pane">About</div>
                        <div id="profile" class="tab-pane">Profile</div>
                        <div id="contact" class="tab-pane">Contact</div>
                    </div>
                </div>
            </section>
            <!--tab nav start-->

            <!--tab nav start-->
            <section class="panel">
                <header class="panel-heading tab-bg-dark-navy-blue">
                    <ul class="nav nav-tabs">
                        <li>
                            <a data-toggle="tab" href="#home-2">
                                <i class="fa fa-home"></i>
                            </a>
                        </li>
                        <li class="active">
                            <a data-toggle="tab" href="#about-2">
                                <i class="fa fa-user"></i>
                                About
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#contact-2">
                                <i class="fa fa-envelope-o"></i>
                                Contact
                            </a>
                        </li>
                    </ul>
                </header>
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="home-2" class="tab-pane ">
                            Home
                        </div>
                        <div id="about-2" class="tab-pane active">About</div>
                        <div id="contact-2" class="tab-pane ">Contact</div>
                    </div>
                </div>
            </section>
            <!--tab nav end-->


            <!--tab nav start-->
            <section class="panel">
                <header class="panel-heading tab-bg-dark-navy-blue tab-right ">
                    <ul class="nav nav-tabs pull-right">
                        <li class="active">
                            <a data-toggle="tab" href="#home-3">
                                <i class="fa fa-home"></i>
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#about-3">
                                <i class="fa fa-user"></i>
                                About
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#contact-3">
                                <i class="fa fa-envelope-o"></i>
                                Contact
                            </a>
                        </li>
                    </ul>
                </header>
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="home-3" class="tab-pane active">
                            Home
                        </div>
                        <div id="about-3" class="tab-pane">About</div>
                        <div id="contact-3" class="tab-pane">Contact</div>
                    </div>
                </div>
            </section>
            <!--tab nav end-->

            <!--navigation start-->
            <nav class="navbar navbar-inverse" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Brand</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Link</a></li>
                        <li><a href="javascript:;">Link</a></li>
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li><a href="#">Separated link</a></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="javascript:;">Link</a></li>
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
            <!--navigation end-->

            <div class="row">
                <div class="col-md-12">
                    <!--notification start-->
                    <section class="panel">
                        <div class="panel-body">
                            <div class="alert alert-success alert-block fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <h4>
                                    <i class="icon-ok-sign"></i>
                                    Success!
                                </h4>
                                <p>Best check yo self, you're not looking too good...</p>
                            </div>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>Oh snap!</strong> Change a few things up and try submitting again.
                            </div>
                            <div class="alert alert-success fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>Well done!</strong> You successfully read this important alert message.
                            </div>
                            <div class="alert alert-info fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
                            </div>
                            <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>Warning!</strong> Best check yo self, you're not looking too good.
                            </div>

                        </div>
                    </section>
                    <!--notification end-->
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!--label and badge start-->
                    <section class="panel">
                        <div class="panel-body">
                            <p class="text-muted text-center">Labels</p>
                            <p class="text-center">
                                <span class="label label-default">label</span>
                                <span class="label label-primary">Primary</span>
                                <span class="label label-success">Success</span>
                                <span class="label label-info">Info</span>
                                <span class="label label-inverse">Inverse</span>
                                <span class="label label-warning">Warning</span>
                                <span class="label label-danger">Danger</span>
                            </p>
                            <p class="text-muted text-center">Badges</p>
                            <p class="m-bot-none text-center">
                                <span class="badge">5</span>
                                <span class="badge bg-primary">10</span>
                                <span class="badge bg-success">15</span>
                                <span class="badge bg-info">20</span>
                                <span class="badge bg-inverse">25</span>
                                <span class="badge bg-warning">30</span>
                                <span class="badge bg-important">35</span>
                            </p>
                        </div>
                    </section>
                    <!--label and badge end-->
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!--modal start-->
                    <section class="panel">
                        <header class="panel-heading">
                            Modal Dialogs
                    <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-cog"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <a class="btn btn-success" data-toggle="modal" href="#myModal">
                                Dialog
                            </a>
                            <a class="btn btn-warning" data-toggle="modal" href="#myModal2">
                                Confirm
                            </a>
                            <a class="btn btn-danger" data-toggle="modal" href="#myModal3">
                                Alert !
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Modal Tittle</h4>
                                        </div>
                                        <div class="modal-body">

                                            Body goes here...

                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                            <button class="btn btn-success" type="button">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal -->
                            <!-- Modal -->
                            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Modal Tittle</h4>
                                        </div>
                                        <div class="modal-body">

                                            Body goes here...

                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                            <button class="btn btn-warning" type="button"> Confirm</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal -->
                            <!-- Modal -->
                            <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Modal Tittle</h4>
                                        </div>
                                        <div class="modal-body">

                                            Body goes here...

                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-danger" type="button"> Ok</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal -->

                        </div>
                    </section>
                    <!--modal end-->
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <!--gritter notification start-->
                    <section class="panel">
                        <header class="panel-heading">
                            Gritter Notifications
                    <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-cog"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                        </header>
                        <div class="panel-body">
                            <p class="text-muted">Click on below buttons to check it out.</p>
                            <a id="add-regular" class="btn btn-default btn-sm" href="javascript:;">Regular</a>
                            <a id="add-sticky" class="btn btn-success  btn-sm" href="javascript:;">Sticky</a>
                            <a id="add-without-image" class="btn btn-info  btn-sm" href="javascript:;">Imageless</a>

                            <a id="add-gritter-light" class="btn btn-warning  btn-sm" href="javascript:;">Light</a>
                            <a id="add-max" class="btn btn-primary  btn-sm" href="javascript:;">Max of 3</a>
                            <a id="remove-all" class="btn btn-danger  btn-sm" href="#">Remove all</a>
                        </div>
                    </section>
                    <!--gritter notification end-->
                </div>
            </div>

            <!--carousel start-->
            <section class="panel">
                <div id="c-slide" class="carousel slide auto panel-body">
                    <ol class="carousel-indicators out">
                        <li class="active" data-slide-to="0" data-target="#c-slide"></li>
                        <li class="" data-slide-to="1" data-target="#c-slide"></li>
                        <li class="" data-slide-to="2" data-target="#c-slide"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item text-center active">
                            <h3>Bucket Admin is an Awesome Dashboard</h3>
                            <p>Awesome admin template</p>
                            <small class="text-muted">Based on Latest Bootstrap 3.0.3</small>
                        </div>
                        <div class="item text-center">
                            <h3>Well Organized</h3>
                            <p>Awesome admin template</p>
                            <small class="text-muted">Huge UI Elements</small>
                        </div>
                        <div class="item text-center">
                            <h3>Well Documentation</h3>
                            <p>Awesome admin template</p>
                            <small class="text-muted">Very Easy to Use</small>
                        </div>
                    </div>
                    <a data-slide="prev" href="#c-slide" class="left carousel-control">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a data-slide="next" href="#c-slide" class="right carousel-control">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </section>
            <!--carousel end-->

            </div>

            </div>


    <!-- page start-->
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Default Buttons
                     <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-cog"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                </header>
                <div class="panel-body">
                    <button type="button" class="btn btn-default">Default</button>
                    <button type="button" class="btn btn-primary">Primary</button>
                    <button type="button" class="btn btn-success">Success</button>
                    <button type="button" class="btn btn-info">Info</button>
                    <button type="button" class="btn btn-warning">Warning</button>
                    <button type="button" class="btn btn-danger">Danger</button>
                </div>
            </section>
        </div>

        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    Rounded Buttons
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
                </header>
                <div class="panel-body">
                    <button type="button" class="btn btn-round btn-default">Default</button>
                    <button type="button" class="btn btn-round btn-primary">Primary</button>
                    <button type="button" class="btn btn-round btn-success">Success</button>
                    <button type="button" class="btn btn-round btn-info">Info</button>
                    <button type="button" class="btn btn-round btn-warning">Warning</button>
                    <button type="button" class="btn btn-round btn-danger">Danger</button>
                </div>
            </section>
        </div>

        <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    Buttons Size
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
                </header>
                <div class="panel-body">
                    <button type="button" class="btn btn-default btn-lg">Large Button</button>
                    <button type="button" class="btn btn-primary">Default Button</button>
                    <button type="button" class="btn btn-success btn-sm">Small Button</button>
                    <button type="button" class="btn btn-info btn-xs">Extra Small Button</button>
                </div>
            </section>
        </div>
    </div>




    <div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Dropdowns Button
                 <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
                    </header>
                    <div class="panel-body">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">Default <span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div><!-- /btn-group -->
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle btn-sm" type="button">Primary <span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div><!-- /btn-group -->
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-success dropdown-toggle btn-xs" type="button">Success <span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div><!-- /btn-group -->
                    </div>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Split Dropdowns Button
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
                    </header>
                    <div class="panel-body">
                        <div class="btn-group">
                            <button class="btn btn-white" type="button">Default</button>
                            <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button"><span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div><!-- /btn-group -->
                        <div class="btn-group dropup">
                            <button class="btn btn-white" type="button">Dropup</button>
                            <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button"><span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div><!-- /btn-group -->
                    </div>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Justified Button groups
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
                    </header>
                    <div class="panel-body">
                        <div class="btn-group btn-group-justified">
                            <a class="btn btn-success" href="#">Left</a>
                            <a class="btn btn-info" href="#">Middle</a>
                            <a class="btn btn-danger" href="#">Right</a>
                        </div>
                    </div>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Block level button
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
                    </header>
                    <div class="panel-body">
                        <button type="button" class="btn btn-success btn-lg btn-block">Block level button</button>
                        <button type="button" class="btn btn-warning btn-block">Block level button</button>
                        <button type="button" class="btn btn-danger btn-xs btn-block">Block level button</button>
                    </div>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Star Rating Example
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
                    </header>
                    <div class="panel-body">
                              <span class="rating">
                                  <span class="star"></span>
                                  <span class="star"></span>
                                  <span class="star"></span>
                                  <span class="star"></span>
                                  <span class="star"></span>
                              </span>
                    </div>
                </section>
            </div>
        </div>

    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Buttons With Icon
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
                    </header>
                    <div class="panel-body">
                        <button type="button" class="btn btn-primary"><i class="fa fa-cloud"></i> Cloud</button>
                        <button type="button" class="btn btn-success"><i class="fa fa-eye"></i> View </button>
                        <button type="button" class="btn btn-info "><i class="fa fa-refresh"></i> Update</button>
                        <button type="button" class="btn btn-default "><i class="fa fa-cloud-upload"></i> Upload</button>
                        <button data-toggle="button" class="btn btn-white">
                            <i class="fa fa-thumbs-up "></i>
                            89
                        </button>
                        <button data-toggle="button" class="btn btn-white">
                            <i class="fa fa-home"></i>
                        </button>
                    </div>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Group Buttons
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
                    </header>
                    <div class="panel-body">
                        <div class="btn-row">
                            <div class="btn-group">
                                <button class="btn btn-white" type="button">Left</button>
                                <button class="btn btn-white" type="button">Middle</button>
                                <button class="btn btn-white" type="button">Right</button>
                            </div>
                            <div class="btn-group  btn-group-sm">
                                <button class="btn btn-white" type="button">Left</button>
                                <button class="btn btn-white" type="button">Middle</button>
                                <button class="btn btn-white" type="button">Right</button>
                            </div>
                        </div>
                        <p class="text-muted">Vertical button groups</p>
                        <div class="btn-row">
                            <div class="btn-group-vertical">
                                <button class="btn btn-white" type="button">Top</button>
                                <button class="btn btn-white" type="button">Middle</button>
                                <button class="btn btn-white" type="button">Bottom</button>
                            </div>
                        </div>
                        <p class="text-muted">Nested button groups</p>
                        <div class="btn-row">
                            <div class="btn-group">
                                <button class="btn btn-default" type="button">1</button>
                                <button class="btn btn-danger" type="button">2</button>
                                <button class="btn btn-default" type="button">3</button>
                                <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"> Dropdown <span class="caret"></span> </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Dropdown link 1</a></li>
                                        <li><a href="#">Dropdown link 2</a></li>
                                        <li><a href="#">Dropdown link 3</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <p class="text-muted">Multiple button groups</p>
                        <div class="btn-row">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button class="btn btn-info" type="button">1</button>
                                    <button class="btn btn-info active" type="button">2</button>
                                    <button class="btn btn-info" type="button">3</button>
                                    <button class="btn btn-info" type="button">4</button>
                                </div>
                                <div class="btn-group">
                                    <button class="btn btn-success" type="button">5</button>
                                    <button class="btn btn-success" type="button">6</button>
                                    <button class="btn btn-success" type="button">7</button>
                                </div>
                                <div class="btn-group">
                                    <button class="btn btn-warning" type="button">8</button>
                                </div>
                            </div>
                        </div>

                        <p class="text-muted">Group Checkbox</p>
                        <div class="btn-row">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary">
                                    <input type="checkbox"> Option 1
                                </label>
                                <label class="btn btn-primary">
                                    <input type="checkbox"> Option 2
                                </label>
                                <label class="btn btn-primary">
                                    <input type="checkbox"> Option 3
                                </label>
                            </div>
                        </div>

                        <p class="text-muted">Group Radio</p>
                        <div class="btn-row">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default">
                                    <input type="radio" name="options" id="option1"> Option 1
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="options" id="option2"> Option 2
                                </label>
                                <label class="btn btn-default">
                                    <input type="radio" name="options" id="option3"> Option 3
                                </label>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    </div>

        </section>

@stop
