@extends('HummingbirdBase::cms.layout')

@section('styles')
    <style type="text/css">
        .user-profile {}
        .user-profile .content-header 
        {
            height:100px;
            /*background:url('https://farm9.staticflickr.com/8653/16645863609_b38343d46b_b.jpg') no-repeat 50% 50%;*/
            background-size:cover;
            position:relative;
            margin:-20px -20px 20px;
            overflow: hidden;
        }

        .user-profile .content-header .header-section {height:auto;color:white;position:absolute;top:0;left:0;right:0;padding:20px;border:0;background:rgb(0,0,0);background:rgba(0,0,0,0.4);}
        .user-profile .content-header .header-section h1 {font-size:2.8rem;font-size:28px;font-weight:300;}
        .user-profile .content-header .header-section h1 > small {color:#EDEDED;}


        .hide {transition: opacity 500 ease-in-out;}

        .permission-holder .row 
        {
            margin-bottom:10px;
            padding-bottom:10px;
            border-bottom:1px solid #CCC;
            margin-left:0;
            margin-right:0;
        }

        .profile img
        {

            border-radius: 50%;
            -webkit-border-radius: 50%;
            border: 10px solid #f1f2f7;
            margin-top: 20px;
        }

        .profile img:hover {opacity:0.5}


    </style>
@stop


@section('content')

    <div class="user-profile">
        <div class="content-header content-header-media" data-background="{{$user->getPreference('cover-image')}}">
            <div class="header-section">
                <h1>{{$user->name}} <br /><small>{{$user->username}}</small></h1>
            </div>
        </div>

        @if (Session::has('success'))
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        {{ Session::get('success') }}
                    </div>
                </div>
            </div>  
        @endif

        @if(Session::has('message'))
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-info fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>  
        @endif

        @if (Session::has('errors'))
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        @foreach ($errors->all('<li>:message</li>') as $error)
                            {{$error}}    
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        <?php echo Form::open(array('url' => App::make('backend_url').'/users/edit/' . $user->id, 'class' => 'form-horizontal')) ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel" style="background-color:white;padding:20px;">
                                <h3>User Details </h3>

                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Name: </label>
                                    <div class="col-sm-10">
                                        <input id="name" name="name" type="text" class="form-control" placeholder="Users name" value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="username" class="col-sm-2 control-label">Username:</label>
                                    <div class="col-sm-10">
                                        <input id="username" name="username" type="text" class="form-control" placeholder="Username" value="{{$user->username}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email:</label>
                                    <div class="col-sm-10">
                                        <input id="email" name="email" type="text" class="form-control" placeholder="Email address" value="{{$user->email}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-2 control-label">Password:</label>
                                    <div class="col-sm-10">
                                        <input type="password" autocomplete="off" name="password" id="password" class='password-strength form-control' size="40" class="left" data-display="myDisplayElement1" /> 
                                        <div class="left" id="myDisplayElement1"></div>
                                        <span class="help-block">Leave blank to keep current password.</span>        
                                    </div>
                                </div>
                                <div class="form-group hide">
                                    <label for="confirm-password" class="col-sm-2 control-label">Confirm Password:</label>
                                    <div class="col-sm-10">
                                        {{Form::password('password_confirmation', array('class' => 'form-control', 'id' => 'confirm-password'))}}
                                    </div>
                                </div>

                                @if(count($roles) > 0)
                                    <div class="form-group">
                                        <label for="role" class="col-sm-2 control-label">Role:</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="role_id" id="role">
                                                <option value="{{ Role::getUserRoleID() }}" <?php if($user->role_id == Role::getUserRoleID()) echo 'selected';?>>{{ Role::getUserRoleName(Role::getUserRoleID()) }}</option>

                                                @foreach($roles as $role)
                                                    <option value="{{ $role->id }}" <?php if($user->role_id == $role->id) echo 'selected';?>><?=str_repeat('&nbsp;&nbsp;&nbsp;', $role->level+1)?>{{ $role->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" name="role_id" value="{{ Role::getUserRoleID() }}" />        
                                @endif

                                <div class="form-group">
                                    <label for="status" class="col-sm-2 control-label">Status:</label>
                                    <div class="col-sm-10">
                                        {{ Form::select('status', $statuses, $user->status, array('class' => 'form-control')) }}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-default btn-primary">Update</button>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-md-4 hide">
                            <div class="profile img-rounded">
                                <img class="img-responsive" src="https://scontent-lhr3-1.xx.fbcdn.net/hphotos-xap1/v/t1.0-9/11220114_10155711156300096_7524622048633022012_n.jpg?oh=658b4c099db4588eb370cd50b5a385ec&oe=560EA9D3" class="" />
                            </div>
                        </div>
                    </div>
                </div>

                @if(count($permissions) > 0)
                    <div class="col-md-12">  
                        <section class="panel" style="background-color:white;padding:20px;">
                            <h3 class="normal">Individual User Permissions</h3>

                            <div id="permissions">
                                @foreach($permissions as $type => $perm_details)
                                    <?php $has_perms = false;
                                    $selected_perms = array();

                                    if(count($perm_details['actions']) > 0)
                                    {?>
                                        <?php 

                                            if(isset($active_perms[$type]) AND count($active_perms[$type] > 0))
                                            {
                                                $has_perms = true;
                                            }
                                        ?>

                                        <div class="permission-holder col-md-3" data-type="<?=$type?>">
                                            <div class="row">
                                                <h5 class="permission-holder-title">{{$perm_details['data']['label']}} <input type="checkbox" name="activate[]" value="<?=$type?>" <?php if($has_perms) echo 'checked="checked"';?>/></h5>

                                                @if($perm_details['data']['description'] != '')
                                                    <p>{{$perm_details['data']['description']}}</p>
                                                @endif
                                                
                                                @foreach($perm_details['actions'] as $key => $action)
                                                    <?php $selected = false; ?>

                                                    <?php 
                                                        $disabled = (strtolower($action) == 'read') ? true:false;
                                                    ?>

                                                    @if((isset($active_perms[$type]) AND in_array($key, $active_perms[$type])) OR strtolower($action) == 'read')
                                                        <?php $selected = true;?>
                                                    @endif

                                                    @if($disabled)
                                                        <input type="hidden" name="perms[<?=$type?>][]" value="{{$key}}" />
                                                    @else
                                                        <div class="perms" <?php if(!$has_perms) echo 'style="display:none;"';?>><input type="checkbox" name="perms[<?=$type?>][]" value="{{$key}}" <?php if($selected) echo 'checked="checked"';?>>&nbsp;{{$action}}<br /></div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    <?php }?>
                                @endforeach
                            </div>
                        </section>
                    </div>
                @endif
            </div>
        {{Form::close()}}
    </div>
@stop

@section('scripts')
    <script src='/themes/hummingbird/default/lib/masonry/masonry.js'></script>
    <script type='text/javascript' src='/assets/cms/js/pStrength.jquery.js'></script>

    <script>
        var $msnry;

        function simpleParallax(el, min_scroll)
        {
            min_scroll = (min_scroll <= 0) ? 0:min_scroll;

            //This variable is storing the distance scrolled
            var scrolled = jQuery('body').scrollTop() + 1;
            scrolled = (scrolled * 0.1) + min_scroll; //calculate new position
            scrolled = (scrolled > 100) ? 100:scrolled; //100% max position
            scrolled = (scrolled < 0) ? 0:scrolled; //0% min position
            
            //Every element with the class "scroll" will have parallax background 
            //Change the "0.3" for adjusting scroll speed.
            el.css('background-position', 'center ' + scrolled + '%');
        }

        function initMasonry()
        {
            // initialize
            $msnry = $('#permissions');

            $msnry.masonry(
            {
                columnWidth: '.permission-holder',
                itemSelector: '.permission-holder'
            });
        }

        $(document).ready(function()
        {
            if($('#permissions .permission-holder').length > 0)
            {
                initMasonry();
            }

            if($('.user-profile .content-header').length > 0)
            {
                if($(".user-profile .content-header").data('background') != '')
                {
                    $(".user-profile .content-header").css('background-image', 'url(' + $(".user-profile .content-header").data('background') + ')');
                    $(".user-profile .content-header, .user-profile .content-header .header-section").css('height', '250px');
                }

                $(window).scroll(function(e) 
                {
                    simpleParallax($('.user-profile .content-header'), 0);
                });
            }

            if($("#password").length > 0)
            {
                $("#password").on('keyup', function()
                {
                    if($(this).val().length > 0)
                    {
                        $("#confirm-password").closest('.form-group').removeClass('hide');
                    }
                    else
                    {
                        $("#confirm-password").closest('.form-group').addClass('hide');
                    }
                });
            }
        });
    </script>
@stop
