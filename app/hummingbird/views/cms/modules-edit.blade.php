@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />

<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

.preview-wrapper
{
    width:100%;
    margin:20px 0;
}

.preview-wrapper .html-structure * { display: block; }
.preview-wrapper .html-structure a {color:blue !important;}

#module label::first-letter
{
    text-transform: uppercase;
}

.ace-editor.html-structure {min-height:300px;}

</style>
@stop

@if(isset($breadcrumbs))
    @include('HummingbirdBase::cms.include.breadcrumbs', array('breadcrumbs' => $breadcrumbs))
@endif

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <section class="panel" style="padding:20px;">
                    @foreach ($errors->all('<li>:message</li>') as $error)
                        {{$error}}    
                    @endforeach

                    {{ Form::open(array('url' => App::make('backend_url').'/modules/edit/' . $module->id, 'class' => 'form-horizontal', 'id' => 'module-submit')) }}
                        <div class="form-group hidden-xs">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default btn-primary pull-right">Update</button>
                            </div>
                        </div>

                        <div id="tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                                <li role="presentation" class="active"><a href="#module" aria-controls="module" role="tab" data-toggle="tab">Module Data</a></li>

                                @if($module->moduletemplate_id === null)
                                    <li role="presentation"><a href="#taxonomy" aria-controls="taxonomy" role="tab" data-toggle="tab">Taxonomy</a></li>                                    
                                @endif

                                @if(General::debugAllowed())
                                    <li role="presentation"><a href="#preview" aria-controls="preview" role="tab" data-toggle="tab">Preview</a></li>
                                @endif
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane" id="details" style="padding-top:20px;">
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label">Title:</label>
                                        <div class="col-sm-10">
                                            <input name="name" id="name" type="text" class="form-control" placeholder="Module Title" value="{{$module->name}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="notes" class="col-sm-2 control-label">Notes:</label>
                                        <div class="col-sm-10">
                                            {{ Form::textarea('notes', $module->notes, array('class' => 'form-control textareas', 'rows' => '3')) }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-10 pull-right">
                                            <p class="small text-right"><span><strong>Shortcode:</strong> {{$module->shortcode()}}</span></p>
                                        </div>  
                                    </div> 

                                    <div class="form-group">
                                        <div class="col-sm-10 pull-right">
                                            <p class="small text-right"><span><strong>Last Updated:</strong> {{ (strtotime($module->created_at) == strtotime($module->updated_at)) ? 'n/a':$module->updated_at->diffForHumans() }}</span></p>
                                        </div>  
                                    </div> 
                                </div>

                                <div role="tabpanel" class="tab-pane active" id="module" style="padding-top:20px;">
                                    @if($module->template())
                                        <?php $template = Moduletemplate::find($module->moduletemplate_id); ?>

                                        <?php $ModuleBuilder = new ModuleParser($template, $module); ?>

                                        <input type="hidden" name="moduletemplate_id" value="{{$module->moduletemplate_id}}" />

                                        <?php 
                                            if(count($ModuleBuilder->get_var_by_key('module_bag')['fields']) > 0)
                                            {
                                                foreach($ModuleBuilder->get_var_by_key('module_bag')['fields'] as $items)
                                                {
                                                    foreach($items as $tag => $item)
                                                    {
                                                        $field_title = $ModuleBuilder->get_field_title($tag, $item['settings']);
                                                        $field_name = $item['settings']['name'];

                                                        switch($tag)
                                                        {
                                                            case 'image':?>
                                                                <div class="form-group media-library-holder module-{{$tag}}" data-attribute="module-{{$tag}}">
                                                                    {{ Form::label($field_title, $field_title, array('class' => 'col-sm-2 control-label')) }}
                                                                    <div class="col-sm-8 image-link">
                                                                        <div class="input-group">
                                                                            <input type="text" name="fields[{{$field_name}}]" class="form-control media-image field" readonly="" @if($module->module_data['structure'][$field_name] != '') value="{{$module->module_data['structure'][$field_name]}}" @endif>
                                                                            <span class="input-group-btn">
                                                                                <span class="btn btn-default btn-file pull-right">
                                                                                    Browse...<input type="file" multiple="" class="browse-media-library" @if($module->module_data['structure'][$field_name] != '') data-collection-id="{{ Media::where('location', '=', $module->module_data['structure'][$field_name])->whereNull('deleted_at')->first()->collection }}" @endif>
                                                                                </span>
                                                                            </span>
                                                                        </div>

                                                                        @if(isset($item['settings']['notes']) AND $item['settings']['notes'] != '')
                                                                            <span class="help-block">{{$item['settings']['notes']}}</span>
                                                                        @endif

                                                                        <span class="help-block @if($module->module_data['structure'][$field_name] == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                                                                    </div>

                                                                    <div class="col-sm-2 @if($module->module_data['structure'][$field_name] == '') hide @endif image-holder">
                                                                        <img src="{{$module->module_data['structure'][$field_name]}}" class="img-responsive" />
                                                                    </div>
                                                                </div>

                                                                <?php break;
                                                            case strpos($tag, 'content') !== false:
                                                                if(strpos($tag, "*content*") !== false)
                                                                {?>
                                                                    <div class="form-group module-{{$tag}}" data-attribute="module-{{$tag}}">
                                                                        <label class="col-sm-2 control-label">{{$field_title}}:</label>
                                                                        <div class="col-sm-10">
                                                                            {{ Form::textarea('fields['.$field_name.']', (isset($module->module_data['structure'][$field_name])) ? $module->module_data['structure'][$field_name]:'', array('class' => 'field redactor-content')) }}
                                                                            @if(isset($item['settings']['notes']) AND $item['settings']['notes'] != '')
                                                                                <span class="help-block">{{$item['settings']['notes']}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                <?php }
                                                                else
                                                                {?>
                                                                    <div class="form-group module-{{$tag}}" data-attribute="module-{{$tag}}">
                                                                        <label class="col-sm-2 control-label">{{$field_title}}:</label>
                                                                        <div class="col-sm-10">
                                                                            {{ Form::textarea('fields['.$field_name.']', $module->module_data['structure'][$field_name], array('class' => 'field form-control textareas', 'rows' => '3')) }}
                                                                            @if(isset($item['settings']['notes']) AND $item['settings']['notes'] != '')
                                                                                <span class="help-block">{{$item['settings']['notes']}}</span>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                <?php }
                                                                break;
                                                            default:?>
                                                                <div class="form-group module-{{$tag}}" data-attribute="module-{{$tag}}">
                                                                    <label for="{{$tag}}" class="col-sm-2 control-label">{{$field_title}}:</label>
                                                                    <div class="col-sm-10">
                                                                        <input name="fields[{{$field_name}}]" id="{{$tag}}" type="text" class="form-control" @if(isset($module->module_data['structure'][$field_name])) value="{{$module->module_data['structure'][$field_name]}}" @endif>
                                                                        @if(isset($item['settings']['notes']) AND $item['settings']['notes'] != '')
                                                                            <span class="help-block">{{$item['settings']['notes']}}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <?php break;
                                                        }
                                                    }
                                                }
                                            }?>

                                            @if($ModuleBuilder->hasTabs())
                                                <div>
                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs" role="tablist" data-min="{{$ModuleBuilder->tabs_data['min']}}" @if(isset($ModuleBuilder->tabs_data['max'])) data-max="{{$ModuleBuilder->tabs_data['max']}}" @endif>
                                                        <li role="presentation"><a href="#tab_settings" aria-controls="tab_settings" role="tab" data-toggle="tab">Settings</a></li>

                                                        @for($i = 1; $i <= $ModuleBuilder->tabs_data['min']; $i++)
                                                            <li role="presentation" class="@if($i == 1) active @endif"><a href="#tab_{{$i}}" aria-controls="tab_{{$i}}" role="tab" data-toggle="tab">Tab {{$i}}</a></li>
                                                        @endfor
                                                    </ul>
                                                       
                                                    <!-- Tab panes -->
                                                    <div class="tab-content"> 
                                                        <div role="tabpanel" class="tab-pane" id="tab_settings">
                                                            <h3 class="normal">Details</h3>

                                                            @if(isset($ModuleBuilder->tabs_data['title']) AND $ModuleBuilder->tabs_data['title'] != '')
                                                                <h4 class="normal">{{$ModuleBuilder->tabs_data['title']}}</h4>
                                                            @endif

                                                            @if(isset($ModuleBuilder->tabs_data['desc']) AND $ModuleBuilder->tabs_data['desc'] != '')
                                                                <p>{{$ModuleBuilder->tabs_data['desc']}}</p>
                                                            @endif

                                                            <?php 
                                                                $order = (isset($module->module_data['structure']['tab_settings']) AND $module->module_data['structure']['tab_settings'] != '') ? $module->module_data['structure']['tab_settings']:false;

                                                                if(!$order)
                                                                {
                                                                    for($i = 1; $i <= $ModuleBuilder->tabs_data['min']; $i++)
                                                                    {
                                                                        $order .= $i . ',';
                                                                    }

                                                                    $order = substr($order, 0, -1);
                                                                }
                                                            ?>

                                                            <div class="form-group module-tab_settings">
                                                                <label class="col-sm-2 control-label">Order:</label>
                                                                <div class="col-sm-10">
                                                                    <input name="fields[tab_settings]" type="text" class="form-control"  value="{{$order}}" >
                                                                    <span class="help-block">Change the order, and which options will appear, by entering them here.</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @for($i = 1; $i <= $ModuleBuilder->tabs_data['min']; $i++)
                                                            <div role="tabpanel" class="tab-pane @if($i == 1) active @endif" id="tab_{{$i}}">
                                                                <div class="clearfix">&nbsp;</div>
                                                                <?php 

                                                                    foreach($ModuleBuilder->get_var_by_key('tabs_data')['fields'] as $items)
                                                                    {
                                                                        foreach($items as $tag => $item)
                                                                        {
                                                                            $collection_id = NULL;
                                                                            // General::pp($item, true);
                                                                            $field_title = $ModuleBuilder->get_field_title($tag, $item['settings']);
                                                                            $field_name = 'tab_' . $i . '_' . $item['settings']['name'];

                                                                            switch($tag)
                                                                            {
                                                                                case 'image':
                                                                                    if(isset($module->module_data['structure'][$field_name]) AND $module->module_data['structure'][$field_name] != '')
                                                                                    {
                                                                                        $collection = Media::where('location', '=', $module->module_data['structure'][$field_name])->whereNull('deleted_at')->first();
                                                                                        $collection_id = (null !== $collection) ? $collection->collection:NULL;
                                                                                    }?>

                                                                                    <div class="form-group media-library-holder module-{{$tag}}" data-attribute="module-{{$tag}}">
                                                                                        {{ Form::label($field_title, $field_title, array('class' => 'col-sm-2 control-label')) }}
                                                                                        <div class="col-sm-8 image-link">
                                                                                            <div class="input-group">
                                                                                                <input type="text" name="fields[{{$field_name}}]" class="form-control media-image field" readonly="" @if(isset($module->module_data['structure'][$field_name]) AND $module->module_data['structure'][$field_name] != '') value="{{$module->module_data['structure'][$field_name]}}" @endif>
                                                                                                <span class="input-group-btn">
                                                                                                    <span class="btn btn-default btn-file pull-right">
                                                                                                        Browse...<input type="file" multiple="" class="browse-media-library"  @if(isset($collection_id) AND $collection_id !== NULL) data-collection-id="{{$collection_id}}" @endif>
                                                                                                    </span>
                                                                                                </span>
                                                                                            </div>
                                                                                            @if(isset($item['settings']['notes']) AND $item['settings']['notes'] != '')
                                                                                                <span class="help-block">{{$item['settings']['notes']}}</span>
                                                                                            @endif
                                                                                            <span class="help-block @if(isset($module->module_data['structure'][$field_name]) AND $module->module_data['structure'][$field_name] == '') hide @endif"><a href="#" class="remove-media-library">Remove featured image</a></span>
                                                                                        </div>

                                                                                        <div class="col-sm-2 @if(isset($module->module_data['structure'][$field_name]) AND $module->module_data['structure'][$field_name] == '') hide @endif image-holder">
                                                                                            <img @if(isset($module->module_data['structure'][$field_name]) AND $module->module_data['structure'][$field_name]) src="{{$module->module_data['structure'][$field_name]}}" @endif class="img-responsive" />
                                                                                        </div>
                                                                                    </div>

                                                                                    <?php break;
                                                                                case strpos($tag, 'content') !== false:
                                                                                    if(strpos($tag, "*content*") !== false)
                                                                                    {?>
                                                                                        <div class="form-group module-{{$tag}}" data-attribute="module-{{$tag}}">
                                                                                            <label class="col-sm-2 control-label">{{$field_title}}:</label>
                                                                                            <div class="col-sm-10">
                                                                                                {{ Form::textarea('fields['.$field_name.']', (isset($module->module_data['structure'][$field_name])) ? $module->module_data['structure'][$field_name]:'', array('class' => 'field redactor-content')) }}
                                                                                            </div>
                                                                                            @if(isset($item['settings']['notes']) AND $item['settings']['notes'] != '')
                                                                                                <span class="help-block">{{$item['settings']['notes']}}</span>
                                                                                            @endif
                                                                                        </div>
                                                                                    <?php }
                                                                                    else
                                                                                    {?>
                                                                                        <div class="form-group module-{{$tag}}" data-attribute="module-{{$tag}}">
                                                                                            <label class="col-sm-2 control-label">{{$field_title}}:</label>
                                                                                            <div class="col-sm-10">
                                                                                                {{ Form::textarea('fields['.$field_name.']', $module->module_data['structure'][$field_name], array('class' => 'field form-control textareas', 'rows' => '3')) }}
                                                                                            </div>
                                                                                            @if(isset($item['settings']['notes']) AND $item['settings']['notes'] != '')
                                                                                                <span class="help-block">{{$item['settings']['notes']}}</span>
                                                                                            @endif
                                                                                        </div>
                                                                                    <?php }
                                                                                    break;
                                                                                default:?>
                                                                                    <div class="form-group module-{{$tag}}" data-attribute="module-{{$tag}}">
                                                                                        <label for="{{$tag}}" class="col-sm-2 control-label">{{$field_title}}:</label>
                                                                                        <div class="col-sm-10">
                                                                                            <input name="fields[{{$field_name}}]" id="{{$tag}}" type="text" class="form-control" @if(isset($module->module_data['structure'][$field_name])) value="{{$module->module_data['structure'][$field_name]}}" @endif>
                                                                                        </div>
                                                                                        @if(isset($item['settings']['notes']) AND $item['settings']['notes'] != '')
                                                                                            <span class="help-block">{{$item['settings']['notes']}}</span>
                                                                                        @endif
                                                                                    </div>
                                                                                    <?php break;
                                                                            }
                                                                        }
                                                                    }?>
                                                            </div>
                                                        @endfor
                                                    </div>
                                                </div>  
                                            @endif


                                        <?php 
                                            $loop_html = str_replace(array("\r\n", "\r", "\n"), "", $template->html);
                                            $loop_tags = array();
                                            $loop_tags_pattern = '#\{{(.*?)\}}#';
                                            preg_match_all($loop_tags_pattern, $loop_html, $loop_tags);

                                            // if(count($loop_tags) > 0)
                                            // {   
                                            //     foreach($loop_tags[1] as $key => $tag)
                                            //     {
                                            //         $settings_pattern = '#\[(.*?)\]#';
                                            //         $settings_matches = array();

                                            //         preg_match_all($settings_pattern, $tag, $settings_matches);

                                            //         $settings_arr = array();

                                            //         if(count($settings_matches[0]) > 0)
                                            //         {
                                            //             $settings = str_replace(array("[", "]"), "", $settings_matches[0][0]);
                                            //             $settings = explode(":", $settings);

                                            //             foreach($settings as $setting)
                                            //             {
                                            //                 $field = explode("=", $setting);
                                            //                 $settings_arr[$field[0]] = $field[1];
                                            //                 $tag = str_replace($setting, "", $tag);
                                            //             }

                                            //             $tag = str_replace(array("[", "]", ":"), "", $tag);   
                                            //         }

                                            //         $field_title = (isset($settings_arr) AND is_array($settings_arr) AND isset($settings_arr['title']) AND $settings_arr['title'] != '') ? $settings_arr['title']:$tag;                                                }  
                                            // }
                                        ?> 

                                        <script>
                                             var tags   = {{json_encode($loop_tags[1])}};
                                             var gethtml   = '{{$loop_html}}';
                                             var css    = '{{str_replace(array("\r\n", "\r", "\n"), "", $template->css);}}';
                                        </script>
                                    @else
                                        <div class="form-group">
                                            {{ Form::label('html', 'Structure: ', array('class' => 'col-sm-2')) }}
                                            <div class="col-sm-10">
                                                <span class="help-block">The structure of your new module. Will appear on page as decided here.</span>
                                                <textarea name="html" class="hide" id="html"></textarea>
                                                <div class="ace-editor html-structure" data-type="html">{{trim(htmlentities($module->module_data['structure']))}}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('css', 'Custom CSS: ', array('class' => 'col-sm-2')) }}
                                            <div class="col-sm-10">
                                                <span class="help-block">If provided, custom CSS will appear in <span class="italic">head</span> section of your website.</span>
                                                <textarea name="css" class="hide" id="css"></textarea>
                                                <div class="ace-editor" data-type="css">{{trim($module->module_data['css'])}}</div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('js', 'Custom JavaScript: ', array('class' => 'col-sm-2')) }}
                                            <div class="col-sm-10">
                                                <span class="help-block">If provided, custom Javascript will appear before the final <span class="italic">body</span> section of your website.</span>
                                                <textarea name="js" class="hide" id="js"></textarea>
                                                <div class="ace-editor" data-type="javascript">{{trim($module->module_data['js'])}}</div>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                @if($module->moduletemplate_id === null)
                                    <div role="tabpanel" class="tab-pane" id="taxonomy" style="padding-top:20px;">
                                        @include('HummingbirdBase::cms.taxonomy-layout', array('taxonomy' => $taxonomy))
                                    </div>
                                @endif

                                @if(General::debugAllowed())
                                    <div role="tabpanel" class="tab-pane" id="preview" style="padding-top:20px;">
                                        <div class="preview-wrapper">
                                            @if($module->template())
                                                <div class="html-structure">
                                                    {{$template->html}}
                                                </div>

                                                @if($template->css != '') 
                                                    <div class="styling">
                                                        <style type="text/css">
                                                            {{$template->css}}
                                                        </style>
                                                    </style>
                                                @endif
                                            @else
                                                <div class="html-structure">
                                                    {{$module->module_data['structure']}}
                                                </div>

                                                @if($module->module_data['css'] != '') 
                                                    <div class="styling">
                                                        <style type="text/css">
                                                            {{$module->module_data['css']}}
                                                        </style>
                                                    </style>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group hidden-xs">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default btn-primary pull-right">Update</button>
                            </div>
                        </div>
                    {{Form::close()}}
                </section>
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
    <script src="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>
    <script src="/themes/hummingbird/default/lib/media-library/media-library.js"></script>
    <script src="/themes/hummingbird/default/lib/ace/ace.js"></script>

    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        var editor;

        function responsifyImages()
        {
            $(".preview-wrapper img").each(function() 
            {
                $(this).addClass("img-responsive");
            });
        }

        $('.ace-editor').each(function( index ) 
        {
            editor = ace.edit(this);
            editor.setTheme("ace/theme/twilight");
            editor.getSession().setMode("ace/mode/" + $(this).data('type'));
            editor.setOptions({
                'maxLines': 50,
                'minLines': 10,
                'firstLineNumber': 1
            });
        });

        $(document).ready(function()
        {
            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }

            if($('#module-submit').length == 1)
            {
                $("#module-submit").submit(function(e)
                {
                    $('.ace-editor').each(function( index ) 
                    {
                        editor = ace.edit(this);
                    
                        $(this).prev().val(editor.getSession().getValue());
                    });

                    return;
                });
            }

            $(window).bind('keydown', function(event) {
                if (event.ctrlKey || event.metaKey) {
                    switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                    case 'f':
                    case 'g':
                        event.preventDefault();
                        alert('ctrl-g');
                        $(".panel form").submit();
                        break;
                    }
                }
            });


            if($(".preview-wrapper").length > 0)
            {
                responsifyImages();
            }

            if($(".nav-tabs").length > 0)
            {

                $(".nav-tabs li a").click(function()
                {
                    if($(this).attr('href') == '#preview')
                    {
                        if($("#module #html").length > 0)
                        {
                            $('#module .ace-editor').each(function( index ) 
                            {
                                editor = ace.edit(this);
                            
                                $(this).prev().val(editor.getSession().getValue());
                            });

                            var html = $("#module #html").val();
                            var css = $("#module #css").val();

                            $("#preview .preview-wrapper .html-structure").html(html);
                            $("#preview .preview-wrapper .styling style").html(css);

                            responsifyImages();
                        }
                        else
                        {
                            /* Transferring content for tags */
                            var preview_html = gethtml;

                            $.each(tags, function(index, value)
                            {
                                var field_value = $.trim($("#module .module-" + value + " .field").val());

                                if(field_value != '')
                                {
                                    field_value = (value == 'image') ? '<img src="' + $("#module .module-" + value + " .field").val() + '" />':$("#module .module-" + value + " .field").val();
                                }

                                preview_html = preview_html.replace("\{\{"+value+"\}\}", field_value);
                            });

                            $("#preview .preview-wrapper .html-structure").html(preview_html);
                            $("#preview .preview-wrapper .styling style").html(css);

                            responsifyImages();
                        }
                    }
                })
            }
        });
    </script>
@stop

