@extends('HummingbirdBase::cms.layout')

@section('content')

<h1><a href="{{url(App::make('backend_url').'/'.$modelname.'s')}}">All {{ucfirst($modelname)}}</a> > Export {{$modelname.'s'}}</h1>

<?php echo Form::open(array('url' => App::make('backend_url').'/'.$modelname.'s/export/')) ?>

<table cellpadding="5" cellspacing="0" id="edit">
    <tbody>
        
        @foreach($fields as $field)
        
        <tr>
            <td class="row_name">{{ Form::label($field, $field) }}</td>
            <td>{{ Form::checkbox($field, $field, $field) }}</td>
        </tr>
        
        @endforeach
        
    </tbody>
</table>


<p><input type="submit" value="Export"></p>
<?php echo Form::close() ?>

@stop
