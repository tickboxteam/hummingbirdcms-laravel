@extends('HummingbirdBase::cms.layout')

@section('styles')
<style type="text/css">
    .user-profile {}
    .user-profile .content-header 
    {
        height:100px;
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size:cover;
        position:relative;
        margin:-20px -20px 20px;
        overflow: hidden;
    }

    .user-profile .content-header .header-section {height:auto;color:white;position:absolute;top:0;left:0;right:0;padding:20px;border:0;background:rgb(0,0,0);background:rgba(0,0,0,0.4);}
    .user-profile .content-header .header-section h1 {font-size:2.8rem;font-size:28px;font-weight:300;}
    .user-profile .content-header .header-section h1 > small {color:#EDEDED;}


    .hide {transition: opacity 500 ease-in-out;}
</style>
@stop

@section('content')

<div class="user-profile">
    <div class="content-header content-header-media" data-background="{{$user->getPreference('cover-image')}}">
        <div class="header-section">
            <h1>{{$user->name}} ({{$user->role->name}})<br /><small>{{$user->username}}</small></h1>
        </div>
    </div>

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>  
    @endif

    @if(Session::has('message'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-info fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    {{ Session::get('message') }}
                </div>
            </div>
        </div>  
    @endif

    @if (Session::has('errors'))
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="alert alert-block alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    @foreach ($errors->all('<li>:message</li>') as $error)
                        {{$error}}    
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    
<?php echo Form::open(array('url' => App::make('backend_url').'/profile/', 'class' => 'form-horizontal')) ?>
<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name: </label>
                <div class="col-sm-10">
                    <input id="name" name="name" type="text" class="form-control" placeholder="Users name" value="{{$user->name}}">
                </div>
            </div>
            <div class="form-group">
                <label for="username" class="col-sm-2 control-label">Username:</label>
                <div class="col-sm-10">
                    <input id="username" name="username" type="text" class="form-control" placeholder="Username" value="{{$user->username}}">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email:</label>
                <div class="col-sm-10">
                    <input id="email" name="email" type="text" class="form-control" placeholder="Email address" value="{{$user->email}}">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password:</label>
                <div class="col-sm-10">
                    <input type="password" autocomplete="off" name="password" id="password" class='password-strength form-control' size="40" class="left" data-display="myDisplayElement1" /> 
                    <div class="left" id="myDisplayElement1"></div>
                    <span class="help-block">Leave blank to keep current password.</span>        
                </div>
            </div>
            <div class="form-group hide">
                <label for="confirm-password" class="col-sm-2 control-label">Confirm Password:</label>
                <div class="col-sm-10">
                    {{Form::password('password_confirmation', array('class' => 'form-control', 'id' => 'confirm-password'))}}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default btn-primary">Update</button>
                    <a href="/hummingbird/logout" class="btn btn-default pull-right"><i class="fa fa-sign-out"></i> Logout</a>
                </div>
            </div>
        </section>
    </div>
</div>

{{Form::close()}}
</div>

@stop

@section('scripts')
<script type='text/javascript' src='/assets/cms/js/pStrength.jquery.js'></script>

<script>
    function simpleParallax(el, min_scroll)
    {
        min_scroll = (min_scroll <= 0) ? 0:min_scroll;

        //This variable is storing the distance scrolled
        var scrolled = jQuery('body').scrollTop() + 1;
        scrolled = (scrolled * 0.3) + min_scroll; //calculate new position
        scrolled = (scrolled > 100) ? 100:scrolled; //100% max position
        scrolled = (scrolled < 0) ? 0:scrolled; //0% min position
        
        //Every element with the class "scroll" will have parallax background 
        //Change the "0.3" for adjusting scroll speed.
        el.css('background-position', 'center ' + scrolled + '%');
    }

    $(document).ready(function()
    {
        if($('.user-profile .content-header').length > 0)
        {
            if($(".user-profile .content-header").data('background') != '')
            {
                $(".user-profile .content-header").css('background-image', 'url(' + $(".user-profile .content-header").data('background') + ')');
                $(".user-profile .content-header, .user-profile .content-header .header-section").css('height', '250px');
            }

            $(window).scroll(function(e) 
            {
                simpleParallax($('.user-profile .content-header'), 50);
            });
        }

        if($("#password").length > 0)
        {
            $("#password").on('keyup', function()
            {
                if($(this).val().length > 0)
                {
                    $("#confirm-password").closest('.form-group').removeClass('hide');
                }
                else
                {
                    $("#confirm-password").closest('.form-group').addClass('hide');
                }
            });
        }
    });
</script>

@stop
