@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php 
                        $last_el = end($breadcrumbs);
                        $last_el = key($breadcrumbs);
                    ?>

                    @foreach ($breadcrumbs as $key => $breadcrumb)
                        <li>
                            @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '' AND $key != $last_el)
                                <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon'] AND $key == 0) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                            @else
                                @if($breadcrumb['icon'] AND $key == 0) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                            @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop