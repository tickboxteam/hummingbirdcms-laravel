
<script>
    $(document).ready(function() {


        if ($('#ckeditor').length > 0) {
            CKEDITOR.replace('ckeditor');
        }

        $('.section_nav .subnav-section h1 a').click(function(e) {
            e.preventDefault();

            $(this).parent().parent().find('h1 > a, ul').toggle();
        });
        
        if($('.redactor').length > 0) {
            $('.redactor').redactor();
        }
        
        if ($('.password-strength').length > 0) {

            $('.password-strength').pStrength({
                'passwordValidFrom': <?php echo Config::get('HummingbirdBase::hummingbird.passwordstrength') ?>,
                'changeBackground': false,
                'onPasswordStrengthChanged': function(passwordStrength, strengthPercentage) {
                    if ($(this).val()) {
                        $.fn.pStrength('changeBackground', this, passwordStrength);
                    } else {
                        $.fn.pStrength('resetStyle', this);
                    }
                    $('#' + $(this).data('display')).html('Your password strength is ' + strengthPercentage + '%');
                },
                'onValidatePassword': function(strengthPercentage) {
                    $('#' + $(this).data('display')).html(
                            $('#' + $(this).data('display')).html() + ' Great, now you can continue to register!'
                            );
                }
            });
        }
    });


</script>