
<fieldset>
    <legend>Plugin blocks</legend>
    
    @foreach($blocks as $block)
    
        @include($block)
    
    @endforeach
    
    
</fieldset>