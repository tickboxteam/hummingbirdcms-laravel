@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')
@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {{ Session::get('success') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-info alert-danger fade in">
                {{ Session::get('message') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {{ Session::get('error') }}
            </div>
        </div>
    </div>  
@endif

    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                <h1 class="normal">Edit redirect for: {{$redirect->from}}</h1>

                @if(count($errors) > 0) 
                    @foreach ($errors->all('<li class="error red" style="color:red;">:message</li>') as $error)
                        {{$error}}    
                    @endforeach
                @endif

                <?php echo Form::open(array('route' => array(App::make('backend_url').".redirections.update", 'redirections' => $redirect->id), 'method' => 'PUT', 'class' => 'form-horizontal')) ?>
                    <div class="form-group">
                        <label for="from" class="col-sm-2 control-label">Redirect: <strong>*</strong></label>   
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input readonly type="text" class="from form-control" placeholder="From URL" value="{{$redirect->from}}" name="from" />
                                <span class="input-group-addon" style="border-left: 0; border-right: 0;">-</span>
                                <input readonly type="text" class="to form-control" placeholder="From URL" value="{{$redirect->to}}" name="to" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="type" class="col-sm-2 control-label">Type: <strong>*</strong></label>
                        <div class="col-sm-10">
                            {{Form::select('type', Redirections::get_types_selection(), $redirect->type, array('class' => 'form-control'))}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="type" class="col-sm-2 control-label">Category: </label>
                        <div class="col-sm-10">
                            <select name="category" id="category" class="form-control">
                                <option value="">--- None ---</option>

                                @if(count($categories) > 0)
                                    @foreach($categories as $category)
                                        <option value="{{$category}}" @if($redirect->category == $category) selected="selected" @endif>&nbsp;&nbsp;&nbsp;{{$category}}</option>
                                    @endforeach
                                @endif

                                <option disabled></option>
                                <option value="NEW">Create new category</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group hide">
                        <div class="col-sm-offset-2 col-sm-6">
                            <input class="custom_field form-control" id="custom_category" type="text" name="custom_category">
                            <span class="help-block">Enter a new category name for this redirect</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="comments" class="col-sm-2 control-label">Comments: </label>
                        <div class="col-sm-10">
                            <textarea name="comments" class="form-control textareas" data-summary-limit="255">{{$redirect->comments}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="pull-right btn btn-default btn-primary">Update</button>
                        </div>
                    </div>
                <?php echo Form::close() ?>
            </section>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            if($("#category").length > 0)
            {
                $("#category").change(function()
                {
                    if($("#category").val() == 'NEW')
                    {
                        $("#custom_category").closest('.form-group').removeClass("hide");   
                    }
                    else
                    {
                        $("#custom_category").closest('.form-group').addClass("hide");
                    }
                });
            }
        });
    </script>
@stop