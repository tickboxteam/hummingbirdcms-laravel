@extends('HummingbirdBase::cms.layout')

@section('content')

<?php $even = false; ?>

<h1>All menu items</h1>

<table class='results' cellpadding='5' cellspacing='0'>
    <thead>
        <th>Name</th>
        <th>Page</th>
        <th>Menu</th>
        <th>Order</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($menuitems as $menuitem)
        
        <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
            <td>{{$menuitem->name}}</td>
            <td>{{$menuitem->page->title}}</td>
            <td>{{$menuitem->menu->name}}</td>
            <td>{{$menuitem->order}}</td>
            <td>
                <a href='/{{App::make('backend_url')}}/menuitems/edit/{{$menuitem->id}}'>Edit</a> | 
                <a href='/{{App::make('backend_url')}}/menuitems/delete/{{$menuitem->id}}'>Delete</a>
            </td>
        </tr>
        
        @endforeach
    </tbody>
</table>

<h1>Add new menu item</h1>
<?php echo Form::open(array('url' => App::make('backend_url').'/menuitems/add', 'method' => 'post')) ?>
    <table cellpadding="5" cellspacing="0" id="add-mini">
        <tbody>
            <tr>
                <td class="row_name">Name: <strong>*</strong></td>
                <td><input class="required input_box" id="name" type="text" name="name"></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" value="Add menu item" name="add">
                </td>
            </tr>
        </tbody>
    </table>
<?php echo Form::close()?>

@stop
