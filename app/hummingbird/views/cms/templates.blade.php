@extends('HummingbirdBase::cms.layout')

@section('content')
    <section id="template-side" class="col-md-2 hide">
        <ul>
            <li>
                <div class="btn-group">
                    <button id="mbcode_size_phone" type="button" class="btn btn-info" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="Smartphone Portrait<br/><small>(shortcut alt + q)</small>"><i class="fa fa-mobile"></i></button>
                    <button id="mbcode_size_phone_v" type="button" class="btn btn-info" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="Smartphone Landscape<br/><small>(shortcut alt + w)</small>"><i class="fa fa-mobile l90"></i></button>
                    <button id="mbcode_size_tablet" type="button" class="btn btn-info" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="Tablet Portrait<br/><small>(shortcut alt + e)</small>"><i class="fa fa-tablet"></i></button>
                    <button id="mbcode_size_desktop" type="button" class="btn btn-info" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="Laptop<br/><small>(shortcut alt + r)</small>"><i class="fa fa-laptop"></i></button>
                    <button id="mbcode_size_tablet_v" type="button" class="btn btn-info" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="Desktop<br/><small>(shortcut alt + t)</small>"><i class="fa fa-desktop"></i></button>
                    <button id="mbcode_size_full" type="button" class="btn btn-info active" rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-original-title="Fit to screen<br/><small>(shortcut alt + y)</small>"><i class="fa fa-arrows"></i></button>
                </div>
            </li>
        </ul>
    </section>


    <article id="template-area" class="col-sm-12">
        <div class="f_pad" id="flexicontainer"></div>
    </article>

    <style>
        #template-area {
            background:white;
            min-height:600px;
            border:1px solid #CCC;
        }

        #template-area.template-hold
        {
            background:#2A313A url('/themes/hummingbird/default/images/checkerboard.gif') repeat top left;
            opacity:0.25;
        }



#left_col {
    display: inline;
    float: left;
    margin-right: 0px;
    padding: 0;
    text-align: left;
    width: 225px;
    background-color: #ccc; height: 800px;
}

#save_button{ float:right; margin-right:40px; display: none;}

#right_col {
    color: #6e7063;
    min-width: 737px;
    padding: 0;
    text-align: left;
    width: auto;
}

.holding {display: none;}

.structure_input { margin-bottom:20px;}

.clearfix:after {
    content: ".";
    display: block;
    clear: both;
    visibility: hidden;
    line-height: 0;
    height: 0;
}

.tools { position: absolute; top: 0px; right: 0px;}

.tool {
    float: right;
    height: 20px;
    padding: 3px;
    text-align: center;
    width: 20px;
}

.tool:hover { opacity: 0.8; cursor: pointer; }

.tag                                                { border-radius:3px 0px 3px 0px; background-color: #fff; border: 1px solid #ccc; 
    color: #333; font-weight: bold; font-size: 13px; padding: 3px 6px; display: inline-block; top: -1px; left: 0px; position: absolute; }
.container_top h1                                   { margin-top: 0px; padding-top: 20px; }


#flexicontainer.container-fluid                     { background-color: #ffffff; min-height: 300px; padding-top: 40px; }
#flexicontainer.container-fluid .row              { margin-left:0px; margin-right: 0px;}
#flexicontainer.container-fluid .row .column {padding-top:20px;}
.f_pad                                              { padding:15px; border-radius:3px; position: relative; border: 1px solid #ccc; }
.row    { background-color: #fafafa; min-height: 100px; margin-bottom: 10px; padding-top: 40px;}
.row:before, .row:after { display: inline;}

.column                                             { background-color:white;min-height: 100px; }
.row:before {
    background-color: #f5f5f5;
    border: 1px solid #dddddd;
    border-radius: 4px 0;
    color: #9da0a4;
    content: "Row";
    font-size: 12px;
    font-weight: bold;
    left: -1px;
    line-height: 2;
    padding: 3px 7px;
    position: absolute;
    top: -1px;
}

.column:before {
    background-color: #f5f5f5;
    border: 1px solid #dddddd;
    border-radius: 4px 0;
    color: #9da0a4;
    content: "Column";
    font-size: 12px;
    font-weight: bold;
    left: -1px;
    line-height: 2;
    padding: 3px 7px;
    position: absolute;
    top: -1px;
}

.container-fluid:before {
    background-color: #f5f5f5;
    border: 1px solid #dddddd;
    border-radius: 4px 0;
    color: #9da0a4;
    content: "Container";
    font-size: 12px;
    font-weight: bold;
    left: -1px;
    line-height: 2;
    padding: 3px 7px;
    position: absolute;
    top: -1px;
}

.ui-sortable-helper {border:1px dashed #ccc;visibility:visible !important;}
.ui-sortable-placeholder {border:1px dashed #ccc;visibility:visible !important;}


    </style>
@stop


@section('scripts')
    
    <script>
        $(document).ready(function()
        {
            $("#nav-toggle").trigger('click');

            if($("#template-side .btn").length > 0)
            {
                $("#template-side .btn").click(function(e)
                {
                    switch($(this).attr('id'))
                    {
                        case 'mbcode_size_phone':
                            $("#template-area").css('max-width', '320px');
                            break;
                        case 'mbcode_size_phone_v':
                            $("#template-area").css('max-width', '480px');
                            break;
                        case 'mbcode_size_tablet':
                            $("#template-area").css('max-width', '320px');
                            break;
                        case 'mbcode_size_desktop':
                            $("#template-area").css('max-width', '980px');
                            break;
                        case 'mbcode_size_tablet_v':
                            $("#template-area").css('max-width', '320px');
                            break;
                        case 'mbcode_size_full':
                            $("#template-area").css('max-width', '');
                            break;
                    }
                });   
            }

            if($("#template-area").children().length == 0)
            {
                $("#template-area").addClass("template-hold");
            }
        });
    </script>
@stop