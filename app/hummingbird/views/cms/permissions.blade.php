@extends('HummingbirdBase::cms.layout')

@section('content')

<?php $even = false; ?>

<h1>All Permissions</h1>

<table class='results' cellpadding='5' cellspacing='0'>
    <thead>
        <th>Name</th>
        <th>Actions</th>
    </thead>
    <tbody>
        @foreach($permissions as $permission)
        
        <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
            <td>{{$permission->display_name}}</td>
            <td>
                <a href='/{{url(App::make('backend_url')}}/permissions/edit/{{$permission->id}}'>Edit</a> | 
                <a href='/{{url(App::make('backend_url')}}/permissions/delete/{{$permission->id}}'>Delete</a>
            </td>
        </tr>
        
        @endforeach
    </tbody>
</table>
@stop
