@extends('HummingbirdBase::cms.layout')

@section('content')

<?php $even = false; ?>

<h1>All Plugins</h1>
<hr/>

<table class='results' cellpadding='5' cellspacing='0'>
    <thead>
        <th>Name</th>
        <th>Action</th>
    </thead>
    <tbody>
        @foreach($plugins as $plugin)
        
        <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
            <td>{{$plugin}}</td>
            @if (Plugins::isInstalled($plugin))
            <td><a href='/{{App::make('backend_url')}}/plugins/uninstall/{{$plugin}}'>Uninstall</a></td>
            @else
            <td><a href='/{{App::make('backend_url')}}/plugins/install/{{$plugin}}'>Install</a></td>
            @endif
            
        </tr>
        
        @endforeach
    </tbody>
</table>

@stop
