@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            @if($tag)
                <div class="clearfix">
                    <h1 class="pull-left">Editing: <span class="italic">&quot;{{$tag->name}}&quot;</span></h1>

                    {{ Form::open(array('route' => array('hummingbird.tags.destroy', $tag->id), 'method' => 'delete')) }}
                        <button type="submit" class="pull-right btn btn-xs btn-danger"><i class="fa fa-trash"></i> Remove</button>
                    {{ Form::close() }} 
                </div>

                <div class="row">
                    <div class="col-md-8">
                        {{ Form::open(array('action' => array('TagsController@update', $tag->id), 'method' => 'put', 'class' => 'form-horizontal')); }}
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Tag name:</label>
                                <div class="col-sm-10">
                                    <input name="name" id="name" type="text" class="tax-name form-control" placeholder="Tag name" value="{{$tag->name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                <div class="col-sm-10">
                                    <input name="slug" id="slug" type="text" class="form-control" placeholder="Tag slug" value="{{$tag->slug}}">
                                    <div class="regenerate @if(Str::slug($tag->name) == $tag->slug) hide @endif hide">
                                        <a href="#" class="btn btn-xs"><i class="fa fa-cog"></i> Generate new slug</a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Description:</label>
                                <div class="col-sm-10">
                                    {{ Form::textarea('description', $tag->description, array('class' => 'form-control textareas', 'size' => '1x1')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default btn-primary">Update</button>
                                </div>
                            </div>
                        {{Form::close()}}
                    </div>

                    <div class="col-md-4">
                        @if(count($references) > 0)
                            <div class="table">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <td>Title/Type</td>
                                            <td>Author/Creator</td>
                                            <td>Created</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($references as $reference)
                                            <?php $item = (new $reference->tax_type)->find($reference->item_id); ?>

                                            <tr>
                                                <td><a href="/{{General::backend_url()}}/{{$item->cms_url}}/edit/{{$item->id}}">{{$item->title}}</a> <i class="italic">({{$item->cms_type}})</i></td>
                                                <!-- <td>@if (null !== $item->user_id AND isset($user)) {{$user->name}} @else - @endif</td> -->
                                                <td>@if(method_exists($item, 'user')) @if($item->user()->name !== null) {{$item->user()->name}} @else - @endif @else - @endif</td>
                                                <td>{{$item->created_at}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p>No references</p>
                        @endif
                    </div>
                </div>
            @else
                <h1><a href="/{{General::backend_url()}}/tags/">Tags</a> &raquo; Error</h1>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="alert alert-block alert-danger fade in">
                            There was a problem locating that tag. Please go back try again.
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
</div>

@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }
        });
    </script> 
@stop
