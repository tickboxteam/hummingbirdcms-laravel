@extends('HummingbirdBase::cms.layout')

@section('styles')

@stop

@section('breadcrumbs')
	@if (count($breadcrumbs) > 0)
		<div class="row">
		    <div class="col-md-12">
		        <!--breadcrumbs start -->
		        <ul class="breadcrumb">
		        	<?php $i = 0;?>
			        @foreach ($breadcrumbs as $breadcrumb)
			        	<?php $i++;?>
			            <li>
			            @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
			            	<a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
			            @else
			            	@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
			            @endif
			            </li>
			        @endforeach
		        </ul>
		        <!--breadcrumbs end -->
		    </div>
		</div>
	@endif
@stop

@section('content')

<section style="background-color:white;padding:20px;" class="panel">


<div class="clearfix">
                <h1 class="pull-left">Form Builder</h1>
   <a class="pull-right btn btn-info" style="margin-left:30px;"  href="<?=General::backend_url()?>/../form-builder/build"> <i class="fa fa-plus-circle"></i> &nbsp;&nbsp;&nbsp;&nbsp;Create a New Form</a>

   <a class="pull-right btn btn-info cf_regenerate" href="#"> <i class="fa fa-plus-refresh"></i> &nbsp;&nbsp;&nbsp;&nbsp;Rebuild all form HTML</a>

                            </div>

    <?php if($forms): ?>
    <div id="right_col"  class="cf_wrap" >
    <table class="table table-striped results"cellspacing="0" cellpadding="5">
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>Name</th>
            <th>Date Created</th>        
            <th>Tag</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($forms as $form): $form = (array) $form; ?>
        <tr>
            <td><a class="cf_button cf_edit_button" data-cf_id="<?=$form['id']?>" href="<?=General::backend_url() //why is this returning /hummingbird/hummingbird ?>/../form-builder/edit/<?= $form['id']?>">Edit Form</a></td>
            <td><a class="cf_button cf_del_button" data-cf_id="<?=$form['id']?>" href="<?=General::backend_url()?>/../form-builder/delete/<?=$form['id']?>">Delete Form</a></td>
            <td><?=$form['title']?></td>
            <td><?=date('d M Y', $form['date_created']);?></td>
            <td>[cf:<?=$form['id']?> title:<?=$form['title']?>]</td>
        </tr>
        <?php endforeach; ?>
        </tbody>
        </tr>
    </table>
    </div>
<?php endif; ?>

</section>
@stop

@section('scripts')
	<script src='/themes/hummingbird/default/lib/contactforms/jqueryui.js'></script>
	<script src='/themes/hummingbird/default/lib/contactforms/contact_forms.js'></script>

@stop