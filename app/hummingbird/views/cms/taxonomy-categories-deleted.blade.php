@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1 class="normal pull-left">Deleted Categories</h1>

            @if(count($categories) > 0)
                <!-- Button trigger modal -->
                {{ Form::open(array('action' => array('CategoriesController@purge', 'all'), 'method' => 'delete')) }}
                    <button type="submit" class="pull-right btn btn-danger"><i class="fa fa-trash"></i> Purge all</button>
                {{ Form::close() }}

                {{ Form::open(array('action' => array('CategoriesController@reinstate', 'all'), 'method' => 'delete')) }}
                    <button type="submit" class="pull-right btn btn-info" style="margin-right:5px;"><i class="fa fa-arrow-circle-left"></i> Reinstate all</button>
                {{ Form::close() }}
            @endif

            <div class="clearfix">&nbsp;</div>

            @if(count($categories) > 0)
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Category name</th>
                                <th>Description</th>
                                <th>Deleted</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        @foreach($categories as $cat)
                            <tr>
                                <td>{{$cat->name}}</td>
                                <td>{{$cat->description}}</td>
                                <td>{{ $cat->deleted_at->diffForHumans() }}</td>
                                <td>

                                    {{ Form::open(array('action' => array('CategoriesController@reinstate', $cat->id), 'method' => 'put', 'class' => 'inline-block')) }}

                                        <button type="submit" class="btn btn-xs btn-info" title="Reinstate {{$cat->name}}"><i class="fa fa-arrow-circle-left"></i></button> 
                                    {{ Form::close() }}

                                    {{ Form::open(array('action' => array('CategoriesController@purge', $cat->id), 'method' => 'delete', 'class' => 'inline-block')) }}
                                        <button type="submit" class="btn btn-xs btn-danger" title="Permanently deleted {{$cat->name}}"><i class="fa fa-trash"></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>

                <div class="clearfix"></div>
            @else
                <div class="clearfix">&nbsp;</div>
                <div class="alert alert-box alert-warning text-center">No categories have been deleted.</div>
            @endif
        </div>
    </section>
</div>

@stop
