@extends('HummingbirdBase::cms.layout')

@section('styles')

@stop

@section('content')

<div class="row">
	<div class="col-md-12">
		<h1><a href="{{url(App::make('backend_url').'/menus')}}">All Menus</a> > Edit Menu: {{$menu->name}}</h1>
	</div>
</div>
{{ Form::open(array('action' => array('MenuController@postEdit', $menu->id), 'method' => 'post', 'id' => 'scale-form', 'class' => 'form-horizontal')); }}
<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
			<div class="col-md-8">
				<div class="form-group">
			        <label for="name" class="col-sm-2">Name:</label>
			        	<div class="col-sm-6">
			        		<input class="input_box form-control" id="name" type="text" name="name" value="{{$menu->name}}">
			        	</div>
			    </div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
			    	<input type="submit" class="btn btn-success pull-right" id="save" value="Save Menu"/>
				</div>
			</div>
			<h2>Add Menu Items:</h2>
				<div class="col-md-8">
					<div class="form-group">
						<label for="menu_item_name" class="col-sm-2">Menu Item:</label>
							<div class="col-sm-6">
								<input class="input_box form-control" id="menu_item_name" type="text" name="menu_item_name">
							</div>
					</div>
					<div class="form-group">
						<label for="url" class="col-sm-2">URL:</label>
						<div class="col-sm-6">
							<input class="input_box form-control" id="url" type="text" name="url">
							<span class="help-block"><strong>Internal</strong>: /test-url/<br /><strong>External:</strong> http://www.google.com</strong></span>
						</div>
					</div>
					<div class="form-group hide">
						<label for="parent" class="col-sm-2">Parent:</label>
							<div class="col-sm-2">
								<input class="input_box form-control" id="parent" type="text" name="parent">
							</div>
					</div>
					<div class="form-group hide">
						<label for="order" class="col-sm-2">Order:</label>
							<div class="col-sm-2">
								<input class="input_box form-control" id="order" type="text" name="order">
							</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<input type="submit" class="btn btn-default btn-primary pull-right" id="add" value="Add Menu Item"/>
					</div>
				</div>
				<div class="table">
	                <table class="table table-striped">
	                    <thead>
	                        <th>Name</th>
	                        <th>Url</th>
					        <th>Actions</th>
	                    </thead>
	                    <tbody>
	                    
	                     @foreach($menuitems as $menuitem)
	                     
	                     	<tr>
	                     		<td>
	                     			<span class="item-name">{{$menuitem->menu_item_name}}</span>
	                     			<span class="edit-item hide">
	                     				<input type="hidden" name="menu_items[]" value="{{$menuitem->id}}" />
	                     				<input type="hidden" class="old-name" name="item_name_original[{{$menuitem->id}}]" value="{{$menuitem->menu_item_name}}" />
	                     				<input type="text" class="input_box form-control new-name" name="item_name[{{$menuitem->id}}]" value="{{$menuitem->menu_item_name}}" />
	                     			</span>
	                     		</td>
	                     		<td>
	                     			<span class="item-url"><a href='{{$menuitem->url}}'>{{$menuitem->url}}</a></span>
	                     			<span class="edit-item hide">
	                     				<input type="hidden" class="old-url" name="item_url_original[{{$menuitem->id}}]" value="{{$menuitem->url}}" />
	                     				<input type="text" class="input_box form-control new-url" name="item_url[{{$menuitem->id}}]" value="{{$menuitem->url}}" />
	                     			</span>
	                     		</td>
								<td>
									<a href='/{{App::make('backend_url')}}/menus/edit/{{$menuitem->id}}' class="btn btn-xs btn-info edit-menu editing" data-action="edit"><i class="fa fa-edit"></i></a>
									<a href='/{{App::make('backend_url')}}/menus/edit/{{$menuitem->id}}' class="btn btn-xs btn-warning editing save-menu hide" data-action="edit"><i class="fa fa-save"></i></a>
                             		<a href='/{{App::make('backend_url')}}/menus/deleteItem/{{$menuitem->id}}' class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                             	</td>
	                     	</tr>
	                     @endforeach
	                     </tbody>
	                </table>
	            </div>
		</section>
	</div>
</div>

{{Form::close()}}

@stop



@section('scripts')
	<script>
		$(document).ready(function()
		{
			if($(".edit-menu").length > 0)
			{
				$(".edit-menu").click(function(e)
				{
					e.preventDefault();

					var parent = $(this).closest('tr');

					parent.find('span, .editing').toggleClass('hide');
				});
			}

			if($(".save-menu").length > 0)
			{
				$(".save-menu").click(function(e)
				{
					e.preventDefault();

					var parent = $(this).closest('tr');

					var new_name = parent.find('.new-name').val();
					var old_name = parent.find('.old-name').val();

					var new_url = parent.find('.new-url').val();
					var old_url = parent.find('.old-url').val();

					if(new_name != old_name && new_name != '')
					{
						parent.find('span.item-name').text(new_name);
					}

					if(new_url != old_url && new_url != '')
					{
						parent.find('span.item-url a').attr('href', new_url);
						parent.find('span.item-url a').text(new_url);
					}

					parent.find('span, .editing').toggleClass('hide');
				});
			}
		});
	</script>
@stop