@extends('HummingbirdBase::cms.layout')

@section('content')

<h1><a href="{{url(App::make('backend_url').'/templates')}}">All Templates</a> > Edit template: {{$template->name}}</h1>

<?php echo Form::open(array('url' => '/cms/templates/edit/' . $template->id)) ?>

<table cellpadding="5" cellspacing="0" id="edit">
    <tbody>
        <tr>
            <td>{{ Form::label('name', 'Name: ') }}</td>            
            <td>{{ Form::text('name', $template->name) }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('live', 'Live? ') }}</td> 
            <td> {{ Form::hidden('live', 0); }}  {{ Form::checkbox('live', 1, $template->live) }}</td> 
        </tr>
        <tr>
            <td>{{ Form::label('type', 'Type: ') }}</td> 
            <td>{{ Form::select('type', $types, $template->type) }}</td> 
        </tr>
        <tr>
            <td>{{ Form::label('layout', 'Layout: ') }}</td> 
            <td>{{ Form::select('layout', $layouts, $template->layout) }}</td> 
        </tr>
        <tr>
            <td>{{ Form::label('positions', 'Positions: ') }}</td> 
            <td>{{ Form::text('positions', $template->positions) }}</td> 
        </tr>
        <tr>
            <td>{{ Form::label('html', 'HTML: ') }}</td> 
            <td>{{ Form::textarea('html', $template->html, array('class' => 'redactor')) }}</td> 
        </tr>
    </tbody>
</table>


<p><input type="submit" value="Update Template"></p>
<?php echo Form::close() ?>

@stop
