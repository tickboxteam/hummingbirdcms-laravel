@extends('HummingbirdBase::cms.layout')

@section('content')
	<style>
		.update-box {background-color:white;border-radius: 0;border:0;}
		.update-box.alert-success {border-left:3px solid #3c763d;}
		.update-box.alert-warning {border-left:3px solid #8a6d3b;}
		.update-box.alert-danger {border-left:3px solid #a94442;}
		.update-box.alert-info {border-left:3px solid #31708f;}

		.update-box.cover
		{
			color:white;
			-webkit-box-shadow: 0px 0px 10px 0px rgba(51,51,51,0.5);
			-moz-box-shadow: 0px 0px 10px 0px rgba(51,51,51,0.5);
			box-shadow: 0px 0px 10px 0px rgba(51,51,51,0.5);
		}

		.update-box.cover.alert-danger
		{
			background-color:#a94442;
			border-left:3px solid #86201E;
		}

		.update-box.cover.alert-success
		{
			background-color:#3c763d;
			border-left:3px solid #284A29;
		}

		.update-box.cover.alert-warning
		{
			background-color:#8a6d3b;
			border-left:3px solid #5D451C;
		}

		.update-box.cover.alert-info
		{
			background-color:#31708f;
			border-left:3px solid #1A506B;
		}
		#footer { height:66px; }
	</style>

	<div class="row">
		<div class="col-md-12">
			<h1>System Updates</h1>
			<p>Update the system, manage and install plugins and themes.</p>
		</div>

		<div class="col-md-12">
			@if(method_exists(new StringHelper, 'new_text'))
				{{StringHelper::new_text()}}
			@endif
		</div>

		@if(Session::has('upgrade_failure'))
			<div class="col-md-12">
				<div class="update-box cover alert alert-danger">{{Session::get('upgrade_failure')}}</div>
			</div>
			<?php Session::forget('upgrade_failure');?>
		@endif

		@if(Session::has('upgrade_complete') AND $settings->value['behind'] <= 0)
			<div class="col-md-12">
				<div class="update-box cover alert alert-success">{{Session::get('upgrade_complete')}}</div>
			</div>
			<?php Session::forget('upgrade_complete');?>
			<?php Session::forget('upgrade_clean');?>
		@endif
	</div>

	@if(!isset($composer) OR null === $composer)
		<div class="alert alert-danger">Access to composer is disabled. Contact your system administrator.</div>
	@endif

	@if(isset($permissions['core']))
		<div class="row">
			<div class="col-md-8">
				<h5 class="normal">Hummingbird Core</h5>

				@if($permissions['core'])
					<div class="update-box alert alert-danger">Directory "/app" not writeable</div>
				@else
					{{Form::open(array('url'=> App::make('backend_url').'/updates/run/', 'method'=>'post'))}}
						{{Form::hidden('type', 'core')}}
						
						@if(isset($settings->value['last_checked']))
							<p class="normal">We last checked your system {{\Carbon\Carbon::createFromTimeStamp($settings->value['last_checked'])->diffForHumans()}}. We will check again on <strong>{{date("D M Y", $settings->value['next_check'])}} at {{date("H:i:s", $settings->value['next_check'])}}</strong>.</p>
						@endif

						@if($settings->value['behind'] >= 10)
							<div class="update-box alert alert-danger"><i class="fa fa-thumbs-down"></i> You are {{$settings->value['behind']}} {{General::singular_or_plural($settings->value['behind'], 'version', 's')}} behind, please update immediately.</div>
						@elseif($settings->value['behind'] > 5)
							<div class="update-box alert alert-warning">You are {{$settings->value['behind']}} {{General::singular_or_plural($settings->value['behind'], 'version', 's')}} behind, please consider updating to receive the latest updates.</div>
						@elseif($settings->value['behind'] > 0)
							<div class="update-box alert alert-info">You are {{$settings->value['behind']}} {{General::singular_or_plural($settings->value['behind'], 'version', 's')}} behind. The latest version is <strong>{{$settings->value['latest_version']}}</strong></div>
						@else
							<div class="update-box alert alert-success"><i class="fa fa-thumbs-up"></i> You are up to date with HummingbirdCMS.</div>
						@endif

						<button type="submit" class="btn btn-default" name="action" value="validate">Check for latest version</button>

						@if($settings->value['behind'] > 0)
							<button type="submit" class="btn btn-primary hb3-update" name="action" value="update"><i class="fa fa-cog"></i> Update Hummingbird</button>

							<div class="hide notification-live-wrapper">
								<div id="progress-handler">
									<span class="expand"></span>
								</div>

								<div class="notification-live-updates"></div>
							</div>

							<style>
#progress-handler {
    width: 100%;
    /* Full Width */
	height: 5px;
}

.expand {
    width: 0%;
    height: 3px;
    background: #00AEEF;
    position: absolute;
    box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.3);
	-webkit-transition: all 3s ease-in-out;
	-moz-transition: all 3s ease-in-out;
	-o-transition: all 3s ease-in-out;
	transition: all 3s ease-in-out;
}



								.notification-live-updates > span
								{
									font-family: monospace;
									font-size: 1.25em;
									clear:both;
									display: block;
								}

								.notification-live-updates > span.failed { color:red; }
								.notification-live-updates > span.completed { color:#50B152; font-weight:bold; }
								.notification-live-updates > span.completed:after { color:#50B152; font-weight:normal; }

								.notification-live-updates > span:before
								{
									content: "hb3:~$ ";
									font-style: italic;
									font-size:0.75em;
								}

								.notification-live-updates > span:last-child:not(.finished):after
								{
									content: " [Please wait...]";
									opacity: 0;
									animation: cursor 1.5s infinite;
									color:#00AEEF;
								}

								@keyframes cursor
								{
									0% { opacity: 0; }
									40% { opacity: 0; }
									50% { opacity: 1; }
									90% { opacity: 1; }
									100% { opacity: 0; }
								}

								/* styling */
								.notification-live-wrapper
								{
									box-sizing: border-box;
									/*display: table-cell;*/
									vertical-align: middle;
									background-color:#2A313A;
									color:white;
									width:100%;
									margin:10px 0;
									position: relative;
								}

								.notification-live-wrapper .notification-live-updates
								{
									padding:10px 20px;
								}
							</style>

						@endif
					{{Form::close()}}
				@endif
			</div>
			<div class="col-md-4" style="background-color:white;">
				<h4>Core package versions</h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Type</th>
							<th>Version</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><strong>Hummingbird Core: </strong></td>
							<td>{{Config::get('HummingbirdBase::hummingbird.version')}}</td>
						</tr>
						<tr>
							<td><strong>Composer: </strong></td>
							<td>{{$composer}}</td>
						</tr>

						@if(isset($laravel))
							@foreach($laravel as $package => $version)
								<tr>
									<td><strong>{{$package}}: </strong></td>
									<td>{{$version}}</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	@endif

	@if(isset($permissions['themes']))
		<h3>Theme updates</h3>
		<p>Current version: 1.4</p>
		<p>The following updates are available:</p>
		<ul>
		    <li><input type='checkbox' /> 1.5 - Restyle for homepage</li>
		    <li><input type='checkbox' /> 1.6 - Fixes to contact us page</li>
		</ul>

		@if(!$permissions['themes'])
			<div class="alert alert-danger">Themes folder not writeable</div>
		@endif
	@endif

	@if(isset($permissions['plugins']))
		<h3>Plugin Updates</h3>
		<p>Plugin set up and in use by HB3</p>

		@if(!$permissions['plugins'])
			<div class="alert alert-danger">Plugins folder not writeable</div>
		@endif

		<div class="hide">
			<h5>Events</h5>

			<p>Current version: 2.4</p>
			<p>The following updates are available:</p>
			<ul>
			    <li><input type='checkbox' /> Events (core) 2.5 - Lng lat added</li>
			    <li><input type='checkbox' /> Events (hooks) 2.5 - Lng lat extended for Luton Culture</li>
			</ul>

			<h5>Venues</h5>

			<p>Current version: 1.0</p>
			<p>You are up to date with Venues</p>

			<br/>
		</div>
	@endif

	@if(isset($permissions['hooks']))
		<h3>Hooks Updates</h3>

		<p>Hooks set up and in use by HB3</p>

		@if(!$permissions['hooks'])
			<div class="alert alert-danger">Hooks folder not writeable</div>
		@endif
	@endif
@stop

@section('scripts')
	<script>
		var complete = false;
		var timer;

		function poll() 
		{
			if(!complete)
			{
				$.ajax(
				{ 
					url: "/hummingbird/updates/progress", 
					cache: false,
					success: function(response)
					{
					    if (response != '') 
						{
							var data = $.parseJSON(response);

							$(".expand").css('width', data.progress + '%');

							if(response.messages != '')
							{
								$.each(data.updates, function(index, value)
								{
									if($( ".notification-live-updates span[data-target='" + index + "']" ).length)
									{
										$( ".notification-live-updates span[data-target='" + index + "']" ).html(value);
									}
									else
									{ 
										$(".notification-live-updates").append('<span data-target="'+index+'">'+value+'</span>').delay( 800 );
									}
								});
							}

							if(data.progress == 100)
							{
								complete = true;

								if(typeof data.status != 'undefined' && !data.status)
								{
									$(".notification-live-updates").append('<span class="finished failed">' + data.message + '</span>');
								}
								else
								{
									$(".notification-live-updates span:last-child").addClass("finished");
									$(".notification-live-updates").append('<span class="finished completed">Page will reload in <span class="timer" data-reload="3">3 seconds</span></span>');
								
									var sec = parseInt($('.notification-live-updates span.completed.finished span.timer').data('reload'));
									
									var clocktimer = setInterval(function()
									{
										sec--;
										$('.notification-live-updates span.completed.finished span.timer').data('reload', sec);
										var text = (sec > 1) ? sec + ' seconds':sec + ' second' 
									   	$('.notification-live-updates span.completed.finished span.timer').text(text);
									   	
									   	if (sec == 1)
									   	{
									   		clearInterval(clocktimer);
									  	} 
									}, 1000);
								}

								setTimeout(function(){
									window.location.href = '/hummingbird/updates';
								},3000);
							}
						}
						timer = setTimeout(poll, 1500);
					}
				});
			}
			else
			{
				timer = clearTimeout();
			}
		}

		$(function()
		{
			$(".hb3-update").click(function(e)
			{
				e.preventDefault();

				$(".update-box.cover").remove();

				$(this).attr('disabled', true);

				var data = {};
				data.action = 'update';
				data.type 	= 'core';

				$.ajax({
					url: "/hummingbird/updates/run",
			        data: data,
			        type: 'POST',
			        cache: false,
					beforeSend: function()
					{
						$(".notification-live-wrapper").removeClass('hide');
						$(".notification-live-updates").append('<span data-target="prepare">Updating Hummingbird...</span>');
						timer = setTimeout(poll, 1500);
					},
					success: function(response)
					{
						if(response != '')
						{
							var data = $.parseJSON(response);

							if(!data.status && data.progress == 100)
							{
								window.location.href = '/hummingbird/updates';
							}
						}

						timer = clearTimeout();
					}
				});
			});
		});
	</script>
@stop