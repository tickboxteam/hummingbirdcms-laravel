@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.css" />
@stop

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            @if($category)
                <div class="clearfix">
                    <h1 class="pull-left"><a href="/{{General::backend_url()}}/categories/">Categories</a> &raquo; &quot;{{$category->name}}&quot;</h1>

                    {{ Form::open(array('route' => array('hummingbird.categories.destroy', $category->id), 'method' => 'delete')) }}
                        <button type="submit" class="pull-right btn btn-xs btn-danger"><i class="fa fa-trash"></i> Remove</button>
                    {{ Form::close() }} 
                </div>

                <div class="row">
                    <div class="col-md-8">
                        {{ Form::open(array('action' => array('CategoriesController@update', $category->id), 'method' => 'put', 'class' => 'form-horizontal')); }}
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Name:</label>
                                <div class="col-sm-10">
                                    <input name="name" id="name" type="text" class="tax-name form-control" placeholder="Tag name" value="{{$category->name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="slug" class="col-sm-2 control-label">Slug:</label>
                                <div class="col-sm-10">
                                    <input name="slug" id="slug" type="text" class="form-control" placeholder="Tag slug" value="{{$category->slug}}">
                                    <div class="regenerate @if(Str::slug($category->name) == $category->slug) hide @endif hide">
                                        <a href="#" class="btn btn-xs"><i class="fa fa-cog"></i> Generate new slug</a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Description:</label>
                                <div class="col-sm-10">
                                    {{ Form::textarea('description', $category->description, array('class' => 'form-control textareas', 'size' => '1x1')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Parent:</label>
                                <div class="col-sm-4">
                                    <select name="parent" id="parent" class="selectpicker form-control"  data-live-search="true">
                                        <option value="">--- Top Level ---</option>
                                        @foreach($list_cats as $cat)
                                            <option value="{{$cat->id}}" @if($category->parent == $cat->id) selected="selected" @endif>{{$cat->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default btn-primary">Update</button>
                                </div>
                            </div>
                        {{Form::close()}}
                    </div>

                    <div class="col-md-4">
                        @if(count($references) > 0)
                            <div class="table">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <td>Title/Type</td>
                                            <td>Author/Creator</td>
                                            <td>Created</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($references as $reference)
                                            <?php $item = (new $reference->tax_type)->find($reference->item_id); ?>

                                            <tr>
                                                <td><a href="/{{General::backend_url()}}/{{$item->cms_url}}/edit/{{$item->id}}">{{$item->title}}</a> <i class="italic">({{$item->cms_type}})</i></td>
                                                <!-- <td>@if (null !== $item->user_id AND isset($user)) {{$user->name}} @else - @endif</td> -->
                                                <td>@if(method_exists($item, 'user')) @if($item->user()->name !== null) {{$item->user()->name}} @else - @endif @else - @endif</td>
                                                <td>{{$item->created_at}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p>No references</p>
                        @endif
                    </div>
                </div>
            @else
                <h1><a href="/{{General::backend_url()}}/tags/">Tags</a> &raquo; Error</h1>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="alert alert-block alert-danger fade in">
                            There was a problem locating that tag. Please go back try again.
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
</div>

@if($category)    
    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                <h4>Children</h4>

                @if(count($children) > 0)
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Slug</th>
                                    <th># used</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($children as $cat)
                                    <tr>
                                        <td>{{$cat->name}}</td>
                                        <td>{{$cat->slug}}</td>
                                        <td>{{$cat->used()}}</td>
                                        <td>
                                            <a href='/{{App::make('backend_url')}}/categories/{{$cat->id}}' class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="clearfix">&nbsp;</div>
                    <div class="alert alert-info text-center">No child categories have been assigned/created.</div>
                @endif
            </section>
        </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>You are about to delete the category &quot;<strong class="deleting"></strong>&quot;.</p>
                    <p>Do you want to proceed?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
@endif

@stop

@section('scripts')
    <script src="/themes/hummingbird/default/lib/multipleselect/bootstrap-select.min.js"></script>

    <script>
        $(document).ready(function()
        {
            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }

            if($('.tax-name').length > 0)
            {
                //used for showing a new auto generate slug area
            }

            $('#confirm-delete').on('show.bs.modal', function(e)
            {
                $('.deleting').html($('.delete-category ').data('name'));
            });
        });
    </script> 
@stop
