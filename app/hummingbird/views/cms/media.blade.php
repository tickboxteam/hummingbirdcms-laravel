@extends('HummingbirdBase::cms.layout')

@section('styles')
	<link rel="stylesheet" href="/themes/hummingbird/default/lib/dropzone/css/dropzone.css" type="text/css" />

	<style>
		.dropzone { border: 2px dashed #00AEEF; border-radius: 5px; background: white; padding:15px;}
		.dropzone .dz-message { font-weight: 400;font-size:20px;font-size:2rem;color:#646C7F; }
		.dropzone .dz-message .note { font-size:12px;font-size: 1.2rem; font-weight: 200; display: block; margin-top: 1.4rem; color:#333333;}
		.nogutter {padding-left:0;padding-right:0;}
		.dropzone .dz-preview .dz-progress .dz-upload {
			background: #00AEEF;
		}
	</style>
@stop

@section('breadcrumbs')
	@if (count($breadcrumbs) > 0)
		<div class="row">
		    <div class="col-md-12">
		        <!--breadcrumbs start -->
		        <ul class="breadcrumb">
		        	<?php $i = 0;?>
			        @foreach ($breadcrumbs as $breadcrumb)
			        	<?php $i++;?>
			            <li>
			            @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
			            	<a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
			            @else
			            	@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
			            @endif
			            </li>
			        @endforeach
		        </ul>
		        <!--breadcrumbs end -->
		    </div>
		</div>
	@endif
@stop

@section('content')

@if (Session::has('success'))
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="alert alert-block alert-success fade in">
	            <button data-dismiss="alert" class="close close-sm" type="button">
	                <i class="fa fa-times"></i>
	            </button>
	            @if(count(Session::get('success')) > 0)
	            	@foreach (Session::get('success') as $message)
	            		<li>{{$message}}</li>
	            	@endforeach
	            @else
	            	{{ Session::get('success') }}
	            @endif
	        </div>
        </div>
	</div>	
@endif

@if(Session::has('message'))
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="alert alert-block alert-info fade in">
	            <button data-dismiss="alert" class="close close-sm" type="button">
	                <i class="fa fa-times"></i>
	            </button>
	            {{ Session::get('message') }}
	        </div>
        </div>
	</div>	
@endif

@if (Session::has('errors'))
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="alert alert-block alert-danger fade in">
	            <button data-dismiss="alert" class="close close-sm" type="button">
	                <i class="fa fa-times"></i>
	            </button>

	            @if(is_array($errors) AND count($errors) > 0)
	            	@foreach($errors->all() as $message)
	            		<li>{{ $message }}</li>
	            	@endforeach
	            @else
	            	{{ Session::get('errors') }}
	            @endif
	        </div>
        </div>
	</div>
@endif

@if($import_media !== null AND $import_media !== false)
	<!-- <button type="button" class="btn btn-round btn-default" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-refresh"> Import</i></button> -->
@endif

<button type="button" class="btn btn-round btn-primary media-upload"><i class="fa fa-cloud-upload"> File Upload</i></button>
<button type="button" class="btn btn-round btn-default add-collection pull-right" data-toggle="modal" data-target="#add-collection"><i class="fa fa-new"></i> Add Collection</button>

<!-- <button type="button" class="btn btn-round btn-success"><i class="fa fa-plus"> New Collection</i></button> -->
<!-- <button type="button" class="btn btn-round btn-success"><i class="fa fa-cloud-download"> Import from URL</i></button> -->


<h1 class="normal">@if($current_collection == '') Media Centre @else {{$collection->name}} @endif</h1>

<div id="dropzone-media-holder" class="hide">
	<div class="dz-preview dz-upload  dz-clickable">
		<span><i class="fa fa-plus"></i></span>
	</div>
</div>

<style>


.nomargin {margin:0;}

.form-horizontal .control-label.text-left {text-align:left;}

#media-image-store {padding:15px;margin:-15px;background-color:#F9F9FB; color:#2A313A;height:461px;overflow: scroll}
#media-image-store .form-group {padding:15px 15px 0 15px;}
#media-image-store .form-group.image {padding:0px;}
</style>

{{ Form::open(array('id' => 'updateFiles', 'action' => 'MediaController@updateUploadedFiles', 'method' => 'post', 'class' => 'hide')); }}
	<input type="hidden" name="collection" value="{{ $current_collection }}" />
{{ Form::close() }}

<div class="row clearfix" id="dropzone" style="display:none;" >
	<div class="col-md-12 text-center" style="margin:15px 0;"> 
		{{ Form::open(array('id' => 'dropzoneUpload', 'action' => 'MediaController@uploadFile', 'method' => 'post', 'files' => true, 'enctype' =>"multipart/form-data", 'class' => 'dropzone form-horizontal relative')); }}

			<input type="hidden" name="ajax" value="1" />
			<input type="hidden" name="collection" value="{{ $current_collection }}" /> 

			<div class="dz-default dz-message">
				<span>Drop files here or click to upload</span>
			</div>

			<div id="file-processing" class="hide col-md-12 nogutter"></div>

			<div id="media-image-store" class="col-md-4 pull-right hide">
		        <div class="form-group image">
		        	<div class="col-sm-12 image-holder hide">
		        		<img class="img-responsive" />
		        	</div>
		        	<div class="col-sm-12 file-holder hide">
		        		<i class="fa fa-5x"></i>
		        	</div>
		        </div>

		        <div class="form-group nomargin">
		        	<div class="col-sm-12">
		        		<h6 class="text-left nomargin">Details</h6>
		        	</div>
		        </div>

		        <div class="form-group" id="title-holder">
		            <label for="title" class="col-sm-12 control-label text-left">Title:</label>
		            <div class="col-sm-12">
		                <input id="title" type="text" class="media-name form-control" placeholder="Title" value="">
		            </div>
		        </div>

		        <div class="form-group" id="alt-holder">
		            <label for="alt" class="col-sm-12 control-label text-left">Alternative Text:</label>
		            <div class="col-sm-12">
		                <input id="alt" type="text" class="media-alt form-control" placeholder="Caption" value="">
		            </div>
		        </div>

		        <div class="form-group" id="caption-holder">
		            <label for="caption" class="col-sm-12 control-label text-left">Caption:</label>
		            <div class="col-sm-12">
		                <input id="caption" type="text" class="media-name form-control" placeholder="Caption" value="">
		            </div>
		        </div>

		        <div class="form-group" id="description-holder">
		            <label for="description" class="col-sm-12 control-label text-left">Description:</label>
		            <div class="col-sm-12">
		            	<textarea class="textareas form-control" id="description" placeholder="Description"></textarea>
		            </div>
		        </div>
			</div>

			<button class="btn btn-primary trigger-update col-sm-2 absolute">Commit changes</button>

		{{ Form::close() }}
	</div>
</div>

<style>

#dropzoneUpload.open {height:465px;max-height:465px;}
#dropzoneUpload .trigger-update {display:none;left:15px;bottom:15px;border-radius: 0;}
#dropzoneUpload .trigger-update:hover {cursor: pointer;}
#dropzoneUpload.open .trigger-update {display:block;}

#file-processing {height: 365px;overflow: scroll;}

.collection {border-radius:5px;background-color:white;margin:10px 0;}
.collection .caption {padding:10px 15px;}
.collection h3 {font-weight:normal;font-size:18px;font-size:1.8rem;}
.collection p {margin-top: 10px;color: #80858e;}

#dropzone form {float:left;width:100%}
.dropzone .dz-preview {float:left;margin:0;}
.dropzone .dz-preview .dz-image {border-radius: 0 !important;}


.dz-upload {cursor:pointer;background-color:#F9F9FB;border:1px solid #E9EDF0;border-left:0;text-align: center;padding:2em 1em;width:120px;height:120px;color:#aaa;}
.dz-upload span {font-size:40px;font-size:4rem;}
.dz-upload:hover, .dz-upload span:hover .dz-upload i.fa:hover {cursor:pointer !important;}
.dz-upload:hover {border:1px dashed #E9EDF0;border-left:0;color:#2A313A;}
</style>

<?php 
	$FileHelper = new FileHelper;
	$valid_files = $FileHelper->getValidKeys();
?>



@if(count($valid_files) > 0)
	<div class="row hide">
		<div class="col-sm-12" style="margin-bottom:10px;">
		<!-- filters -->
			<ul id="filters" class="tags" style="list-style-type:none;padding-left:0;margin-left:0;">
				<li class="pull-left active" style="margin:0 5px;"><a class="btn-sm btn-outline btn-primary" href="#link" data-filter="all">Show All</a></li>
				
				@foreach($valid_files as $file)
					<li class="pull-left" style="margin:0 5px;"><a class="btn-sm btn-outline btn-default" href="#link" data-filter="{{ $file }}">{{ ucfirst($file) }}</a></li>
				@endforeach
			
			</ul>
		</div><!-- /.col -->
	</div>
@endif

@if (count($media_collections) == 0 AND count($media_items) == 0)
	<div class="row">
		<div class="col-md-12 text-center">
			<div class="alert alert-block alert-danger fade in">
	            <button data-dismiss="alert" class="close close-sm" type="button">
	                <i class="fa fa-times"></i>
	            </button>
	            <strong>Oh snap!</strong> There aren't any collections or media items yet uploaded. Try uploading some.
	        </div>
        </div>
	</div>	
@endif

<div class="row">
	@if (count($media_collections) > 0)
		@foreach($media_collections as $collection)
		    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
		    	<div class="collection">
		            <div class="caption">
		                <h3>{{ $collection->name }}<a href="/{{General::backend_url()}}/media/edit/{{$collection->id}}" class=" btn-xs" role="button"><i class="fa fa-edit"></i></a></h3>
		                @if($collection->description != '')
		                	<p>{{$collection->description}}</p>
		               	@else
		               		<p>{{$collection->name}}</p>
		               	@endif

		                <p>
		                    <a href="/{{General::backend_url()}}/media/view/{{$collection->id}}" class="view-collection btn-sm btn-info" role="button">Show</a>
		                    <a href="/{{General::backend_url()}}/media/delete/{{$collection->id}}" data-url="/{{General::backend_url()}}/media/delete/{{$collection->id}}?check=1" class="delete-collection btn btn-danger btn-xs pull-right" role="button"><i class="fa fa-trash"></i></a>
		                </p>
		            </div>
			    </div>
			</div>

<!-- 			<div class="col-sm-4">
				<article class="album">
					<header>
						<a href="http://demo.neontheme.com/extra/gallery-single/">
							<img src="http://demo.neontheme.com/assets/images/album-thumb-1.jpg">
						</a>
					</header>
					
					<section class="album-info">
						<h3><a href="http://demo.neontheme.com/extra/gallery-single/">Album Title</a></h3>
						<p>Can curiosity may end shameless explained. True high on said mr on come. </p>
					</section>

					<footer style="border-top:1px solid #f0f0f0;">
						<div class="album-images-count"> <i class="fa fa-files-o"></i> 55</div>
						<div class="album-options"> 
							<a href="#"> <i class="entypo-cog"></i> </a> 
							<a href="#"> <i class="entypo-trash"></i> </a> 
						</div>
					</footer>
				</article>
			</div> -->
		@endforeach
	@endif
	<div class="col-md-12">
		<hr />
	</div>
</div>

@if (count($media_items) > 0)
	<div class="row media">
		<div class="col-sm-12">
					<!-- <div class="col-sm-12"> -->
			@if(!$parent_id)
				<h5>Unattached Media</h5>
			@else
				<h5>Media</h5>
			@endif
		<!-- </div> -->
			<div id="media-lib" class="gallery">
				@foreach($media_items as $media)
					<?php 
						$file_extension = $FileHelper->getFileExtension($media->location);
						$isImage = $FileHelper->isImage($file_extension);
					?>

				    <div class="col-xs-4 col-sm-4 col-md-2 thumb" data-target="images">
				        <div class="images link-click" data-href="/{{General::backend_url()}}/media/view-media/{{$media->id}}">
				        	@if($isImage)
					        	<img src="{{$media->mediaCentreThumb()}}" alt="{{$media->filename}}" />
					        @else
					        	<div class="filename text-center"><i class="fa fa-file-{{Str::slug($file_extension)}} fa-2x text-center"></i><br />{{$media->filename}}</div>
					        @endif
				        </div>

				        <div class="options"> 
				     		<span class="option view btn-default"><a href="/{{General::backend_url()}}/media/view-media/{{$media->id}}"><i class="fa fa-pencil"></i></a></span>
				        	
				        	@if($isImage)
					        	<span class="option edit btn-info">
					        		<a href="/{{General::backend_url()}}/media/edit-image/{{$media->id}}"><i class="fa fa-crop"></i></a>
								</span>
							@endif
				        	
				        	<span class="option delete btn-danger"><a href="/{{General::backend_url()}}/media/delete-item/{{$media->id}}" class="media-delete" data-id="{{$media->id}}"><i class="fa fa-trash"></i></a></span>
				        </div>
				    </div>
				@endforeach
			</div>
		</div>
	</div>

	<style>
		.gallery .thumb {
			margin-top:15px;
			margin-bottom:15px;
		}

		.gallery .thumb .images .filename {word-wrap:break-word;font-size:1.3rem;font-size:13px;}

		.gallery .thumb .images img 
		{
			position: relative;
			display:block;
			max-width:100%;
			height:auto;
			width:100%;
			transition: all 0.5s ease;
			opacity:1.0;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
		}

		.gallery .thumb .options
		{
			position:absolute;
			top:-6px;
			right:6px;
			display:none;
		}

		.gallery .thumb .options .option
		{
			font-size:1.2rem;
			display: inline-block;
			line-height: 1;
			margin-left: 2px;
			color: white;
			width: 24px;
			height: 24px;
			line-height: 24px;
			-webkit-border-radius: 12px;
			-webkit-background-clip: padding-box;
			-moz-border-radius: 12px;
			-moz-background-clip: padding;
			border-radius: 12px;
			background-clip: padding-box;
			text-align: center;
			-moz-box-shadow: 0 2px 5px rgba(0,0,0,.2);
			-webkit-box-shadow: 0 2px 5px rgba(0,0,0,.2);
			box-shadow: 0 2px 5px rgba(0,0,0,.2);
		}

		.gallery .thumb .options .option a {
			color:white;
			display:block;
		}

		.gallery .thumb .options .option.btn-default a {color:white;}
		.gallery .thumb .options .option.btn-default {background-color: #2A313A}
		.gallery .thumb .options .option.btn-default:hover {background-color:black;} 

		.link-click {display:block;}
		.link-click:hover {text-decoration: underline;cursor:pointer;}
	</style>
@endif


@if($import_media !== null AND $import_media !== false)
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<h2>Importing media</h2>
				<p>We have found {{count($files)}} media items ready to be imported.</p><p>You can either import them all or import a select few.</p>
				<h3>Files:</h3>

				{{ Form::open(array('action' => 'MediaController@importFiles', 'method' => 'post')); }}
					@foreach($files as $file)
						<?php $file = str_replace(base_path(). '/import', "", $file);?>
						<input class="checkbox" type="checkbox" name="file[]" value="{{ $file }}" />{{ $file }}<br />
					@endforeach

					{{ Form::submit('Import Collection'); }}
				</form>
			</div>
		</div>
	</div>
@endif


<div id="add-collection" class="modal fade">
    <div class="modal-dialog clearfix">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close redactor-modal-btn redactor-modal-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add new collection</h4>
            </div>

			{{ Form::open(array('action' => 'MediaController@store', 'method' => 'post')); }}

	            <div class="modal-body">
	            	<p>Collections help keep your media items tidy. <i class="italic">A tidy house, is a tidy mind!</i>.</p>

	            	<input type="hidden" name="parent_collection" value="{{ $parent_id }}" />

	                <!-- Text input-->
	                <div class="form-group">
	                    <label for="collection-name" class="control-label">Name:</label>
	                    <input type="text" class="form-control" name="collection-name" id="collection-name" />
	                </div>
	            </div>

	            <div class="modal-footer">
	                <footer>
	                    <button type="submit" class="btn btn-primary">Add Collection</button>
	                </footer>
	            </div>
            {{ Form::close() }}
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('scripts')
	<script src='/themes/hummingbird/default/lib/dropzone/js/dropzone.js'></script>
	<script src="http://imagesloaded.desandro.com/imagesloaded.pkgd.js"></script>
	<script src='/themes/hummingbird/default/lib/masonry/masonry.js'></script>

	<script>
		var $msnry;
		var active_media_upload;
		var timeout;

		Dropzone.autoDiscover = false;

		function initMasonry()
		{
			// initialize
			$msnry = $('#media-lib');

			$msnry.imagesLoaded( function() {
				$msnry.masonry(
				{
					columnWidth: '#media-lib .thumb',
					itemSelector: '#media-lib .thumb'
				});
			});
		}

		$(document).ready(function()
		{
            if($('.form-control.textareas').length > 0)
            {
                $(".form-control.textareas").autogrow();
            }

			if($("ul#filters").length > 0)
			{
				$("ul#filters li a").click(function(e)
				{
					e.preventDefault();

					if($(this).data('filter') == 'all')
					{
						$(".media .thumb").removeClass("hide");
					}
					else
					{
						$(".media .thumb:not([data-target=" + $(this).data('filter') + "])").addClass("hide");
						$(".media .thumb[data-target=" + $(this).data('filter') + "]").removeClass("hide");
					}
				});

				if($('#media-lib').length == 1)
				{
					$("#media-lib .thumb").hover(function() { // Mouse over
						$(this).find('.options').fadeIn();
						$(this).siblings().stop().fadeTo(300, 0.5);
						$(this).parent().siblings().stop().fadeTo(300, 0.3); 
					}, function() { // Mouse out
						$(this).find('.options').fadeOut();
						$(this).siblings().stop().fadeTo(300, 1);
						$(this).parent().siblings().stop().fadeTo(300, 1);
					});
				}
			}

			if($('#media-lib').length == 1)
			{
				initMasonry();
			}

			if($(".link-click").length > 0)
			{
				$(".link-click").click(function()
				{
					window.location.href = $(this).data('href');
				});
			}

			if($(".delete-collectiond").length > 0)
			{
				$(".delete-collection").click(function(e)
				{
					e.preventDefault();

					$.post($(this).data('url'), function(response)
					{

					});
				});
			}

			if($(".media-delete").length > 0)
			{
				$(".media-delete").click(function(e)
				{
					e.preventDefault();

					clearTimeout(timeout);

					var data = {};
					data.ajax = true;

                    $.ajax({
                        dataType: "json",
                        cache: false,
                        url: $(this).attr('href'),
                        data: data,
                        success: $.proxy(function(response)
                        {
                        	if(response.state)
                        	{
                        		var message = '<div class="row messages">';
                        			message += '<div class="col-md-12 text-center">';
                        				message += '<div class="alert alert-success alert-block">';
                        					message += '<h4>Success</h4>';
	            							message += response.message;
	            						message += '</div>';
	            					message += '</div>';
	            				message += '</div>';

	            				$(".content-section").prepend(message);

                        		$(this).closest('.thumb').remove();

                        		if($("#media-lib").find('.thumb').length > 0)
                        		{
                        			initMasonry();
                        			$("#media-lib .thumb").css('opacity', '1');
                        		}
                        		else
                        		{
                        			$("#media-lib").closest(".row").remove();
                        		}
                        	}
                        	else
                        	{
                        		var message = '<div class="row messages">';
                        			message += '<div class="col-md-12 text-center">';
                        				message += '<div class="alert alert-block alert-danger fade in">';
	            							message += response.message;
	            						message += '</div>';
	            					message += '</div>';
	            				message += '</div>';

	            				$(".content-section").prepend(message);
                        	}
                            
                        }, this)
                    });

					timeout = setTimeout(function()
					{
						$(".content-section .row.messages").fadeOut('slow').queue(function(next)
						{
							$(this).remove();
						});
					}, 3000);
				});
			}

			$(document).on("click", ".dz-upload", function(e)
			{
				e.preventDefault();
				$(this).closest('form').trigger('click');
			});

			$(document).on('click', '.trigger-update', function(e)
			{
				e.preventDefault();
				/* Store the new data before moving on */
				if(typeof active_media_upload !== 'undefined')
				{
					var old_data = $.parseJSON($("#media_"+active_media_upload).val());

					old_data.title = $("#media-image-store #title").val();
					old_data.description = $("#media-image-store #description").val();
					old_data.alt = (old_data.image !== false) ? $("#media-image-store #alt").val():'';
					old_data.caption = (old_data.image !== false) ? $("#media-image-store #caption").val():'';

					$("#media_"+active_media_upload).val(JSON.stringify(old_data));
				}

				$("#updateFiles").trigger('submit');
			});

			$(document).on('click', ".upload-media", function(e)
			{
				e.preventDefault();

				$("#dropzoneUpload").addClass('open');

				/* Store the new data before moving on */
				if(typeof active_media_upload !== 'undefined')
				{
					var old_data = $.parseJSON($("#media_"+active_media_upload).val());

					old_data.title = $("#media-image-store #title").val();
					old_data.description = $("#media-image-store #description").val();
					old_data.alt = (old_data.image !== false) ? $("#media-image-store #alt").val():'';
					old_data.caption = (old_data.image !== false) ? $("#media-image-store #caption").val():'';

					$("#media_"+active_media_upload).val(JSON.stringify(old_data));
				}

				active_media_upload = $(this).find('.dz-type').data('media-id');
				var data = $.parseJSON($("#media_"+active_media_upload).val());
				var form = $("#media-image-store");

				if(data.image !== false)
				{
					form.find('.image .image-holder img').attr('src', data.image).parent().removeClass('hide');
					form.find('.image .file-holder').addClass('hide');
					form.find('#alt-holder, #caption-holder').removeClass('hide');
				}
				else
				{
					form.find('.image .file-holder').removeClass('hide').find('i').remove();
					form.find('.image .file-holder').append('<i class="fa fa-5x '+data.icon+'"></i>');
					form.find('.image .image-holder').addClass('hide');
					form.find('#alt-holder, #caption-holder').addClass('hide');
				}

				form.find('#title').val(data.title);
				form.find('#description').val(data.description);
				form.find('#alt').val(data.alt);
				form.find('#caption').val(data.caption);

				if(data.image === false)
				{
					form.find('#description').val('');
					form.find('#caption').val('');	
				}

				$("#file-processing").removeClass('col-md-12').addClass("col-md-8");
				$("#media-image-store").removeClass('hide');
			});
		
			var myDropzone = new Dropzone("#dropzoneUpload");

			myDropzone.on('success', function(file, response)
			{
				var _html = $(file.previewElement).append('<div class="dz-type" data-media-id="'+response.id+'"></div>');
				_html = $(_html).addClass("upload-media");

				var data = {};
				data.title = response.filename;
				data.description = '';
				data.alt = '';
				data.caption = '';
				data.image = (response.is_image) ? response.location:false;
				data.icon = (!data.image) ? response.icon:false;

				$("#updateFiles").append('<input type="hidden" name="media['+response.id+']" id="media_'+response.id+'" />');
				$("#updateFiles #media_"+response.id).val(JSON.stringify(data));

				$("#file-processing").append(_html);
			});

			myDropzone.on('error', function(file, response)
			{
				$("#file-processing .dz-upload").remove();
				myDropzone.removeFile(file);

				var message = '<div class="alert alert-block alert-danger text-center">';
				message += response;
				message += '</div>';

				$("#dropzone-media-holder").before(message).delay(3000).queue(function(next)
				{
					$(".alert").remove();
				});
			});

			myDropzone.on("complete", function(file)
			{
				$("#dropzoneUpload .dz-upload").remove();
				
				if($("#dropzoneUpload #file-processing").children().length > 0)
				{
					$("#dropzoneUpload #file-processing").append($("#dropzone-media-holder").html());
				}
			});

			myDropzone.on("addedfile", function(file)
			{
				$("#file-processing").removeClass("hide");
				$(".alert").remove();
				$("#dropzoneUpload .dz-upload").remove();

				var _html = $(file.previewElement).remove('.dz-progress').append('<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>');

				$("#file-processing").append(_html);

				return;
			});

			myDropzone.on("uploadprogress", function(file, progress, bytesSent)
			{
			    // Display the progress
			    $('.dz-progress .dz-upload').data('dz-uploadprogress', progress);
			});
		});
	</script>
@stop