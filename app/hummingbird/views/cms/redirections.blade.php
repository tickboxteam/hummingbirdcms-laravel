@extends('HummingbirdBase::cms.layout')

@section('breadcrumbs')
    @if (count($breadcrumbs) > 0)
        <div class="row">
            <div class="col-md-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <?php $i = 0;?>
                    @foreach ($breadcrumbs as $breadcrumb)
                        <?php $i++;?>
                        <li>
                        @if($breadcrumb['url'] != $_SERVER["REQUEST_URI"] AND $breadcrumb['url'] != '')
                            <a href="{{ $breadcrumb['url'] }}">@if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}</a>
                        @else
                            @if($breadcrumb['icon']) <i class="{{$breadcrumb['icon']}}"></i> @endif{{$breadcrumb['title']}}
                        @endif
                        </li>
                    @endforeach
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
    @endif
@stop

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-success fade in">
                {{ Session::get('success') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('message'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-info alert-danger fade in">
                {{ Session::get('message') }}
            </div>
        </div>
    </div>  
@endif

@if (Session::has('error'))
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="alert alert-block alert-danger fade in">
                {{ Session::get('error') }}
            </div>
        </div>
    </div>  
@endif

<div class="row">
    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h1 class="pull-left normal">Website Redirects</h1>

            @if(count($redirects) > 0)
                {{ Form::open(array('route' => array(App::make('backend_url').'.redirections.destroy', 'remove-all'), 'method' => 'delete')) }}
                    <button type="submit" class="btn btn-danger pull-right" style="margin-left:10px;"><i class="fa fa-trash"></i> Remove all</button>
                {{ Form::close() }}
            @endif

            <div class="row clearfix" style="margin-top:10px;">
                <div class="col-md-12"> 
                    @if(count($redirects) > 0)
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Type</th>
                                        <th>Date Added</th>
                                        <th>Last Used</th>
                                        <th># Used</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($redirects as $redirect)
                                    
                                    <tr>
                                        <td>{{$redirect->from}}</td>
                                        <td>{{$redirect->to}}</td>
                                        <td>{{$redirect->type}}</td>
                                        <td>{{ date("d-m-Y H:i:s", strtotime($redirect->created_at))}}</td>

                                        @if(null === $redirect->last_used)
                                            <td>n/a</td>
                                        @else
                                            <td>{{ date("d-m-Y H:i:s", strtotime($redirect->last_used))}}</td>
                                        @endif

                                        
                                        <td>{{$redirect->no_used}}</td>

                                        <td>
                                            <a href="{{General::frontend_url() . $redirect->from}}" class="btn btn-default btn-xs"><i class="fa fa-globe"></i></a>
                                            <a href="{{URL::route(App::make('backend_url').'.redirections.edit', array('redirections'=>$redirect->id))}}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
                                            {{ Form::open(array('route' => array(App::make('backend_url').'.redirections.destroy', $redirect->id), 'method' => 'delete', 'class' => 'inline-block')) }}
                                                <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center"> 
                            @if(null === Input::get('s') AND Input::get('s') == '')
                                {{ $redirects->appends(Request::except('page'))->links(); }}
                            @endif
                        </div>
                    @else
                        <div class="clearfix">&nbsp;</div>
                        <div class="alert alert-info text-center">No redirects have been added</div>
                    @endif
                </div>
            </div>
        </section>
    </div>
</div>

@if(Auth::user()->hasAccess('create', get_class(new Redirections)))
    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                <h3 class="pull-left normal">Add new redirect</h3>

                <!-- Button trigger modal -->
                <button type="button" class="pull-right btn btn-default" data-toggle="modal" data-target="#help"style="margin-left:10px;"><i class="fa fa-info"></i> Help</button>

                <div class="clearfix">&nbsp;</div>
                 
                @if(count($errors) > 0) 
                    @foreach ($errors->all('<li class="error red" style="color:red;">:message</li>') as $error)
                        {{$error}}    
                    @endforeach
                @endif

                <div class="table-responsive">
                    <?php echo Form::open(array('router' => App::make('backend_url').'.redirections.store', 'method' => 'post')) ?>
                        <table class="table table-striped table-hover">
                            <tbody>
                                <tr>
                                    <td class="row_name">From: <strong>*</strong></td>
                                    <td><input class="form-control" id="from" type="text" name="from"></td>
                                </tr>
                                <tr>
                                    <td class="row_name">To: <strong>*</strong></td>
                                    <td><input class="form-control" id="from" type="text" name="to"></td>
                                </tr>
                                <tr>
                                    <td class="row_name">Type: <strong>*</strong></td>
                                    <td>{{Form::select('type', Redirections::get_types_selection(), '', array('class' => 'form-control'))}}</td>
                                </tr>
                                <tr>
                                    <td class="row_name">Category:</td>
                                    <td>
                                        <div class="form-group">
                                            <select name="category" id="category" class="form-control">
                                                <option value="">--- None ---</option>

                                                @if(count($categories) > 0)
                                                    @foreach($categories as $category)
                                                        <option value="{{$category}}">&nbsp;&nbsp;&nbsp;{{$category}}</option>
                                                    @endforeach
                                                @endif

                                                <option disabled></option>
                                                <option value="NEW">Create new category</option>
                                            </select>
                                        </div>

                                        <div class="form-group hide">
                                            <input class="custom_field form-control" id="custom_category" type="text" name="custom_category">
                                            <span class="help-block">Enter a new category name for this redirect</span>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="row_name">Comments:</td>
                                    <td><textarea name="comments" class="form-control textareas" data-summary-limit="255"></textarea></td>
                                </tr>

                                <tr>
                                    <td colspan="2" align="center">
                                        <button type="submit" class="pull-right btn btn-default btn-primary">Add redirect</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <?php echo Form::close()?>
                </div>
            </section>
        </div>
    </div>
@endif


<div class="modal fade" id="help">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Help: Website Redirects</h4>
            </div>

            <div class="modal-body">
                <h4>You can:</h4>
                <p>Keep your content fresh and clean by removing dead links. Google will applaud you for it! Go one step further and also create marketing URLs. <strong><i>Easy.</i></strong></p>
                <p>You can create:</p>
                <ul>
                    <li>301 Redirects or permanent redirects</li>
                    <li>302 Redirects or temporary redirects</li>
                    <li>Visible redirects - to give the user a sense of movement</li>
                </ul>

                <h4>Other Helpful hints:</h4>
                <ul>
                    <li>Export:
                        <ul>
                            <li>Export redirects not used in the last six months</li>
                            <li>Export all (or customised) reports</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section('scripts')
    <script>
        $(document).ready(function()
        {
            if($("#category").length > 0)
            {
                $("#category").change(function()
                {
                    if($("#category").val() == 'NEW')
                    {
                        $("#custom_category").closest('.form-group').removeClass("hide");   
                    }
                    else
                    {
                        $("#custom_category").closest('.form-group').addClass("hide");
                    }
                });
            }
        });
    </script>
@stop