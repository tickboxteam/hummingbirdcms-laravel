@extends('HummingbirdBase::cms.layout')

@section('content')

<div class='home_icons'>
    
    <h1>Dashboard</h1>
    <p>Get started by selecting a menu item</p>

    @foreach ($dashboard_widgets as $dashboard_widget)
    
        @include($dashboard_widget)
    
    @endforeach


    <?php $colors = array('blue', 'green', 'red', 'yellow');?>

    <div class="row hide">
	    <div id="dashboard_widgets">
	    	@for($i = 1; $i <= 6; $i++)
	    		<div class="widget col-sm-{{rand(1,8)}}">
	    			<div class="content" style="background-color:{{$colors[array_rand($colors)]}}">
	    				<h5>Widget #{{$i}}</h5>
	    			</div>
	    		</div>
	    	@endfor
	    </div>
	</div>
</div>


<style>

.widget {height:100px;margin:10px 0;}
.widget-med {height:175px;}
.widget-large {height:350px;}

.widget .content {width:100%;height:100%;padding:20px;text-align:center;}

</style>

@stop


@section('scripts')
	<script src="http://imagesloaded.desandro.com/imagesloaded.pkgd.js"></script>
	<script src='/themes/hummingbird/default/lib/masonry/masonry.js'></script>

	<script>
		var $msnry;

		$(document).ready(function()
		{
			// initialize
			$msnry = $('#dashboard_widgetas');

			$msnry.imagesLoaded( function() {
				$msnry.masonry(
				{
					columnWidth: '.widget',
					itemSelector: '.widget'
				});
			});
		});
	</script>
@stop
