@extends('HummingbirdBase::cms.layout')

@section('styles')
    <link rel="stylesheet" href="/themes/hummingbird/default/lib/templates/builder.css" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
@stop


@section('content')

{{ Form::open(array('action' => array('TemplateNewController@postEdit', $template->id), 'method' => 'post', 'id' => 'scale-form', 'class' => 'form-horizontal')); }}
    <div class="row">
        <div class="col-md-12">
            <section class="panel" style="background-color:white;padding:20px;">
                <div class="col-md-12 container_top">
                    <div class="page_header">
                        <h1 class="pull-left">Template Builder</h1>   
                        @if(!$template->locked)<a href="#" class="pull-right clean btn btn-danger btn-xs"><i class="fa fa-trash"> Clear HTML</i></a>@endif       
                    </div>
                </div>

                <div class="clearfix">&nbsp;</div> 

                @if(!$template->locked)
                    <div class="structure_input">
                        <div class="html">Add: 
                            <a href="#" data-size="container">Container</a> |
                            <a href="#" data-size="row">Row</a> |
                            <a href="#" data-size="single-column">Single Column</a> | 
                            <a href="#" data-size="12">Full width</a> | 
                            <a href="#" data-size="6,6">1/2</a> | 
                            <a href="#" data-size="4,4,4">1/4</a> |  
                            <a href="#" data-size="3,3,3,3">1/3</a> | 
                            <a href="#" data-size="2,2,2,2,2,2">1/6</a>
                        </div>

                        <br />

                        <input type="text" name="structure" value="" placeholder="3, 6, 3"/>
                        <input type="button" class="btn btn-info btn-sm"  id="structure_button" value="Insert Row"/>
                        <input type="button" class="btn btn-success pull-right" id="save_button" value="Save Template" style=""/>
                    </div>
                @endif
            </section>
        </div>

        <div class="col-md-12" style="margin-bottom:20px;">
            <textarea class="hide" id="generated_html" name="html">{{$template->html}}</textarea>

            <div class="flexi_outer">
                <div class="relative" id="flexicontainer">
                    {{$template->html}}
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <section class="panel" style="background-color:white;padding:20px;">
            <h4>Template Details</h4>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Template name:</label>
                <div class="col-sm-10">
                    <input name="name" id="name" type="text" class="form-control" placeholder="Template Name" value="{{$template->name}}">
                </div>
            </div>

            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea style="height:auto;" class="form-control textareas" rows="5" name="description" id="description" placeholder="Enter description...">{{$template->description}}</textarea>
                </div>
            </div>

            @if($template->locked)
                <div class="form-group">
                    <div class="col-sm-offset-10 col-sm-2">
                    <input type="button" class="btn btn-success pull-right" id="save_button" value="Update Template" style=""/>
                    </div>
                </div>
            @endif
        </section>
    </div>
</form>

@include('HummingbirdBase::cms.templates-new-modal')

@stop


@section('scripts')
    @if($template->locked)<script>var locked = true;</script>@endif
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="/themes/hummingbird/default/lib/templates/builder.init.js"></script>
    <script src="/themes/hummingbird/default/lib/templates/builder.js"></script>
@stop