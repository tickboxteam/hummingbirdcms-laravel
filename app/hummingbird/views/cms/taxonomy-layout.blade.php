<div class="form-group">
    <div class="col-sm-10">
        <h4>Taxonomy</h4>
    </div>
</div>

<?php $List_categories = App::make('CategoriesController')->getIndex_call();?>

<?php $categories = Categories::type()->whereNull('deleted_at')->orderBy('name', 'ASC')->get(); ?>

<div class="form-group taxonomy-group">
    <h6>Categories</h6>

    <div class="col-md-4">
        <div class="row">
            <div class="col-sm-12">
                <select id="categories" data-type="categories" class="selectpicker" data-live-search="true" multiple>
                    <option value=""></option>

                    <?php $i = 1;?>
                    @foreach($List_categories as $category)
                        <option value="{{$category->id}}" data-index="{{$i}}" @if(in_array($category->id, $taxonomy['category'])) disabled selected @endif>{{$category->name}}</option>
                        <?php $i++;?>
                    @endforeach
                </select>
                <a href="#" class="add-taxonomy btn btn-md btn-default">Add</a>
            </div>
        </div>
    </div>

    <div class="col-md-8 taxonomy-holder">
        @if(count($taxonomy['category']) > 0)
            @foreach($taxonomy['category'] as $category_id)
                <?php 
                    $item = $categories->filter(function($taxonomy) use ($category_id) {
                        return $taxonomy->id == $category_id;
                    })->first();
                ?>

                @if(null !== $item)
                    <div class="taxonomy relative">
                        <input type="hidden" name="taxonomy[]" value="{{$item->id}}" />
                        <span class="badge badge--hummingbird badge-sm">{{ $item->name }}</span>
                        <a class="taxonomy-remove" title="Remove Option" href="#">x</a>
                    </div>
                @endif
            @endforeach
        @endif
        <div class="no-taxonomy alert alert-box alert-warning text-center @if(count($taxonomy['category']) > 0) hide @endif">No categories currently assigned</div>
    </div>
</div>

<?php $tags = Tags::type()->whereNull('deleted_at')->orderBy('name', 'ASC')->get();?>

<div class="form-group taxonomy-group">
    <h6>Tags</h6>

    <div class="col-md-4">
        <div class="row">
            <div class="col-sm-12">
                <select id="tags" data-type="tags" class="selectpicker" data-live-search="true" multiple>
                    <option value=""></option>

                    @foreach($tags as $tag)
                        @if(!in_array($tag->id, $taxonomy['tag']))
                            <option value="{{$tag->id}}">{{$tag->name}}</option>
                        @endif
                    @endforeach
                </select>
                <a href="#" class="add-taxonomy btn btn-md btn-default">Add</a>
            </div>
        </div>
    </div>

    <div class="col-md-8 taxonomy-holder">
        @if(count($taxonomy['tag']) > 0)
            @foreach($taxonomy['tag'] as $tag_id)
                <?php 
                    $item = $tags->filter(function($taxonomy) use ($tag_id) {
                        return $taxonomy->id == $tag_id;
                    })->first();
                ?>

                @if(null !== $item)
                    <div class="taxonomy relative">
                        <input type="hidden" name="taxonomy[]" value="{{$item->id}}" />
                        <span class="badge badge--hummingbird badge-sm">{{ $item->name }}</span>
                        <a class="taxonomy-remove" title="Remove Option" href="#">x</a>
                    </div>
                @endif
            @endforeach
        @endif
        <div class="no-taxonomy alert alert-box alert-warning text-center @if(count($taxonomy['tag']) > 0) hide @endif">No tags currently assigned</div>
    </div>
</div> 