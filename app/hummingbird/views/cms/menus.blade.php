@extends('HummingbirdBase::cms.layout')

@section('styles')

@stop

@section('content')

<?php $even = false; ?>

<div class="row">
	<div class="col-md-12">
		<section class="panel" style="background-color:white;padding:20px;">
            <h1 class="pull-left">Menus</h1>
            
            <div class="table">
            	<table class="results table table-striped">
            		<thead>
				        <th>Name</th>
				        <th>Location</th>
				        <th>Actions</th>
				    </thead>
				    <tbody>
			        @foreach($menus as $menu)
			        
			        <tr <?php echo ($even) ? 'class="even"': '';$even = !$even;?>>
			            <td>{{$menu->name}}</td>
			            <td>{{$menu->location}}</td>
			            <td>
			                <a href='/{{App::make('backend_url')}}/menus/edit/{{$menu->id}}' class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                             <a href='/{{App::make('backend_url')}}/menus/delete/{{$menu->id}}' class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
			            </td>
			        </tr>
			        
			        @endforeach
			    	</tbody>
            	</table>
            </div>
            <div class="row">
	            <div class="col-md-12">
		        	<h1>Add new menu</h1>
					<?php echo Form::open(array('url' => App::make('backend_url').'/menus/add', 'method' => 'post')) ?>
					     <div class="col-md-8">
							<div class="form-group">
						        <label for="row_name" class="col-sm-2">Name:</label>
						        	<div class="col-sm-6">
						        		<input class="input_box form-control" id="row_name" type="text" name="row_name">
						        	</div>
						    </div>
						</div>
					    <div class="form-group">
							<div class="col-sm-4">
						    	<input type="submit" class="btn btn-success pull-right" id="add" value="Add Menu"/>
							</div>
						</div>
					<?php echo Form::close()?>
				</div>
			</div>
    	</section>
	</div>
</div>

@stop


