<?php namespace Hummingbird;

use App;
use File;
use General;
use Config;
use Illuminate\Support\ServiceProvider;

class HummingbirdServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap Hummingbird.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Register Hummingbird
         */
        $HummingbirdBase = base_path() . '/app/hummingbird';
        /*
         * Check files exist, that we need for routing, events and filtering
         */
        $files = array('/routes.php', '/events.php', '/filters.php');

        // Main routes, events and filters for Hummingbird
        foreach($files as $file)
        {
            if(\File::exists($HummingbirdBase . $file))
            {
                 require $HummingbirdBase . $file;
            }
        }

        /* Register commands */
        $this->registerCommands();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        /*
         * Register Hummingbird
         */
        $HummingbirdBase = base_path() . '/app/hummingbird';
        $this->package('app/hummingbird', 'HummingbirdBase', $HummingbirdBase);

        /*
         * Define path constants
         */
        if (!defined('HUMMINGBIRD_PATH')) define('HUMMINGBIRD_PATH', base_path() . '/app/hummingbird');
        if (!defined('HUMMINGBIRD_PLUGIN_PATH')) define('HUMMINGBIRD_PLUGIN_PATH', base_path() . '/plugins');

        /*
         * Register permission manager
         * Requires: 
         *  definition (permission-type)
         *  permissions (array)
         */
        // App::singleton('cms.auth', function() { return \AuthManager::instance(); });

        /*
         * Register navigation manager
         * Requires: 
         *  definition (empty[dashboard], content, users, settings, plugins)
         *  options (array)
         *      label
         *      link
         *      icon
         *      children (array)
         *          label
         *          link
         *          icon
         */
        App::singleton('cms.navigation', function() { return new \NavigationManager; }); // get a CMS navigation menu set up


        /*
         * Register Widget Manager
         */

        /*
         * Register Block Manager
         */

        /*
         * Register Core system, modules, widgets, blocks, navigation items
         */

        /*
         * Register all plugins
         */
        // $pluginManager = new \Plugins;
        // $pluginManager->registerAll();

        /*
         * Register other module providers
         */
        // foreach (\File::directories(HUMMINGBIRD_PLUGIN_PATH) as $plugin) {
            // if (strtolower(trim($module)) == 'system') continue;
            // dd($plugin);
        //     App::register('Plugins\\' . \File::name($plugin) . '\ServiceProvider');
        // }


        /*
         * Register all hooks
         */
        // if($_SERVER["REMOTE_ADDR"] == '127.0.0.11')
        // {
            // \General::pp('REGISTERING');

            // \General::pp('FINISHED BOOTING');
            // $this->app->register('Philipbrown\Supyo\SupyoServiceProvider');
            // $this->app->register('Tickbox\Foo\FooServiceProvider');
            // $this->package('Tickbox/Foo\FooServiceProvider', null, base_path() . '/plugins/tickbox/foo/');

            // \General::pp('/plugins/tickbox/foo/');

            // $this->app->register('Tickbox\Foo\FooServiceProvider');
            $this->register_plugins();
        // }
    }

    public function register_plugins()
    {
        $valid_plugins = array('Shop', 'Vacancies', 'Microsites'); //temporary

        $providers = File::directories(base_path() . '/workbench/');

        /* iterate over locations */
        if(count($providers) > 0)
        {
            foreach($providers as $provider)
            {
                /* Clean and get provider name */
                $Vendor_Name = explode("/", $provider);
                $Vendor_Name = ucfirst(trim(end($Vendor_Name)));

                /* Now get packages inside */
                $packages = File::directories($provider);

                if(count($packages) > 0)
                {
                    foreach($packages as $package)
                    {
                        $Package_Name = explode("/", $package);
                        $Package_Name = ucfirst(trim(end($Package_Name)));

                        if(in_array($Package_Name, $valid_plugins))
                        {
                            // mail('webmaster@tickboxmarketing.co.uk', 'Hummingbird3', $Vendor_Name . '\\' . $Package_Name .'\\' . $Package_Name . 'ServiceProvider');
                            /* Register - Shop, Vacancies */
                            $this->app->register($Vendor_Name . '\\' . $Package_Name .'\\' . $Package_Name . 'ServiceProvider');
                        }
                    }
                }
            }
        }


        // $this->package('/plugins/ecommerce', null, base_path() . '/plugins/ecommerce');
        // $this->app->register('Tickbox\Shop\ShopServiceProvider');
        // $this->app->register('Tickbox\PasswordProtected\PasswordProtectedServiceProvider');

        // $PluginsBase = base_path() . '/plugins';
        // $Plugins = \File::directories($PluginsBase);

        // if(count($Plugins) > 0)
        // {
        //     foreach($Plugins as $Plugin)
        //     {
        //         $Plugin_to_str = (String) $Plugin;
        //         $__Plugin = explode("/", $Plugin_to_str);

        //         // \General::pp($__Plugin, true);

        //         switch(end($__Plugin))
        //         {
        //             case 'ecommerce':
        //                 $this->app->register('\Plugins\Ecommerce\ShopServiceProvider');
        //                 /*
        //                  * Add routes, if available
        //                  */
        //                 $routesFile = $Plugin . '/routes.php';

        //                 if (\File::exists($routesFile)) {
        //                     require $routesFile;
        //                 }

        //                 break;
        //         }
        //     }
        // }
    }

    public function registerCommands()
    {
        $commands = File::allFiles(HUMMINGBIRD_PATH . '/commands/');

        if(count($commands) > 0)
        {
            foreach($commands as $command)
            {
                $file_info = pathinfo($command);
                
                // $obj = new $file_info['filename'];
                // $this->commands($obj->getName());
                $this->commands($file_info['filename']);
            }
        }
    }
}