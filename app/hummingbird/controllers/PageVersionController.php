<?php

class PageVersionController extends CmsbaseController 
{
    public $code_location = 'pages';
        
    public function getIndex()
    {
        $this->data['tag'] = 'Manage Page Versions';
        $this->data['pageversions'] = PageVersion::all();
        return View::make('HummingbirdBase::cms/pageversions', $this->data);
    }
    
    public function getEdit($id)
    {
        $this->data['page'] = Page::find($id);
        $this->data['tag'] = 'Edit '.$this->data['page']->title;
        $this->getCommonData($id);
        return View::make('HummingbirdBase::cms/pageversions-edit', $this->data);
    }
    
    public function postEdit($id)
    {
        $input = Input::except('_token');
        $page = Page::find($id);
        $page->update($input);
        $page->permalink = $page->url;
        $page->save();
        
        return Redirect::to(App::make('backend_url').'/pageversions/edit/'.$id);
    }
    
    public function getCommonData($id = 0)
    {
        $this->data['pageversions'] = Page::get_selection('Select a parent page', array($id));
        $this->data['templates'] = Template::get_selection();
        $this->data['galleries'] = Gallery::get_selection();
        $this->data['blog_feed_styles'] = Page::get_blog_feed_styles();
        $this->data['statuses'] = Page::get_statuses();
    }

    public function postAdd()
    {
        $inputs = Input::all();
        $page = Page::create($inputs); // don't validate here, as it's only version??
        return \Redirect::to(App::make('backend_url').'/pageversions/edit/'.$page->id);
    }
    
    public function getDelete($id)
    {
        Page::find($id)->delete();
        return \Redirect::to(App::make('backend_url').'/pageversions');
    }
    
    public function search()
    {
        $this->data['pageversions'] = parent::search_model('page');
        return View::make('HummingbirdBase::cms/pageversions', $this->data);
    }
    
    public function getVersions($page_id = false)
    {
        if (!$page_id) return $this->getIndex();
        
        $this->data['pageversions'] = PageVersion::where('page_id','=',$page_id)->orderby('id', 'desc')->get();
        $this->data['tag'] = 'Edit '.$this->data['pageversions']->first()->title;
        #$this->getCommonData($id);
        return View::make('HummingbirdBase::cms/pageversions', $this->data);
    }
    
    public function getExport()
    {
        return parent::export_model('Page');
    }
    
    public function postExport()
    {
        return parent::run_export('Page', 'page_export.csv');        
    }
    
    public function restoreVersion($version_id)
    {
        $version = PageVersion::find($version_id);
        
        $page = Page::find($version->page_id);
        
        $page->update($version['attributes']);
        return Redirect::to(App::make('backend_url').'/pages/edit/'.$page->id);
    }
}
