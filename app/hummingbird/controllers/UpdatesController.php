<?php 

class UpdatesController extends CmsbaseController
{   
    public $permissions         = [];
    public $areas               = array('core');
    public $progress            = NULL;
    public $progress_messages   = [];

    public $error_code    = [
        'maintenance'   => [
            'down'      => "Hummingbird failed to update because we could not put the site into maintenance mode.",
            'up'        => "Hummingbird failed to update because we could not bring the site out of maintenance mode.",
        ],
        'backup_prep'   => 'There was a problem creating the necessary backup utilities.',
        'files'         => 'There was a problem upgrading your database, please contact your system administrator.',
        'migrations'    => 'There was a problem upgrading your database, please contact your system administrator.',
        'completion'    => 'There was a problem completing your upgrades, please contact your system administrator.'
    ];

    public $update_tasks        = array
    (
        'prepare' => array
        (
            'progress'          => 10,
            'message'           => 'Updating Hummingbird...',
            'completed'         => 'Complete'
        ),
        'prep_backup' => array 
        (
            'progress'          => 20,
            'message'           => 'Preparing website for backup',
            'completed'         => 'Complete'
        ),
        'database_backup' => array
        (
            'progress'          => 25,
            'message'           => 'Backing up database',
            'completed'         => 'Complete'
        ),
        'file_backup' => array 
        (
            'progress'          => 30,
            'message'           => 'Backing up website files',
            'completed'         => 'Complete'
        ),
        'process_updates' => array 
        (
            'progress'          => 80,
            'message'           => 'Unpacking updates',
            'completed'         => 'Complete'
        ),
        'completed' => array 
        (
            'progress'          => 100,
            'message'           => NULL,
            'completed'         => 'Hummingbird has been updated'
        )
    );

    /**
     * ***---***
     *
     * @return 
     */
    public function __construct()
    {
        /* Updating process - cleanup */
        $this->beforeFilter('@cleanUpgrade');

        parent::__construct();

        $input                          = Input::all();
        $this->Updater                  = new Updater;
        $this->data['current_version']  = Config::get('HummingbirdBase::hummingbird.version');
        $this->data['settings']         = Setting::where('key', '=', 'system_update')->first();
        $this->next_check               = (null !== $this->data['settings'] AND isset($this->data['settings']->value['next_check'])) ? $this->data['settings']->value['next_check']:NULL;
        
        /* No setting key - set this up */
        if(null === $this->data['settings'] OR $this->data['settings'] === FALSE)
        {
            $this->data['settings'] = new Setting;
            $this->data['settings']->key = 'system_update';
            $this->data['settings']->value = serialize(array('current_version' => $this->data['current_version']));
            $this->data['settings']->save();
        }

        /* Running updates and/or checking progress */
        if(!Request::ajax())
        {
            /* loading the different options */
            foreach($this->areas as $area)
            {
                switch($area)
                {
                    case 'core':
                        $directory = '/app';
                        break;
                    case 'themes':
                        $directory = '/themes/public';
                        break;
                    case 'plugins':
                        $directory = '/workbench';
                        break;
                    case 'hooks':
                        $directory = '/hooks';
                        break;
                }

                $this->permissions[$area] = (!File::isWritable(base_path() . $directory)) ? TRUE:FALSE;
            }
            
            /* Get composer version */
            $this->data['composer'] = (Request::ip() == '127.0.0.1') ? exec('php ~/composer.phar --version'):exec('php /usr/local/bin/composer --version');
            if($this->data['composer'] == '') unset($this->data['composer']);

            if(null !== $this->data['composer'])
            {
                $this->data['composer'] = trim(explode(" ", $this->data['composer'])[2]);
            }

            /* Get laravel version */
            if(File::exists(base_path() . '/composer.json'))
            {
                $contents = json_decode(file_get_contents(base_path() . '/composer.json'));
                $this->data['laravel'] = $contents->require;
            }

            /* Check versions */
            if(null === $this->data['settings'] OR $this->data['settings'] === FALSE OR $this->next_check < time() OR null !== Session::get('force_check')) $this->versionsBehind();
        }
    }

    /**
     * ***---***
     *
     * @return 
     */
    public function run_these_updates()
    {
        set_time_limit(-1);

        /* Check if a request has been made */
        if(Request::method('post') AND Input::has('action'))
        {
            switch(Input::get('action'))
            {
                case 'update':
                    Session::put("HB3UPDATE", array());
                    Session::save();

                    $this->rollback( $this->maintenance('down') , 1);

                    $this->update_info('prepare', true);
                    
                    $this->runUpdate();

                    $this->update_info('completed', true);

                    $this->rollback( $this->maintenance('up') , 6);

                    File::put($this->Updater->getUpdatePath() . "/.complete", 'Hummingbird3 upgrade completed.');

                    Session::put('force_check', true);
                    Session::put('upgrade_complete', "Hummingbird3 has been successfully upgraded to " . $this->data['settings']->value['latest_version']);
                    Session::put('clean_upgrade', true);

                    Session::save();
                    return;

                    break;
                case 'validate':
                default:
                    $this->versionsBehind();
                    return Redirect::to('/hummingbird/updates');
                    break;
            }
        }

        /* Nothing to do go back to the dashboard */
        if(!Input::has('action')) return Redirect::to('/hummingbird/updates/');
    }

    public function rollback($state = null, $step = 1, $callback = '')
    {
        if(null !== $state AND $state === FALSE)
        {
            $this->forget_update_session();
            
            $response = array( "progress" => 100, "status" => false );

            switch($step)
            {
                case 1:
                default:
                    Log::info($this->error_code['maintenance']['down']);
                    $response['message'] = $this->error_code['maintenance']['down'];
                    break;
                case 2:
                    $this->Updater->removeCurrentBackupDirectory();
                    $this->maintenance('up');

                    Log::info($this->error_code['backup_prep']);
                    $response['message'] = $this->error_code['backup_prep'];
                    break;
                case 3:
                case 4:
                case 5:
                    $backup_success = false;
                    $files_moved    = $this->Updater->getMessages('moved_files');
                    $failed_msgs    = $this->Updater->getMessages('files');

                    if($files_moved === TRUE)
                    {
                        // Roll back to the most recent backup
                        $backup_success = $this->Updater->reinstateRecentBackup('core', array('current_version' => $this->data['settings']->value['current_version'], 'latest_version' => $this->data['settings']->value['latest_version'], 'versions' => $this->data['settings']->value['versions'], 'behind' => $this->data['settings']->value['behind']), (($step == 3)) ? 10:20);

                        if($backup_success)
                        {
                            /* Remove latest backup as we dont need it */
                            $this->Updater->removeCurrentBackupDirectory();
                        }
                        else
                        {
                            //no backup!!!! -What the hell
                            $failed_msgs = (null !== $this->Updater->getMessages('backup-reinstate-error')) ? $this->Updater->getMessages('backup-reinstate-error'):$failed_msgs;
                        }
                    }

                    /* Change the message for database or configuration steps */
                    if($step > 3)
                    {
                        $failed_msgs_new = ($step == 4) ? $this->Updater->getMessages('database'):$this->Updater->getMessages('config-updating');
                        $failed_msgs = ($failed_msgs_new != '') ? $failed_msgs_new:$failed_msgs;
                    }
                        
                    /* Backup found and re-instated - bring the site back up */
                    if((!$files_moved) OR ($files_moved AND $backup_success)) $this->maintenance('up');

                    /* Log this message and return all messages */
                    Log::info($failed_msgs);
                    $response['message'] = $failed_msgs;
                    break;
                case 6:
                    Log::info($this->error_code['maintenance']['up']);
                    $response['message'] = $this->error_code['maintenance']['up'];
                    break;
            }

            $response['message'] = (isset($response['message']) AND $response['message'] == '') ? 'There was a problem upgrading Hummingbird, please try again later':$response['message'];

            echo json_encode($response);

            Session::put('upgrade_failure', $response['message']);
            Session::save();
            exit;
        }
    }

    /**
     * ***---***
     *
     * @return 
     */
    public function getCoreName($type)
    {
        switch($type)
        {
            case 'core':
            default:
                return 'Core';
        }
    }

    /**
     * Showing index of the System Updator
     *
     * @return Response
     */
    public function index()
    {
        $this->data['tag'] = 'Upgrading Hummingbird';
        return View::make('HummingbirdBase::cms.updates')->with($this->data)->with('permissions', $this->permissions);
    }


    /**
     * Showing index of the System Updator
     *
     * @return Response
     */
    public function shows($type = 'core')
    {
        $this->data['tag'] = 'Upgrading Hummingbird: ' . $this->getCoreName($type);

        $this->permissions = array();
        $this->permissions['core'] = (File::isWritable(base_path() . '/app')) ? TRUE:FALSE;

        return View::make('HummingbirdBase::cms.updates')->with($this->data)->with('permissions', $this->permissions);
    }

    /**
     * ***---***
     *
     * @return 
     */
    public function array_except($array, $keys)
    {
        return array_diff_key($array, array_flip((array) $keys));   
    } 

    /**
     * ***---***
     *
     * @return 
     */
    public function versionsBehind($force_all_versions = FALSE)
    {
        $this->forget_update_session();

        $behind     = -1;
        $versions = $this->Updater->getCoreVersions($this->data['current_version'], $force_all_versions);

        /* Get latest version */
        $latest_version = key($versions); //get last key

        /* Versions behind */
        $versions = array_reverse($versions);
        foreach($versions as $key => $data)
        {
            if(strpos($key, $this->data['current_version']) !== false)
            {
                $behind = 0;
                continue;
            }

            if($behind >= 0)
            {
                if($key != $this->data['current_version'])
                {
                    $behind++;
                }
            }
        }

        $checked_data = array(
            'latest_version'    => $latest_version,
            'behind'            => $behind,
            'versions'          => $versions,
            'last_checked'      => time(),
            'next_check'        => time() + (60 * 60 * 3) //3 hours
        );

        if(!empty($this->data['settings']->value))
        {
            $checked_data = array_merge($this->data['settings']->value, $checked_data);
        }
        
        $this->data['settings']->value = serialize($checked_data);
        $this->data['settings']->save();

        if(null !== Session::get('force_check')) Session::forget('force_check');
    }


    /**
     * ***---***
     *
     * @return 
     */
    public function runUpdate()
    {
        switch($this->Updater->allow_backup)
        {
            case TRUE:
                $this->rollback( $this->backupSite() , 2);
                break;
            default:
                $this->update_info('prep_backup', true, 'SKIPPED');
                $this->update_info('database_backup', true, 'SKIPPED'); 
                $this->update_info('file_backup', true, 'SKIPPED'); 
                break;
        }

        if($this->data['settings']->value['current_version'] != $this->data['settings']->value['latest_version'])
        {
            /* Move files */
            $this->rollback( $this->Updater->migrate_files('core'), 3);

            /* Migrate database changes */
            $this->rollback( $this->Updater->migrate_database('core', $this->data['settings']->value['current_version'], $this->data['settings']->value['latest_version'], $this->data['settings']->value['versions'], $this->data['settings']->value['behind']), 4);

            /* Complete the changes - remove latest version downloaded */
            $this->rollback( $this->Updater->completeUpdate('core', array(
                'version'   => $this->data['settings']->value['latest_version'],
                'build'     => $this->data['settings']->value['versions'][$this->data['settings']->value['latest_version']]->raw_node
            )), 5);

            /* Complete process and return */
            $this->update_info('process_updates', true);
        }
    }

    /**
     * Place the website into maintenance mode
     *
     * @return boolean false - if the attempt fails
     */
    public function maintenance($mode = 'up')
    {
        switch($mode)
        {
            case 'up':
                Log::info('HummingbirdCore finished updating. New version is: ' . $this->data['settings']->value['latest_version']);

                if(File::exists(base_path() . '/.maintenance'))
                {
                    File::delete(base_path() . '/.maintenance');
                }

                if(File::exists(base_path() . '/.maintenance')) return FALSE;
                break;
            case 'down':
                try
                {
                    File::put(base_path() . '/.maintenance', '');
                    Log::info('HummingbirdCore currently updating to ' . $this->data['settings']->value['latest_version']);
                }
                catch (Exception $e)
                {
                    Log::error("Problem creating the maintenance file: $e");
                    return FALSE;
                }

                break;
        }
    }

    /**
     * ***---***
     *
     * @return 
     */
    public function backupSite($custom_name = 'Core Update')
    {   
        /* Create new directory */
        $backup_location    = ($custom_name != '') ? 'backup-'.Str::slug($custom_name) . '-' :'backup-';
        $backup_directory   = base_path() . "/backups/$backup_location" . time();
        $database_backup    = 'database';
        $backup_info        = 'info.yaml';

        /* Try creating the directories for the backup */
        try
        {
            File::makeDirectory("$backup_directory/$database_backup", 0775, true);
        }
        catch (Exception $e)
        {
            $this->error_code['backup_prep'] = "There was a problem creating the backup directory &quot;$backup_directory&quot;";
            return false;
        }

        $this->Updater->setCurrentBackupDirectory("$backup_directory");

        /* Try moving the configuration file */
        try
        {
            File::put($backup_directory . "/$backup_info", 'Backing up of Hummingbird');
        }
        catch (Exception $e)
        {
            $this->error_code['backup_prep'] = "There was a problem creating the backup file";
            return false;
        }   


        /* Run the database backup */
        $this->update_info('prep_backup', true);
        $success = $this->Updater->backup_database($backup_directory . "/$database_backup/export.sql");

        if(!$success)
        {
            $this->error_code['backup_prep'] = "There was a problem exporting the database";
            return false;
        }

        $this->update_info('database_backup', true);

        /* Backup the website files */
        $backup_name = 'files.tar.gz';
        $output = $this->Updater->backup_site($backup_name);

        if(File::exists(base_path() . "/$backup_name"))
        {
            try
            {
                File::move(base_path() . "/$backup_name",  "$backup_directory/$backup_name");
                $this->update_info('file_backup', true);
            }
            catch (Exception $e)
            {
                $this->error_code['backup_prep'] = "There was a problem exporting creating a full site backup";
                return false;
            } 
        }
        else
        {
            $this->error_code['backup_prep'] = "There was a problem exporting creating a full site backup";
            return false;
        }
    }

    /**
     * ***---***
     *
     * @return 
     */
    public function update_info($key = null, $completed = FALSE, $custom_message = FALSE)
    {
        $messages = (null === Session::get("HB3UPDATE")) ? array():Session::get("HB3UPDATE");
        
        if($completed) $messages['progress'] = $this->update_tasks[$key]['progress'];

        if(null !== $key)
        {
            $message = (null !== $this->update_tasks[$key]['message']) ? $this->update_tasks[$key]['message'].'...':'';
            $completed = (null !== $this->update_tasks[$key]['completed']) ? $this->update_tasks[$key]['completed']:'Complete';

            if($completed AND $custom_message !== FALSE) $completed = $custom_message;

            $messages['updates'][$key] = ($completed) ? $message . $completed:$message;
        }

        if($completed)
        {
            $next_arr = $this->get_next($this->update_tasks, $key);

            if(is_array($next_arr))
            {
                $messages['updates'][key($next_arr)] = $next_arr[key($next_arr)]['message'];
            }
        }

        Log::info("Upgrading process @" . $messages['progress'] . " %");

        Session::put("HB3UPDATE", $messages);
        Session::save();
    }

    /**
     * ***---***
     *
     * @return JSON $messages
     */
    public function return_progress()
    {
        if(null === Session::get("HB3UPDATE"))
        {
            Log::error('HB3Update Session not being created.');
            return;
        }

        /* Valid request */
        $messages = Session::get("HB3UPDATE");

        /* Forget if completed */
        if((isset($messages['progress']) AND $messages['progress'] == 100) OR File::exists($this->Updater->getUpdatePath() . '/.complete'))
        {
            Log::info('HB3 upgrade completed');
            $messages['progress'] = 100;

            $this->forget_update_session();
        }

        /* Return */
        echo json_encode($messages);
        return;
    }

    /**
     * ***---***
     *
     * @return 
     */
    public function get_next($array, $current_key)
    {
        $currentKey = key($array);

        while ($currentKey !== null && $currentKey != $current_key)
        {
            next($array);
            $currentKey = key($array);
        }

        $current = next($array);

        return (is_array($current) AND !empty($current)) ? array(key($array) => current($array)):FALSE;
    }

    /**
     * ***---***
     *
     * @return 
     */
    public function forget_update_session()
    {
        Session::forget('HB3UPDATE');
        Session::save();
    }

    public function cleanUpgrade()
    {
        if(!Request::ajax()) 
        {
            /* Temp file can now be removed */
            if(null !== Session::get('clean_upgrade'))
            {
                return (new Updater)->cleanUpgrade();
            }
        }
    }
}