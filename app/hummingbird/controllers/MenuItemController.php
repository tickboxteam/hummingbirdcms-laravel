<?php

class MenuItemController extends CmsbaseController {
    
    public $code_location = 'pages';
    
    public function getIndex()
    {
        $this->data['tag'] = 'Manage Menuitems';
        $this->data['menuitems'] = Menuitem::all();
        return View::make('HummingbirdBase::cms/menuitems', $this->data);
    }
    
    public function getEdit($id)
    {
        $this->data['menuitem'] = Menuitem::find($id);
        $this->data['tag'] = 'Edit '.$this->data['menuitem']->title;
        $this->getCommonData();
        return View::make('HummingbirdBase::cms/menuitems-edit', $this->data);
    }
    
    public function getCommonData()
    {
        $this->data['menuitems'] = Menuitem::get_selection();
    }
    
    public function postAdd()
    {
        $input = Input::except('_token', 'add');        
        $menuitem = (new Menuitem)->fill($input);
        if(!$menuitem->save()){
            return Redirect::to(App::make('backend_url').'/menuitems')->withErrors($menuitem->errors());
        }
        return Redirect::to(App::make('backend_url').'/menuitems/edit/'.$menuitem->id);
    }

    // Show a menuitem by slug
    public function show($slug = 'home')
    {
        $redirect = $this->checkRedirects($slug);
        
        if($redirect) {
            return Redirect::to($redirect);
        }
        
        $menuitem = Menuitem::where('permalink', '=', $slug)->get()->first();
        
        if($menuitem) {
        
            // todo: get menuitem, and use that as view

            return View::make('menuitems.test')->with('menuitem', $menuitem);
        }
        
        App::abort(404);
    }

    
    public function checkRedirects($slug)
    {
        # see if redreict exists
        $redirect = Redirection::where('from', '=', $slug)->first();
        
        if(!$redirect) return false;
        
        # update count
        $redirect->no_used += 1;
        $redirect->last_used = date('Y-m-d H:i:s');
        $redirect->save();
        
        # redirect to url given
        return $redirect->to;
    }
}
