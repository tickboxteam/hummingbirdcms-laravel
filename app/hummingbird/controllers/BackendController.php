<?php

class BackendController extends CmsbaseController {
    
    public static function normalizeUrl($url)
    {
        if (substr($url, 0, 1) != '/')
            $url = '/'.$url;

        if (substr($url, -1) == '/')
            $url = substr($url, 0, -1);

        if (!strlen($url))
            $url = '/';

        return $url;
    }

    /**
     * Splits an URL by segments separated by the slash symbol.
     *
     * @param string $url URL to segmentize.
     * @return array Returns the URL segments.
     */
    public static function segmentizeUrl($url)
    {
        $url = self::normalizeUrl($url);
        $segments = explode('/', $url);

        $result = array();
        foreach ($segments as $segment) {
            if (strlen($segment))
                $result[] = $segment;
        }

        return $result;
    }

    public function run($url = null)
    {
        $params = $this->segmentizeUrl($url);

        $action = (!isset($params[1])) ? 'read':$params[1];

        dd($params);
    }
}
