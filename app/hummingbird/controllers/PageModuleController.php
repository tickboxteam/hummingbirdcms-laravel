<?php

class PageModuleController extends CmsbaseController {
    
    public $code_location = 'pages';
    
    public function getIndex()
    {
        $this->data['tag'] = 'Manage Page Modules';
        $this->data['pagemodules'] = Pagemodule::all();
        $this->getCommonData();
        return View::make('HummingbirdBase::cms/pagemodules', $this->data);
    }
    
    public function getEdit($id)
    {
        $this->data['pagemodule'] = Pagemodule::find($id);
        $this->data['tag'] = 'Edit '.$this->data['pagemodule']->title;
        $this->getCommonData();
        return View::make('HummingbirdBase::cms/pagemodules-edit', $this->data);
    }
    
    public function getCommonData()
    {
        $this->data['pages'] = Page::get_selection();
        $this->data['modules'] = Module::get_selection();
    }
    
    public function postAdd()
    {
        $input = Input::except('_token', 'add');
        $pagemodule = (new Pagemodule)->fill($input);
        if(!$pagemodule->save()) {
            return Redirect::to(App::make('backend_url').'/page-modules')->withErrors($pagemodule->errors());
        }
        return Redirect::to(App::make('backend_url').'/page-modules/edit/'.$pagemodule->id);
    }
    
    public function getPagemodulesForArea($area, $page_id)
    {
        $modules = Pagemodule::join('modules', 'pagemodules.module_id', '=', 'modules.id')
                ->join('moduletemplates', 'modules.moduletemplate_id', '=', 'moduletemplates.id')
                ->where('page_id', '=', $page_id)->where('area', '=', $area)->orderby('position', 'asc')->get();
        
        return Response::json($modules);
    }
    
    public function addPagemodule($area, $page_id, $module_id, $position = 0)
    {
        $newPageModule = new Pagemodule;
        $newPageModule->area = $area;
        $newPageModule->page_id = $page_id;
        $newPageModule->module_id = $module_id;
        $newPageModule->position = $position;
        $newPageModule->save();
        
        
        return ($newPageModule->id > 0) ? Response::json(true) : Response::json(false);
    }
    
}
