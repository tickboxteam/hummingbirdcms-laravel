<?php

class CmsbaseController extends Controller
{
    protected $user;
    protected $model;

    public $data; // data to pass to views
    public $site_settings;
    public $search = false;
    public $pagination = 15;
    

    function __construct()
    {
        /* initialise */
        $this->user = Auth::user();

        if(null === $this->user) return Redirect::to(General::backend_url());

        if($this->search)
        {
            $insert = array();

            $pages = Page::whereNull('deleted_at')->where('status', '=', 'public')->where('searchable', '=', 1)->get();
            $page_class = get_class((new Page));
            foreach($pages as $page)
            {
                /* Create new search record - change to be an event listener */
                (new Searcher)->addToSearch($page);

                // $insert[] = array(
                //     'item_id' => $page->id,
                //     'title' => $page->title,
                //     'url' => $page->permalink,
                //     'content' => $page->content,
                //     'created_at' => $page->created_at,
                //     'updated_at' => $page->updated_at,
                //     'type' => $page_class
                // );
            }

            $events = Events::whereNull('deleted_at')->where('live', '=', 1)->where('searchable', '=', 1)->get();
            $event_class = get_class((new Events));
            foreach($events as $event)
            {
                /* Create new search record - change to be an event listener */
                (new Searcher)->addToSearch($event);

                // $insert[] = array(
                //     'item_id' => $event->id,
                //     'title' => $event->title,
                //     'url' => $event->url,
                //     'summary' => $event->summary,
                //     'content' => $event->description,
                //     'created_at' => $event->created_at,
                //     'updated_at' => $event->updated_at,
                //     'type' => $event_class
                // );
            }

            // $news = Post::whereNull('deleted_at')->where('status', '=', 'public')->where('searchable', '=', 1)->get();
            // $post_class = get_class((new Post));
            // foreach($news as $article)
            // {
                /* Create new search record - change to be an event listener */
                // (new Searcher)->addToSearch($article);

                // $insert[] = array(
                //     'item_id' => $article->id,
                //     'title' => $article->title,
                //     'url' => $article->url,
                //     'summary' => $article->summary,
                //     'content' => $article->content,
                //     'created_at' => $article->created_at,
                //     'updated_at' => $article->updated_at,
                //     'type' => $post_class
                // );
            // }

            // DB::table('search')->insert($insert);
        }


        /* Set up */


        if (extension_loaded('imagick')) 
            Image::configure(array('driver' => 'imagick'));

        $this->site_settings = Setting::all();

        $this->data['breadcrumbs'] = array(
            0 => array(
                'classes' => '',
                'icon' => 'fa fa-home',
                'title' => 'Home',
                'url' => '/' . General::backend_url()
            )
        ); //breadcrumb manager
    }

    public function createDatabaseBackup($contents)
    {
        $filename = 'database-backup-' . date("Y-m-d H:i:s");
        $save_path = base_path() . '/backups/databases/';

        if(File::is_writable($save_path . $filename))
        {
            $bytes_written = File::put($save_path . $filename, $contents);
            if ($bytes_written === false)
            {
                die("Error writing to file");
            }
        }
        else
        {
            die( 'unable to write' );
        }
    }
    
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
    
    function get_cms_nav()
    {
        return array();
        
        // todo: need to do this dynamically
        $distinct_pairs = CmsNav::select(DB::raw('mainsectionname, sectionname'))->groupBy(array('mainsectionname', 'sectionname'))->get();
        
        $nav = array();
        
        foreach($distinct_pairs as $distinct_pair) {
            $nav[$distinct_pair->mainsectionname][$distinct_pair->sectionname] = CmsNav::where('live','=',1)->where('mainsectionname','=',$distinct_pair->mainsectionname)->where('sectionname','=',$distinct_pair->sectionname)->get();
            
        }
        
        
        return $nav;
    }
    
    public function search_model($model_name)
    {
        $search = Search::make(array(
            'model' => $model_name, 
            'searchterm' => Request::get('search'),
            'which_end' => 'back'));
        
        $search->run();       
        
        return $search->results;
    }
    
    public function export_model($modelname)
    {
        $model = new $modelname;
        $this->data['modelname'] = strtolower($modelname);
        $this->data['fields'] = $model->export_fields;
        return View::make('HummingbirdBase::cms.export', $this->data);
    }
    
    public function run_export($modelname, $filename)
    {
        $fields = Input::except('_token');
        
        $results = $modelname::select(DB::raw(implode(",", $fields)))->get();
        
        // headings
        $output = '"' . implode('","', $fields) . '"';
        $output .= "\r\n";
 
        foreach ($results as $row) {
            $output .= '"' . implode('","',$row->toArray()) . '"' . "\r\n";
        }
        
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
        );
 
        return Response::make(rtrim($output, "\n"), 200, $headers);        
    }
    
    public function forbidden($redirect = false)
    {
        if($redirect) return Redirect::to(General::backend_url() . General::get_backend_url('forbidden'));
        
        return View::make('HummingbirdBase::cms.errors.unauthorised', $this->data);
    }




    /**
     * Add a leading slash to the URL - remove trailing slash
     *
     * @param string $url URL to normalize.
     * @return string Returns normalized URL.
     */
    public static function normalizeUrl($url)
    {
        if (substr($url, 0, 1) != '/')
            $url = '/'.$url;

        if (substr($url, -1) == '/')
            $url = substr($url, 0, -1);

        if (!strlen($url))
            $url = '/';

        return $url;
    }

    /**
     * Splits an URL by segments separated by the slash symbol.
     *
     * @param string $url URL to segmentize.
     * @return array Returns the URL segments.
     */
    public static function segmentizeUrl($url)
    {
        $url = self::normalizeUrl($url);
        $segments = explode('/', $url);

        $result = array();
        foreach ($segments as $segment) {
            if (strlen($segment))
                $result[] = $segment;
        }

        return $result;
    }

    /**
     * Find controller that we need
     * If controller not found, returns CMS 404 page
     * If controller, or action, not available then returns 500 forbidden
     * @param string $url Specifies the requested page URL.
     * If the parameter is omitted, the current URL used.
     * @return string Returns the processed page content.
     */
    public function run($url = null)
    {
        //if the URL is null or blank, then we need to go to the dashboard
        if(null === $url) $url = 'dashboard';

        $url = General::backend_url() . '/' . $url;

        dd($url);
        
        $params = $this->segmentizeUrl($url);

        $action = (!isset($params[1])) ? 'read':$params[1];

    }

    public function last_db_query()
    {
        $queries = DB::getQueryLog();
        $last_query = end($queries);
        return $last_query;
    }

    public function addToBreadcrumb($data)
    {
        $this->data['breadcrumbs'][] = $data;
    }

    public function renderBreadcrumb()
    {
        foreach($this->breadcrumbs as $crumb)
        {
            $this->addToBreadcrumb($crumb);
        }
    }

    public function error($exception = null)
    {
        $messages = array(
            'There are 320 different species of hummingbird found throughout the Americas. The smallest of all is the tiny bee hummingbird, Mellsuga helenae. They are among the smallest of birds.',
            'Their English name derives from the characteristic hum made by their rapid wing beats.',
            'Each species of hummingbird makes a different humming sound, determined by the number of wing beats per second.',
            'They can hover in mid-air by rapidly flapping their wings 12–90 times per second (depending on the species).',
            'During courtship, the wingtips of the ruby-throated hummingbird beats up to 200 times per second, as opposed to the usual wing beat of 90 times per second.  When the female appears, her partner displays by flying to and fro in a perfect arc.  The pair then dives up and down vertically, facing each other.',
            'They can also fly backwards, and are the only group of birds able to do so.',
            'They can fly at speeds exceeding 15 m/s or 54 km/h.',
            'Male hummingbirds are reported to have achieved speeds of almost 400 body lengths per second when swooping in an effort to impress females.',
            'They have feet so tiny that they cannot walk on the ground, and find it awkward to shuffle along a perch.',
            'They have a short, high-pitched squeaky call.',
            'Some are so small, they have been known to be caught by dragonflies and praying mantis, trapped in spider’s webs, snatched by frogs and stuck on thistles.',
            'The hummingbird needs to eat twice its bodyweight in food every day, and to do so they must visit hundreds of flowers daily.',
            'Hummingbirds eat nectar for the most part, although they may catch an insect now and then for a protein boost.',
            'Their super fast wing beats use up a lot of energy, so they spend most of the day sitting around resting. To save energy at night, many species go into torpor (a short-term decrease in body temperature and metabolic rate).',
            'Despite their tiny size, the ruby-throated hummingbird makes a remarkable annual migration, flying over 3000km from the eastern USA, crossing over 1000km of the Gulf of Mexico in a single journey to winter in Central America.',
            'The ruby-throated hummingbird has only approximately 940 feathers on its entire body.',
            'Before migrating, the hummingbird stores a layer of fat equal to half its body weight.',
            'The female hummingbird builds a tiny nest high up in a tree, often up to six metres from the ground. She coats the outside with lichen and small pieces of bark and lines the inside with plant material.',
            'The male takes no responsibility in rearing the young and may find another mate after the first brood hatches.',
            'Some species of hummingbird are so rare that they have been identified only from the skins exported to Europe.'
        );
    
        return View::make('HummingbirdBase::cms.errors.404', array('exception' => $exception, 'quote' => $messages[array_rand($messages)]));
    }
}
