<?php

class TemplateNewController extends CmsbaseController {
    
    public function getIndex()
    {
        $this->data['tag'] = 'Manage Templates';
        $this->data['templates'] = Template::whereNull('deleted_at')->get();
        return View::make('HummingbirdBase::cms/templates-new', $this->data);
    }
    
    public function getEdit($id)
    {
        $this->data['template'] = Template::find($id);

        if($this->data['template'])
        {
            if(Input::get('lock'))
            {
                $this->data['template']->locked = 1;

                if($this->data['template']->save())
                {
                    Activitylog::log([
                        'action' => 'UPDATED',
                        'type' => get_class($this->data['template']),
                        'link_id' => $this->data['template']->id,
                        'description' => 'Template updated',
                        'notes' => Auth::user()->username . " has locked the template &quot;" . $this->data['template']->name . "&quot;"
                    ]);

                    return Redirect::to(App::make('backend_url').'/templates-new/')->with('success', 'Template has been locked.');
                }
                else
                {
                    DD("COULD NOT BE LOCKED");
                }
            }
            else
            {
                $this->data['tag'] = 'Edit '.$this->data['template']->title;
                return View::make('HummingbirdBase::cms/templates-new-edit', $this->data);                
            }
        }
        else
        {
            return Redirect::to(App::make('backend_url').'/templates-new/')->with('error', 'Template does not exist.');
        }
    }
    
    public function postEdit($id)
    {
        $input = Input::all();
        $template = Template::find($id);
        
        if($template->update($input))
        {
            Activitylog::log([
                'action' => 'UPDATED',
                'type' => get_class($template),
                'link_id' => $template->id,
                'description' => 'Template updated',
                'notes' => Auth::user()->username . " has updated the template &quot;" . $template->name . "&quot;"
            ]);

            return Redirect::to(App::make('backend_url').'/templates-new/edit/'.$id);
        }
dd();
        return Redirect::to(App::make('backend_url').'/templates-new/edit/'.$id)->with('error', 'There was a problem updating the template');
    }

    public function postAdd()
    {
        $input = Input::except('_token', 'add');        
        $template = (new Template)->fill($input);
        if($template->save())
        {
            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($template),
                'link_id' => $template->id,
                'description' => 'A new template has been created',
                'notes' => Auth::user()->username . " created a page template"
            ]);

            return Redirect::to(App::make('backend_url').'/templates-new')->withErrors($template->errors());
        }
        return Redirect::to(App::make('backend_url').'/templates-new/edit/'.$template->id);
    }
    
    public function getDelete($id)
    {
        if(is_numeric($id) AND $id > 0)
        {
            $template = Template::find($id);

            if(null !== $template)
            {
                $template_name = $template->name;
                $template->deleted_at = time();

                if($template->save())
                {
                    Activitylog::log([
                        'action' => 'DELETED',
                        'type' => get_class(new Template),
                        'link_id' => null,
                        'description' => 'Template deleted',
                        'notes' => Auth::user()->username . " deleted the template &quot;$template_name&quot;"
                    ]);

                    return Redirect::to(App::make('backend_url').'/templates-new/')->with('success', 'Template has been deleted.');
                }
                else
                {
                    return Redirect::to(App::make('backend_url').'/templates-new/')->with('error', 'Template could not be deleted.');
                }
            }
            else
            {
                return Redirect::to(App::make('backend_url').'/templates-new/')->with('error', 'Template could not be found. Please try again.');
            }
        }

        return Redirect::to(App::make('backend_url').'/templates-new/')->with('error', 'Please select a template to delete.');
    }

    public function returnTemplateHTML($id)
    {
        $data = array();

        if(is_numeric($id) AND $id > 0)
        {
            $template = Template::where('id', '=', $id)->whereNull('deleted_at')->take(1)->get();

            if(count($template) == 1)
            {
                $data = array(
                    'state' => true,
                    'message' => $template[0]->html
                );
            }
            else
            {
                $data = array(
                    'state' => false,
                    'message' => 'Template could not be found'
                );
            }
        }
        else
        {
            $data = array(
                'state' => false,
                'message' => 'Template could not be found'
            );
        }

        return json_encode($data);
    }
}
