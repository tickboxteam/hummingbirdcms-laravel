<?php

class ModuleController extends CmsbaseController
{
    public function __construct(Module $Module)
    {
        parent::__construct();

        $this->model = $Module;

        $this->data['moduletemplates'] = Moduletemplate::get_selection();

        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Modules',
            'url' => '/' . General::backend_url() . '/modules/'
        ); //breadcrumb manager    
    }

    public function getIndex()
    {
        if($this->user->hasAccess('read', get_class($this->model)))
        {
            $this->data['tag'] = 'Manage Modules';
            
            $this->data['modules'] = Module::whereNull('deleted_at')->orderBy('name', 'ASC')->get();

            if(count($this->data['modules']) > 0)
            {
                $modules_grouped = array();

                foreach($this->data['modules'] as $module)
                {
                    $modules_grouped[$module->moduletemplate_id][] = $module;
                }

                ksort($modules_grouped);

                $this->data['modules'] = $modules_grouped;
            }

            return View::make('HummingbirdBase::cms/modules', $this->data);
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function getEdit($id)
    {
        if($this->user->hasAccess('update', get_class($this->model)))
        {
            $this->data['module'] = Module::whereNull('deleted_at')->where('id', '=', $id)->first();

            if(null !== $this->data['module'])
            {
                $this->data['tag'] = 'Edit '.$this->data['module']->title;
                
                $this->data['breadcrumbs'][] = array(
                    'classes' => '',
                    'icon' => '',
                    'title' => 'Edit: ' . $this->data['module']->name,
                    'url' => '/' . General::backend_url() . '/modules/edit/' . $this->data['module']->id
                ); //breadcrumb manager  

                if(!$this->data['module']->template())
                {
                    $this->data['taxonomy'] = $this->data['module']->taxonomy();
                }

                return View::make('HummingbirdBase::cms/modules-edit', $this->data);
            }
            else
            {
                return Redirect::to(App::make('backend_url').'/modules/?fail=noedit');
            }
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function postEdit($id)
    {   
        if($this->user->hasAccess('update', get_class($this->model)))
        {
            $input = Input::except('_token');
            $module = Module::whereNull('deleted_at')->where('id', '=', $id)->first();

            if(null !== $module)
            {
                $messages = [
                    'name.required' => 'The :attribute field is required.',
                    'name.unique' => 'The :attribute must be unique.',
                    'name.max' => 'The :attribute must be shorter than 255 characters'
                ];

                $validator = Validator::make($input, [
                    'name' => "required|unique:modules,name,$id|max:255"
                ], $messages);

                if ($validator->fails())
                {
                    return Redirect::to(App::make('backend_url').'/modules/edit/' . $id)->withErrors($validator->errors());
                }
                else
                {
                    $module->update($input);
                    
                    $data = array();

                    if(!$module->template())
                    {
                        $data['structure'] = Input::get('html');
                        $data['css'] = Input::get('css');
                        $data['js'] = Input::get('js');
                        $module->module_data = serialize($data);

                        /* Store cats and tags */
                        $input['taxonomy'] = (isset($input['taxonomy'])) ? $input['taxonomy']:array();
                        $module->storeTaxonomy($input['taxonomy']);
                    }
                    else
                    {
                        foreach($input['fields'] as $key => $field)
                        {
                            $data['structure'][$key] = $field;
                        }

                        $module->module_data = serialize($data);
                    }

                    if($module->save())
                    {
                        Activitylog::log([
                            'action' => 'UPDATED',
                            'type' => get_class($module),
                            'link_id' => $module,
                            'description' => 'Updated module',
                            'notes' => Auth::user()->username . " updated the module - &quot;$module->name&quot;"
                        ]);  

                        return Redirect::to(App::make('backend_url').'/modules/edit/' . $id);
                    }
                    else
                    {
                        return Redirect::to(App::make('backend_url').'/modules/edit/' . $id . '/?fail=nosave');
                    }
                }
            }
            else
            {
                return Redirect::to(App::make('backend_url').'/modules/?fail=no-module-found');    
            }
        }
        else
        {
            return parent::forbidden();
        }
    }
    
    public function postAdd()
    {
        if($this->user->hasAccess('create', get_class($this->model)))
        {
            $input = Input::except('_token', 'add');        
            $module = (new Module)->fill($input);

            $messages = [
                'name.required' => 'The :attribute field is required.',
                'name.unique' => 'The :attribute must be unique.',
                'name.max' => 'The :attribute must be shorter than 255 characters'
            ];

            $validator = Validator::make($input, [
                'name' => 'required|unique:modules|max:255'
            ], $messages);

            if ($validator->fails())
            {
                return Redirect::to(App::make('backend_url').'/modules')->withErrors($validator->errors());
            }
            else
            {
                if(!$module->save())
                {
                    return Redirect::to(App::make('backend_url').'/modules/?fail=noadd');
                }
                else
                {
                    Activitylog::log([
                        'action' => 'CREATED',
                        'type' => get_class($this->model),
                        'link_id' => $module->id,
                        'description' => 'Created a new module',
                        'notes' => Auth::user()->username . " created a new module - &quot;$module->name&quot;"
                    ]); 

                    return Redirect::to(App::make('backend_url').'/modules/edit/'.$module->id);
                }
            }
        }
        else
        {
            return parent::forbidden();
        }
    }

    public function replicate($id)
    {
        if($this->user->hasAccess('create', get_class($this->model)))
        {
            if(isset($id) AND is_numeric($id) AND $id > 0)
            {
                $module = Module::where('deleted_at')->where('id', '=', $id)->first(); //get module to replicate

                if(null !== $module)
                {
                    $new_module = $module->replicate();
                    $new_module->name .= ' ' . time();
                    
                    if($new_module->save())
                    {
                        return Redirect::to(App::make('backend_url').'/modules/edit/' . $new_module->id);
                    }                
                    else
                    {
                        return Redirect::to(App::make('backend_url').'/modules/?fail=problem-replicating');                        
                    }
                }
                else
                {
                    return Redirect::to(App::make('backend_url').'/modules/?fail=no-module-found');                    
                }
            }
            return Redirect::to(App::make('backend_url').'/modules/?fail=nothing-to-replicate');
        }
        else
        {
            return parent::forbidden();
        }
    }

    public function getDelete($id)
    {
        if($this->user->hasAccess('delete', get_class($this->model)))
        {
            $this->model = Module::where('id', '=', $id)->whereNull('deleted_at')->first();

            if(null !== $this->model)
            {
                $name = $this->model->name;
                $this->model->delete($id);

                Activitylog::log([
                    'action' => 'DELETED',
                    'type' => get_class($this->model),
                    'link_id' => $id,
                    'description' => 'Deleted module',
                    'notes' => Auth::user()->username . " deleted the module &quot;$name&quot; role"
                ]);

                return Redirect::to(App::make('backend_url').'/modules/');
            }
            else
            {
                //return errors too
                return Redirect::to(App::make('backend_url').'/modules/?fail=nodelete');
            }
        }
        else
        {
            return parent::forbidden();
        }
    }    
    
    public function getModulesForArea($area)
    {
        $modules = Module::join('moduletemplates', 'modules.moduletemplate_id', '=', 'moduletemplates.id')
                ->select('*')->where('moduletemplates.areas', 'like', '%'.$area.'%')->get();
        
        return Response::json($modules);
    }


    public function import()
    {
        ini_set('memory_limit','2048M');
        set_time_limit(180);

        $files_used_array = array();


        // DB::unprepared(file_get_contents(base_path() . '/import/sql/modules.sql')); //hb_old_modules
        // DB::unprepared(file_get_contents(base_path() . '/import/sql/modulecontent.sql')); //hb_old_modulecontent

        $modules = DB::table('old_modules')->get();

        if(count($modules) > 0)
        {
            foreach($modules as $module)
            {
                $data = array();

                /* Create module */
                // $new_module = new Module;
                // $new_module['name'] = $module->name;
                // $new_module['moduletemplate_id'] = $module->templateid;

                // if($module->deleted == 1)
                // {
                //     $new_module['deleted_at'] = date("Y-m-d H:i:s");
                // }

                /* Get each one and insert it */

                $module_fields = DB::table('old_modulecontent')->where('moduleid', '=', $module->id)->orderBy('id', 'ASC')->get();

                /* Create new media collection inside migration */
                $collection = array();
                $collection['name'] = $module->name . time();
                $collection['parent_collection'] = 3;
                $collection['permalink'] = Str::slug($collection['name']);

                /* insert and get id for upload */
                $collection_id = DB::table('media-collections')->insertGetId($collection);

                Activitylog::log([
                    'action' => 'CREATED',
                    'type' => get_class(new Media),
                    'link_id' => $collection_id,
                    'url' => General::backend_url() . "/media/view/$collection_id",
                    'description' => 'Created collection',
                    'notes' => "System created a new collection &quot;".$collection['name']."&quot;"
                ]);

                /* process module */
                foreach($module_fields as $field)
                {
                    if(strpos($field->value, 'uploads/images') !== false)
                    {
                        if(!isset($files_used_array[$field->value]))
                        {
                            $base_image = base_path() . '/import/images/modules/' . str_replace("/uploads/images/modules/", "", $field->value);
                            
                            $source = App::make('MediaController')->uploadFile_byFunction($base_image, $collection_id);

                            if($source !== false)
                            {
                                $files_used_array[$field->value] = $data['structure'][$field->field] = $source;
                            }
                        }
                        else
                        {
                            /* already uploaded - 2 */
                            $data['structure'][$field->field] = $files_used_array[$field->value];
                        }
                    }
                    else
                    {
                        // no images but we could have documents/audio/etc
                        if(strpos($field->value, 'uploads/documents') !== false)
                        {
                            if(!isset($files_used_array[$field->value]))
                            {
                                // /uploads/documents/1330707182_imagemod.jpg
                                $base_image = base_path() . '/import/uploads/documents/' . str_replace("/uploads/documents/", "", $field->value);
                                
                                $source = App::make('MediaController')->uploadFile_byFunction($base_image, $collection_id);

                                if($source !== false)
                                {
                                    $files_used_array[$field->value] = $data['structure'][$field->field] = $source;
                                }
                            }
                            else
                            {
                                /* already uploaded -2s*/
                                $data['structure'][$field->field] = $files_used_array[$field->value];
                            }
                        }
                        else
                        {
                            $data['structure'][$field->field] = $field->value;
                        }
                    }
                }

                // $new_module->module_data = serialize($data);

                // $new_module->save();

                // Activitylog::log([
                //     'action' => 'CREATED',
                //     'type' => get_class($new_module),
                //     'link_id' => $new_module->id,
                //     'description' => 'Created new module',
                //     'notes' => Auth::user()->username . " created the module - &quot;$new_module->name&quot;"
                // ]);
            }
        } 

        /* Keep records safe for later */
        $media_xref = array();

        foreach($files_used_array as $key => $value)
        {
            $media_xref[] = array(
                'source' => $key,
                'destination' => $value
            );
        }

        if(count($media_xref) > 0)
        {
            DB::table('media_xref')->insert($media_xref);
        }

        die('Modules completed');
    }

}
