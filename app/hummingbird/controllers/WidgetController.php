<?php

class WidgetController extends CmsbaseController {
    
    public $code_location = 'dashboard';
    
    public function getIndex()
    {
        $this->data['tag'] = 'Manage Widgets';
        $this->data['widgets'] = Widget::all();
        return View::make('HummingbirdBase::cms/widgets', $this->data);
    }
    
    public function getEdit($id)
    {
        $this->data['widget'] = Widget::find($id);
        $this->data['tag'] = 'Edit '.$this->data['widget']->title;
        $this->getCommonData();
        return View::make('HummingbirdBase::cms/widgets-edit', $this->data);
    }
    
    public function getCommonData()
    {
        $this->data['widgets'] = Widget::get_selection();
    }
    
    public function postAdd()
    {
        $input = Input::except('_token', 'add');        
        $widget = (new Widget)->fill($input);
        if(!$widget->save()) {
            return Redirect::to(App::make('backend_url').'/widgets')->withErrors($widget->errors());
        }
        return Redirect::to(App::make('backend_url').'/widgets/edit/'.$widget->id);
    }

    // Show a widget by slug
    public function show($slug = 'home')
    {
        $redirect = $this->checkRedirects($slug);
        
        if($redirect) {
            return Redirect::to($redirect);
        }
        
        $widget = Widget::where('permalink', '=', $slug)->get()->first();
        
        if($widget) {
        
            // todo: get widget, and use that as view

            return View::make('widgets.test')->with('widget', $widget);
        }
        
        App::abort(404);
    }

    
    public function checkRedirects($slug)
    {
        # see if redreict exists
        $redirect = Redirection::where('from', '=', $slug)->first();
        
        if(!$redirect) return false;
        
        # update count
        $redirect->no_used += 1;
        $redirect->last_used = date('Y-m-d H:i:s');
        $redirect->save();
        
        # redirect to url given
        return $redirect->to;
    }
}
