<?php

class PageController extends CmsbaseController 
{
    public $code_location = 'pages';
    public $pagination = 15;
    public $reset_positions = FALSE;

    /**
     * Inject the models.
     * @param Post $post
     * @param User $user
     */
    public function __construct(Page $page, User $user)
    {
        parent::__construct();

        $this->page = $page;
        $this->user = Auth::user();

        if($this->reset_positions)
        {
            $this->processUpdates();
        }
    }

    /*
    * Start with the homepage - run an update on all the internal pages, and assign them a position
    */
    public function processUpdates($parent_id = 1)
    {
        $pages = Page::where('parentpage_id', '=', $parent_id)->get();

        $position = 0;
        foreach($pages as $page)
        {
            $position++;
            $page->position = $position;
            $page->save();

            /* Update all page versions */
            $page_versions = PageVersion::where('page_id', '=', $page->id)->get();
            foreach($page_versions as $version)
            {
                $version->position = $position;
                $version->save();
            }

            $this->processUpdates($page->id);
        }
    }

    public function getIndex($pagelist = '')
    {
        if($this->user->hasAccess('read', get_class($this->page)))
        {
            $this->data['tag'] = 'Manage Pages';
            $this->data['page_list'] = (new Page)->cms_list_pages();

            if(Input::get('s') AND Input::get('s') != '')
            {
                $this->data['pages'] = $this->cms_search();
            }
            elseif($pagelist == 'trash')
            {
                $this->data['pages'] = Page::where('status', 'trash')->paginate($this->pagination);
            }
            else
            {
                $this->data['pages'] = Page::whereNull('deleted_at')->where('status', '!=', 'trash')->paginate($this->pagination);
            }

            return View::make('HummingbirdBase::cms/pages', $this->data);
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised'); //replace with CMS Error/Unauthorised controller
        }
    }
    
    public function getTrash()
    {
        return $this->getIndex($pagelist = 'trash');
    }

    public function getRestore($id)
    {
        
        $page           = Page::find($id);
        $page->status   = 'draft';
        $page->save();

        return $this->getEdit($id);
    }

    public function getEdit($id = false)
    {
        if($this->user->hasAccess('update', get_class($this->page)))
        {
            $version = (!Input::get('version')) ? false:Input::get('version'); //get the version
            $this->data['page_version'] ='';
            $this->data['page_list'] = (new Page)->cms_list_pages($id);

            if(!$id AND !$version) return Redirect::to(App::make('backend_url').'/pages/');

            if($version)
            {
                $this->data['page'] = PageVersion::where('id', '=', $version)->where('page_id', '=', $id)->first();
                $this->data['page_id'] = $this->data['page']->page_id;
                $this->data['page_version'] = '/?version='.$this->data['page']->id;
            }

            if(!isset($this->data['page']))
            {
                $this->data['page'] = Page::find($id);
                $this->data['page_id'] = $this->data['page']->id;
                $this->data['versions'] = PageVersion::where('page_id', '=', $id)->whereNull('deleted_at')->orderBy('id', 'DESC')->take(5)->get();
            }

            $this->data['meta'] = $this->getMetaData($id);
            $this->data['templates'] = Template::get_selection();
            $this->data['taxonomy'] = $this->data['page']->taxonomy();

            $this->data['tag'] = 'Edit '.$this->data['page']->title;
            $this->getCommonData($id);        
            
            $html = ($this->data['page']->template) ? $this->data['page']->template->html : '';
            
            $html = str_replace('{{BANNER}}', '<div id="banner" class="image-selector"></div>', $html);
            $html = str_replace('{{CONTENT}}', '<textarea name="content" class="redactor content">'.$this->data['page']->content.'</textarea>', $html);
            
            $module_html = '<div class="module-selector"><div class="tools">';
            $module_html .= '<a class="action-module btn btn-info" data-action="add" data-toggle="modal" href="#add-module">ADD</a>&nbsp;';
            $module_html .= '<a class="action-module btn btn-info" data-action="edit" data-toggle="modal" href="#edit-module">EDIT</a>&nbsp;';
            $module_html .= '<a class="action-module btn btn-info" data-action="find" data-toggle="modal" href="#find-module">FIND</a>&nbsp;';
            $module_html .= '<a class="action-module btn btn-info" data-action="new" data-toggle="modal" href="#new-module">NEW</a>&nbsp;';
            $module_html .= '<a class="action-module btn btn-info" data-action="remove" data-toggle="modal" href="#remove-module">REMOVE</a>&nbsp;';
                    
            $module_html .= '</div></div>';
            
            // $areas = Templatearea::all();
            
            // foreach($areas as $area) {
            
            //     $html = str_replace("{{MODULES:$area->name}}", $module_html, $html);
            
            // }
            
            $this->data['html'] = $html;
            return View::make('HummingbirdBase::cms/pages-edit', $this->data);
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised');
        }
    }
    
    public function postEdit($id)
    {
        if($this->user->hasAccess('update', get_class($this->page)))
        {
            $activity_log_url = '';
            $version = (!Input::get('version')) ? false:Input::get('version'); //get the version

            $input = Input::except('_token');
            $page = Page::find($id);
            $page = $page->fill($input);
            // $page->url = $input['url'];
            $page->content = $input['generated_html'];
            $page->parentpage_id = ($input['parentpage_id'] == '') ? NULL:$input['parentpage_id'];
            $page->protected = (null !== Input::get('protected'));
            $page->permalink = $page->url;

            if($input['action'] == 'publish')
            {
                if($this->user->hasAccess('publish', get_class($this->page)))
                {
                    $page->status = 'public';
                    
                    if(!$page->protected)
                    {
                        $page->username = '';
                        $page->password = '';
                    }

                    $page->save();

                    /* Create new search record - change to be an event listener */
                    (new Searcher)->addToSearch($page);

                    /* Check if this is running */
                    if (!$page->save()) {
                        return Redirect::to(App::make('backend_url').'/pages/edit/'.$id)->withErrors($page->errors());
                    }
                }
            }

            /* Create version */
            if($version)
            {
                $page_version = PageVersion::where('id', '=', $version)->where('page_id', '=', $id)->first();
            }
            else
            {
                $page_version = new PageVersion;
            }

            $page_version = $page_version->fill(Input::all());
            $page_version['title'] = $page['title'];
            $page_version['hash'] = $page['url'].time();
            $page_version['page_id'] = $page->id;
            $page_version['url'] = $page['url'];
            $page_version['content'] = $page['content'];
            $page_version['permalink'] = $page['permalink'];
            $page_version['parentpage_id'] = $page->parentpage_id; //removed for preventing parent page overrides
            $page_version['user_id'] = Auth::user()->id;

            if(!$page->protected)
            {
                $page_version['username'] = '';
                $page_version['password'] = '';
            }

            $page_version->save();

            /* Store cats and tags */
            $input['taxonomy'] = (isset($input['taxonomy'])) ? $input['taxonomy']:array();
            $page->storeTaxonomy($input['taxonomy'], $page, get_class($page));

            /* Store meta */
            $this->setMetaData($id, $input);

            /* Store activity */
            Activitylog::log([
                'action' => 'UPDATED',
                'type' => get_class($page),
                'link_id' => $page->id,
                'description' => 'Updated page',
                'notes' => Auth::user()->username . " has updated &quot;$page->title&quot;"
            ]);

            if($input['action'] == 'preview')
            {
                Session::put('preview', true);
            }     
            
            if($input['action'] == 'publish')
            {
                return Redirect::to(App::make('backend_url').'/pages/edit/'.$id);
            }
            else
            {
                return Redirect::to(App::make('backend_url').'/pages/edit/'.$id.'/?version='.$page_version->id);   
            }
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised');
        }
    }
    
    public function getCommonData($id = 0)
    {
        $this->data['pages'] = Page::get_selection('Select a parent page', array($id));
        #$this->data['templates'] = Template::get_selection();
        $this->data['galleries'] = Gallery::get_selection();
        $this->data['blog_feed_styles'] = Page::get_blog_feed_styles();
        $this->data['statuses'] = Page::get_statuses();
        $this->data['blocks'] = array(); // todo: get from Plugins
        $this->data['responsive'] = Setting::getWhere('key', '=', 'responsive');
    }

    public function postAdd()
    {
        if($this->user->hasAccess('create', get_class($this->page)))
        {
            $input = Input::except('_token'); //remove filters
            $page = (new Page)->fill($input);
            $page['url'] = Str::slug($page['title']);
            $page['permalink'] = '';
            $page['parentpage_id'] = (!Input::get('parentpage_id')) ? 1:Input::get('parentpage_id');
            $page['user_id'] = Auth::user()->id;

            if(!$page->save()){
                return \Redirect::to(App::make('backend_url').'/pages')->withErrors($page->errors());
            }

            /* Create version */
            $page_version = (new PageVersion)->fill($input);
            $page_version['hash'] = $page['url'].time();
            $page_version['page_id'] = $page->id;
            $page_version['url'] = $page['url'];
            $page_version['permalink'] = $page['permalink'];
            $page_version['parentpage_id'] = Input::get('parentpage_id');
            $page_version['user_id'] = Auth::user()->id;
            $page_version->save();

            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($page),
                'link_id' => $page->id,
                'description' => 'Created page',
                'notes' => Auth::user()->username . " created a new page - &quot;$page->title&quot;"
            ]);

            return \Redirect::to(App::make('backend_url').'/pages/edit/'.$page->id);
        }
    }
    
    public function getDelete($id)
    {
        if($this->user->hasAccess('delete(oid)', get_class($this->page)))
        {
            // die();
            $page = Page::find($id);
            $page_title = (isset($page->title)) ? "&quot;$page->title&quot;":'';

            Activitylog::log([
                'action' => 'DELETED',
                'type' => get_class($page),
                'link_id' => null,
                'description' => 'Deleted page',
                'notes' => Auth::user()->username . " deleted the page " . $page_title
            ]);

            $page->status = 'trash';
            $page->save();

            /* Delete from the search page */
            (new Searcher)->deleteSearch($id, get_class($page));

            /* Redirect */
            return \Redirect::to(App::make('backend_url').'/pages')->with('success', 'Article has been moved to trash.');
        }
    }
    
    public function search()
    {
        $this->data['pages'] = parent::search_model('page');
        return View::make(App::make('backend_url').'/pages', $this->data);
    }
    
    public function getExport()
    {
        return parent::export_model('Page');
    }
    
    public function postExport()
    {
        return parent::run_export('Page', 'page_export.csv');        
    }


    /**
     *
     * 
     *
     *
     */  
    public function publish( $page_id, $value )
    {
        if($this->user->hasAccess('publish', get_class($this->page)))
        {
            $page_id = str_replace("page:", "", $page_id); //remove this so we can get the page number

            if(is_numeric($page_id) AND is_numeric($value))
            {
                $this->page = Page::whereNull('deleted_at')->where('id', '=', $page_id)->first();

                if(null !== $this->page)
                {
                    $current_publish_val = $this->page->isLive(); //true = 1, false = 0

                    if($value != $current_publish_val)
                    {
                        $this->page->status = ($value == 1) ? 'public':'draft';
                        $this->page->save();

                        /* Update searcher */
                        (new Searcher)->addToSearch($this->page);

                        /* Store activity */
                        $activity_text = ($value == 1) ? 'Page published':'Page unpublished';
                        $activity_desc = ($value == 1) ? " published &quot;".$this->page->title."&quot;":" unpublished &quot;".$this->page->title."&quot;";

                        Activitylog::log([
                            'action' => 'UPDATED',
                            'type' => get_class($this->page),
                            'link_id' => $this->page->id,
                            'description' => $activity_text,
                            'notes' => Auth::user()->username . " has $activity_desc"
                        ]);

                        Session::flash((($value == 1) ? 'success':'message'), "&quot;".$this->page->title."&quot; - $activity_text");
                        return Redirect::to(App::make('backend_url').'/pages/');
                    }
                    else
                    {
                        Session::flash('message', 'No changes could be made to ' . $this->page->title);
                        return Redirect::to(App::make('backend_url').'/pages/');
                    }
                }
                else
                {
                    Session::flash('error', 'Page could not be found.');
                    return Redirect::to(App::make('backend_url').'/pages/');
                }
            }
            else
            {
                Session::flash('error', 'There was a problem processing your request, please try again.');
                return Redirect::to(App::make('backend_url').'/pages/');
            }
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised'); //replace with CMS Error/Unauthorised controller
        }
    }

    /**
     *
     * 
     *
     *
     */  
    public function reorder( )
    {
        if($this->user->hasAccess('read', get_class($this->page)))
        {
            $this->data['tag'] = 'Re-ordering pages';
            $this->data['page_list'] = (new Page)->cms_list_pages();

            return View::make('HummingbirdBase::cms/pages-re-order', $this->data);
        }
        else
        {
            return View::make('HummingbirdBase::cms.errors.unauthorised'); //replace with CMS Error/Unauthorised controller
        }
    }


    public function import($action = 'install')
    {
        ini_set('memory_limit','1024M');

        /* data to insert */
        $insert_data = array();

        switch($action)
        {
            case 'upload':
                // DB::unprepared(file_get_contents(base_path() . '/import/sql/sections.sql'));
                // DB::unprepared(file_get_contents(base_path() . '/import/sql/pageindex.sql'));
                // DB::unprepared(file_get_contents(base_path() . '/import/sql/pagecontent_standard.sql'));
                break;
            case 'sections':
                /* Insert sections (although not sections) */
                $data = DB::table('old_sections')->orderBy('id', 'asc')->get();

                foreach($data as $migrate_page)
                {
                    $page = new Page;
                    $page->parentpage_id = NULL;
                    $page->title = $migrate_page->name;
                    $page->url = Str::slug($page->title);
                    $page->permalink = $page->url;
                    $page->show_in_nav = 1;
                    $page->searchable = 1;
                    $page->custom_menu_name = $page->title;
                    $page->status = ($migrate_page->live) ? 'public':'draft';

                    if($migrate_page->deleted)
                    {
                        $page->status = 'deleted';
                        $page->deleted_at = date("Y-m-d H:i:s");
                    }

                    $page->save();

                    /* Create a new version */
                    $page_version                   = new PageVersion;
                    $page_version['hash']           = $page->url.time();
                    $page_version['page_id']        = $page->id;
                    $page_version['title']          = $page->title;
                    $page_version['url']            = $page->url;
                    $page_version['permalink']      = $page->permalink;
                    $page_version['parentpage_id']  = $page->parentpage_id;
                    $page_version['status']         = $page->status;
                    $page_version['show_in_nav']    = 1;
                    $page_version['searchable']     = 1;
                    $page_version['custom_menu_name'] = $page->title;

                    if($migrate_page->deleted)
                    {
                        $page_version->status = 'deleted';
                        $page_version->deleted_at = date("Y-m-d H:i:s");
                    }

                    $page_version->save();
                }
                break;
            case 'pages':
                set_time_limit(0); // 300 = 5minutes, 600 = 10 minutes, 1200 = 20 minutes, 0 = infinite

                // /* import pagecontent also with old versions */
                $sections = DB::table('old_sections')->orderBy('id', 'asc')->get();
                //$section = DB::table('old_sections')->orderBy('id', 'asc')->first();

                foreach($sections as $section)
                {
                    $where = array(
                        'parent_id' => 0,
                        'sectionid' => $section->id
                    );

                    $pages = DB::table('old_pageindex')->where($where)->orderBy('id', 'asc')->get();   

                    if(count($pages) > 0)
                    {
                        //has pages
                        $this->importPages($pages, $where['sectionid'], $where['parent_id']);
                    }

                    $deleted_where = array(
                        'sectionid' => $section->id
                    );

                    // DB::table('old_pageindex')->where($deleted_where)->delete();

                }
                break;
            case 'content-updates':
                $pages_updated = DB::table('pages_backup')->whereIn('id', array('1','3','4','7','8','9','10','11','34'))->get();

                if(count($pages_updated) > 0)
                {
                    // General::pp($pages_updated, true);
                    foreach($pages_updated as $__page)
                    {
                        $page = Page::find($__page->id);

                        $page->content = $__page->content;
                        $page->save();
                    }
                }

                General::pp('COMPLETED');

                break;
            case 'downloads':

                $pages = Page::whereNull('deleted_at')->where('id', '>', 34)->orderBy('id', 'ASC')->get();

                if(null !== $pages)
                {
                    foreach($pages as $page)
                    {
                        /* Get old page id */
                        $old_page = DB::table('pages_xref')->where('new_id', '=', $page->id)->first();

                        if(null !== $old_page)
                        {
                            $old_page_id    = $old_page->old_id;

                            /* 
                                Do something with the documents attached 
                                
                                NOT DOING THIS NOW AS IT HAS EMBEDS - NOT SURE WHAT TO DO WITH THESE! Possibly can just be uploaded to the page.

                            */
                            $page_documents = DB::table('old_downloads_xref')
                                ->join('old_downloads', 'old_downloads_xref.dl_id', '=', 'old_downloads.id')
                                ->where('old_downloads_xref.page_id', '=', $old_page_id)
                                ->where('old_downloads_xref.type', '=', 'pages')
                                ->select('old_downloads_xref.order_id as old_d_x_order_id', 'old_downloads_xref.deleted as old_d_x_deleted', 'old_downloads.*')
                                ->orderBy('old_d_x_order_id', 'ASC')
                                ->get();

                            if(count($page_documents) > 0)
                            {
                                $collection     = DB::table('media-collections')->where('name', '=', $page->title . $page->id)->whereNull('deleted_at')->first();
                                
                                if(null === $collection)
                                {
                                    /* Create new media collection inside migration */
                                    $collection = array();
                                    $collection['name'] = $page->title . $page->id;
                                    $collection['parent_collection'] = 2; //Events collection
                                    $collection['permalink'] = Str::slug($collection['name']);

                                    // /* insert and get id for upload */
                                    $collection_id = DB::table('media-collections')->insertGetId($collection);

                                    Activitylog::log([
                                        'action' => 'CREATED',
                                        'type' => get_class(new Media),
                                        'link_id' => $collection_id,
                                        'url' => General::backend_url() . "/media/view/$collection_id",
                                        'description' => 'Created collection',
                                        'notes' => "System created a new collection &quot;".$collection['name']."&quot;"
                                    ]);
                                }
                                else
                                {
                                    $collection_id = $collection->id;
                                }


                                $insert_page_docs = array();

                                foreach($page_documents as $page_document)
                                {
                                    if($page_document->filename != '')
                                    {
                                        //filename = 1328109287.jpg - need to be absolute
                                        $orig_file = '/uploads/documents/' . $page_document->filename;

                                        /* find in database */
                                        $resource = DB::table('media_xref')->where("source", '=', trim($orig_file))->first();

                                        if(null !== $resource)
                                        {
                                            $source = $resource->destination;
                                        }
                                        else
                                        {
                                            /* process images */
                                            $base_file = base_path() . '/import/uploads/documents/' . $page_document->filename;

                                            $source = App::make('MediaController')->uploadFile_byFunction($base_file, $collection_id);

                                            if($source !== false)
                                            {
                                                /* Add to media_xref */
                                                DB::table('media_xref')->insert(array
                                                    (
                                                        'source' => $orig_file,
                                                        'destination' => $source
                                                    )
                                                );
                                            }
                                        }

                                        if($source !== false)
                                        {
                                            if($page_document->deleted == 0 AND $page_document->old_d_x_deleted == 0)
                                            {
                                                $insert_page_docs[] = array(
                                                    'item_id' => $page->id,
                                                    'document' => $source
                                                );
                                            }
                                        }
                                    }
                                }

                                if(count($insert_page_docs) > 0)
                                {
                                    DB::table('pages_docs')->insert($insert_page_docs);
                                }
                            }
                        }
                    }
                }

                break;
            case 'process-images':
                set_time_limit(0);
                /* Process images - /userfiles/images/      */ 

                $pages = Page::whereRaw('content REGEXP \'src="/userfiles/images\'')->get();
                // $pages = Page::where('id', '=', 303)->get();
                // $pages = Page::find(654);

                // DatabaseHelper::getDatabaseQueries();
                // die();

                if(count($pages) > 0)
                {
                    foreach($pages as $page)
                    {
                        $collection     = DB::table('media-collections')->where('name', '=', $page->title . $page->id)->whereNull('deleted_at')->first();
                        
                        if(null === $collection)
                        {
                            /* Create new media collection inside migration */
                            $collection = array();
                            $collection['name'] = $page->title . $page->id;
                            $collection['parent_collection'] = 2; //Events collection
                            $collection['permalink'] = Str::slug($collection['name']);

                            // /* insert and get id for upload */
                            $collection_id = DB::table('media-collections')->insertGetId($collection);

                            Activitylog::log([
                                'action' => 'CREATED',
                                'type' => get_class(new Media),
                                'link_id' => $collection_id,
                                'url' => General::backend_url() . "/media/view/$collection_id",
                                'description' => 'Created collection',
                                'notes' => "System created a new collection &quot;".$collection['name']."&quot;"
                            ]);
                        }
                        else
                        {
                            $collection_id = $collection->id;
                        }

                        /* Load the html and get images */
                        $doc = new DOMDocument();
                        @$doc->loadHTML($page->content);

                        $tags = $doc->getElementsByTagName('img');

                        foreach ($tags as $tag)
                        {
                            $src = $tag->getAttribute('src');

                            if(strpos($src, 'userfiles') !== false)
                            {
                                $new_src = General::clean_unicode($src);

                                General::pp("<br />--START--");

                                General::pp("OLD: ".$src);
                                General::pp("NEW: ".$new_src);

                                /* find in database */
                                $resource = DB::table('media_xref')->where("source", '=', trim($new_src))->first();

                                DatabaseHelper::getDatabaseQueries();

                                if(null !== $resource)
                                {
                                    $page->content = str_replace($src, $resource->destination, $page->content);
                                    $page->content = str_replace($new_src, $resource->destination, $page->content);
                                }
                                else
                                {
                                    /* process images */
                                    $base_image = base_path() . '/import' . $new_src;

                                    General::pp($base_image);
                                    
                                    $resource = App::make('MediaController')->uploadFile_byFunction($base_image, $collection_id);

                                    if($resource !== false)
                                    {
                                        $page->content = str_replace($src, $resource, $page->content);

                                        /* Add to media_xref */
                                        DB::table('media_xref')->insert(array
                                            (
                                                'source' => $new_src,
                                                'destination' => $resource
                                            )
                                        );
                                    }
                                }

                                General::pp("--END--<br />");
                            }
                        }

                        $page->save();
                    }
                }

                General::pp('COMPLETED');


                /* Needs to be done in both pages and page versions */
                break;
            case 'process-images-versions':
                set_time_limit(0);
                $images = array();

                /* Process images - /userfiles/images/      */ 

                $pages = PageVersion::whereRaw('content REGEXP \'src="/userfiles\'')->get();
                // $pages = PageVersion::whereRaw('content REGEXP \'src="/userfiles\'')->where('page_id', '=', 652)->get();
 

// | page_id |
// +---------+
// |     652 |
// |     654 |
// |     609 |
// |     606 |
// |     526 |
// |     224 |
// |     158 |
// |     123 |
// |     120 |
// |      44 |
// |      42 |
// |     634 |
// |     456 |
// |     455 |
// |     179 |
// |     140 |
// |     129 |

                if(count($pages) > 0)
                {
                    foreach($pages as $page)
                    {
                        $collection     = DB::table('media-collections')->where('name', '=', $page->title . $page->page_id)->whereNull('deleted_at')->first();
                        
                        if(null === $collection)
                        {
                            /* Create new media collection inside migration */
                            $collection = array();
                            $collection['name'] = $page->title . $page->page_id;
                            $collection['parent_collection'] = 2; //Events collection
                            $collection['permalink'] = Str::slug($collection['name']);

                            // /* insert and get id for upload */
                            $collection_id = DB::table('media-collections')->insertGetId($collection);

                            Activitylog::log([
                                'action' => 'CREATED',
                                'type' => get_class(new Media),
                                'link_id' => $collection_id,
                                'url' => General::backend_url() . "/media/view/$collection_id",
                                'description' => 'Created collection',
                                'notes' => "System created a new collection &quot;".$collection['name']."&quot;"
                            ]);
                        }
                        else
                        {
                            $collection_id = $collection->id;
                        }

                        /* Load the html and get images */
                        $doc = new DOMDocument();
                        @$doc->loadHTML($page->content);

                        $tags = $doc->getElementsByTagName('img');

                        foreach ($tags as $tag)
                        {
                            $src = $tag->getAttribute('src');

                            if(strpos($src, 'userfiles') !== false)
                            {
                                if(!in_array($src, $images))
                                {
                                    $images[] = $src;
                                }
                                
                                $new_src = General::clean_unicode($src);

                                // General::pp("<br />--START--");

                                // General::pp("OLD: ".$src);
                                // General::pp("NEW: ".$new_src);

                                /* find in database */
                                $resource = DB::table('media_xref')->where("source", '=', trim($src))->first();

                                // DatabaseHelper::getDatabaseQueries();

                                if(null !== $resource)
                                {
                                    $page->content = str_replace($src, $resource->destination, $page->content);
                                    $page->content = str_replace($new_src, $resource->destination, $page->content);
                                }
                                else
                                {
                                    $resource = DB::table('media_xref')->where("source", '=', trim($new_src))->first();

                                    if(null !== $resource)
                                    {
                                        $page->content = str_replace($src, $resource->destination, $page->content);
                                        $page->content = str_replace($new_src, $resource->destination, $page->content);
                                    }
                                    else
                                    {
                                        /* process images */
                                        $base_image = base_path() . '/import' . $new_src;

                                        // General::pp($base_image);
                                        
                                        $resource = App::make('MediaController')->uploadFile_byFunction($base_image, $collection_id);

                                        if($resource !== false)
                                        {
                                            $page->content = str_replace($src, $resource, $page->content);

                                            /* Add to media_xref */
                                            DB::table('media_xref')->insert(array
                                                (
                                                    'source' => $new_src,
                                                    'destination' => $resource
                                                )
                                            );
                                        }
                                    }
                                }

                                // General::pp("--END--<br />");
                            }
                        }

                        $page->save();
                    }
                }

                General::pp($images);

                General::pp('COMPLETED');


                /* Needs to be done in both pages and page versions */
                break;
            case 'process-other':
                set_time_limit(0);
                /* Process documents - /uploads/documents/     and      /userfiles/files/      */ 
                $pages = Page::whereRaw('(content REGEXP \'href="/uploads\' OR content REGEXP \'href="/userfiles\') ORDER BY `id` asc')->get();

                if(count($pages) > 0)
                {
                    foreach($pages as $page)
                    {
                        $collection     = DB::table('media-collections')->where('name', '=', $page->title . $page->page_id)->whereNull('deleted_at')->first();
                        
                        if(null === $collection)
                        {
                            /* Create new media collection inside migration */
                            $collection = array();
                            $collection['name'] = $page->title . $page->page_id;
                            $collection['parent_collection'] = 2; //Events collection
                            $collection['permalink'] = Str::slug($collection['name']);

                            // /* insert and get id for upload */
                            $collection_id = DB::table('media-collections')->insertGetId($collection);

                            Activitylog::log([
                                'action' => 'CREATED',
                                'type' => get_class(new Media),
                                'link_id' => $collection_id,
                                'url' => General::backend_url() . "/media/view/$collection_id",
                                'description' => 'Created collection',
                                'notes' => "System created a new collection &quot;".$collection['name']."&quot;"
                            ]);
                        }
                        else
                        {
                            $collection_id = $collection->id;
                        }

                        /* Load the html and get images */
                        $doc = new DOMDocument();
                        @$doc->loadHTML($page->content);

                        $links = $doc->getElementsByTagName('a');

                        if(count($links) > 0)
                        {
                            $updated = false;

                            foreach ($links as $link)
                            {
                                $src = $update_src = $link->getAttribute('href');

                                if(strpos($src, '/uploads/documents/2015') === FALSE)
                                {
                                    if(strpos($src, '/uploads/images/') === FALSE)
                                    {
                                        if(strpos($src, 'http://') !== false)
                                        {
                                            $url = parse_url($src);

                                            $src = $url['path'];
                                        }

                                        if(strpos($src, '/uploads/') !== false OR strpos($src, '/userfiles/') !== false)
                                        {
                                            /* find in database */
                                            $resource = DB::table('media_xref')->where("source", '=', trim($src))->first();

                                            if(null !== $resource)
                                            {
                                                $updated = true;
                                                $page->content = str_replace($update_src, $resource->destination, $page->content);
                                            }
                                            else
                                            {
                                                /* process images */
                                                $base_image = base_path() . '/import' . $src;
                                                
                                                $resource = App::make('MediaController')->uploadFile_byFunction($base_image, $collection_id);

                                                if($resource !== false)
                                                {
                                                    $updated = true;
                                                    $page->content = str_replace($update_src, $resource, $page->content);

                                                    /* Add to media_xref */
                                                    DB::table('media_xref')->insert(array
                                                        (
                                                            'source' => $src,
                                                            'destination' => $resource
                                                        )
                                                    );
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if($updated) $page->save(); //only if we moved something
                        }
                    }
                }

                General::pp('COMPLETED');

                /* Needs to be done in both pages and page versions */
                break;
            case 'process-other-versions':
                set_time_limit(0);
                /* Process documents - /uploads/documents/     and      /userfiles/files/      */ 
                /* Process documents - /uploads/documents/     and      /userfiles/files/      */ 
                $pages = PageVersion::whereRaw('(content REGEXP \'href="/uploads\' OR content REGEXP \'href="/userfiles\') ORDER BY `id` asc')->get();

                if(count($pages) > 0)
                {
                    foreach($pages as $page)
                    {
                        $collection     = DB::table('media-collections')->where('name', '=', $page->title . $page->page_id)->whereNull('deleted_at')->first();
                        
                        if(null === $collection)
                        {
                            /* Create new media collection inside migration */
                            $collection = array();
                            $collection['name'] = $page->title . $page->page_id;
                            $collection['parent_collection'] = 2; //Events collection
                            $collection['permalink'] = Str::slug($collection['name']);

                            // /* insert and get id for upload */
                            $collection_id = DB::table('media-collections')->insertGetId($collection);

                            Activitylog::log([
                                'action' => 'CREATED',
                                'type' => get_class(new Media),
                                'link_id' => $collection_id,
                                'url' => General::backend_url() . "/media/view/$collection_id",
                                'description' => 'Created collection',
                                'notes' => "System created a new collection &quot;".$collection['name']."&quot;"
                            ]);
                        }
                        else
                        {
                            $collection_id = $collection->id;
                        }

                        /* Load the html and get images */
                        $doc = new DOMDocument();
                        @$doc->loadHTML($page->content);

                        $links = $doc->getElementsByTagName('a');

                        if(count($links) > 0)
                        {
                            $updated = false;

                            foreach ($links as $link)
                            {
                                $src = $update_src = $link->getAttribute('href');

                                if(strpos($src, '/uploads/documents/2015') === FALSE)
                                {
                                    if(strpos($src, '/uploads/images/') === FALSE)
                                    {
                                        if(strpos($src, 'http://') !== false)
                                        {
                                            $url = parse_url($src);

                                            $src = $url['path'];
                                        }

                                        if(strpos($src, '/uploads/') !== false OR strpos($src, '/userfiles/') !== false)
                                        {
                                            /* find in database */
                                            $resource = DB::table('media_xref')->where("source", '=', trim($src))->first();

                                            if(null !== $resource)
                                            {
                                                $updated = true;
                                                $page->content = str_replace($update_src, $resource->destination, $page->content);
                                            }
                                            else
                                            {
                                                /* process images */
                                                $base_image = base_path() . '/import' . $src;
                                                
                                                $resource = App::make('MediaController')->uploadFile_byFunction($base_image, $collection_id);

                                                if($resource !== false)
                                                {
                                                    $updated = true;
                                                    $page->content = str_replace($update_src, $resource, $page->content);

                                                    /* Add to media_xref */
                                                    DB::table('media_xref')->insert(array
                                                        (
                                                            'source' => $src,
                                                            'destination' => $resource
                                                        )
                                                    );
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if($updated) $page->save(); //only if we moved something
                        }
                    }
                }

                General::pp('COMPLETED');

                /* Needs to be done in both pages and page versions */
                break;
            case 'finished':
                // Schema::dropIfExists('old_sections');
                // Schema::dropIfExists('old_pageindex');
                // Schema::dropIfExists('old_pagecontent_standard');
                break;
            case 'taxonomy':
                /* get all events */
                $page_class = get_class(new Page);

                /* Get all old pages */
                $pages = DB::table("old_pageindex")->orderBy('id', 'ASC')->get();

                foreach($pages as $page)
                {
                    /* Get new page id - HB3 page id */
                    $new_page = DB::table('pages_xref')->where('old_id', '=', $page->id)->first();

                    if(null !== $new_page)
                    {
                        $new_page_id    = $new_page->new_id;

                        $tags = explode(",", $page->tags); //get tags

                        foreach($tags as $tag)
                        {
                            $tag = trim($tag);

                            if($tag != '')
                            {
                                $stored_tag = Tags::where('name', '=', $tag)->where('type', '=', 'tag')->first();

                                if(count($stored_tag) == 0)
                                {
                                    $new_tag = new Tags;
                                    $new_tag->name = $tag;
                                    $new_tag->slug = Str::slug($tag);
                                    $new_tag->type = 'tag';
                                    $new_tag->parent = null;
                                    $new_tag->save();
                                }
                                else
                                {
                                    $id = $stored_tag->id;  
                                }

                                if(count($stored_tag) == 0) $id = $new_tag->id;

                                $insert_data[] = array('term_id' => $id, 'tax_type' => $page_class, 'item_id' => $new_page_id);
                            }
                        }
                    }
                }

                if(count($insert_data) > 0)
                {
                    DB::table('taxonomy_relationships')->insert($insert_data);
                }
                break;
        }  
    }

    public function importPages($pages, $section_id = 0, $parent_id = 0, $new_parent_id = null)
    {

        $page_template = '<div id="hb-template-wrapper">
            <div class="row hb-item clearfix row-wrapper hb_row">
                <div class="col-md-9 hb-item column hb_column" id="">
                    <div class="item redactor-wrapper relative">[content]</div>
                </div>
                <div class="col-md-3 hb-item column hb_column" id="">
                    [modules]
                </div>
            </div>
        </div>';

        $page_module_template = '<div class="item redactor-wrapper relative">[module-id]</div>';

        if(count($pages) > 0)
        {
            foreach($pages as $migrate_page)
            {
                $page       = NULL;
                $page_id    = NULL;
                $pageobj    = NULL; //page object

                /* Get old page versions */
                $page_versions = DB::table('old_pagecontent_standard')->where('pageid', '=', $migrate_page->id)->orderBy('id', 'asc')->get(); //page versions
                // $page_versions = DB::table('old_pagecontent_standard')->where('name', '!=', '')->where('pageid', '=', $migrate_page->id)->orderBy('id', 'asc')->take(1)->get(); //page versions

                /* iterate over page versions */
                foreach($page_versions as $paged)
                {
                    if($paged->name != '')
                    {
                        /* new page object as we dont need more than 1 */
                        if(!$pageobj)
                        {
                            $page = new Page;   
                        }
                        
                        $page->parentpage_id = (null !== $new_parent_id) ? $new_parent_id:$section_id + 1; //set section or parent page id

                        /* Update page details */
                        $page->title = $paged->name;
                        $page->url = $migrate_page->url;
                        $page->permalink = $page->url;
                        $page->status = ($paged->published) ? 'public':'draft'; //page version is published
                        $page->content = $paged->body;
                        $page->user_id = $paged->user_id; //user who created this
                        $page->show_in_nav = 1;
                        $page->searchable = 1;
                        $page->custom_menu_name = $paged->name;
                        $page->deleted_at = ($migrate_page->deleted) ? date("Y-m-d H:i:s"):NULL;

                        /* get current page conten */
                        $page_content = '<h1>' . $page->title . '</h1>' . $page->content; //update page

                        /* Set up new page content */
                        $__new_page_content = $page_template;
                        $__new_page_content = str_replace('[content]', $page_content, $__new_page_content); //get page template

                        /* store modules - in side bar */
                        $module_content = ''; //blank to start with
                        $__page_modules = ($migrate_page->sidemods != '') ? unserialize($migrate_page->sidemods):array();

                        if(is_array($__page_modules))
                        {
                            if(count($__page_modules) > 0)
                            {
                                foreach($__page_modules as $module)
                                {
                                    $__page_module = Module::whereNull('deleted_at')->where('id', '=', $module)->first();

                                    if(null !== $__page_module)
                                    {
                                        // module has not been deleted or does not exist
                                        $module_content .= str_replace("[module-id]", "[HB::MODULE(" . $__page_module->id . ")]", $page_module_template);
                                    }
                                }
                            }
                        }

                        $__new_page_content = str_replace('[modules]', $module_content, $__new_page_content); //get page template

                        /* Store content */
                        $page->content = $__new_page_content; //store this

                        if(!$pageobj)
                        {
                            $pageobj = TRUE;
                            $page->save();

                            $page_id = $page->id;
                        }
                        else
                        {
                            if($paged->published == 1)
                            {
                                $page->save();

                                /* Create new search record - change to be an event listener */
                                // (new Searcher)->addToSearch($page);
                            }
                        }

                        /* Reference */
                        // DB::table('pages_xref')->insert(
                        //     array(
                        //         'old_id' => $migrate_page->id,
                        //         'new_id' => $page_id
                        //     )
                        // );


                        /* Create a new version */
                        $page_version = new PageVersion;
                        $page_version->hash           = $page->url.time();
                        $page_version->page_id        = $page_id;
                        $page_version->title          = $page->title;
                        $page_version->url            = $page->url;
                        $page_version->permalink      = $page->permalink;
                        $page_version->parentpage_id  = $page->parentpage_id;
                        $page_version->status         = $page->status;
                        $page_version->show_in_nav    = 1;
                        $page_version->searchable     = 1;
                        $page_version->custom_menu_name = $page->title;
                        $page_version->content        = $page->content;
                        $page_version->deleted_at     = ($migrate_page->deleted) ? date("Y-m-d H:i:s"):NULL;
                        $page_version->save();
                    }
                } 

                /* Get children */
                $where = array(
                    'parent_id' => $migrate_page->id,
                    'sectionid' => $section_id
                );

                $children_pages = DB::table('old_pageindex')->where($where)->orderBy('id', 'asc')->get();  

                if(count($children_pages) > 0)
                {
                    /* Import children */
                    $this->importPages($children_pages, $where['sectionid'], $where['parent_id'], $page_id);
                }
            }
        }
    }

    public function getMetaData($id, $version_id = null)
    {
        $content = array();

        $metasets = DB::table('meta_content')->where('item_id', '=', $id)->where('item_type', '=', get_class(new Page))->get();

        if(count($metasets) > 0)
        {
            foreach($metasets as $meta)
            {
                $content[$meta->key] = $meta->value;
            }
        }

        return $content;
    }

    public function setMetaData($id, $data = array(), $version_id = null)
    {
        if(!empty($data))
        {
            $metasets = DB::table('meta_content')->where('item_id', '=', $id)->where('item_type', '=', get_class(new Page))->delete();

            $insert_array = array();

            $insert_array[] = array('item_id' => $id, 'item_type' => get_class(new Page), 'key' => 'meta_title', 'value' => $data['meta_title']);
            $insert_array[] = array('item_id' => $id, 'item_type' => get_class(new Page), 'key' => 'meta_keywords', 'value' => $data['meta_keywords']);
            $insert_array[] = array('item_id' => $id, 'item_type' => get_class(new Page), 'key' => 'meta_description', 'value' => $data['meta_description']);

            $metasets = DB::table('meta_content')->insert($insert_array);
        }
    }

    /**
     *
     *
     * @param  
     * @return 
     */
    public function cms_search($ajax = false)
    {
        if(Input::get('ajax')) $ajax = true;

        $words = preg_replace('/[^a-zA-Z0-9\s]/', '', trim(Input::get('s')));
        $words = explode(" ", $words);

        $to_search = array();

        foreach($words as $word)
        {
            // $word = trim($word);
            // $to_search[] = Porter::Stem($word);
            $to_search[] = trim($word);
        }

        $to_search_words = trim(implode(" ", $to_search));

        $results = (new Page)->select(DB::raw(getenv('DB_PREFIX') . "pages.*, 
            (
                (
                    1 * (
                            MATCH(title) AGAINST(?)
                        )
                )
                +
                (
                    1 * (
                            MATCH(url) AGAINST(?)
                        )
                )
                +
                (
                    1 * (
                            MATCH(content) AGAINST(?)
                        )
                )
            ) AS `score`"))
        // $search will NOT be bind here
        // it will be bind when calling setBindings
        ->whereRaw("MATCH(title, url, content) AGAINST(?)")
        // I want to keep only published quotes
        // ->where('approved', '=', 1)
        // Order by the rank column we got with our FULLTEXT index
        ->orWhere(function($query) use ($words)
        {
            foreach($words as $word)
            {
                $query->orWhereRaw("(title LIKE '%%$word%%' OR url LIKE '%%$word%%' OR content LIKE '%%$word%%')");
            }
        })
        ->havingRaw('score > 0')
        ->whereNull('deleted_at')
        ->orderBy('score', 'DESC')
        // Bind variables here
        // We really need to bind ALL variables here
        // question marks will be replaced in the query
        ->setBindings([$to_search_words, $to_search_words, $to_search_words, $to_search_words])
        ->get();


        if(count($results) == 0)
        {
            /* Do a standard search */

// SELECT p.*,
//        ((name LIKE '%samsung%') + (name LIKE '%galaxy%') + (name LIKE '%s4%')) as hits
// FROM   myprods p
// WHERE  name LIKE '%samsung%' OR name LIKE '%galaxy%' OR name LIKE '%s4%';

            $custom_select = getenv('DB_PREFIX') . 'pages.*, (';
            
            foreach($words as $word)
            {
                $custom_select .= "(title LIKE '%".$word."%') + (url LIKE '%".$word."%') + (content LIKE '%".$word."%') +";
            }

            $custom_select = substr($custom_select, 0, -1);

            $custom_select .= ') AS `score`';

            $results = (new Page)->select(DB::raw($custom_select))
            ->orWhere(function($query) use ($words)
            {
                foreach($words as $word)
                {
                    $query->orWhereRaw("(title LIKE '%%$word%%' OR url LIKE '%%$word%%' OR content LIKE '%%$word%%')");
                }
            })
            ->havingRaw('score > 0')
            ->orderBy('score', 'DESC')
            ->get();
        }

        // DatabaseHelper::getDatabaseQueries();die();

        return $results;
    }
}
