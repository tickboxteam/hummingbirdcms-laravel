<?php

class ActivitylogController extends CmsbaseController
{
    /**
     * Inject the models.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getIndex()
    {
        if($this->user->hasAccess('read', get_class(new Activitylog)))
        {
            $this->data['tag'] = 'Manage Activity Logs';

            $user_id = (null !== Input::get('user')) ? Input::get('user'):null;

            $this->data['activitylogs'] = Activitylog::byuser($user_id)->orderBy('id', 'DESC')->get();
            return View::make('HummingbirdBase::cms/activitylogs', $this->data);
        }
        else
        {
            return $this->forbidden(true);
        }
    }

    public function export()
    {
        if($this->user->hasAccess('export', get_class(new Activitylog)))
        {}
        else
        {
            return $this->forbidden(true);
        }
    }

    public function destroy()
    {
        if($this->user->hasAccess('delete', get_class(new Activitylog)))
        {}
        else
        {
            return $this->forbidden(true);
        }
    }
}
