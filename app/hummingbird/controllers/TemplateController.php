<?php

class TemplateController extends CmsbaseController {
    
    public $code_location = 'pages';
    
    public function getIndex()
    {
        $this->data['tag'] = 'Manage Templates';
        $this->data['templates'] = Template::all();
        return View::make('HummingbirdBase::cms/templates', $this->data);
    }
    
    public function getEdit($id)
    {
        $this->data['template'] = Template::find($id);
        $this->data['tag'] = 'Edit '.$this->data['template']->title;
        $this->getCommonData();
        return View::make('HummingbirdBase::cms/templates-edit', $this->data);
    }
    
    public function postEdit($id)
    {
        $input = Input::all();
        $template = Template::find($id);
        $template->update($input);
        
        return Redirect::to(App::make('backend_url').'/templates/edit/'.$id);
    }
    
    public function getCommonData()
    {
        $this->data['templates'] = Template::get_selection();
        $this->data['types'] = Template::get_types();
        $this->data['layouts'] = Template::get_layouts();
    }
    
    public function postAdd()
    {
        $input = Input::except('_token', 'add');        
        $template = (new Template)->fill($input);
        if(!$template->save()) {
            return Redirect::to(App::make('backend_url').'/templates')->withErrors($template->errors());
        }
        return Redirect::to(App::make('backend_url').'/templates/edit/'.$template->id);
    }
    
    public function getDelete($id)
    {
        Template::destroy($id);
        return Redirect::to(App::make('backend_url').'/templates');
    }
}
