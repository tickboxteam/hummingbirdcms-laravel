<?php

class UserController extends CmsbaseController
{
    public $preview_user = false;    
    public $code_location = 'users';
    protected $roles = array();

    public function __construct()
    {
        parent::__construct();
        
        if(null !== Session::get('view_user_perms'))
        {
            $this->preview_user = $this->setPreviewUser(Session::get('view_user_perms'));
        }

        $this->roles = App::make('RoleController')->getIndex_call();

        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => 'fa fa-users',
            'title' => 'User',
            'url' => Request::url()
        ); //breadcrumb manager
    }

    public function getIndex()
    {
        if($this->user->hasAccess('read', get_class(new User)))
        {
            $this->data['tag'] = 'Manage Users';

            $this->data['users'] = User::where('id', '!=', Auth::id())
                ->where('hidden', '=', '0')
                ->whereNull('deleted_at');

            if(!$this->user->isSuperUser())
            {
                $this->data['users'] = $this->data['users']->whereIn('role_id', $this->roles);
            }

            $this->data['users'] = $this->data['users']->get();

            $this->data['roles'] = $this->roles;

            return View::make('HummingbirdBase::cms/users', $this->data);
        }
        else
        {
            return $this->forbidden(true);
        }
    }
    
    public function getEdit($id)
    {
        if($this->user->hasAccess('update', get_class(new User)))
        {
            if($this->user->id == (int) $id)
            {
                // looking at yourself?
                return Redirect::to(App::make('backend_url').'/profile/');
            }
            else
            {
                // if($this->user->canEdit($id))
                $this->data['user'] = User::where('id', '=', $id)->whereNull('deleted_at');

                if(!$this->user->isSuperUser())
                {
                    $this->data['user'] = $this->data['user']->whereIn('role_id', $this->roles);
                }

                $this->data['user'] = $this->data['user']->first();

                if(null !== $this->data['user'])
                {
                    $this->data['tag'] = 'Edit '.$this->data['user']->username;
                    $this->data['help'] = 'We are going to be editing this user. Simply fill in their details';
                    $this->data['roles'] = $this->roles;

                    $this->getCommonData($id);

                    // $this->data['breadcrumbs'][] = array(
                    //     'classes' => '',
                    //     'icon' => 'fa fa-users',
                    //     'title' => $this->data['user']->name,
                    //     'url' => Request::url()
                    // ); //breadcrumb manager

                    return View::make('HummingbirdBase::cms/users-edit', $this->data);
                }
                else 
                {
                    //need to send errors
                    return Redirect::to(App::make('backend_url').'/users/');
                }
            }
        }
        else
        {
            if($this->user->hasAccess('read', get_class(new User)))
            {
                return Redirect::to(App::make('backend_url').'/users/')->with('errors', 'You do not have permission to edit.');
            }

            return $this->forbidden(true);
        }
    }
    
    private function getCommonData($user_id)
    {
        $this->data['permissions'] = Permission::getAllPermissions(User::find($user_id)->getRole()->parentrole_id);
        $this->data['active_perms'] = Permission::getUserPermissions($user_id);
        $this->data['statuses'] = User::get_statuses();
    }
    
    public function postEdit($id)
    {
        if($this->user->hasAccess('update', get_class(new User)))
        {
            $input = Input::except('perms');
            $user = User::find($id);

            $old_role = $user->role;
            
            if($input['password'] == '' AND $input['password_confirmation'] == '') 
            {
                unset($input['password']); // ensure blank passwords aren't stored
                unset($input['password_confirmation']); // ensure blank passwords aren't stored
            }

            $update_user = User::getRules();

            if(!isset($input['password'])) 
            {
                unset($update_user['password']);
                unset($update_user['password_confirmation']);
            }

            $user->fill($input);

            if(!$user->updateSave($update_user)) 
            {
                return Redirect::to(General::backend_url().'/users/edit/'.$id)->withErrors($user->errors());
            }
            
            // update perms depending on whether role has changed
            if($input['role_id'] == $old_role->id) {
                // role is the same, set perms as given
                $post_perms = Input::get('perms');
                $activate_perms = Input::get('activate');

                /* Now update new permissions */
                Permission::overrideUserPermissions($id, $activate_perms, $post_perms);
            } else {
                // role has changed, set perms as new role
                $role_perms = Permission::getRolePermissions($user->role_id);
                Permission::updateUserPermissions($user->id, $role_perms);
            }


            Activitylog::log([
                'action' => 'UPDATED',
                'type' => get_class($user),
                'link_id' => $user->id,
                'description' => 'Updated user',
                'notes' => Auth::user()->username . ' updated ' . $user->name . '\'s account'
            ]);

            return Redirect::to(General::backend_url().'/users/edit/'.$id);
        }
        else
        {
            if($this->user->hasAccess('read', get_class(new User)))
            {
                return Redirect::to(App::make('backend_url').'/users/')->with('errors', 'You do not have permission to edit.');
            }

            return $this->forbidden(true);
        }
    }
    
    public function postAdd()
    {
        if($this->user->hasAccess('create', get_class(new User)))
        {
            $input = Input::except('_token', 'add');
            
            $user = (new User)->fill($input);
            
            if(!$user->updateSave(User::getRules('create'))) {
                return Redirect::to(General::backend_url().'/users')->withErrors($user->errors());
            }

            // Store permissions
            Permission::updateUserPermissions($user->id, Permission::getRolePermissions($user->role_id));
            
            Activitylog::log([
                'action' => 'CREATED',
                'type' => get_class($user),
                'link_id' => $user->id,
                'description' => 'Created user',
                'notes' => Auth::user()->username . ' created an account for ' . $user->name
            ]);
            
            return Redirect::to(General::backend_url().'/users/edit/'.$user->id);
        }
        else
        {
            if($this->user->hasAccess('read', get_class(new User)))
            {
                return Redirect::to(App::make('backend_url').'/users/')->with('errors', 'You do not have permission to edit.');
            }

            return $this->forbidden(true);
        }
    }
    
    public function getDelete($id)
    {
        if($this->user->hasAccess('delete', get_class(new User)))
        {
            if(Auth::user()->id != $id)
            {
                $user = User::find($id);

                Activitylog::log([
                    'action' => 'DELETED',
                    'type' => get_class($user),
                    'link_id' => $user->id,
                    'description' => 'Deleted user',
                    'notes' => Auth::user()->username . ' deleted ' . $user->name
                ]);

                $user->delete();

                return Redirect::to(General::backend_url().'/users')->with('success', 'User has been deleted.');      
            }
            else
            {
                return Redirect::to(App::make('backend_url').'/users/')->with('errors', 'You can not delete yourself.');
            }
        }
        else
        {
            if($this->user->hasAccess('read', get_class(new User)))
            {
                return Redirect::to(App::make('backend_url').'/users/')->with('errors', 'You do not have permission to delete.');
            }

            return $this->forbidden(true);
        }
    }

    public function profile()
    {
        if(null !== $this->user)
        {
            if($this->user->previewMode())
            {
                // in user preview mode
                return Redirect::to(App::make('backend_url') . '/users/preview/?action=remove');
            }
            else if($this->user->role->previewMode())
            {
                //in role preview mode
                return Redirect::to(App::make('backend_url') . '/roles/preview/?action=remove');
            }

            if (Request::isMethod('post'))
            {
                $input = Input::except('_token', 'add');

                if($input['password'] == '' AND $input['password_confirmation'] == '') 
                {
                    unset($input['password']); // ensure blank passwords aren't stored
                    unset($input['password_confirmation']); // ensure blank passwords aren't stored
                }

                $update_user = User::getRules();

                if(!isset($input['password'])) 
                {
                    unset($update_user['password']);
                    unset($update_user['password_confirmation']);
                }

                $this->user->fill($input);

                if(!$this->user->updateSave($update_user)) 
                {
                    return Redirect::to(General::backend_url().'/profile')->withErrors($this->user->errors());
                }
                
                Activitylog::log([
                    'action' => 'UPDATED',
                    'type' => get_class($this->user),
                    'link_id' => $this->user->id,
                    'description' => 'Updated profile',
                    'notes' => Auth::user()->name . ' updated their profile '
                ]);

                return Redirect::to(General::backend_url().'/profile/')->with('success', 'Profile updated.');
            }

            $this->data['user'] = $this->user;

            return View::make('HummingbirdBase::cms/users-profile', $this->data);
        }
    }
    
    public function getExport()
    {
        return parent::export_model('User');
    }
    
    public function postExport()
    {
        return parent::run_export('User', 'user_export.csv');        
    }

    public function import()
    {
        DB::unprepared(file_get_contents(base_path() . '/import/sql/cms_users_import.sql'));

        // $roles = DB::table('roles')->where('id', '>', '1')->orderBy('id')->lists('id');

        $migrate_users = DB::table('cms_users_import')->get();

        if(count($migrate_users) > 0)
        {
            $import_users = array();

            foreach($migrate_users as $user)
            {
                if($user->username != '')
                {
                    $role = 2;
                    $new_user = array();
                    $new_user['username'] = $user->username;
                    $new_user['password'] = md5(time());
                    $new_user['name'] = $user->forename . ' ' . $user->surname;
                    $new_user['email'] = $user->username;
                    $new_user['status'] = ($user->live == 1 AND $user->deleted != 1) ? 'active':'inactive';
                    $new_user['role_id'] = $role;
                    $new_user['hidden'] = ($user->show) ? 0:1;

                    $import_users[] = $new_user;
                }
            }

            DB::table('users')->insert($import_users);
            Schema::dropIfExists('cms_users_import');

            foreach($import_users as $user)
            {
                $user = User::where('email', '=', $user['email'])->first();

                Activitylog::log([
                    'action' => 'CREATED',
                    'type' => get_class($user),
                    'link_id' => $user->id,
                    'description' => 'Created user',
                    'notes' => "System created $user->name as a user"
                ]);
            }

        } 

        return Redirect::to(General::backend_url() . '/users/');
    }

    public function preview($user_id = null)
    {
        switch(Input::get('action'))
        {
            case 'remove':
                $this->removePreviewPerms();
                break;
            default:
                Session::put('view_user_perms', $user_id);
                break;
        }

        return Redirect::to(General::backend_url());
    }

    public function removePreviewPerms()
    {
        Session::forget('view_user_perms');

        return Redirect::to(General::backend_url().'');
    }

    // don't think this is needed, as we can access from the session
    public function setPreviewUser($user_id)
    {
        $user = User::find($user_id);
        return $user->name;
    }
}