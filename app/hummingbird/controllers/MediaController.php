<?php

class MediaController extends CmsbaseController
{

	public function __construct()
	{
		parent::__construct(); //boot the base controller

        // $this->importstructure();
        // $this->update_old_docs();

		$this->activateUserDefaults = false;
		$this->data['collections'] = array();
        $this->data['import_media'] = false;

        /* Get all the collections - ordered by parent_collection (Null first, n+1 last) */
        $this->all_collections = DB::table('media-collections')->whereNull('deleted_at')->orderBy('id', 'ASC')->get();

        /* Store collections in that we need */
        $this->buildTree($this->all_collections);

        $this->data['breadcrumbs'][] = array(
            'classes' => '',
            'icon' => '',
            'title' => 'Media Centre',
            'url' => '/' . General::backend_url() . '/media/'
        );

        // $media_settings['image_sizes'][] = array('width' => 250);
        // (new Setting)->newSetting('media', $media_settings);

        // $this->settings = Setting::getWhere('key', '=', 'media');


        // 0 = //media centre view (non-deletable)
        // 1 = //media centre view (non-deletable) - thumbnail

        $this->image_sizes = Setting::where('key', '=', 'media')->first();

        $this->image_sizes = (!isset($this->image_sizes->id) OR null === $this->image_sizes->id) ? false:$this->image_sizes->value;

        if(!isset($this->image_sizes['sizes']) OR count($this->image_sizes['sizes']) == 0)
        {
        	$this->image_sizes['sizes'] = array(
	        	0 => array (
	        		'name' => 'File Manager Thumbnail',
	        		'type' => 'Default',
	        		'description' => 'File Manager Thumbnails',
	        		'width' => 300
	        	)
	        );

	        (new Setting)->newSetting('media', $this->image_sizes);
        }
	}

    public function buildTree($elements, $parentId = null, $level = 0) 
    {
        foreach ($elements as $key => $element) {
            /* Top level parent */

            if ($element->parent_collection !== $parentId) continue;

            $element->name = str_repeat("&nbsp;", $level*2).$element->name;
            $this->data['collections'][] = $element;
            $this->buildTree($elements, $element->id, $level + 1);
        }
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = null)
	{
		if($this->user->hasAccess('read', get_class(new Media)))
		{
			$return = $this->checkDefaultMedia(Auth::user()->preferences, $id);

			if(null !== $return) return $return;

			$this->data['current_collection'] = (!$id) ? '':$id;
			$this->data['parent_id'] = (!$id) ? '':$id;

			if(!$id)
			{
				$this->data['media_collections'] = DB::table('media-collections')->whereNull('parent_collection')->get();
				$this->data['media_items'] = Media::inCollection(false)->parent(true)->deleted()->get(); //just get all files		
			}
			else
			{
				$this->data['media_collections'] = DB::table('media-collections')->where('parent_collection', '=', $id)->get();
				$this->data['media_items'] = Media::inCollection($id)->parent(true)->deleted()->get(); //just get all files	


				/* Build breadcrumb */
				$this->data['collection'] = DB::table('media-collections')->where('id', '=', $id)->whereNull('deleted_at')->first();
				$parent_collection = $this->data['collection']->parent_collection;
				$breadcrumbs = array(
					0 => array(
			            'classes' => '',
			            'icon' => '',
			            'title' => $this->data['collection']->name,
			            'url' => '/' . General::backend_url() . '/media/view/' . $this->data['collection']->id
					)
				);

				while(null !== $parent_collection)
				{
					$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->whereNull('deleted_at')->first();

					array_unshift($breadcrumbs, array
						(
							'classes' => '',
				            'icon' => '',
				            'title' => $parent__collection->name,
				            'url' => '/' . General::backend_url() . '/media/view/' . $parent__collection->id
						)
					);
					
					$parent_collection = $parent__collection->parent_collection;
				}

				foreach($breadcrumbs as $crumb)
				{
					$this->addToBreadcrumb($crumb);
				}
			}

			$files = File::allFiles(base_path() . '/import');

			if(count($files) > 0)
			{
				$this->data['import_media'] = true;
				$this->data['files'] = $files;
			}

			return View::make('HummingbirdBase::cms.media', $this->data);	
		}
		else
		{
			return $this->forbidden(true);
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		dd("create");
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if($this->user->hasAccess('create', get_class(new Media)))
		{
			/*
			* Check name has been posted
			* Check name does not exist
			* Check parent of collection to make sure
			* Insert new media collection name
			* 	Does the collection need to be stored in a parent?
			*/

			$rules = array(
				'collection-name' => 'required|max:255'
			);

			$validator = Validator::make(Input::all(), $rules, array('required' => 'The :attribute is required.', 'max' => 'Collection name should not be any larger than 255 characters'));

			if($validator->fails())
			{
				// $this->pp($validator->messages());
				$messages = $validator->messages();
				// Log error in activity log
				return Redirect::to('/hummingbird/media/')->withInput()->withErrors($messages);
			}
			else
			{
				// add to collections
				// set parent (if required);
				if (Input::has('collection-name'))
				{
					$parent_collection = (Input::has('parent_collection')) ? Input::get('parent_collection'):NULL;

					/* Query scope here - http://laravel.com/docs/4.2/eloquent#query-scopes */
					$collection = DB::table('media-collections')->where('name', '=', Input::get('collection-name'))->where('parent_collection', '=', $parent_collection)->whereNull('deleted_at')->get();

					if(empty($collection))
					{
						$data = array('name' => Input::get('collection-name')); //data to insert
						$data['parent_collection'] = $parent_collection; //set parent

						$data['permalink'] = Str::slug($data['name']);
						$set_permalink = false;

						$counter = 1;
						while(!$set_permalink)
						{
							$perm_check = DB::table('media-collections')->where('permalink', '=', $data['permalink'])->get();

							/* We have this URL */
							if(!empty($perm_check))
							{
								$data['permalink'] = Str::slug($data['name']) . $counter;
								$counter++;
							}
							else
							{
								/* No url found - break */
								$set_permalink = true;
							}
						}

						$collection_id = DB::table('media-collections')->insertGetId($data);

						if(is_numeric($collection_id) AND $collection_id > 0)
						{
							//nothing
							//$this->getMyDBQuery();

					        Activitylog::log([
					            'action' => 'CREATED',
					            'type' => get_class(new Media),
					            'link_id' => $collection_id,
					            'url' => General::backend_url() . "/media/view/$collection_id",
					            'description' => 'Created collection',
					            'notes' => Auth::user()->username . " created a new collection &quot;".$data['name']."&quot;"
					        ]);
						}
						else
						{
							//$this->pp('Could not insert.');
						}
					}
					else
					{
						//$this->pp('Folder already exists.');
					}
				}

				if(isset($data['parent_collection']) AND null !== $data['parent_collection'])
				{
					return Redirect::to(General::backend_url().'/media/view/'.$data['parent_collection']);
				}
				
				return Redirect::to(General::backend_url().'/media/');
			}
		}
		else
		{
			return $this->forbidden(true);
		}
	}

	/**
	 * 
	 *
	 * @return Response
	 */
	public function postEditMediaItem($id)
	{
		$FileHelper = new FileHelper;
		$input = Input::except('_token', 'add');

		$action = (null !== $input['action']) ? $input['action']:'update';

		/* needs validation */
		$media = Media::find($id);
		$media->fill($input);

		if(!$FileHelper->isImage(File::extension(base_path() . $media->location)))
		{
			$media->alt = '';
			$media->caption = '';
		}

		$media->save();

        /* Store cats and tags */
        $input['taxonomy'] = (isset($input['taxonomy'])) ? $input['taxonomy']:array();
        $media->storeTaxonomy($input['taxonomy'], $media, get_class($media));

        /* lets roll all changes to children - only available to parents */
		if($action == 'update-all')
		{
			if($media->parent === NULL)
			{
				/* get all children - excluding self */
				$media_children = Media::where('parent', '=', $id)->where('id', '!=', $id)->whereNull('deleted_at')->get();
			}
			else
			{
				/* All versions - excluding self */
				$media_children = Media::orWhere(function($query) use ($media)
					{
						$query->where('id', '=', $media->parent)
							->orWhere('parent', '=', $media->parent);
					})	
					->where('id', '!=', $id)->whereNull('deleted_at')->get();
			}

			foreach($media_children as $child)
			{
				$child->fill($input);
				$child->save();
				$child->storeTaxonomy($input['taxonomy'], $child, get_class($child));
			}
		}

		/* If successful, return */
		return Redirect::to(General::backend_url().'/media/view-media/'.$media->id)->with('success', 'Media item has been updated.');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(null !== $id AND is_numeric($id))
		{
			$this->data['media_items'] = Media::get(); //just get all files

			return View::make('HummingbirdBase::cms.media', $this->data);
		}

		return Redirect::to(General::backend_url().'/media/')->with('errors', 'That collection is not available. Please try again.');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->data['import_media'] = false;
		$this->data['collection'] = DB::table('media-collections')->whereNull('deleted_at')->find($id);

		$files = File::allFiles(base_path() . '/import');

		if(count($files) > 0)
		{
			$this->data['import_media'] = true;
			$this->data['files'] = $files;
		}

		/* Build breadcrumb */
		$this->data['collection'] = DB::table('media-collections')->where('id', '=', $id)->whereNull('deleted_at')->first();
		$parent_collection = $this->data['collection']->parent_collection;
		$breadcrumbs = array(
			0 => array(
	            'classes' => '',
	            'icon' => '',
	            'title' => $this->data['collection']->name,
	            'url' => '/' . General::backend_url() . '/media/view/' . $this->data['collection']->id
			)
		);

		while(null !== $parent_collection)
		{
			$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->whereNull('deleted_at')->first();

			array_unshift($breadcrumbs, array
				(
					'classes' => '',
		            'icon' => '',
		            'title' => $parent__collection->name,
		            'url' => '/' . General::backend_url() . '/media/view/' . $parent__collection->id
				)
			);
			
			$parent_collection = $parent__collection->parent_collection;
		}

		foreach($breadcrumbs as $crumb)
		{
			$this->addToBreadcrumb($crumb);
		}

		return View::make('HummingbirdBase::cms.media-collection', $this->data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$collection = DB::table("media-collections")->where('id', '=', $id)->first();

		DB::table('media-collections')
		->where('id', '=', $id)
		->update(
			array
				(
					'name' => Input::get('name'),
					'description' => Input::get('description'),
					'parent_collection' => (Input::get('parent_collection') == '') ? NULL:Input::get('parent_collection')
				)
			);

        Activitylog::log([
            'action' => 'UPDATED',
            'type' => get_class(new Media),
            'link_id' => $id,
            'url' => General::backend_url() . "/media/edit/$collection->id",
            'description' => 'Updated details for a collection',
            'notes' => Auth::user()->username . " updated the details for &quot;$collection->name&quot;"
        ]);

		return Redirect::to(General::backend_url().'/media/edit/'.$id)->with('success', 'Collection details have been updated.');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id = null)
	{
		if(null !== $id)
		{
			$this->checkDefaultMedia(Auth::user()->preferences, $id);

			$collection = DB::table('media-collections')->where('id', '=', $id)->first();
			$return_url = General::backend_url() . '/media/';
			$return_url .= (null !== $collection->parent_collection) ? 'view/' .$collection->parent_collection:'';

			if((new Media)->whereNull('deleted_at')->inCollection($id)->count() > 0 OR DB::table('media-collections')->where('parent_collection', '=', $id)->whereNull('deleted_at')->count() > 0)
			{
				// On success return back to the default place we came back from - so we delete item 8, redirect back to parent which is 1
				// Show flash also
				return Redirect::to($return_url)->with('errors', 'There is data within this collection. Please delete these before trying to delete this collection.');
			}
			else
			{
				if(null !== $collection)
				{
					DB::table('media-collections')->where('id', '=', $id)->delete();

			        Activitylog::log([
			            'action' => 'DELETED',
			            'type' => get_class(new Media),
			            'link_id' => $collection->id,
			            'description' => 'Collection deleted',
			            'notes' => Auth::user()->username . " has deleted the collection &quot;$collection->name&quot;"
			        ]);

					return Redirect::to($return_url)->with('message', '&quot;' . $collection->name . '&quot; has been deleted.');
				}
				// if($id === $this->base_user_dir) dd("destroy");

				// dd("no-permission " . $id . "-" . $this->base_user_dir);

				/* we need to:

				1) Check there are no files inside the collection (may need to loop the collections inside also)
				2) If no files - delete
				3) If yes, cancel and tell user that files must be deleted first

				*/
			}
		}

		// On success return back to the default place we came back from - so we delete item 8, redirect back to parent which is 1
		// Show flash also
		return Redirect::to(General::backend_url().'/media/')->with('errors', 'That collection is not available. Please try again.');
	}

	public function destroyMedia($id = null)
	{
		$return_url = (null !== Request::server('HTTP_REFERER')) ? Request::server('HTTP_REFERER'):General::backend_url() . '/media/';

		if(null !== $id)
		{
			$item = Media::where('id', '=', $id)->locked()->whereNull('deleted_at')->first();
			
			if(null !== $item)
			{
				DB::table('media-items')->where('id', '=', $id)->update
					(
						array
						(
							'deleted_at' => date("Y-m-d H:i:s")
						)
					);

		        Activitylog::log([
		            'action' => 'DELETED',
		            'type' => get_class($item),
		            'link_id' => $item->id,
		            'description' => 'Media item deleted',
		            'notes' => Auth::user()->username . " has deleted &quot;$item->filename&quot; ($item->location) from the media library"
		        ]);

		        //(new FileHelper)->deleteFiles(base_path() . $item->location, $item->getFileLocations($item->return_children_resources()));

		        if(Input::get('ajax'))
		        {
		        	$message = (object) array('state' => true, 'message' => "$item->location has been removed from the media library");
		        	return Response::json($message, 200);
		        }
		        else
		        {
		        	return Redirect::to($return_url)->with('message', '&quot;' . $item->filename . '&quot; has been deleted.');
		        }
			}
		}



		// On success return back to the default place we came back from - so we delete item 8, redirect back to parent which is 1
		// Show flash also
        if(Input::get('ajax'))
        {
        	$message = (object) array('state' => false, 'message' => "$item->location can not be removed. Please try again.");
        	return Response::json($message, 200);
		}
		else
		{	
			return Redirect::to($return_url)->with('error', 'That item is not available. Please try again.');
		}
	}

	/**
	 * Importing multiple files via the import facility
	 *
	 * @param  return  boolean (true|false)
	 * @return Response
	 */
	public function importFiles()
	{
		dd("importing");
	}

	/**
	 * Upload specified media item to the server and insert into the database
	 *
	 * @param  return  boolean (true|false)
	 * @return Response
	 */
	public function uploadFile($ajax = true)
	{
		if((Input::get('noajax') != 'noajax' OR Request::isMethod('post')) AND Input::get('ajax') == 0) $ajax = false;


		// /* Upload file */ - DONE 
		// /* check upload correct */ - DONE
		// /* check upload set */ - DONE
		/* check extension - DONE */
			/* Throw error if not correct extension - DONE */
			/* Upload item if correct extension - DONE */
		/* Maximum file size check - DONE */
			/* Validate Image size - DONE */
			/* Validate Documents size - DONE */
			/* Validate Videos size - DONE */
			/* Validate Audio size - DONE */
			/* Validate Other size - DONE */
		/* Insert into correct folder */
		/* Add file to database */
		/* Add file to collection if necessary */

		/* on install create those folders */

		/*
			Image - 8MB
			Documents - 8MB
			Videos - 50MB
			Audio - 50MB
			Other - 1MB
		*/

		if (Input::hasFile('file'))
		{
			if (Input::file('file')->isValid())
			{
				$input = Input::except('_token', 'add');

				$item = Input::file('file');
				$file_ext = Str::lower($item->getClientOriginalExtension());
				$file_name = $item->getClientOriginalName();

				$FileHelper = new FileHelper;

				if($FileHelper->isValidExtension($file_ext))
				{
					if(!$FileHelper->checkMediaFileSize($item, $file_ext))
					{
			            $file_type = $FileHelper->getValidKeyByExt($file_ext);
			            $save_path = '/uploads/'.$file_type.'/'.date("Y-m-d").'/';

						$filename = $FileHelper->formatFileName(time().'__'.str_replace($file_ext, "", $file_name));
						$filename .= '.'.$file_ext;
						$__isImage = $FileHelper->isImage($file_ext);

						if (File::isWritable(base_path().'/uploads/'.$file_type))
						{
							if($FileHelper->uploadFile($item, $filename, base_path() . $save_path))
							{
								/* save item */
								$media = new Media;
								$media->title = $file_name;
								$media->filename = $file_name;
								$media->location = '/uploads/'.$file_type.'/'.date("Y-m-d").'/'.$filename;
									
								if(Input::has('collection'))
								{
									$media->collection = Input::get('collection');
								}

								$media->save();
								$media->state = true;
								$media->is_image = $__isImage;
								$media->icon = 'fa-file-' . Str::slug($file_ext);

								if($media->is_image)
								{
									/* if image */
									foreach($this->image_sizes['sizes'] as $size)
									{
										$width = (isset($size['width'])) ? $size['width']:null;
										$height = (isset($size['height'])) ? $size['height']:null;

										$media_child = (new Media)->fill((array) $media);
										$media_child->title = $file_name;
										$media_child->filename = $file_name;
										$media_child->parent = $media->id;
										$media_child->mc_only = 1;
										$media_child->locked = 1;

										$FileHelper->create_image($filename, $save_path, $width, $height, $media_child);
									}
								}

								if(!$ajax)
								{
									Session::flash('success', 'We uploaded your file.');
								}
								else
								{
									return Response::json($media, 200);
								}
							}
							else
							{
								if(!$ajax)
								{
									//non-ajax request
									Session::flash('errors', 'There was a problem uploading your file.');
								}
								else
								{
				        			return Response::json("There was a problem uploading &quot;$file_name&quot;.", 400);
								}
							}
						}
						else
						{
							return Response::json("The folder $file_type is not writeable. Please try again.", 400);
						}
					}
					else
					{
						if(!$ajax)
						{
							//non-ajax request
							Session::flash('errors', 'The file uploaded ('.$file_name.') is too big.'.$FileHelper->getErrorByKey('filesize'));
						}
						else
						{
		        			return Response::json('The file uploaded ('.$file_name.') is too big.'.$FileHelper->getErrorByKey('filesize'), 400);
						}	
					}
				}
				else
				{
					$files = implode(", ", $FileHelper->getAllExtensions());

					if(!$ajax)
					{
						//non-ajax request
						Session::flash('errors', 'Please upload a valid file. Valid file types include: '.$files);
					}
					else
					{
	        			return Response::json('Please upload a valid file. Valid file types include: '.$files, 400);
					}		
				}
			}
			else
			{
				if(!$ajax)
				{
					//non-ajax request
					Session::flash('errors', 'We could not upload your file. Please try again.');
				}
				else
				{
        			return Response::json('We could not upload your file. Please try again.', 400);
				}
			}
		}
		else
		{
			$item = Input::file('file');

			$error = (null === $item) ? 'There was a problem processing your request.':$this->get_error_by_code($item->getError());

			if(!$ajax)
			{
				Session::flash('errors', $error);
				return Redirect::to(General::backend_url().'/media');
			}
			else
			{
				return Response::json($error, 400);
			}
		}

		if(!$ajax) return Redirect::to(General::backend_url().'/media');
	}

	/**
	 *
	 */
	public function uploadFile_byFunction($file, $collection_id = null)
	{
		$FileHelper = new FileHelper;

		if(File::exists($file))
		{
			//file exists
			$file_ext = Str::lower(File::extension($file));
			$file_name = basename($file);
		
			// if($FileHelper->isValidExtension($file_ext))
			// {
	            $file_type = $FileHelper->getValidKeyByExt($file_ext);
	            $save_path = '/uploads/'.$file_type.'/'.date("Y-m-d").'/';

				$filename = $FileHelper->formatFileName(time().'__'.str_replace($file_ext, "", $file_name));
				$filename .= '.'.$file_ext;
				$__isImage = $FileHelper->isImage($file_ext);

				if (File::isWritable(base_path().'/uploads/'.$file_type))
				{
					if($FileHelper->uploadFile_byFunction($file, $filename, base_path() . $save_path))
					{
						General::pp('uploaded');
						/* save item */
						$media = new Media;
						$media->save();
						$ID = $media->id;

						$media->title = $file_name;
						$media->filename = $file_name;
						$media->location = '/uploads/'.$file_type.'/'.date("Y-m-d").'/'.$filename;
							
						if(isset($collection_id) AND $collection_id > 0)
						{
							$media->collection = $collection_id;
						}

						$media->save();
						
						// $media->state = true;
						$is_image = $__isImage;
						// $icon = 'fa-file-' . Str::slug($file_ext);

						if($is_image)
						{
							/* if image */
							foreach($this->image_sizes['sizes'] as $size)
							{
								$width = (isset($size['width'])) ? $size['width']:null;
								$height = (isset($size['height'])) ? $size['height']:null;

								$media_child = $media->replicate();
								// $media_child = (new Media)->fill((array) $media);
								// $media_child->title = $file_name;
								// $media_child->filename = $file_name;
								$media_child->parent = $media->id;
								$media_child->collection = NULL;
								$media_child->mc_only = 1;
								$media_child->locked = 1;

								$FileHelper->create_image($filename, $save_path, $width, $height, $media_child);
							}

							return $save_path . $filename;
						}

						return $save_path . $filename;
					}
					else
					{
						General::pp('not uploaded');
					}
				}
				else
				{
					General::pp('not writeable');
				}
			// }
		}
		else
		{
			General::pp('Missing file' . $file);
			return false;
		}

		General::pp('Random fail');
		return false;
	}

	/**
	 * Media items uploaded, now update the required fields
	 *
	 * @return Response
	 */
	public function updateUploadedFiles()
	{
		$items = Input::get('media');
		$collection = (null !== Input::get('collection')) ? Input::get('collection'):null;

		if(count($items) > 0)
		{
			$updated = array();

			foreach($items as $media_id => $media_data)
			{
				$media_data = (array) json_decode($media_data);

				$media = Media::find($media_id);
				$media->fill($media_data);
				$media->save();

				/* Do we have children */
				if($media->children() > 0)
				{	
					//Yes we do - update them too
					$children = Media::where('parent', '=', $media->id)->whereNull('deleted_at')->get();

					foreach($children as $child)
					{
						$child->fill($media_data);
						$child->save();
					}
				}

				$updated[] = 'Meta data updated for &quot;' . $media->filename . '&quot;';
			}	

			Session::flash('success', $updated);
		}
		else
		{
			Session::flash('errors', 'No files to update.');
		}

		if(null !== $collection)
		{
			return Redirect::to(General::backend_url().'/media/view/' . $collection);
		}
		
		return Redirect::to(General::backend_url().'/media');
	}

	private function checkDefaultMedia($user_pref, $id)
	{
		if($this->activateUserDefaults)
		{
			if(null !== $user_pref)
			{
				$user_prefs = unserialize($user_pref); //unserialize data

				if(null !== $user_prefs['permissions']['default-collection'])
				{
					if(is_numeric($user_prefs['permissions']['default-collection']) AND $id != $user_prefs['permissions']['default-collection'])
					{
						return Redirect::to(General::backend_url().'/media/view/'.$user_prefs['permissions']['default-collection']);
					}

					$this->base_user_dir = $user_prefs['permissions']['default-collection'];
				}
			}
		}
		return;
	}

	public function getMediaModalTemplate()
	{
		$html['template'] = '<section id="redactor-modal-advanced">'
	            . '<label>Enter a text</label>'
	            . '<textarea id="mymodal-textarea" rows="6"></textarea>'
	            . '</section>';
		return json_encode($html);
	            // return $html;
	}

	public function mediaLibraryRequest()
	{
		$images = Media::type()->get(); //just get all files
		$html = '';

		foreach($images as $image)
		{
			$html .= '<div class="col-sm-3"><div class="thumbnail imagemanager-insert"><img src="' . $image['location'] . '" alt="' . $image['filename'] . '" title="' . $image['filename'] . '" /></div></div>';
		}

		return json_encode($html);
	}

	private function notAvailable()
	{
		return;
	}

	public function postEditImage($image)
	{
		ini_set("memory_limit", "1024M"); //maximum memory limit - 1GB for large files - smaller than 10MB 

		$this_image = Media::find($image);
		$new__image = Image::make(base_path() . $this_image->location);

		switch($_POST["update"])
		{
			case 'rotate':
				$rotate = (Input::has('rotate')) ? Input::get('rotate'):NULL;

				if(null !== $rotate)
				{
					$new__image->rotate($rotate * 45);

			        $data = base64_encode($new__image->encode());

			        return $data;
				}

				break;
			case 'scale':
				// Resize the image
		        $new__image->resize($_POST['width'], $_POST['height'], function ($constraint)
		        {
		            $constraint->aspectRatio();
		            $constraint->upsize();
		        });

		        $data = base64_encode($new__image->encode());

		        return $data;
				break;
			case 'save':
				$save_path = '/uploads/images/'.date("Y-m-d").'/';

				if($_POST['type'] == 'crop')
				{

					// dd($_POST);
					if($_POST['width'] != $new__image->width() AND $_POST['height'] != $new__image->height())
					{
						//not the same - resize
				        $new__image->resize($_POST['width'], $_POST['height'], function ($constraint)
				        {
				            $constraint->aspectRatio();
				            $constraint->upsize();
				        });
					}

					/* Now Crop */
					$new__image->crop($_POST['w'], $_POST['h'], $_POST['x'], $_POST['y']);
				}
				else if($_POST['type'] == 'scale')
				{
					// Resize the image
			        $new__image->resize($_POST['width'], $_POST['height'], function ($constraint)
			        {
			            $constraint->aspectRatio();
			            $constraint->upsize();
			        });
				}

				if(!File::isWritable(base_path() . $save_path))
				{
					$result = File::makeDirectory(base_path() . $save_path, 0775); /* folder does not exist - create it */

					while(!$result)
					{
						$result = File::makeDirectory(base_path() . $save_path, 0775, true); /* folders do not exist - create recursively */
					}
				}

				//upload
				$filename = explode(".", $this_image->filename);
				$filename[0] = $filename[0] . "[" . $new__image->width() . "x" . $new__image->height() ."]";
				$filename = implode(".", $filename);

				if($new__image->save(base_path() . $save_path . $filename, 100))
				{
			        $child_image = $this_image->replicate();
			        $child_image->title = $this_image->title;
			        $child_image->filename = $this_image->filename;
			    	$child_image->location = $save_path . $filename;
			    	$child_image->parent = $this_image->getParentId(); 
			    	$child_image->collection = NULL;
			    	$child_image->mc_only = NULL;
			    	$child_image->locked = NULL;
			    	$child_image->save();

			    	$child_image->replicateTaxonomy();

			    	return Redirect::to(General::backend_url().'/media/view-media/' . $child_image->id);
				}
				break;
		}
	}

	public function editImage($image)
	{
		$FileHelper = new FileHelper;

		if(null !== $image)
		{
			$this->data['image'] = DB::table('media-items')->where('id', '=', $image)->first();

			if(count($this->data['image']) == 1)
			{
				if(File::exists(base_path() . $this->data['image']->location))
				{
					$file_extension = $FileHelper->getFileExtension($this->data['image']->location);

					//we have an image - check it
					if($FileHelper->isImage($file_extension))
					{
						$breadcrumbs[] = 
							array(
					            'classes' => '',
					            'icon' => '',
					            'title' => (isset($this->data['image']->title) AND $this->data['image']->title != '') ? $this->data['image']->title:'unnamed',
					            'url' => '/' . General::backend_url() . '/media/view-media/' . $this->data['image']->id
							);

						/* Parent - Build breadcrumb */
						if($this->data['image']->parent > 0)
						{
							$parent_item = Media::where('id', '=', $this->data['image']->parent)->whereNull('deleted_at')->first();

							$parent_collection = $parent_item->collection; //collection for these items
							array_unshift($breadcrumbs, array
								(
						            'classes' => '',
						            'icon' => '',
						            'title' => $parent_item->title,
						            'url' => '/' . General::backend_url() . '/media/view-media/' . $parent_item->id
						        )
							);
						}
						else
						{
							$parent_collection = $this->data['image']->collection; //collection for these items
							
							array_unshift($breadcrumbs, array
								(
						            'classes' => '',
						            'icon' => '',
						            'title' => $this->data['image']->title,
						            'url' => '/' . General::backend_url() . '/media/view-media/' . $this->data['image']->id
						        )
							);
						}

						while(null !== $parent_collection)
						{
							$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->whereNull('deleted_at')->first();

							array_unshift($breadcrumbs, array
								(
									'classes' => '',
						            'icon' => '',
						            'title' => $parent__collection->name,
						            'url' => '/' . General::backend_url() . '/media/view/' . $parent__collection->id
								)
							);
							
							$parent_collection = $parent__collection->parent_collection;
						}

						foreach($breadcrumbs as $crumb)
						{
							$this->addToBreadcrumb($crumb);
						}

						//this is an image
						return View::make('HummingbirdBase::cms.media-edit-image', $this->data);
					}
					else
					{
						return Redirect::to(General::backend_url().'/media/')->with('message', 'Can not edit this media item as it is not an image. Please try again.');
					}	
				}
				else
				{
					return Redirect::to(General::backend_url().'/media/')->with('message', 'Image no longer found on the server. <a href="">Delete permanently?</a>.');
				}
			}
			else
			{
				return Redirect::to(General::backend_url().'/media/')->with('message', 'Could not locate image.');
			}
		}

		return Redirect::to(General::backend_url().'/media/')->with('message', 'No image was selected.');
	}


	public function viewItem($id)
	{
		$FileHelper = new FileHelper;
		$item = Media::find($id);

		if(null !== $item)
		{
			$item->children = (null === $item->parent) ? $item->children():false;

			$item->media_description = (null !== $item->description) ? "$item->filename - $item->description":$item->title;
			$this->data['media'] = $item;
			$this->data['is_image'] = ($FileHelper->isImage(File::extension(base_path() . $item->location))) ? TRUE:FALSE;
	        $this->data['taxonomy'] = $item->taxonomy();

			/* Build breadcrumb */

			$breadcrumbs = array(
				0 => array(
		            'classes' => '',
		            'icon' => '',
		            'title' => (($item->title != '') ? $item->title:$item->filename),
		            'url' => ''
				)
			);

			$parent_collection = $item->collection;

			if(null !== $item->parent)
			{
				$parent = Media::find($item->parent);
				$parent_collection = $parent->collection;

				array_unshift($breadcrumbs, array
					(
						'classes' => '',
			            'icon' => '',
			            'title' => (($parent->title != '') ? $parent->title:$parent->filename),
			            'url' => '/' . General::backend_url() . '/media/view-media/' . $parent->id
					)
				);
			}

			while(null !== $parent_collection)
			{
				$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->whereNull('deleted_at')->first();

				array_unshift($breadcrumbs, array
					(
						'classes' => '',
			            'icon' => '',
			            'title' => $parent__collection->name,
			            'url' => '/' . General::backend_url() . '/media/view/' . $parent__collection->id
					)
				);
				
				$parent_collection = $parent__collection->parent_collection;
			}

			foreach($breadcrumbs as $crumb)
			{
				$this->addToBreadcrumb($crumb);
			}

			return View::make('HummingbirdBase::cms.media-view', $this->data);
		}

		return Redirect::to(General::backend_url().'/media/')->with('error', 'Media item could not be found.');
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function settings()
	{
		// $this->data['settings'] = $this->settings;
		$this->data['image_sizes'] = $this->image_sizes['sizes'];
		return View::make('HummingbirdBase::cms.media-settings', $this->data);
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function showSettings($id)
	{
		die("SHOW SETTINGS");
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function createSetting()
	{
		if($this->user->hasAccess('create', get_class(new Media)))
		{
			/*
			* Check name has been posted
			* Check name does not exist
			* Check parent of collection to make sure
			* Insert new media collection name
			* 	Does the collection need to be stored in a parent?
			*/

			$rules = array(
				'title' => 'required|max:255',
				'width' => 'required_without:height|Integer',
				'height' => 'required_without:width|Integer'
			);

			$validator = Validator::make(Input::all(), $rules, array('required' => 'The :attribute is required.', 'max' => ':attribute should not be any larger than 255 characters'));

			if($validator->fails())
			{
				$messages = $validator->messages();

				// Log error in activity log
				return Redirect::to('/hummingbird/media/settings/')->withInput()->withErrors($messages);
			}
			else
			{
				// add to collections
				// set parent (if required);
				if (Input::has('title'))
				{
					foreach($this->image_sizes['sizes'] as $size)
					{
						if($size['name'] == Input::get('title'))
						{
							$messages = new Illuminate\Support\MessageBag;
							$messages->add('error', 'Name of image resize already exists.');
							continue;
						}
					}

					if(!isset($messages))
					{
						$data = array();
						$data['name'] = Input::get('title');
						$data['type'] = (null !== Input::get('type')) ? Input::get('type'):'';
						$data['description'] = (null !== Input::get('description')) ? Input::get('description'):'';
						$data['width'] = (null !== Input::get('width')) ? Input::get('width'):false;
						$data['height'] = (null !== Input::get('height')) ? Input::get('height'):false;

						$this->image_sizes['sizes'][] = $data;

						(new Setting)->updateSettings('media', $this->image_sizes);


						/* Activity */
						if(is_numeric($data['width']) AND is_numeric($data['height']))
						{
							$activity_text = '(' . $data['width'] . 'px by ' . $data['height'] . 'px)';
						}
						else
						{
							if(is_numeric($data['width']))
							{
								$activity_text = '(Max width: ' . $data['width'] . 'px)';
							}
							else
							{
								$activity_text = '(Max height: ' . $data['height'] . 'px)';
							}
						}

				        Activitylog::log([
				            'action' => 'UPDATED',
				            'type' => get_class(new Setting),
				            'link_id' => null,
				            'url' => "",
				            'description' => 'Added a new media size',
				            'notes' => Auth::user()->username . " added a new media size " . $activity_text
				        ]);

					}
					else
					{
						return Redirect::to('/hummingbird/media/settings/')->withInput()->withErrors($messages);
					}
				}
				
				return Redirect::to(General::backend_url().'/media/settings/');
			}
		}
		else
		{
			return $this->forbidden(true);
		}
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function deleteSetting()
	{
		die("DELETE SETTINGS");
	}

	/**
	 *
	 *
	 * @param  
	 * @return 
	 */
	public function updateSetting($id)
	{
		die("UPDATE SETTINGS");
	}


	public function globalMediaLibrary()
	{
		$FileHelper = new FileHelper;

		$input = Input::all();

		if(isset($input['action']) AND $input['action'] == 'item')
		{
			$this->data['image'] = DB::table('media-items')->where('id', '=', $input['item'])->first();
			// $this->data['featured'] = (null !== $input['featured'] OR isset($input['featured'])) ? $input['featured']:false;
			$this->data['featured'] = false;

			if(count($this->data['image']) == 1)
			{
				$this->data['is_image'] = false;

				if(File::exists(base_path() . $this->data['image']->location))
				{
					$file_extension = $FileHelper->getFileExtension($this->data['image']->location);

					//we have an image - check it
					if($FileHelper->isImage($file_extension))
					{
						$this->data['is_image'] = true;
						$this->data['versions'] = Media::where('parent', '=', $this->data['image']->id)->whereNull('deleted_at')->get();

						/* Build breadcrumb */
						$parent_collection = $this->data['image']->collection;
						$breadcrumbs = array(
							0 => array(
					            'classes' => '',
					            'icon' => '',
					            'title' => $this->data['image']->filename,
					            'url' => '/' . General::backend_url() . '/media/view-media/' . $this->data['image']->id
							)
						);

						while(null !== $parent_collection)
						{
							$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->whereNull('deleted_at')->first();

							array_unshift($breadcrumbs, array
								(
									'classes' => '',
						            'icon' => '',
						            'title' => $parent__collection->name,
						            'url' => '/' . General::backend_url() . '/media/view/' . $parent__collection->id
								)
							);
							
							$parent_collection = $parent__collection->parent_collection;
						}

						foreach($breadcrumbs as $crumb)
						{
							$this->addToBreadcrumb($crumb);
						}
					}
				}
			}

			$html = View::make('HummingbirdBase::cms.media-global-view', $this->data)->render();
		}
		else
		{
			if(isset($input['collection']) AND $input['collection'] > 0)
			{
				$input['type'] = (!isset($input['type']) OR $input['type'] == '') ? true:$input['type'];

				$this->data['media_collections'] = DB::table('media-collections')->where('parent_collection', '=', $input['collection'])->get();
				$this->data['media_items'] = Media::inCollection($input['collection'])->parent(true)->deleted()->get(); //just get all files

				switch((String) $input['type'])
				{
					case 'images':
					case 'documents':
					case 'audio':
					case 'video':
						$this->data['media_items'] = (new Media)->isType($input['type'], $FileHelper, $this->data['media_items']);
						break;
					default:
						break;
				}

				/* Build breadcrumb */
				$this->data['collection'] = DB::table('media-collections')->where('id', '=', $input['collection'])->whereNull('deleted_at')->first();
				$parent_collection = $this->data['collection']->parent_collection;
				$breadcrumbs = array(
					0 => array(
						'id' => $this->data['collection']->id,
			            'classes' => '',
			            'icon' => '',
			            'title' => $this->data['collection']->name,
			            'url' => '/' . General::backend_url() . '/media/view/' . $this->data['collection']->id
					)
				);

				while(null !== $parent_collection)
				{
					$parent__collection = DB::table('media-collections')->where('id', '=', $parent_collection)->whereNull('deleted_at')->first();

					array_unshift($breadcrumbs, array
						(
							'id' => $parent__collection->id,
							'classes' => '',
				            'icon' => '',
				            'title' => $parent__collection->name,
				            'url' => '/' . General::backend_url() . '/media/view/' . $parent__collection->id
						)
					);
					
					$parent_collection = $parent__collection->parent_collection;
				}

				foreach($breadcrumbs as $crumb)
				{
					$this->addToBreadcrumb($crumb);
				}
			}
			else
			{
				$this->data['media_collections'] = DB::table('media-collections')->whereNull('parent_collection')->get();
				$this->data['media_items'] = Media::inCollection(false)->parent(true)->deleted()->get(); //just get all files

				if(isset($input['type']))
				{
					switch($input['type'])
					{
						case 'images':
						case 'documents':
						case 'audio':
						case 'video':
							$this->data['media_items'] = (new Media)->isType($input['type'], $FileHelper, $this->data['media_items']);
							break;
						default:
							break;
					}
				}
			}
			
			$html = View::make('HummingbirdBase::cms.media-global', $this->data)->render();
		}

		return Response::json(array('html' => $html));
	}

	public function get_error_by_code($code = '')
	{
		$errors = array(
			0 => 'There is no error, the file uploaded with success.', //
			1 => 'The uploaded file exceeds the maximum upload size. Please contact your system administrator.', //
			2 => 'The maximum file size has been exceeded.', //The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.
			3 => 'There was a problem uploading this file. The file has only been uploaded partially.', //The uploaded file was only partially uploaded.
			4 => 'No file uploaded', //No file was uploaded.
			6 => 'Can not upload the file specified as no temporary folder exists. Please contact your system administrator.', // Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.
			7 => 'Failed to write file to disk.', //Failed to write file to disk. Introduced in PHP 5.1.0.
			8 => 'A configuration issue has caused your file to stop uploading. Please contact your system administrator.' //A PHP extension stopped the file upload. Introduced in PHP 5.2.0.
		);

		return $errors[$code];
	}

	public function importstructure()
	{
		$cats = DB::table('old_download_categories')->orderBy('id', 'ASC')->get();

		if(null !== $cats)
		{
			$collections 		= array();
			$inserted_counter 	= 0;
			$deleted_counter 	= 0;

			foreach($cats as $cat)
			{
				switch($cat->type)
				{
					case 'video':
						$collection_id = 187;
						break;
					case 'audio':
						$collection_id = 186;
						break;
					default:
						$collection_id = 185;
						break;
				}

				$permalink = Str::slug($cat->name);
				$set_permalink = false;

				$counter = 1;
				while(!$set_permalink)
				{
					$perm_check = DB::table('media-collections')->where('permalink', '=', $permalink)->get();

					/* We have this URL */
					if(!empty($perm_check))
					{
						$permalink = Str::slug($cat->name) . $counter;
						$counter++;
					}
					else
					{
						/* No url found - break */
						$set_permalink = true;
					}
				}

				
				$collections[] = array(
					'name' => $cat->name,
					'description' => $cat->description,
					'permalink' => $permalink,
					'parent_collection' => $collection_id,
					'created_at' => date("Y-m-d H:i:s"),
					'updated_at' => date("Y-m-d H:i:s"),
					'deleted_at' => (($cat->deleted == 0) ? NULL:date("Y-m-d H:i:s"))
				);

				$inserted_counter++;

				if($cat->deleted == 1)
				{
					$deleted_counter++;
				}
			}

            if(count($collections) > 0)
            {
                DB::table('media-collections')->insert($collections);
		        	
                $description_text = (count($collections) == 1) ? 'Created one collection':'Created '.count($collections). ' collections';
                $admin_txt = "SYSTEM" . strtolower($description_text);

		        Activitylog::log([
		            'action' => 'CREATED',
		            'type' => get_class(new Media),
		            'link_id' => 188,
		            'url' => General::backend_url() . "/media/view/188",
		            'description' => $description_text,
		            'notes' => $admin_txt
		        ]);
            }

            if(count($deleted_counter) > 0)
            {
                $description_text = ($deleted_counter == 1) ? 'Deleted one collection':'Deleted '.$deleted_counter. ' collections';
                $admin_txt = "SYSTEM" . strtolower($description_text);

		        Activitylog::log([
		            'action' => 'CREATED',
		            'type' => get_class(new Media),
		            'link_id' => NULL,
		            'url' => General::backend_url() . "/media/",
		            'description' => $description_text,
		            'notes' => $admin_txt
		        ]);
            }
		}


		/* NOW INSERT DOCUMENTS */
		$docs = DB::table('old_downloads')->orderBy('id', 'ASC')->get();

		if(null !== $docs)
		{
			foreach($docs as $doc)
			{
				if($doc->filename != '' AND $doc->embed == '')
				{
					switch($doc->doctype)
					{
						case 'video':
							$orig_folder = '/uploads/videos/';
							break;
						case 'audio':
							$orig_folder = '/uploads/audio/';
							break;
						default:
							$orig_folder = '/uploads/documents/';
							break;
					}

					$collection_id = ($doc->catid + 188);

		            //filename = 1328109287.jpg - need to be absolute
		            $orig_file = $orig_folder . $doc->filename;

		            /* find in database */
		            $resource = DB::table('media_xref')->where("source", '=', trim($orig_file))->first();

		            if(null !== $resource)
		            {
		                $source = $resource->destination;
		            }
		            else
		            {
		                /* process images */
		                $base_file = base_path() . '/import' . $orig_folder . $doc->filename;
		                
		                $source = $this->uploadFile_byFunction($base_file, $collection_id, $doc->name);

		                if($source !== false)
		                {
			                /* Add to media_xref */
			                DB::table('media_xref')->insert(array
			                    (
			                        'source' => $orig_file,
			                        'destination' => $source
			                    )
			                );
		                }
		            }

	                if($source !== false)
	                {
			            $media = Media::where('location', '=', $source)->first();
			            // General::pp($media);
				        
				        Activitylog::log([
				            'action' => 'CREATED',
				            'type' => get_class($media),
				            'link_id' => $media->id,
				            'url' => General::backend_url() . "/media/view-media/" . $media->id,
				            'description' => "Media item migrated",
				            'notes' => "SYSTEM has migrated &quot;$media->filename&quot; ($media->location) to the media library"
				        ]);

			            if($doc->deleted == 1)
			            {
			            	$media->deleted_at = date("Y-m-d H:i:s");
			            	$media->save();

					        Activitylog::log([
					            'action' => 'DELETED',
					            'type' => get_class($media),
					            'link_id' => 1,
					            'description' => 'Media item deleted',
					            'notes' => "SYSTEM has deleted &quot;$media->filename&quot; ($media->location) from the media library"
					        ]);
			            }
			        }
				}
			}
		}

		die();
	}

	public function update_old_docs()
	{
		$old_docs = DB::table('old_downloads')->where('embed', '=', '')->get();

		foreach($old_docs as $doc)
		{
			$res_doc = Media::where('filename', '=', $doc->filename)->first();

			if(null !== $res_doc)
			{
				$res_doc->title = $doc->name;

				if($doc->deleted == 1)
				{
					$res_doc->deleted_at = date("Y-m-d H:i:s");

			        Activitylog::log([
			            'action' => 'DELETED',
			            'type' => get_class($res_doc),
			            'link_id' => $res_doc->id,
			            'description' => 'Media item deleted',
			            'notes' => "SYSTEM has deleted &quot;$res_doc->filename&quot; ($res_doc->location) from the media library"
			        ]);
				}

				$res_doc->save();
			}
		}

		die();
	}
}
