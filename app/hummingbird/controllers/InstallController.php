<?php

/**
 * Installing Hummingbird Controller
 *
 * @package HummingbirdBase
 * @author Daryl Phillips, Jo Cavil
 *
 */
class InstallController extends Controller 
{    
    public $data = array();
    
	public function getIndex()
	{
        Session::put('install_url', Installer::uri());
        $this->data['step'] = 0;
		return View::make('HummingbirdBase::install/welcome', $this->data);
	}
    
	public function getDatabase()
	{
        Session::put('install_url', Installer::uri() . '/database');
        $this->data['step'] = 1;
		return View::make('HummingbirdBase::install/database', $this->data);
	}    
    
	public function postDatabase()
	{
        Input::flash();
        $input = Input::except('_token');
        $installer = new Installer;
        
        if(!$installer->setupCredentials($input)) {
            return Redirect::to(Installer::uri().'/database')->withErrors($installer->errors);
        }
        
        return Redirect::to(Installer::uri().'/run-migrations')->with('token', 'from_hb');
        
	}
    
    // this needs to be a new request so that the new .env.php file is picked up
    public function getRunMigrations()
    {
        if(Session::get('token') != 'from_hb') {
            return Redirect::to(Installer::uri().'/database')->withErrors(array('general' => 'Invalid route'));
        }
        
        $installer = new Installer;
        if (!$installer->runMigrations()) {
            return Redirect::to(Installer::uri().'/database')->withErrors($installer->errors);
        }
        
        return Redirect::to(Installer::uri().'/site');
    }
        
	public function getSite()
	{
        Session::put('install_url', Installer::uri() . '/site');
        $this->data['step'] = 2;
        $this->data['site_key'] = Str::limit(Crypt::encrypt(time()), 32, '');
		return View::make('HummingbirdBase::install/site', $this->data);
	}
    
	public function postSite()
	{
        Input::flash();
        $input = Input::except('_token');
        $installer = new Installer;
        
        if (!$installer->updateSiteSettings($input)) {
            return Redirect::to(Installer::uri().'/site')->withErrors($installer->errors);
        }
        
        return Redirect::to(Installer::uri().'/permissions');
	}
    
	public function getPermissions()
	{
        Session::put('install_url', Installer::uri() . '/permissions');
        $this->data['step'] = 3;
        
		return View::make('HummingbirdBase::install/permissions', $this->data);
	}
    
	public function postPermissions()
	{
        Input::flash();
        $input = Input::except('_token');
        $installer = new Installer;
        
        if (!$installer->addAdministrator($input)) {
            return Redirect::to(Installer::uri().'/permissions')->withErrors($installer->errors);
        }
        
        /*// not sure if the below should be in its own new step? fails incorrectly at the moment
        if (!$installer->checkFilePermissions()) {
            return Redirect::to(Installer::uri().'/permissions')->withErrors($installer->errors);
        }*/
        
        return Redirect::to(Installer::uri().'/plugins');
	}
    
	public function getPlugins()
	{
        // there's a big here: for some reason the session var in Installer::updateSiteSettings() isn't being persisted, and i don't know why
        if (Session::get('installtype') != 'advanced') return $this->finished(new Installer);
        
        Session::put('install_url', Installer::uri() . '/plugins');        
        $this->data['step'] = 4;
		return View::make('HummingbirdBase::install/plugins', $this->data);
	}
    
    public function postPlugins()
    {        
        Input::flash();
        $input = Input::except('_token');
        $installer = new Installer;
        
        if (!$installer->runPluginMigrations($input)) {
            return Redirect::to(Installer::uri().'/plugins')->withErrors($installer->errors);
        }
        
        return $this->finished($installer);
    }
    
    private function finished(Installer $installer)
    {
        $installer->complete();
        Session::flush();
        
        // redirect to finish page
        return Redirect::to(Installer::uri().'/done');        
    }


}
