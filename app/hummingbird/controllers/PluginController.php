<?php

class PluginController extends CmsbaseController {
    
    public $code_location = 'plugins';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $this->data['tag'] = 'Manage Plugins';
        $this->data['plugins'] = Plugins::all();
        return View::make('HummingbirdBase::cms/plugins', $this->data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
    
    public function install($plugin)
    {
        Plugins::install(array($plugin));
        return Redirect::to(App::make('backend_url') . '/plugins');
    }
    
    public function uninstall($plugin)
    {
        Plugins::uninstall(array($plugin));
        return Redirect::to(App::make('backend_url') . '/plugins');
    }


}
