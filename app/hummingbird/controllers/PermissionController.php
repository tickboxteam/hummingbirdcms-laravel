<?php

use Zizaco\Entrust\EntrustPermission;

class PermissionController extends CmsbaseController
{
    
    public $code_location = 'users';
    
    public function getIndex()
    {
        $this->data['tag'] = 'Manage Permissions';
        $this->data['permissions'] = Permission::all();
        return View::make('HummingbirdBase::cms/permissions', $this->data);
    }
    
    public function getEdit($id)
    {
        $this->data['permission'] = Permission::find($id);
        $this->data['tag'] = 'Edit '.$this->data['permission']->title;
        $this->getCommonData();
        return View::make('HummingbirdBase::cms/permissions-edit', $this->data);
    }
    
    public function postEdit($id)
    {
        $input = Input::except('_token');
        $permission = Permission::find($id);
        $permission->update($input);
        $permission->save();
        
        return Redirect::to(App::make('backend_url').'/permissions');
    }
    
    public function getCommonData()
    {
        #$this->data['permissiontemplates'] = Permissiontemplate::get_selection();
    }
    
    public function postAdd()
    {
        $input = Input::except('_token', 'add');
        $permission = Permission::create($input);        
        return Redirect::to(App::make('backend_url').'/permissions/edit/'.$permission->id);
    }
}