<?php

/*
|--------------------------------------------------------------------------
| Site Error Controller
|--------------------------------------------------------------------------
|
| Here is where you can monitor the errors that appear on the site. Manage
| and delete wherever necessary to make sure your site maintains a healthy
| platform.
|
| Needs to INSTALL
| Needs to REGISTER NAVIGATION MENU
| Needs to REGISTER WIDGETS (CMS and FRONTEND)
| Needs to EXPORT DATA
|
*/
class ErrorController extends CmsbaseController {
    
    public $code_location = 'dashboard';

	/**
	 * Displaying all errors
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->data['tag'] = 'Manage Errors';
		$this->data['errors'] = Error::orderBy('accessed', 'DESC')->paginate(15);

		return View::make('HummingbirdBase::cms/errors', $this->data);
	}



	/**
	 * Remove this error
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $activity_log = array(
            'action' => 'DELETED',
            'type' => get_class(new Error),
            'link_id' => null,
            'description' => '',
            'notes' => ''
        );

		if($id == 'remove-all')
		{
            $count = number_format(count(DB::table('errors')->get()));

            if($count > 0)
            {
                $activity_log['description'] = 'Cleared the error log';
                $activity_log['notes'] = Auth::user()->name . " cleared $count errors from the error log.";
            
                Error::truncate();
                Session::flash('message', 'Successfully wiped the error log.');
            }
            else
            {
                Session::flash('error', 'There are no errors to remove.');
            }
        }
		else
		{
			$error = Error::find($id);

            if(null !== $error)
            {
                $activity_log['description'] = "Error removed &quot;$error->url&quot;";
                $activity_log['notes'] = Auth::user()->name . " removed &quot;$error->url&quot; from the error log";
            
                $error->delete();
                Session::flash('message', 'Successfully deleted &quot;'.$error->url.'&quot; as an error.');
            }
            else
            {
                Session::flash('error', 'Error could not be found.');
            }
        }

        Activitylog::log($activity_log); //record activity

		return Redirect::to(App::make('backend_url').'/errors');
	}



	public function import()
	{
		ini_set('memory_limit','2048M');
		set_time_limit(120);

  		// DB::unprepared(file_get_contents(base_path() . '/import/sql/404_log.sql.zip'));

  		/* First run - created elements */
        $export_data = DB::select( DB::raw("SELECT url, min(datetime) as created, max(datetime) as updated, COUNT(url) as accessed FROM 404_log GROUP BY url ORDER BY accessed DESC LIMIT 0,6000") );

        if(count($export_data) > 0)
        {
            $data = array();
            $empty = array();

            foreach($export_data as $item)
            {
            	$empty[] = $item->url;

        		$data[$item->url] = array(
        			'accessed' => $item->accessed,
        			'type' => $this->getErrorType($item->url),
        			'created_at' => $item->created,
        			'updated_at' => $item->updated
        		);
            }

            if(count($data) > 0)
            {
            	$import_data = array();

            	foreach($data as $key => $error)
            	{
            		$error['url'] = $key;
            		$import_data[] = $error;
            	}

	            DB::table('errors')->insert($import_data);
	            Schema::dropIfExists('404_log');

	            // DB::table('404_log')->whereIn('url', $empty)->delete();

                Activitylog::log([
                    'action' => 'UPDATED',
                    'type' => get_class(new Error),
                    'link_id' => null,
                    'description' => count($data) . ' errors have been migrated from HB2',
                    'notes' => "System migrated 404 errors from HB2"
                ]);
            }
        } 

        return Redirect::to(General::backend_url() . '/errors/');
	}

	public function getErrorType($url)
	{
        $file_exts = array(
            'images' => array(
                'jpg', 
                'jpeg', 
                'pjpeg', 
                'gif', 
                'png'
            ),
            'Documents' => array(
                'pdf', 
                'doc', 
                'docx', 
                'xls', 
                'xlsx', 
                'ppt',
                'pptx',
                'zip',
                'rar',
                'txt'
            ),
            'Audio' => array(
                'mp3',
                'wav',
                'aac',
                'flac',
                'ogg',
                'wma'
            ),
            'Video' => array(
                'flv',
                'mov',
                'mp4',
                'm4v',
                'wmv'
            )
        );

        $type = 'URL';
        $path_parts = pathinfo($url);

        if(isset($path_parts['extension']))
        {
            $path_parts['extension'] = strtolower($path_parts['extension']); //lower the extension
            $type = 'Files'; //standard error if file not found in allowed types

            foreach($file_exts as $type => $exts)
            {
                if(in_array($path_parts['extension'], $exts))
                {
                    $type = $type;
                    break 1;
                }
            }
        }

        return $type;
	}
}