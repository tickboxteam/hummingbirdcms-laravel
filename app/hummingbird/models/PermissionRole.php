<?php

/*
 * 
 */
class PermissionRole extends Eloquent
{
    public $table = 'perm_role';
    
    public $fillable = array('permission_id', 'role_id');
    
    public function permission()
    {
        return $this->belongsTo('Permission');
    }
    
    public function role()
    {
        return $this->belongsTo('Role');
    }
}

?>
