<?php 

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Categories extends LaravelBook\Ardent\Ardent 
{
    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

    public $table = 'taxonomy';

    protected $fillable = array('name', 'parent', 'description');

    protected $guarded = array();

    protected $type = 'category';

    public function hasChildren()
    {
        return (Categories::where('parent', '=', $this->id)->whereNull('deleted_at')->count() > 0) ? true:false;
    }

    public function exists($name)
    {
        $categories = Categories::whereNull("deleted_at")->type()->where(function($query) use ($name)
            {
                $query->orWhere('name', '=', $name)
                    ->orWhere('slug', '=', Str::slug($name));
            })->get();

        return (count($categories) > 0) ? true:false; // does this exist?
    }

    public function getType()
    {
        return $this->type;
    }

    public static function deletedCategories($return_count = false)
    {
        $categories = Categories::whereNotNull('deleted_at')->type();

        if($return_count)
        {
            return $categories->count();
        }

        return $categories->get();
    }

    // public function events()
    // {
    //     $this->belongsToMany('Events', 'taxonomy_relationships', 'term_id', 'id');
    // }


    // public function insert()
    // {
		// parent::save();

		// Activitylog::log([
		//     'action' => 'CREATED',
		//     'type' => get_class($this),
		//     'link_id' => $event->id,
		//     'description' => 'Updated event',
		//     'notes' => Auth::user()->username . " updated details for the event &quot;$event->title&quot;"
		// ]);
    // }

    public function usedByBreakdown()
    {
        return DB::table('taxonomy_relationships')
                    ->where('term_id', '=', $this->id)
                    ->groupBy('tax_type')
                    ->orderBy('results', 'DESC')
                    ->having('results', '>', 0)
                    ->select(DB::raw('count(term_id) AS results, tax_type'))
                    ->get(); 
    }

    public function used()
    {
        return DB::table('taxonomy_relationships')->where('term_id', '=', $this->id)->count();
    }

    public function scopeType($query)
    {
        return $query->where('type', '=', $this->type);
    }

    public function scopeChildOf($query, $value)
    {
        return $query->where('parent', '=', $value);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = (Str::slug($this->name) != $value) ? Str::slug($value):Str::slug($this->name);
    }

    public function setParentAttribute($value)
    {
        $this->attributes['parent'] = ($value == '' OR $value < 1) ? NULL:$value;
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = trim($value);
    }

    public function scopeSearch($query, $value)
    {
        return $query;
        return $query->whereRaw('MATCH (name, slug, description) AGAINST (?)', array($value));
    }

    public function assignImage()
    {
        return ($this->attributes['name'] == 'Events') ? true:false;
    }

    public function relations($type = null, $all = true)
    {
        $this_q = DB::table('taxonomy_relationships')->where('term_id', '=', $this->id);

        $return = ($all) ? $this_q->get():false;

        return (null !== $type) ? $this_q->where('tax_type', $type)->get():$return;
    }
}

?>
