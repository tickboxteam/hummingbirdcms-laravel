<?php

class Media extends Eloquent {

    public $table = 'media-items';
    public $fillable = array('title', 'filename', 'location', 'caption', 'alt', 'description', 'parent', 'collection');
 	
    protected $sizes = array(
        'file-list'
    );

    public function setCollectionAttribute($value)
    {
        $this->attributes['collection'] = ($value == '' OR !is_numeric($value)) ? NULL:$value;
    }

    public function getFileLocations($items)
    {
        $data = array();

        foreach($items as $item)
        {
            $data[] = base_path() . $item->location;
        }

        return $data;
    }

    public function return_children_resources()
    {
        return Media::where('parent', '=', $this->id)->get();
    }

    public function children()
    {
        return DB::table($this->table)->where('parent', '=', $this->id)->whereNull('deleted_at')->count();
    }

    public function scopeLocked($query, $locked = NULL)
    {
        return $query->where('locked', '=', $locked);
    }

 	public function scopeDeleted($query)
 	{
		return $query->whereNull('deleted_at');
 	}

    public function scopeType($query, $type = '')
    {
        if($type != '')
        {
            return $query->where('type', '=', $type);//return images only
        }
    }

    public function scopeInCollection($query, $id)
    {
        if(!$id) return $query->whereNull('collection');

        return $query->where('collection', '=', $id);
    }

    public function scopeParent($query, $has_parent = true)
    {
        if($has_parent)
        {
            return $query->whereNull('parent');
        }
    }

    public function collection()
    {
    	$collection = DB::table("media-collections")->where('id', '=', $this->collection)->first();

    	if(count($collection) < 1)
    	{
    		return (object) array('name' => 'Root media library');
    	}

    	return $collection;
    }

    public function collectionLink()
    {
 		if(null !== $this->collection)
 		{
 			return 'view/'.$this->collection;
 		}
    }

    public function storeTaxonomy($tags, $media, $class)
    {
        DB::table('taxonomy_relationships')->where('tax_type', '=', $class)->where('item_id', '=', $media->id)->delete();

        $input = array();
        foreach($tags as $tag)
        {
            $input[] = array('term_id' => $tag, 'tax_type' => $class, 'item_id' => $media->id);
        }

        DB::table('taxonomy_relationships')->insert($input);
    }

    public function taxonomy()
    {
        $data = array('category' => array(), 'tag' => array());
        $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->get();

        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }

    public function replicateTaxonomy()
    {
        $tax_items = DB::table('taxonomy_relationships')->where('tax_type', '=', get_class($this))->where('item_id', '=', $this->parent)->get();

        $new_taxonomy = array();

        if(count($tax_items) > 0)
        {
            foreach($tax_items as $item)
            {
                $new_taxonomy[] = array('term_id' => $item->term_id, 'tax_type' => get_class($this), 'item_id' => $this->id);
            }

            if(count($new_taxonomy) > 0) DB::table('taxonomy_relationships')->insert($new_taxonomy);
        }
    }

    public function mediaCentreThumb()
    {
        $image = Media::where('parent', '=', $this->id)->where('mc_only', '=', '1')->first();

        if(null === $image) return $this->location;

        return $image->location;
    }

    public function getParentId()
    {
        if($this->parent === NULL) return $this->id;

        return Media::find($this->parent)->id;
    }


    public function isType($type = false, FileHelper $FileHelper, $items)
    {
        if($type !== false)
        {
            $data = array();
            $extensions = $FileHelper->getExtensions($type);

            foreach($items as $item)
            {
                if($FileHelper->validateExtension($item->location, $extensions, false))
                {
                    $data[] = $item;
                }
            }

            return $data;
        }

        return;
    }
}