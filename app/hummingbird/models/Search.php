<?php

class Searcher extends LaravelBook\Ardent\Ardent
{
    public $table = 'search';

    protected $fillable = array('item_id', 'title', 'url', 'summary', 'content', 'type');

    protected $guarded = array(); 

    /**
     *
     * 
     *
     *
     */  
    function addToSearch($item)
    {
        $item_type = get_class($item);
            General::pp($item_type);
        $search_item = Searcher::where('item_id', '=', $item->id)->where('type', '=', $item_type)->first();
            General::pp($search_item);

        /* Did we find the item */
        if($item->searchable)
        {
            if(null === $search_item)
            {
                $search_item = new Searcher;
            }

            $search_item->item_id   = $item->id;
            $search_item->title     = $item->title;
            $search_item->url       = (isset($item->permalink)) ? $item->permalink:$item->url;
            $search_item->summary   = (isset($item->summary)) ? $item->summary:'';
            $search_item->content   = (isset($item->content)) ? $item->content:$item->description;
            $search_item->type      = $item_type;
            $search_item->save(); //save/update this into the table
            General::pp("SAVED" . __LINE__);
        }
        else
        {
            General::pp("NOT SAVED" . __LINE__);
            if(null !== $search_item)
            {
                $search_item->delete(); //remove this from the table
            }
        }
    }

    /**
     *
     * 
     *
     *
     */  
    function deleteSearch($item_id, $item_type)
    {
        Searcher::where('item_id', '=', $item_id)->where('type', '=', $item_type)->delete();
    }
}
