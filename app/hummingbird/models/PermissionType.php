<?php

/*
 * 
 */
class PermissionType extends Eloquent
{
    public $table = 'permission_types';

    protected $guarded = array();

    public $timestamps = false;

    /**
     * 
     * @param 
     * @return 
     */
	public function insert($data, $return = false)
	{
		$type = ucfirst(strtolower(trim($data['type'])));

		if($type != '')
		{
	        $perm_type = \PermissionType::where('type', '=', $type)->get();

	        if(!$perm_type->count() > 0)
	        {
	            $perm_type = new \PermissionType;
                $perm_type->type = $type;
                $perm_type->label = $data['label'];
                $perm_type->group = $data['group'];
                $perm_type->description = $data['description'];
                $perm_type->live = $data['live'];
                $perm_type->save();
	        }
	        else
	        {
	        	$perm_type = $perm_type->first();
	        }

            if($return === TRUE)
            {
            	return $perm_type;
            }
        }		
	}
}
?>
