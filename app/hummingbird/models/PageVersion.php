<?php

class PageVersion extends LaravelBook\Ardent\Ardent {

    public $table = 'pages_versions';
    public $cms_url = '';
    
    protected $fillable = array('title', 'url', 'parentpage_id', 'content', 'enable_comments', 'hide_from_search', 'status', 'protected', 'username', 'show_in_nav', 'searchable', 'search_image', 'featured_image', 'custom_menu_name');
    // protected $guarded = array('password');
    
    public $search_field = 'content';
    public $export_fields = array('title', 'url', 'content', 'enable_comments', 'hide_from_search', 'status', 'protected');

    
    public function page()
    {
        return $this->belongsTo('Page');
    }
    
    public function parentpage()
    {
        return $this->belongsTo('Page');
    }
    

    public function template()
    {
        return $this->belongsTo('Template');
    }

    public function gallery()
    {
        return $this->belongsTo('Gallery');
    }      
    
    public function setUrlAttribute($value)
    {
        #echo "value: $value,";
        $this->attributes['url'] = ($value == '') ? PagesHelper::get_url_from_title($this->attributes['title']) : $value;
        #echo $this->attributes['permalink'];exit;
    }

    public function setPasswordAttribute($value)
    {
        if(strlen($value) == 0) return $this->attributes['password'] = '';
        
        $this->attributes['password'] = Hash::make($value);
    }

    public function setHashAttribute($value)
    {
        $this->attributes['hash'] = Hash::make($value);
    }
    
    public function getPermalinkAttribute($value)
    {
        return PagesHelper::build_slug($this->attributes['url'], $this->attributes['parentpage_id']);
    }
    
    public static function get_selection($emptyitem_text = 'Select a page', $excludes = array())
    {
        $selection = array(0 => $emptyitem_text);
        
        $pages = Page::all();
        
        foreach($pages as $page) {
            if(in_array($page->id, $excludes)) continue;
            $selection[$page->id] = $page->title;
        }
        
        return $selection;
    }
    
    public static function get_blog_feed_styles()
    {
        return array();
    }

    public static function get_statuses()
    {
        return array('draft' => 'Draft', 'public' => 'Public');
    }

    public function taxonomy()
    {
        $data = array('category' => array(), 'tag' => array());
        $terms = DB::table('taxonomy_relationships')->leftJoin('taxonomy', 'taxonomy_relationships.term_id', '=', 'taxonomy.id')->where('item_id', '=', $this->page_id)->where('tax_type', '=', get_class($this))->select('taxonomy.id', 'taxonomy.type')->get();

        foreach($terms as $term)
        {
            $data[$term->type][] = $term->id;
        }

        return $data;
    }
    
    public static function boot()
    {
        parent::boot();
        
        // saving is fired on both creating and updating
        Page::saving(function($page) {
            $page->permalink = PagesHelper::build_slug($page->url, $page->parentpage_id);
        });
    }
    
}
