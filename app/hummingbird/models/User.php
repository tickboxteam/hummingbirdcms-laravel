<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class User extends LaravelBook\Ardent\Ardent implements UserInterface, RemindableInterface 
{
    use UserTrait, RemindableTrait, SoftDeletingTrait;
    use HasRole {
        can as hasrole_can;
        ability as hasrole_ability;
    }

    public $table = 'users';
    public $cms_url = 'users';
    protected $dates = ['deleted_at'];

    // password confirmation added here for validation. beforeSave() removes this before calling the db
    protected $fillable = array('name', 'username', 'email', 'status', 'role_id', 'password', 'password_confirmation', 'last_login', 'remember_token');
    protected $hidden = array('password', 'remember_token');
        
    public $export_fields = array('forename', 'surname', 'username', 'email', 'status');
    
    public static $permission_type = 'users';
    
    public static function boot()
    {
        parent::boot();
        // User::observe(new UserObserver);
    }
    
    // need to do here because can't make static variable using function username_regex()
    public static function getRules($action = 'update')
    {
        switch($action) {
            
            case 'create':
                return array(
                    'name' => 'required',
                    // 'username' => 'required|unique:users|bad_usernames',
                    'username' => 'required|unique:users',
                    'email' => 'required|email|unique:users',
                    // 'password' => 'required|bad_passwords|confirmed',
                    'password' => 'required|confirmed',
                    'password_confirmation' => 'required'
                );
            default:
                return array(
                    'name' => 'required',
                    // 'username' => 'required|bad_usernames',
                    'username' => 'required',
                    'email' => 'required|email',
                    // 'password' => 'bad_passwords|confirmed'
                    'password' => 'confirmed'
                );                
        }
    }
    
    public static function getNameAttribute($value)
    {
        switch($value)
        {
            case '':
                return "System";
                break;
        }

        return $value;
    }

    // to match above structure
    public static function getMessages()
    {
        return array(
            'username.bad_usernames' => 'That username is not allowed.',
            'password.bad_passwords' => 'That password is not allowed.',
        );
    }
    
    public static function get_bad_usernames()
    {
        return Config::get('HummingbirdBase::hummingbird.bad_usernames') ?: array();
    }
    
    public static function get_bad_passwords()
    {
        return Config::get('HummingbirdBase::hummingbird.bad_passwords') ?: array();
    }
    
    public static function get_statuses()
    {
        return array('inactive' => 'Inactive', 'active' => 'Active', 'banned' => 'Banned');
    }
    
    public function getRole()
    {
        return $this->role;
    }

    public function role(){
        return $this->belongsTo('Role');
    }

    public function activity()
    {
        return $this->hasMany('Activitylog');
    }

    public function formatActivity($data)
    {
        if($data !== NULL)
        {
            return $data->action . ' ' . $data->db_table;
        }

        return '-';
    }
    
    /**
     * Detach all roles from a user.
     * 
     * Dont use this on yourself as it might lock you out!
     * 
     * @return object
     */
    public function detachAllRoles()
    {
        DB::table('assigned_roles')->where('user_id', $this->id)->delete();
        return $this;
    }    
    
    public function clearAllPermissions()
    {
        DB::table('permission_overrides')->where('user_id', $this->id)->delete();
        return $this;
    }
    
    // to replace the entrust can() as that relies on a different db structure
    public function hasAccess($action, $type)
    {
        if(null !== Session::get('view_user_perms') && $this->id != Session::get('view_user_perms')) { // second part avoid infinite loops
            // we're in preview mode for user, so use the emulated user's permissions
            $user = User::where('id', '=', Session::get('view_user_perms'))->first();            
            return $user->hasAccess($action, $type);
        }
        
        if(null !== Session::get('view_role_perms') && $this->role_id != Session::get('view_role_perms')) { // second part avoid infinite loops
            // we're in preview mode for role, so use the emulated role's permissions
            $user = new User;
            $user->role_id = Session::get('view_role_perms');       
            return $user->hasAccess($action, $type);
        }                

        if($this->isSuperUser()) return true; // always give superadmin access

        return Permission::userHasAccess($action, $type, $this->id);
    }
    
    // this is an override of the trait HasRole::can(), so we can check for overrides
    public function can($permission)
    {
        throw new Exception("Use User::canAccess() instead");
    }
    
    // this is an override of the trait HasRole::ability(), so we can check for overrides
    public function ability($roles, $permissions, $options = array())
    {
        // todo: check overrides first        
        throw new Exception("Code not writen yet: need to check overides first");
        return $this->hasrole_ability($roles, $permissions, $options);
    }
    
    // ensure we use the defined rules and messages for validation in ardent
    public function updateSave(
            array $rules = array(),
            array $customMessages = array(),
            array $options = array(),
            Closure $beforeSave = null,
            Closure $afterSave = null
        )
    {       
        Validator::extend('bad_usernames', function ($attribute, $value, $parameters){
            return !in_array(strtolower($value), User::get_bad_usernames());
        });
        
        Validator::extend('bad_passwords', function ($attribute, $value, $parameters){
            return !in_array(strtolower($value), User::get_bad_passwords());
        });
        
        $rules = $rules ?: static::getRules();
        $customMessages = $customMessages ?: static::getMessages();

        return parent::save($rules, $customMessages, $options, $beforeSave, $afterSave);
    }
    
    
    public function beforeSave() {
        if($this->isDirty('password')) {
            $this->password = Hash::make($this->password);
        }
        
        unset($this->password_confirmation);

        return true;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    // public function setLastLoginAttribute()
    // {
    //     $this->last_login = date("Y-m-d H:i:s");
    // }

    public function getEmail()
    {
        return $this->email;
    }

    public static function returnActiveLabel($data)
    {
        $states = array('active' => 'success', 'inactive' => 'warning', 'banned' => 'danger');

        return '<span class="label label-' . $states[$data->status] . '">' . ucfirst($data->status) . '</span>';
    }

    public function isSuperUser()
    {
        if(null !== Session::get('view_user_perms') OR null !== Session::get('view_role_perms')) return false;

        return ($this->role->id == 1);
    }

    public function previewMode()
    {
        return (null !== Session::get('view_user_perms'));
    }

    public function getPreference($item)
    {
        switch($item)
        {
            case 'cover-image':
                if($this->email == 'darylp@tickboxmarketing.co.uk')
                {
                    $image = array(
                        'https://s.yimg.com/uy/build/images/sohp/hero/havasu-falls-at-night2.jpg',
                        'https://s.yimg.com/uy/build/images/sohp/hero/artic-sky2.jpg',
                        'https://s.yimg.com/uy/build/images/sohp/hero/shy-boy2.jpg',
                        'https://s.yimg.com/uy/build/images/sohp/hero/wildsee-pizol2.jpg',

                    );
                    return $image[array_rand($image)];
                }
                break;
        }
    }

    public function profile_image()
    {
        if($this->email == 'darylp@tickboxmarketing.co.uk')
        {
            if(File::exists(base_path() . '/themes/hummingbird/default/images/' . $this->username . '.jpg'))
            {
                return '/themes/hummingbird/default/images/' . $this->username . '.jpg';
            }
        }

        return General::gravatarImage($this->email);
    }

    public function profile()
    {
        if(class_exists('\\Tickbox\Shop\Shop'))
        {
            return $this->hasOne('Tickbox\Shop\Models\UserProfile', 'cust_id', 'id');
        }

        return;
    }


}