<?php

// namespace Hummingbird\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    // use \Activity;

    public $table = 'roles';
    public $cms_url = 'roles';
    public $preview_role = false;
    public static $permission_type = 'roles';
    protected $fillable = array('name', 'parentrole_id');

    public static $rules = array(
        'create' => array(
            'name' => 'required|between:4,128|unique:roles',
            'parentrole_id' => 'integer|different_parent'
        ),
        'update' => array(
            'name' => 'required|between:4,128',
            'parentrole_id' => 'integer|different_parent'
        )
    );
    
    public function parentrole()
    {
        return $this->belongsTo('Role');
    }

    public static function get_selection($roles) {
        $selection = array();

        foreach ($roles as $role) {
            $selection[$role->id] = str_repeat('&nbsp;', $role->level) . $role->name;
        }

        return $selection;
    }

    public static function setPreviewRole($val = false) {
        $this->preview_role = $val;
    }

    public function delete() {
        // don't delete superadmin!
        if ($this->name == 'superadmin' OR $this->id == 1 OR $this->locked)
        {
            // if(User::hasNoUsers($this->id))
            // {
                return false;
            // }
        }
        parent::delete();
    }
    
    // don't create loop with parent being itself
    public function save(
        array $rules = array(),
        array $customMessages = array(),
        array $options = array(),
        Closure $beforeSave = null,
        Closure $afterSave = null)
    {
        $id = $this->id;
        
        // define extra rules
        Validator::extend('different_parent', function ($attribute, $value, $parameters) use ($id){
            return ($id != $value);
        });
        
        $rules = $rules ?: static::$rules['update'];
        $customMessages = array('parentrole_id.different_parent' => 'A role cannot be a child of itself.');
        return parent::save($rules, $customMessages, $options, $beforeSave, $afterSave);
    }

    public static function getUserRoleID()
    {
        return Auth::user()->getRole()->id;
    }

    public static function getRoleParentID()
    {
        return Auth::user()->getRole()->parentrole_id;
    }

    public static function getUserRoleName()
    {
        return Auth::user()->getRole()->name;
    }

    public static function showPreviewLink($role_id)
    {
        if(Role::getUserRoleID() != $role_id) return true;
    }

    public function parent()
    {
        return $this->belongsTo('Role', 'parentrole_id');
    }

    public function children()
    {
        return $this->hasMany('Role', 'parentrole_id');
    }

    public function users()
    {
        return $this->hasMany('User', 'role_id');
    }

    public function previewMode()
    {
        return (null !== Session::get('view_role_perms'));
    }
}
