<?php

class Menu extends LaravelBook\Ardent\Ardent 
{

    public $table = 'menus';
    public $cms_url = 'menus';
    protected $fillable = array('name', 'location', 'description');
    
    protected $guarded = array('live');
    use SoftDeletingTrait;
    
    public static function get_selection()
    {
        $selection = array();
        
        $menus = Menu::all();
        
        foreach($menus as $menu) {
            $selection[$menu->id] = $menu->title;
        }
        
        return $selection;
    }


    public function menuitems()
    {
        return $this->hasMany('Menuitem')->whereNull('deleted_at');
    }
    
}
