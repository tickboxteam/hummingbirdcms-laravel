<?php

class Gallery extends LaravelBook\Ardent\Ardent {

    public $table = 'gallerys';
    public $cms_url = 'galleries';
    protected $fillable = array();
    
    protected $guarded = array();
    
    public static function get_selection()
    {
        $selection = array();
        
        $gallerys = Gallery::all();
        
        foreach($gallerys as $gallery) {
            $selection[$gallery->id] = $gallery->title;
        }
        
        return $selection;
    }
    
}
