<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Redirections extends LaravelBook\Ardent\Ardent
{
    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];
    
    public $table       = 'redirections';
    public $cms_url     = 'redirections';
    public $table_stats = 'redirections_stats';

    protected $fillable = array('from', 'to', 'comments', 'type');

    /**
    * 
    */    
    public static function get_types_selection()
    {
        return array(
            '301' => 'Permanent (301)', 
            '302' => 'Temporary (302)', 
            'meta' => 'Visible'
        );
    }


    /**
    *
    */
    public function setFromAttribute($value)
    {
        if($value == '') return; //return blank value

        /* In /models/redirections.php - move to URLHELPER */
        $replace = array(
            'http://',
            'https://',
            'ftp://'
        );

        if(strpos($value, '://') !== false)
        {
            $url = parse_url($value);
            $value = $url['path'];
        }

        $value = str_replace($replace, '', $value);

        if($value[0] != '/') $value = '/' . $value;
        if($value[strlen($value)-1] != '/') $value = $value . '/';

        $this->attributes['from'] = $value;
    }



    /**
    *
    */
    public function addStat()
    {
        $stat = array(
            'redirect_id' => $this->id,
            'visited_at' => date("Y-m-d H:i:s"),
            'ip_address' => $_SERVER["REMOTE_ADDR"],
            'referrer' => (isset($_SERVER["HTTP_REFERER"])) ? $_SERVER["HTTP_REFERER"]:''
        );

        DB::table($this->table_stats)->insert($stat);
    }
}
