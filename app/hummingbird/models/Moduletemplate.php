<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Moduletemplate extends LaravelBook\Ardent\Ardent
{
    use SoftDeletingTrait;

    public $table = 'moduletemplates';
    public $cms_url = 'module-templates';
    protected $fillable = array('name', 'page_id', 'module_id', 'live', 'html', 'editable', 'notes', 'css', 'js');
    
    protected $guarded = array();
    
    public static function get_selection($emptyitem_text = 'Select a template')
    {
        $selection = array();
        
        $moduletemplates = Moduletemplate::whereNull('deleted_at')->get();
        
        foreach($moduletemplates as $moduletemplate)
        {
            $selection[$moduletemplate->id] = $moduletemplate->name;
        }
        
        if(count($selection) > 0) return $selection;
    }
    
    public function page()
    {
        return $this->belongsTo('Page');
    }
    
    public function module()
    {
        return $this->belongsTo('Module');
    }
        
    public function data()
    {
        $data = array();

        $templates = Moduletemplate::all();

        foreach($templates as $template)
        {
            $data['css'][] = $template->css;
            $data['js'][] = $template->js;
        }

        return $data;
    }

    public function getNotes($default = '-')
    {
        return ($this->notes != '') ? $this->notes:$default;
    }

    public function getText()
    {
        return 'Lorem ipsum nulla potenti suspendisse nisl massa venenatis aliquam, ad condimentum senectus tempus risus dictum etiam tempor fames, ipsum quam condimentum platea pulvinar aenean dapibus.';
    }

    public function render($html)
    {
        $media = Media::where('location', 'LIKE', '%.jpg')->orderByRaw("RAND()")->take(1)->first();

        $html = str_replace("{{image}}", '<img src="http://alpha.bristolhealthpartners.org.uk/'.$media->location.'" />', $html);
        $html = str_replace("{{content}}", $this->getText(), $html);
        $html = str_replace("{{*content*}}", $this->getText(), $html);

        echo $html;   
    }



    public function prepareTemplate()
    {
        $module_tags = array();

        /* get all types */
        $loop_html = str_replace(array("\r\n", "\r", "\n"), "", $this->html);
        $loop_tags = array();
        $loop_tags_pattern = '#\{{(.*?)\}}#';
        preg_match_all($loop_tags_pattern, $loop_html, $loop_tags);

        if(count($loop_tags) > 0)
        {   
            foreach($loop_tags[1] as $key => $tag)
            {
                /* check for any settings */
                $settings_pattern = '#\[(.*?)\]#';
                $settings_matches = array();

                preg_match_all($settings_pattern, $tag, $settings_matches);

                $settings_arr = array();

                if(count($settings_matches[0]) > 0)
                {
                    $settings = str_replace(array("[", "]"), "", $settings_matches[0][0]);
                    $settings = explode(":", $settings);

                    foreach($settings as $setting)
                    {
                        $field = explode("=", $setting);
                        $settings_arr[$field[0]] = $field[1];
                        $tag = str_replace($setting, "", $tag);
                    }

                    $tag = str_replace(array("[", "]", ":"), "", $tag);   
                }

                $module_tags[$tag] = array(
                    'settings' => $settings_arr
                );
            }
        }  

        // General::pp($module_tags, true);


        return $module_tags;
    }

    public function field_title($tag, $settings)
    {
        if(!empty($settings) AND isset($settings['title']) AND $settings['title'] != '')
        {
            return $settings['title'];
        }

        return $tag;
    }
}