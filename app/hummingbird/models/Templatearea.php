<?php

class Templatearea extends LaravelBook\Ardent\Ardent {

    public $table = 'templateareas';
    public $cms_url = 'template-areas';
    protected $fillable = array('name', 'description');
    
    protected $guarded = array();
    
    public static function get_selection()
    {
        $selection = array();
        
        $areas = Templatearea::all();
        
        foreach($areas as $area) {
            $selection[$area->id] = $area->name;
        }
        
        return $selection;
    }
    
}
