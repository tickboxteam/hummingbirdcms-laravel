<?php

class Menuitem extends LaravelBook\Ardent\Ardent {

    public $table = 'menuitems';
    public $cms_url = 'menus';
    protected $fillable = array('menu_item_name', 'url', 'menu_id');
    
    protected $guarded = array();
    use SoftDeletingTrait;
    
    public static function get_selection()
    {
        $selection = array();
        
        $menuitems = Menuitem::all();
        
        foreach($menuitems as $menuitem) {
            $selection[$menuitem->id] = $menuitem->title;
        }
        
        return $selection;
    }
    
    public function page()
    {
        return $this->belongsTo('Page');
    }
    
    public function menu()
    {
        return $this->belongsTo('Menu');
    }
    
}
