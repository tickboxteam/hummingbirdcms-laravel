<?php

class Pagemodule extends LaravelBook\Ardent\Ardent {

    public $table = 'pagemodules';
    public $cms_url = 'page-modules';
    protected $fillable = array('page_id', 'module_id', 'live', 'area', 'position');
    
    protected $guarded = array();
    
    public static function get_selection($emptyitem_text = 'Select a page module')
    {
        $selection = array(0 => $emptyitem_text);
        
        $pagemodules = Pagemodule::all()->orderby('position', 'asc');
        
        foreach($pagemodules as $pagemodule) {
            $selection[$pagemodule->id] = "$pagemodule->page->title - $pagemodule->module->name ($pagemodule->position)";
        }
        
        return $selection;
    }
    
    public function page()
    {
        return $this->belongsTo('Page');
    }
    
    public function module()
    {
        return $this->belongsTo('Module');
    }

}
