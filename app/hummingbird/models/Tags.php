<?php 

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Tags extends LaravelBook\Ardent\Ardent 
{
    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

    public $table = 'taxonomy';

    protected $fillable = array('name', 'slug', 'description');

    protected $guarded = array();

    protected $type = 'tag';

    public function exists($name)
    {
        $tags = Tags::whereNull("deleted_at")->type()->where(function($query) use ($name)
            {
                $query->orWhere('name', '=', $name)
                    ->orWhere('slug', '=', Str::slug($name));
            })->get();

        return (count($tags) > 0) ? true:false; // does this exist?
    }    

    public static function boot()
    {
        parent::boot();
    }

    public static function deletedTags()
    {
        return Tags::whereNotNull('deleted_at')->type()->count();
    }

    public function used()
    {
        return DB::table('taxonomy_relationships')->where('term_id', '=', $this->id)->count();
    }

    public function getType()
    {
        return $this->type;
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = (Str::slug($this->name) != $value) ? Str::slug($value):Str::slug($this->name);
    }

    public function scopeType($query)
    {
    	return $query->where('type', '=', $this->type);
    }

    public function counts()
    {
        return DB::table('taxonomy_relationships')->where('term_id', '=', $this->id)->count();
    }

    public function tags()
    {

    	// return $this->morphToMany('Tags', 'taxonomy_relationship', 'term_id');
    	return;
    }

    public function relations($type = null, $all = true)
    {
        $this_q = DB::table('taxonomy_relationships')->where('term_id', '=', $this->id);

        $return = ($all) ? $this_q->get():false;

        return (null !== $type) ? $this_q->where('tax_type', $type)->get():$return;
    }
}