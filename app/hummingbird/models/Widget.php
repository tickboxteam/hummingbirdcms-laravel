<?php

class Widget extends LaravelBook\Ardent\Ardent {

    public $table = 'widgets';
    public $cms_url = 'widgets';
    protected $fillable = array('name');
    
    protected $guarded = array();
    
    public static function get_selection()
    {
        $selection = array();
        
        $widgets = Widget::all();
        
        foreach($widgets as $widget) {
            $selection[$widget->id] = $widget->title;
        }
        
        return $selection;
    }
    
}
